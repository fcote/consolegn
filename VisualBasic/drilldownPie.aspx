<%@ Page Language="VB" Debug="true" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs )

	'set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
    Chart.Title="Item sales"
    Chart.XAxis.Label.Text="Years"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.Type=ChartType.Pies
    
   Chart.DateGrouping = TimeInterval.Quarters
   	Chart.DrillDownChain="Quarters,Months,days=Days,Hours,Minutes"
      
 
    'Add a series
    Chart.Series.Name="Items 1"
    Chart.Series.StartDate= New System.DateTime(2002,1,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,12,31,23,59,59)
    Chart.DefaultSeries.DefaultElement.ShowValue=true
    Chart.Series.SqlStatement= "SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"  							
    Chart.SeriesCollection.Add()
    
 
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Drill Down Pie Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
 
</div>
</body>
</html>
