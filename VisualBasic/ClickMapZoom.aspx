<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Map 'Horizontal;
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample"
   Chart.Visible = False
   Chart.ChartArea.Label.Text = "Please click to zoom and center the map" + ControlChars.Lf + " view on the position clicked."
   Chart.ChartArea.Background = New Background(Color.FromArgb(142, 195, 236), Color.FromArgb(63, 137, 200), 90)
   Chart.Mapping.DefaultShape.Background.Color = Color.LightGray
   
   
   ' This sample demonstrates how to zoom into a map, based on a click position using lat/long coordinates..
   ' FullScreen
   Dim bp As Bitmap = Nothing
   
   ' Set the chart zooming settings if necessary (it was more than once).	
   ' This will ensure the click position will return the correct results because the map was already zoomed when clicked.
   If Not (Page.Request.Params("x2") Is Nothing) And Not (Page.Request.Params("y2") Is Nothing) Then
      Dim x As Single = CSng(Convert.ToDouble(Page.Request.Params("x2")))
      Dim y As Single = CSng(Convert.ToDouble(Page.Request.Params("y2")))
      Chart.Mapping.ZoomCenterPoint = New PointF(x, y)
      Chart.Mapping.ZoomPercentage = 250
   End If
   
   Chart.Mapping.MapLayerCollection.Add("../../images/MapFiles/primusa.shp")
   If Not (Page.Request.Params("y") Is Nothing) And Not (Page.Request.Params("x") Is Nothing) Then
      
      bp = Chart.GetChartBitmap()
      
      ' Get x and y points
      Dim x As Integer = Convert.ToInt32(Page.Request.Params("x"))
      Dim y As Integer = Convert.ToInt32(Page.Request.Params("y"))
      
      Dim p As PointF = Chart.Mapping.GetLatLongCoordinates(New Point(x, y))
      
      Chart.Mapping.ZoomCenterPoint = p
      Chart.Mapping.ZoomPercentage = 250
      
      ' Pass the current click position with the next click.
      imageLabel.Text += "<input type=hidden name=x2 value=" + p.X.ToString() + ">"
      imageLabel.Text += "<input type=hidden name=y2 value=" + p.Y.ToString() + ">"
   End If
   
   ' Generate the chart again.
   bp = Chart.GetChartBitmap()
   Dim fileName As String = Chart.FileManager.SaveImage(bp)
   imageLabel.Text += "<input type=image value=""submit"" border=0 src=""" + fileName + """ ISMAP>"
   
   bp.Dispose()
End Sub 'Page_Load
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
		<form method =get >
		<asp:Label ID=imageLabel Runat=server/>
			</form>
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
