<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Dynamic Annotations</title>
		<script runat="server">




Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   ' This sample demonstrates using annotations with data sources.
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
   Dim sc As SeriesCollection = getRandomData()
   
   ' We'll create an axis marker to use as the source for the annotation.
   Dim am As New AxisMarker("my Marker", New Line(Color.Red), 30)
   Chart.YAxis.Markers.Add(am)
   
   ' Instantiate our annotation.
   Dim a As New Annotation()
   
   ' Any of the following can be used as source but we'll use the axis marker.
   'a.DataSource = sc[0].Elements[0];
   'a.DataSource = sc[0];
   a.DataSource = am
   
   ' Set the text
   a.Label.Text = "Source Name: %name " + ControlChars.Lf + " Value: %High"
   
   
   ' Set a point where the annotation appears
   a.Position = New Point(50, 30)
   
   ' By default the annotation will create a rectangle for the text. In order to place the text on one line we'll 
   ' set the dynamic size property to false.
   a.DynamicSize = False
   
   ' Add the annotation.
   Chart.Annotations.Add(a)
   
   ' Add the random data.
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
