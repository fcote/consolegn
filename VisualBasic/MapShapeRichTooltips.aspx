<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>

	<script runat="server">

		' This sample demonstrates adding rich tooltips with details and microCharts for map shapes.
		Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
			' Setup the chart.
			Chart.Type = ChartType.Map
			Chart.Size = "650x400"
			Chart.Title = "<block fSize='10'> Hover over a state to see more information"
			Chart.TempDirectory = "temp"
			Chart.Debug = True
			Chart.ChartArea.Background.ShadingEffectMode = ShadingEffectMode.Background2

			' Style the title box.
			Chart.TitleBox.Position = TitleBoxPosition.Full
			Chart.TitleBox.Padding = 5
			Chart.TitleBox.Label.Color = Color.White
			Chart.TitleBox.Label.GlowColor = Color.Black
			Chart.TitleBox.Background.Color = Color.DeepSkyBlue
			Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Background1
			Chart.TitleBox.Position = TitleBoxPosition.Full
			Chart.TitleBox.CornerTopRight = BoxCorner.Round
			Chart.TitleBox.CornerTopLeft = Chart.TitleBox.CornerTopRight

			Chart.Mapping.DefaultShape.Background.Color = Color.FromArgb(240, 240, 240)

			' Specify the tooltip infoGrid text.
			Chart.Mapping.DefaultShape.Hotspot.ToolTip = "<Block hAlign='right'>Name: <Block  fStyle='bold'>%State_Name<row><Block hAlign='right'>Population: <Block  fStyle='bold'>%Population<hr><Chart:Pie values='%Males,%Females'>Males - Females<br>%Males - %Females"

			' Add the states layer to the chart.
			Chart.Mapping.MapLayerCollection.Add("../../images/MapFiles/primusa.shp")
		End Sub

	</script>

</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server">
		</dotnet:Chart>
	</div>
</body>
</html>
