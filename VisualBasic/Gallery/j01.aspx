<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Size = "800x600"
   Chart.Title = " Earth false color composite image layer"
   Chart.TempDirectory = "temp"
   Chart.TitleBox.Background.Color = Color.LightSkyBlue
   
   ' This sample demonstrates loading the earth.ecw file.
   Chart.Type = ChartType.Map
   Chart.Mapping.MapLayerCollection.Add("../../../images/MapFiles/earth.ecw")
End Sub 'Page_Load 



</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
