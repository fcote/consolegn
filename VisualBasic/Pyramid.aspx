<%@ Page Language="VB" Description="dotnetCHARTING Component" Debug="true" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   'set global properties
   Chart.Title = "Item sales"
   Chart.Type = ChartType.ComboHorizontal
   Chart.Size = "800X400"
   
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.DefaultElement.ShowValue = True
   
   'Add a series
   Dim de As New DataEngine()
   de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   de.StartDate = New DateTime(2002, 1, 1, 0, 0, 0)
   de.EndDate = New DateTime(2002, 12, 30, 23, 59, 59)
   de.DateGrouping = TimeInterval.Year
   de.SplitByLimit = "2"
   
   ' Chart 1
   de.SqlStatement = "SELECT OrderDate,Total,Name FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY Orders.OrderDate"
   
   Dim sc As SeriesCollection = de.GetSeries()
   Chart.SeriesCollection.Add(sc(0))
   
   ' Chart 2
   Dim ca2 As New ChartArea()
   
   Dim newX As New Axis()
   newX.Label.Text = sc(0).Name + "'s Sales"
   sc(1).XAxis = newX
   
   ca2.SeriesCollection.Add(sc(1))
   ca2.Label.Text = "Others"
   
   Chart.ExtraChartAreas.Add(ca2)
   
   Chart.XAxis.InvertScale = True
   Chart.XAxis.SynchronizeScale.Add(newX)
   Chart.XAxis.Label.Text = sc(1).Name + "'s Sales"
End Sub 'Page_Load 


</script>

	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
