'Imports System.Data
'Imports System.Configuration
'Imports Oracle.DataAccess.Client
'Imports System.Data.SqlClient


'Public Interface IDBHelper
'    Function getDataSet(ByVal sql As String) As DataSet
'    Function getDataSet(ByVal sql As String, ByVal sp_parms As System.Collections.Generic.List(Of DALSPParameter)) As DataSet
'    Function getDataReader(ByVal sql As String) As IDataReader
'    Function getDataReader(ByVal sql As String, ByVal behavior As CommandBehavior) As IDataReader
'    Function getConnection() As IDbConnection
'    Function getErreur() As String
'    Function ExecuteStoredProc(ByVal sp_name As String, ByVal sp_parms As System.Collections.Generic.List(Of DALSPParameter), ByVal sp_return As DALSPReturnParameter) As Object
'    Function ExecuteStoredProc(ByVal sp_name As String, ByVal sp_parms As System.Collections.Generic.List(Of DALSPParameter)) As Object
'    Function ExecuteStoredProc(ByVal sp As StoredProcedure) As Object
'End Interface

'Public Class StoredProcedure

'    Public Name As String
'    Public Connection As IDbConnection
'    Public ReturnParameter As DALSPParameter

'    Private _parms As New System.Collections.Generic.List(Of DALSPParameter)

'    Public ReadOnly Property Parameters() As System.Collections.Generic.List(Of DALSPParameter)
'        Get
'            Return _parms
'        End Get
'    End Property

'End Class

'Public Module HelperAEFactory

'    'Ce module instancie des helpers pour la 
'    'bonne connection.
'    'Si on supportait diff�rentes db on pourrait
'    'aussi les plugger ici.

'    Public Function GetHelper() As IDBHelper
'        Return New OracleHelperAE()
'    End Function

'End Module

'Public Module HelperGrillesFactory

'    Public Function GetHelper() As IDBHelper
'        Return New OracleHelperGrilles()
'    End Function

'End Module

'Public Module HelperPPFactory
'    Public Function GetHelper() As IDBHelper
'        'Return New OracleHelperPP()
'        Return New OracleHelperAE()
'    End Function
'End Module

'Public Class OracleHelper
'    Implements IDBHelper

'    Protected con_string As String = String.Empty
'    Protected erreur As String = String.Empty
'    Const MAX_ESSAIS_CONNECTION As Integer = 5

'    Public Function GetErreur() As String Implements IDBHelper.getErreur
'        Return erreur
'    End Function

'    Public Function getConnection() As System.Data.IDbConnection Implements IDBHelper.getConnection
'        Return _getConnection()
'    End Function

'    Public Function getDataSet(ByVal sql As String) As System.Data.DataSet Implements IDBHelper.getDataSet
'        Return _getDataSet(sql)
'    End Function

'    Public Function getDataSet(ByVal sql As String, ByVal parameters As List(Of DALSPParameter)) As System.Data.DataSet Implements IDBHelper.getDataSet
'        Return _getDataSet(sql, parameters)
'    End Function

'    Private Function _getDataReader(ByVal sql As String) As OracleDataReader
'        Return _getDataReader(sql, CommandBehavior.CloseConnection)
'    End Function

'    Private Function _getDataReader(ByVal sql As String, ByVal behavior As System.Data.CommandBehavior) As OracleDataReader

'        Dim con As OracleConnection = New OracleConnection()

'        con.ConnectionString = con_string
'        Dim cmd As OracleCommand = con.CreateCommand()
'        con.Open()

'        cmd.CommandType = CommandType.Text
'        cmd.CommandText = sql

'        Dim dr As OracleDataReader = cmd.ExecuteReader(behavior)

'        Return dr

'    End Function

'    Public Function getDataReader(ByVal sql As String) As IDataReader Implements IDBHelper.getDataReader
'        Return _getDataReader(sql)
'    End Function

'    Public Function getDataReader(ByVal sql As String, ByVal behavior As System.Data.CommandBehavior) As System.Data.IDataReader Implements IDBHelper.getDataReader
'        Return _getDataReader(sql, behavior)
'    End Function

'    Private Function _getDataSet(ByVal sql As String) As System.Data.DataSet
'        Return _getDataSet(sql, Nothing)
'    End Function

'    Private Function _getDataSet(ByVal sql As String, ByVal parameters As List(Of DALSPParameter)) As DataSet

'        Dim con As IDbConnection = _getConnection()

'        Dim cmd As IDbCommand = con.CreateCommand()

'        cmd.CommandType = CommandType.Text
'        cmd.CommandText = sql

'        'Dim adapter As IDbDataAdapter = _createDataAdapter()
'        Dim adapter As IDbDataAdapter = New Oracle.DataAccess.Client.OracleDataAdapter(sql.ToString, cmd.Connection.ConnectionString)

'        'adapter.SelectCommand = cmd

'        If TypeOf (cmd.Connection) Is Oracle.DataAccess.Client.OracleConnection Then
'            Dim oc As Oracle.DataAccess.Client.OracleConnection = CType(cmd.Connection, Oracle.DataAccess.Client.OracleConnection)
'        End If

'        If Not parameters Is Nothing Then
'            For i As Integer = 0 To parameters.Count - 1
'                Dim parm As Oracle.DataAccess.Client.OracleParameter = New Oracle.DataAccess.Client.OracleParameter(parameters(i).Name, _getOracleType(parameters(i).Type))
'                parm.Direction = ParameterDirection.Input
'                parm.Value = parameters(i).Value
'                adapter.SelectCommand.Parameters.Add(parm)
'            Next
'        End If

'        Dim ctr_essais As Integer = 1
'        Dim reessayer As Boolean = True
'        Dim ds As DataSet = New DataSet()

'        'Le Catch va sortir de la loop si besoin est
'        'Cette loop va attraper les erreurs de connection.
'        'Dim ge As GestionnaireDErreurs = New GestionnaireDErreurs()

'        While (reessayer)
'            Try
'                adapter.Fill(ds)
'                reessayer = False
'            Catch ex As Oracle.DataAccess.Client.OracleException
'                Dim e As Exception = New Exception("OracleException: " & ex.Message, ex)
'                HandleException(e, con, ctr_essais, reessayer, sql)
'            Catch ex As Exception
'                HandleException(ex, con, ctr_essais, reessayer, sql)
'            End Try
'        End While

'        cmd.Dispose()

'        con.Dispose()

'        Return ds

'    End Function

'    Private Sub HandleException(ByVal ex As Exception, ByVal con As IDbConnection, ByRef ctr_essais As Integer, ByRef reessayer As Boolean, ByVal sql As String)
'        If ctr_essais > MAX_ESSAIS_CONNECTION Then
'            'ge.EcrireContexte = True
'            'ge.EcrireTracePile = True
'            'ge.LogErreur("C-002 - Maximum d'essais atteint", "Le maximum d'essais de connection � la base de donn�es (" & Constantes.MAX_ESSAIS_CONNECTION & ") a �t� atteint." & vbCrLf & "SQL: " & sql, ex)
'            erreur = "C-002 - Maximum d'essais atteint. Le maximum d'essais de connection � la base de donn�es (" & MAX_ESSAIS_CONNECTION & ") a �t� atteint." & vbCrLf & "SQL: " & sql
'            reessayer = False
'            Throw ex
'        Else
'            Dim ch As CommonHelper = New CommonHelper()

'            If ch.TestConnection(con) Then
'                'ge.EcrireContexte = False
'                'ge.EcrireTracePile = False
'                'ge.LogErreur("C-003 - Avertissement: La connexion a �chou�.", "Essai " & ctr_essais, ex)
'                erreur = "C-003 - Avertissement: La connexion a �chou�. " & ex.Message
'                ctr_essais += 1
'            Else
'                reessayer = False
'                Throw ex
'            End If
'        End If
'    End Sub

'    Public Function ExecuteCommand(ByVal cmd As IDbCommand) As Integer
'        Dim ctr_essais As Integer = 1
'        Dim reessayer As Boolean = True

'        While (reessayer)

'            Try
'                Return cmd.ExecuteNonQuery()
'                reessayer = False
'            Catch ex As Oracle.DataAccess.Client.OracleException
'                Dim e As Exception = New Exception("OracleException: " & ex.Message, ex)
'                HandleException(e, cmd.Connection, ctr_essais, reessayer, cmd.CommandText)
'            Catch ex As Exception
'                HandleException(ex, cmd.Connection, ctr_essais, reessayer, cmd.CommandText)
'            End Try

'        End While
'    End Function

'    Public Function ExecuteStoredProc(ByVal sp_name As String, ByVal sp_parms As System.Collections.Generic.List(Of DALSPParameter), ByVal sp_return As DALSPReturnParameter) As Object Implements IDBHelper.ExecuteStoredProc

'        Dim conn As IDbConnection = _getConnection()
'        Dim cmd As IDbCommand = conn.CreateCommand
'        Dim return_parm As OracleParameter = Nothing

'        cmd.CommandType = CommandType.StoredProcedure
'        cmd.CommandText = sp_name

'        If Not sp_return Is Nothing Then
'            return_parm = New OracleParameter("return_value", _getOracleType(sp_return.Type), ParameterDirection.ReturnValue)
'            cmd.Parameters.Add(return_parm)
'        End If

'        If sp_parms.Count > 0 Then
'            If TypeOf (sp_parms(0).Value) Is System.Array Then
'                CType(cmd, OracleCommand).ArrayBindCount = CType(sp_parms(0).Value, System.Array).Length
'            End If
'        End If

'        For Each p As DALSPParameter In sp_parms
'            Dim parm As OracleParameter = New OracleParameter(p.Name, _getOracleType(p.Type), ParameterDirection.Input)
'            parm.Value = _getValue(p)
'            cmd.Parameters.Add(parm)
'        Next

'        Try
'            conn.Open()
'            Dim i As Integer = cmd.ExecuteNonQuery()

'            If Not sp_return Is Nothing Then
'                Return return_parm.Value
'            Else
'                Return i
'            End If

'        Catch ex As Exception
'            Throw ex
'        Finally
'            If conn.State <> ConnectionState.Closed Then
'                conn.Close()
'                cmd.Dispose()
'                conn.Dispose()
'            End If
'        End Try

'    End Function

'    Private Function _getConnection() As IDbConnection
'        Dim con As OracleConnection = New OracleConnection()

'        con.ConnectionString = con_string

'        Return con
'    End Function

'    Private Function _createDataAdapter() As IDbDataAdapter
'        Return New OracleDataAdapter()
'    End Function

'    Private Function _getOracleType(ByVal type As DALSPParameter.DALTypes) As OracleDbType
'        Select Case type
'            Case DALSPParameter.DALTypes.Number
'                Return OracleDbType.Int32
'            Case DALSPParameter.DALTypes.Text
'                Return OracleDbType.Varchar2
'            Case DALSPParameter.DALTypes.Date
'                Return OracleDbType.Date
'            Case DALSPParameter.DALTypes.LongText
'                Return OracleDbType.Clob
'        End Select
'    End Function

'    Private Function _getValue(ByVal parm As DALSPParameter) As Object
'        If parm.Type = DALSPParameter.DALTypes.Number Then
'            Return CInt(parm.Value)
'        Else
'            Return parm.Value
'        End If
'    End Function

'    Public Function ExecuteStoredProc(ByVal sp_name As String, ByVal sp_parms As System.Collections.Generic.List(Of DALSPParameter)) As Object Implements IDBHelper.ExecuteStoredProc
'        Return ExecuteStoredProc(sp_name, sp_parms, Nothing)
'    End Function

'    Public Function ExecuteStoredProc(ByVal sp As StoredProcedure) As Object Implements IDBHelper.ExecuteStoredProc

'        Dim conn As IDbConnection

'        If sp.Connection Is Nothing Then
'            conn = _getConnection()
'        Else
'            conn = sp.Connection
'        End If

'        Dim cmd As IDbCommand = conn.CreateCommand
'        Dim return_parm As OracleParameter = Nothing

'        cmd.CommandType = CommandType.StoredProcedure
'        cmd.CommandText = sp.Name

'        If Not sp.ReturnParameter Is Nothing Then
'            return_parm = New OracleParameter("return_value", _getOracleType(sp.ReturnParameter.Type), ParameterDirection.ReturnValue)
'            cmd.Parameters.Add(return_parm)
'        End If

'        If sp.Parameters.Count > 0 Then
'            If TypeOf (sp.Parameters(0).Value) Is System.Array Then
'                CType(cmd, OracleCommand).ArrayBindCount = CType(sp.Parameters(0).Value, System.Array).Length
'            End If
'        End If

'        For Each p As DALSPParameter In sp.Parameters
'            Dim parm As OracleParameter = New OracleParameter(p.Name, _getOracleType(p.Type), ParameterDirection.Input)

'            If CType(cmd, OracleCommand).ArrayBindCount > 0 Then
'                parm.Value = p.Value
'            Else
'                parm.Value = _getValue(p)
'            End If

'            cmd.Parameters.Add(parm)
'        Next

'        Dim bClose As Boolean = False

'        Try

'            If conn.State <> ConnectionState.Open Then
'                conn.Open()
'                bClose = True
'            End If

'            Dim i As Integer = cmd.ExecuteNonQuery()

'            If Not sp.ReturnParameter Is Nothing Then
'                Return return_parm.Value
'            Else
'                Return i
'            End If

'        Catch ex As OracleException
'            Throw ex
'        Catch ex As Exception
'            Throw ex
'        Finally
'            If bClose Then
'                If conn.State <> ConnectionState.Closed Then
'                    conn.Close()
'                    cmd.Dispose()
'                    conn.Dispose()
'                End If
'            End If
'        End Try

'    End Function

'End Class

'Public Class DALSPParameter

'    Private _name As String
'    Public Value As Object
'    Public Type As DALTypes
'    Public Length As Integer
'    Public _direction As DALDirections = DALDirections.Input

'    Public Overridable Property Name() As String
'        Get
'            Return _name
'        End Get
'        Set(ByVal value As String)
'            _name = value
'        End Set
'    End Property

'    Public Overridable Property Direction() As DALDirections
'        Get
'            Return _direction
'        End Get
'        Set(ByVal value As DALDirections)
'            _direction = value
'        End Set
'    End Property

'    Public Sub New()

'    End Sub

'    Public Sub New(ByVal name As String, ByVal value As Object, ByVal type As DALTypes)
'        Me.Name = name
'        Me.Value = value
'        Me.Type = type
'    End Sub

'    Public Sub New(ByVal name As String, ByVal value As Object, ByVal type As DALTypes, ByVal length As Integer)
'        Me.Name = name
'        Me.Value = value
'        Me.Type = type
'        Me.Length = length
'    End Sub

'    Public Enum DALTypes
'        Number
'        Text
'        LongText
'        [Date]
'    End Enum

'    Public Enum DALDirections
'        Input
'        Output
'    End Enum

'End Class


'Public Class DALSPReturnParameter
'    Inherits DALSPParameter

'    Public Sub New()
'        MyBase.Direction = DALDirections.Output
'    End Sub

'    Public Sub New(ByVal type As DALSPParameter.DALTypes)
'        MyBase.Type = type
'        MyBase.Direction = DALDirections.Output
'    End Sub

'    Public Overrides Property Name() As String
'        Get
'            Return "return_value"
'        End Get
'        Set(ByVal value As String)
'            'MyBase.Name = "return_value"
'        End Set
'    End Property

'    Public Overrides Property Direction() As DALSPParameter.DALDirections
'        Get
'            Return DALDirections.Output
'        End Get
'        Set(ByVal value As DALSPParameter.DALDirections)
'            'MyBase.Direction = DALDirections.Output
'        End Set
'    End Property

'End Class

'Public Class OracleHelperAE
'    Inherits OracleHelper
'    Implements IDBHelper

'    Public Sub New()

'        Dim apr As AppSettingsReader = New AppSettingsReader

'        Dim db As String = String.Empty

'        Try
'            db = apr.GetValue("Connexion", System.Type.GetType("System.String")).ToString()
'        Catch e As Exception
'            Dim s As String = "La base de donn�es � utiliser n'a pu �tre identifi�e.  La section Connexion de web.config n'est peut-�tre pas existante ou valide."
'            Throw New Exception(s, e)
'        End Try

'        Dim cs As ConnectionStringsSection = CType(ConfigurationManager.GetSection("connectionStrings"), ConnectionStringsSection)

'        Try
'            con_string = cs.ConnectionStrings(db).ConnectionString
'        Catch ex As Exception

'            con_string = db

'            'Dim s As String = "Il est impossible de d�terminer les param�tres de connexion � l'aide de l'�l�ment Connexion fourni dans le fichier web.config (" & IIf(db.Length > 0, db, "vide").ToString() & ")."
'            'Throw New Exception(s)
'        End Try

'    End Sub

'End Class

'Public Class OracleHelperPP
'    Inherits OracleHelper
'    Implements IDBHelper

'    Public Sub New()

'        Dim apr As AppSettingsReader = New AppSettingsReader

'        Dim db As String = String.Empty

'        Try
'            db = apr.GetValue("ConnexionPP", System.Type.GetType("System.String")).ToString()
'        Catch e As Exception
'            Dim s As String = "La base de donn�es � utiliser n'a pu �tre identifi�e.  La section Connexion de web.config n'est peut-�tre pas existante ou valide."
'            Throw New Exception(s, e)
'        End Try

'        Dim cs As ConnectionStringsSection = CType(ConfigurationManager.GetSection("connectionStrings"), ConnectionStringsSection)

'        Try
'            con_string = cs.ConnectionStrings(db).ConnectionString
'        Catch ex As Exception
'            Dim s As String = "Il est impossible de d�terminer les param�tres de connexion � l'aide de l'�l�ment Connexion fourni dans le fichier web.config (" & IIf(db.Length > 0, db, "vide").ToString() & ")."
'            Throw New Exception(s)
'        End Try

'    End Sub

'End Class

'Public Class OracleHelperGrilles
'    Inherits OracleHelper
'    Implements IDBHelper

'    Public Sub New()

'        Dim apr As AppSettingsReader = New AppSettingsReader

'        'Dim db As String = String.Empty

'        Try
'            con_string = apr.GetValue("GEA.ConnectionString", System.Type.GetType("System.String")).ToString()
'        Catch e As Exception
'            Dim s As String = "La base de donn�es � utiliser n'a pu �tre identifi�e.  La section Connexion de web.config n'est peut-�tre pas existante ou valide."
'            Throw New Exception(s, e)
'        End Try

'    End Sub

'End Class

'Public Class SQLHelper
'    Implements IDBHelper

'    Protected con_string As String = String.Empty
'    Protected erreur As String = String.Empty
'    Const MAX_ESSAIS_CONNECTION As Integer = 5

'    Public Function getDataSet(ByVal sql As String) As System.Data.DataSet Implements IDBHelper.getDataSet
'        Return _getDataSet(sql)
'    End Function

'    Public Function getDataSet(ByVal sql As String, ByVal sp_parms As System.Collections.Generic.List(Of DALSPParameter)) As DataSet Implements IDBHelper.getDataSet
'        Return Nothing
'    End Function

'    Public Function ExecuteStoredProc(ByVal sp_name As String, ByVal sp_parms As System.Collections.Generic.List(Of DALSPParameter), ByVal sp_return As DALSPReturnParameter) As Object Implements IDBHelper.ExecuteStoredProc
'        Return Nothing
'    End Function

'    Public Function ExecuteStoredProc(ByVal sp_name As String, ByVal sp_parms As System.Collections.Generic.List(Of DALSPParameter)) As Object Implements IDBHelper.ExecuteStoredProc
'        Return Nothing
'    End Function

'    Public Function ExecuteStoredProc(ByVal sp As StoredProcedure) As Object Implements IDBHelper.ExecuteStoredProc
'        Return Nothing
'    End Function

'    Private Function _getDataSet(ByVal sql As String) As System.Data.DataSet
'        Dim con As IDbConnection = _getConnection()

'        Dim cmd As IDbCommand = con.CreateCommand()

'        cmd.CommandType = Data.CommandType.Text
'        cmd.CommandText = sql

'        Dim adapter As IDbDataAdapter = _createDataAdapter()

'        adapter.SelectCommand = cmd

'        Dim ctr_essais As Integer
'        Dim reessayer As Boolean = True
'        Dim ds As DataSet = New DataSet()
'        Dim n_rows As Integer = 0
'        'Le Catch va sortir de la loop si besoin est
'        'Cette loop va attraper les erreurs de connection.
'        'Dim ge As GestionnaireDErreurs = New GestionnaireDErreurs()

'        While (reessayer)
'            Try
'                n_rows = adapter.Fill(ds)
'                reessayer = False
'            Catch ex As Exception
'                If ctr_essais > MAX_ESSAIS_CONNECTION Then
'                    'erreur = "C-002 - Maximum d'essais atteint. Le maximum d'essais de connection � la base de donn�es (" & MAX_ESSAIS_CONNECTION & ") a �t� atteint." & vbCrLf & "SQL: " & sql
'                    erreur = "C-002 - Maximum d'essais atteint. Le maximum d'essais de connection � la base de donn�es (" & MAX_ESSAIS_CONNECTION & ") a �t� atteint. SQL: " & sql
'                    reessayer = False
'                    Throw ex
'                Else
'                    Dim ch As CommonHelper = New CommonHelper()

'                    If ch.TestConnection(con) Then
'                        erreur = "C-003 - Avertissement: La connexion a �chou�. " & ex.Message
'                        ctr_essais += 1
'                    Else
'                        reessayer = False
'                        Throw ex
'                    End If
'                End If
'            End Try
'        End While

'        cmd.Dispose()

'        con.Dispose()

'        Return ds

'    End Function
'    Public Function getConnection() As System.Data.IDbConnection Implements IDBHelper.getConnection
'        Return _getConnection()
'    End Function
'    Private Function _getConnection() As IDbConnection
'        Dim maConnexionSQLsrv As SqlConnection
'        maConnexionSQLsrv = New SqlConnection(con_string)
'        Return maConnexionSQLsrv
'    End Function
'    Public Function getDataReader(ByVal sql As String) As IDataReader Implements IDBHelper.getDataReader
'        Return _getDataReader(sql)
'    End Function
'    Public Function getDataReader(ByVal sql As String, ByVal behavior As System.Data.CommandBehavior) As System.Data.IDataReader Implements IDBHelper.getDataReader
'        Return _getDataReader(sql, behavior)
'    End Function

'    Private Function _getDataReader(ByVal sql As String) As SqlDataReader
'        Return _getDataReader(sql, CommandBehavior.CloseConnection)
'    End Function

'    Private Function _getDataReader(ByVal sql As String, ByVal behavior As System.Data.CommandBehavior) As SqlDataReader

'        Dim con As SqlConnection = New SqlConnection()

'        con.ConnectionString = con_string
'        Dim cmd As SqlCommand = con.CreateCommand()
'        con.Open()

'        cmd.CommandType = Data.CommandType.Text
'        cmd.CommandText = sql

'        Dim dr As SqlDataReader = cmd.ExecuteReader(behavior)

'        Return dr

'    End Function
'    Private Function _createDataAdapter() As IDbDataAdapter
'        Return New SqlDataAdapter()
'    End Function
'    Public Function GetErreur() As String Implements IDBHelper.getErreur
'        Return erreur
'    End Function
'End Class

'Public Class SQLHelperSQL
'    Inherits SQLHelper
'    Implements IDBHelper

'    Public Sub New()

'        Dim apr As AppSettingsReader = New AppSettingsReader

'        Dim db As String = String.Empty

'        Try
'            db = apr.GetValue("Connexion_sql", System.Type.GetType("System.String")).ToString()
'        Catch e As Exception
'            Dim s As String = "La base de donn�es � utiliser n'a pu �tre identifi�e.  La section Connexion de web.config n'est peut-�tre pas existante ou valide."
'            Throw New Exception(s, e)
'        End Try

'        Dim cs As ConnectionStringsSection = CType(ConfigurationManager.GetSection("connectionStrings"), ConnectionStringsSection)

'        Try
'            con_string = cs.ConnectionStrings(db).ConnectionString
'        Catch ex As Exception
'            Dim s As String = "Il est impossible de d�terminer les param�tres de connexion � l'aide de l'�l�ment Connexion fourni dans le fichier web.config (" & IIf(db.Length > 0, db, "vide").ToString() & ")."
'            Throw New Exception(s)
'        End Try

'    End Sub

'End Class

'Public Class CommonHelper

'    Public erreur As String

'    Public Function GetErreur() As String
'        Return erreur
'    End Function

'    Public Function TestConnection(ByVal con As IDbConnection) As Boolean

'        Dim est_ouverte As Boolean = (con.State = ConnectionState.Open)

'        Try
'            If Not est_ouverte Then
'                con.Open()
'                Return True
'            End If
'        Catch ex As Exception
'            'Dim ge As GestionnaireDErreurs = New GestionnaireDErreurs()
'            'ge.EcrireTracePile = False
'            'ge.EcrireContexte = False
'            erreur = "C-001 - �chec de connection. Impossible d'ouvrir la connection" & ex.Message
'            Return False
'        Finally
'            If Not (con Is Nothing) And (con.State <> ConnectionState.Closed) And Not est_ouverte Then
'                con.Close()
'            End If
'        End Try

'    End Function

'End Class

