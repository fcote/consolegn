﻿<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="ee.aspx.vb" Inherits="ee" EnableEventValidation="false"%>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%--    <Triggers>         
       <asp:PostBackTrigger ControlID="ExportToExcelButton" />
    </Triggers> --%>
    <br/>
        <span lang="fr-ca" style="font-family: 'Berlin Sans FB Demi'; font-size: xx-large">
    Élèves en échec</span>
    <br />
    <div align="left">
        <br />
        
<%--        <% If Session("role") = "administrateur" Then %>
        <asp:DropDownList ID="ddl_dir" runat="server" 
            DataSourceID="SqlDataSource5" DataTextField="NOM" 
            DataValueField="M_MEMBRE_ID" AutoPostBack="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand=" 
            SELECT M_NOM || ', ' || M_PRENOM || ' - ' || M_ECOLE AS NOM, M_MEMBRE_ID 
            FROM GN_VW_ENSEIGNANTS WHERE TYPE_PERSON = 'D' ORDER BY M_NOM, M_PRENOM ">
        </asp:SqlDataSource><br/>
        <% End If%>--%>
        
        <asp:DropDownList ID="ddl_ecole" runat="server" DataSourceID="SqlDataSource1"
            DataTextField="ECOLE" DataValueField="ECOLE" AutoPostBack="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CS_GN %>"
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand="">
           <%-- <SelectParameters>
                <asp:SessionParameter Name="ID_USER" SessionField="ID_USER" Type="String" />
            </SelectParameters>--%>
        </asp:SqlDataSource>     
           
        <asp:DropDownList ID="ddl_etape" runat="server" AutoPostBack="True">
        </asp:DropDownList>
        <asp:DropDownList ID="ddl_note" runat="server" AutoPostBack="True">
            <asp:ListItem>&lt; 90</asp:ListItem>
            <asp:ListItem>&lt; 80</asp:ListItem>
            <asp:ListItem>&lt; 70</asp:ListItem>
            <asp:ListItem Selected="True">&lt; 60</asp:ListItem>
            <asp:ListItem>&lt; 50</asp:ListItem>
            <asp:ListItem>&gt; 50</asp:ListItem>
            <asp:ListItem>&gt; 60</asp:ListItem>
            <asp:ListItem Value="&gt; 70">&gt; 70</asp:ListItem>
            <asp:ListItem>&gt; 80</asp:ListItem>
            <asp:ListItem>&gt; 90</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btn_chart" runat="server" Text="Chart" Enabled="False" />
        <br />
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" 
            DefaultLoadingPanelID="RadAjaxLoadingPanel1">
            <ClientEvents OnRequestStart="onRequestStart" />
            <AjaxSettings>
            <%--<telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="ddl_note">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="ddl_note" />--%>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            
            
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server" 
            Height="19px" Skin="Default" Width="28px">
        </telerik:RadAjaxLoadingPanel>
        <asp:Panel ID="Panel1" runat="server" Width="100%">
            <telerik:RadGrid ID="RadGrid1" runat="server" 
    AllowFilteringByColumn="True" AllowSorting="True"
            DataSourceID="SqlDataSource2" GridLines="None" 
    ShowGroupPanel="True" Skin="Hay">
                <ExportSettings HideStructureColumns="true" ExportOnlyData="True">
                    <Excel Format="ExcelML" />
                </ExportSettings>
                <MasterTableView AutoGenerateColumns="False" DataSourceID="SqlDataSource2" 
                CommandItemDisplay="Top">
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true" ShowExportToPdfButton="false" ShowAddNewRecordButton="false"
                    ShowRefreshButton="true" />
                    <Columns>
                        <telerik:GridBoundColumn DataField="NOM" HeaderText="NOM" SortExpression="NOM" 
                        UniqueName="NOM">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PRENOM" HeaderText="PRENOM" 
                        SortExpression="PRENOM" UniqueName="PRENOM">
                        </telerik:GridBoundColumn>
                        <%--  <telerik:GridImageColumn UniqueName="PHOTO" HeaderText="PHOTO"
                        ImageHeight="40px" DataImageUrlFields="PHOTO" 
                        DataImageUrlFormatString="~/image.aspx?src={0}.jpg" ItemStyle-HorizontalAlign="Center">
                    </telerik:GridImageColumn>--%>
                        <telerik:GridBoundColumn DataField="NIVEAU" HeaderText="NIVEAU" 
                        SortExpression="NIVEAU" UniqueName="NIVEAU">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CLASSE_CODE" HeaderText="CLASSE_CODE" 
                        SortExpression="CLASSE_CODE" UniqueName="CLASSE_CODE">
                        </telerik:GridBoundColumn>
                        <%--                    <telerik:GridBoundColumn DataField="NOM_E" HeaderText="NOM_E" 
                        SortExpression="NOM_E" UniqueName="NOM_E">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PRENOM_E" HeaderText="PRENOM_E" 
                        SortExpression="PRENOM_E" UniqueName="PRENOM_E">
                    </telerik:GridBoundColumn>--%>
                        <telerik:GridBoundColumn DataField="ENSEIGNANT" HeaderText="ENSEIGNANT" 
                        SortExpression="ENSEIGNANT" UniqueName="ENSEIGNANT">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="NOTE" DataType="System.Decimal" 
                        HeaderText="NOTE" SortExpression="NOTE" UniqueName="NOTE">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="DESCRIPTION" HeaderText="DESCRIPTION" 
                        SortExpression="DESCRIPTION" UniqueName="DESCRIPTION">
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn DataField="M_DATE" DataType="System.DateTime" 
                        HeaderText="M_DATE" SortExpression="M_DATE" UniqueName="M_DATE" 
                        PickerType="DatePicker">
                        </telerik:GridDateTimeColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowDragToGroup="True">
                </ClientSettings>
            </telerik:RadGrid>
        </asp:Panel>
        <br />
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:CS_GN %>"
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" 
            SelectCommand="">
        </asp:SqlDataSource>
    
    
    
    
    
    
    </div>
    
    <div><dotnet:Chart id="chart" runat="server" Visible="false"/></div>
    
</asp:content>