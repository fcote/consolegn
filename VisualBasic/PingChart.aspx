<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates a custom chart type showing ping response times using sub values.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.Type = ChartType.Combo
		Chart.Size = "590x350"
		Chart.Title = ".netCHARTING Sample"

		Chart.LegendBox.Position = New Point(410, 18)

		Chart.DefaultElement.DefaultSubValue.Type = SubValueType.Line
		Chart.DefaultElement.DefaultSubValue.Line.Length = 15
		Chart.DefaultElement.DefaultSubValue.Line.Width = 5
		Chart.DefaultElement.DefaultSubValue.Line.Color = Color.FromArgb(100,Color.Gray)

		Chart.YAxis.Label.Text = "Response (ms)"

		Chart.YAxis.Scale = Scale.Range

		Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Day)

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

		Dim le As LegendEntry = New LegendEntry()
		le.Name = "Distribution"
		le.Background.Color = Color.Gray

		Chart.LegendBox.ExtraEntries.Add(le)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		Dim dt As DateTime = DateTime.Now
		For a = 1 To 1
			Dim s As Series = New Series("Average Response")
			s.LegendEntry.Value = "%Average ms"
			For b = 1 To 21
				Dim e As Element = New Element()
				e.YValue = myR.Next(20)+20
				e.YValueStart = e.YValue - 1
				e.XDateTime = dt.AddHours(b)
				s.Elements.Add(e)

				For c As Integer = 0 To 19
					e.SubValues.Add(SubValue.FromValue(e.YValue+myR.Next(10)))
					e.SubValues.Add(SubValue.FromValue(e.YValue -2- myR.Next(10)))
				Next c
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
