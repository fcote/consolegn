<%@ Page Language="VB" Debug="True" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
 'This sample demonstrates how to use the reloaded data during the post back request.
Sub Page_Load(sender As [Object], e As EventArgs)
   If Page.IsPostBack = False Then
      UserID.Text = Guid.NewGuid().ToString()
      CreateChart()
   Else
      'use the old image
      Dim chartImage As New System.Web.UI.WebControls.Image()
      chartImage.ID = "PostBackChart"
      Dim basePath As String = Request.Path
      basePath = basePath.Substring(0, basePath.LastIndexOf("/"))
      chartImage.ImageUrl = basePath + "/temp/chart" + UserID.Text + ".png"
      chartHolder.Controls.Add(chartImage)
   End If
End Sub 'Page_Load 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = 25 + myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData

Sub CreateChart()
   Dim myChart As New dotnetCHARTING.Chart()
   myChart.Size = "600x350"
   myChart.TempDirectory = "temp"
   myChart.FileName = "chart" + UserID.Text
   myChart.DisableBrowserCache = False
   
   
   ' Set the title.
   myChart.Title = "My Chart"
   
   ' Set the x axis label
   myChart.ChartArea.XAxis.Label.Text = "X Axis Label"
   
   ' Set the y axis label
   myChart.ChartArea.YAxis.Label.Text = "Y Axis Label"
   
   ' Set the bar shading effect
   myChart.ShadingEffect = True
   
   ' Add the random data.
   myChart.SeriesCollection.Add(getRandomData())
   
   chartHolder.Controls.Add(myChart)
End Sub 'CreateChart

</script>
<html>
<head>
<title>Postback Sample</title>
</head>
<body>
<center>
<form id="postback" action="postback.aspx" runat="server">
<asp:TextBox Visible="False" id="UserID" runat="server"></asp:TextBox>
<table>
<tr><td>
<asp:PlaceHolder ID="chartHolder" Runat="server" ></asp:PlaceHolder>
 </tr></td>
 <tr><td align="center">
 <asp:Button Text="Get Chart" Runat=server/>
  </tr></td>
 </form>
 </table>
</center>
</body>
</html>