<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>


<script runat="server">



Sub Page_Load(sender As [Object], e As EventArgs)
   ' Set the title.
   Chart.Title = "My Chart"
   
   ' Set the directory where images are temporarily be stored.
   Chart.TempDirectory = "temp"
   
   ' Disable the legend box
   Chart.LegendBox.Position = LegendBoxPosition.None
   
   ' Set a default gauge face background color.
   Chart.DefaultSeries.Background.Color = Color.White
   
   ' Set he chart size.
   Chart.Size = "600x350"
   
   ' Specify the gauges chart type
   Chart.Type = ChartType.Gauges
   
   ' Create a series with a single element
   Dim s1 As New Series("series 1", New Element("", 50))
   ' Instantiate an axis for the created series.
   s1.YAxis = New Axis()
   
   ' Set visual properties of the axis.
        s1.YAxis.DefaultTick.Line.Width = 10
        s1.YAxis.DefaultTick.Line.EndCap = LineCap.Triangle
   
   ' Instantiate an axis marker.
   Dim am As New AxisMarker("", New Background(Color.Red), 50, 60)
   
   ' Instantiate a background object for the second axis marker.	
   Dim b As New Background(Color.Orange)
   ' Set hatch style properties of the axis marker.
   b.HatchStyle = HatchStyle.LightUpwardDiagonal
   b.HatchColor = Color.White
   
   ' Instantiate the second axis marker.
   Dim am2 As New AxisMarker("", b, 45, 50)
   
   ' Add the two axis markers to the series' axis.
   s1.YAxis.Markers.Add(am)
   s1.YAxis.Markers.Add(am2)
   
   ' Add the series
   Chart.SeriesCollection.Add(s1)
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
