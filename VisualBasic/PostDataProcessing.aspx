<%@ Page Language="VB" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>



<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   'set global properties
   Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   Chart.Title = "Sales"
   Chart.XAxis.Label.Text = "Months"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Mentor=false
   
   Chart.DateGrouping = TimeInterval.Months
   
   'Add a series
   Chart.Series.StartDate = New System.DateTime(2002, 1, 1, 0, 0, 0)
   Chart.Series.EndDate = New System.DateTime(2002, 6, 30, 23, 59, 59)
   Chart.Series.SqlStatement = "SELECT OrderDate,Total, Name FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
   Chart.SeriesCollection.Add()
   
   
   AddHandler Chart.PostDataProcessing, AddressOf OnPostDataProcessing
End Sub 'Page_Load
 
Sub OnPostDataProcessing(sender As [Object])
   chartdatagrid.DataSource = New DataView(Chart.SeriesCollection.GetProcessedDataTable("%YValue", True, True, "Sales"))
   chartdatagrid.DataBind()
End Sub 'OnPostDataProcessing 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Sales Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
 <asp:DataGrid id="chartdatagrid" Width="50%" Font-Names="Arial" HeaderStyle-BackColor="skyblue"
BackColor="lightblue" ShowHeader="true" runat="server"/>
</div>
</body>
</html>
