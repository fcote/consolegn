<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

		<script runat="server">

Private  Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
 
	Chart.Type = ChartType.Combo'Horizontal;
	Chart.Size = "600x350"
	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Title = ".netCHARTING Gallery Sample"
	Chart.DefaultSeries.Type = SeriesType.Line
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
	Chart.DefaultElement.Marker.Visible = False	
 
	' Zooming the x axis.
	Dim zoomArea As ChartArea =  Chart.ChartArea.GetXZoomChartArea(Chart.XAxis,New ScaleRange(30,40),New Line(Color.Red,DashStyle.Dash)) 
	Chart.ExtraChartAreas.Add(zoomArea)
 
	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Dim mySC As SeriesCollection =  getRandomData() 
 
	' Add the random data.
	Chart.SeriesCollection.Add(mySC)
 
End Sub
 
Private Function getRandomData() As SeriesCollection
	Dim SC As SeriesCollection =  New SeriesCollection() 
	Dim myR As Random =  New Random(1) 
 
	Dim a As Integer
	For  a = 1 To  2- 1  Step  a + 1
		Dim s As Series =  New Series() 
		s.Name = "Series " + a.ToString()
		Dim b As Integer
		For  b = 1 To  60- 1  Step  b + 1
			Dim e As Element =  New Element() 
 
			e.YValue = myR.Next(50)
			e.XValue = b+(5*(a-1))
			s.Elements.Add(e)
		Next
		SC.Add(s)
	Next
 
	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
 
 
	Return SC
End Function
</script>
<HTML xmlns="http://www.w3.org/1999/xhtml">
	<HEAD>
		<TITLE>.netCHARTING Gallery Sample</TITLE>	</HEAD>
	<BODY>
		<DIV align="center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</DIV>
	</BODY>
</HTML>
