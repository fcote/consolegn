Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Configuration
Imports dotnetCHARTING


Public Class ChartClass
   Inherits UserControl
   Public ChartObj As dotnetCHARTING.Chart
   
   Public Sub Page_Load([Source] As [Object], E As EventArgs)
      'set global properties
      ChartObj.Title = "Weekday Report"
      ChartObj.ChartArea.XAxis.Label.Text = "Days"
      ChartObj.ChartArea.YAxis.Label.Text = "Dollars (Thousands)"
      ChartObj.TempDirectory = "temp"
      ChartObj.Debug = True
      ChartObj.DefaultSeries.DefaultElement.ShowValue = True
      
      ChartObj.ChartArea.XAxis.ReverseSeries = True
      
      'Adding series programatically
      Dim sr As New Series()
      
      sr.Name = "Vancouver"
      sr.Type = SeriesType.Cylinder
      
      Dim el As New Element("Mon", 2)
      sr.Elements.Add(el)
      el = New Element("Tue", 4)
      sr.Elements.Add(el)
      el = New Element("Wed", 5)
      sr.Elements.Add(el)
      el = New Element("Thr", 6)
      sr.Elements.Add(el)
      el = New Element("Fri", 5)
      sr.Elements.Add(el)
      ChartObj.SeriesCollection.Add(sr)
      
      sr = New Series()
      sr.Name = "Seattle"
      sr.Type = SeriesType.AreaLine
      el = New Element("Mon", 5)
      sr.Elements.Add(el)
      el = New Element("Tue", 8)
      sr.Elements.Add(el)
      el = New Element("Wed", 6)
      sr.Elements.Add(el)
      el = New Element("Thr", 7)
      sr.Elements.Add(el)
      el = New Element("Fri", 6)
      sr.Elements.Add(el)
      ChartObj.SeriesCollection.Add(sr)
   End Sub 'Page_Load 
End Class 'ChartClass
