<%@ Page Language="VB" %>

<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Dim Dt As DateTime = DateTime.Now
   TimeLabel.Text = Dt.ToString()
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Streaming Usage Sample</title></head>
<body>
<div style="text-align:center">
 <font face="Arial" size="2"><asp:Label id="TimeLabel"  runat="server"/></font>
<br>
 <img border="0" src="streaming.aspx"><br><br>
 <font face="Arial" size="2">View the source of this page in your browser to see that streaming.aspx is included<br>in the page with a regular HTML img reference.  There is no reference to an actual image file.
 </font>
</div>
</body>
</html>
