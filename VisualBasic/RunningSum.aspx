<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   'set global properties
   Chart.Title = "Sales for March 11,2002"
   Chart.ChartArea.XAxis.Label.Text = "Hours"
   Chart.LegendBox.Template = "%icon %name"
   Chart.ShowDateInTitle = False
   
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.ChartArea.XAxis.FormatString = "h tt"
   
   Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   Chart.DefaultSeries.StartDate = New System.DateTime(2002, 3, 11, 0, 0, 0)
   Chart.DefaultSeries.EndDate = New System.DateTime(2002, 3, 11, 23, 59, 59)
   
   
   'Add a series
   Chart.Series.Name = "Items"
   Chart.Series.SqlStatement = "SELECT OrderDate, Sum(Total) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
   Chart.Series.Visible = False
   Chart.SeriesCollection.Add()
   
   'Add Sum series
   Chart.Series.Name = "Total"
   Chart.Series.DefaultElement.ShowValue = True
   Chart.Series.Type = SeriesType.Line
   Chart.SeriesCollection.Add(Calculation.RunningSum)
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Running Sum</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
