<%@ Page Language="vb" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>

<script runat="server">
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	Chart.Title = "A chart without zooming"
	Chart.Type = ChartType.Bubble
	Chart.TempDirectory="temp"
	Chart.Debug = True
	Chart.ShadingEffect = True
	Chart.PaletteName = dotnetCHARTING.Palette.Bright
	Chart.DefaultElement.ShowValue = False
	Chart.Size = "1000x350"
	Chart.DefaultSeries.DefaultElement.Transparency = 20
	Chart.XAxis.Label.Text="X Axis Label"
	Chart.YAxis.Label.Text="Y Axis Label"
	' Add the random data.
	Chart.SeriesCollection.Add(getRandomData())

	'Second chart with zooming
	Chart2.Title = "A chart with zooming, right click on the chart to access zooming"

	'Enable zooming with just one property
	Chart2.Zoomer.Enabled =True
	Chart2.Type = ChartType.Bubble
	Chart2.TempDirectory="temp"
	Chart2.Debug = True
	Chart2.ShadingEffect = True
	Chart2.PaletteName = dotnetCHARTING.Palette.Bright
	Chart2.DefaultElement.ShowValue = True
	Chart2.Size = "1000x350"
	Chart2.Zoomer.Enabled = True
	Chart2.DefaultSeries.DefaultElement.Transparency = 20
	Chart2.XAxis.Label.Text="X Axis Label"
	Chart2.YAxis.Label.Text="Y Axis Label"
	' Add the random data.
	Chart2.SeriesCollection.Add(getRandomData())


	'Second chart with Initial zooming
	Chart3.Title = "A chart with initial zooming, right click on the chart for zooming options"
	'set for initial zooming  
	Chart3.Zoomer.Enabled =True
	'Set to zoom at middle of the chart.
	Chart3.XAxis.Viewport.ScaleRange = New ScaleRange(10,30)
	Chart3.YAxis.Viewport.ScaleRange = New ScaleRange(20,40)
	Chart3.Type = ChartType.Bubble
	Chart3.TempDirectory="temp"
	Chart3.Debug = True
	Chart3.ShadingEffect = True
	Chart3.PaletteName = dotnetCHARTING.Palette.Bright
	Chart3.DefaultElement.ShowValue = True
	Chart3.Size = "1000x350"
	Chart3.Zoomer.Enabled = True
	Chart3.DefaultSeries.DefaultElement.Transparency = 20
	Chart3.XAxis.Label.Text="X Axis Label"
	Chart3.YAxis.Label.Text="Y Axis Label"
	' Add the random data.
	Chart3.SeriesCollection.Add(getRandomData())


End Sub
Function getRandomData() As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random()
	For a As Integer = 1 To 6
		Dim s As Series = New Series()
		s.Name = "Series " & a
		For b As Integer = 0 To 4
			Dim e As Element = New Element()
			e.Name = "Element " & b
			e.YValue = myR.Next(50)
			e.XValue = myR.Next(50)
			e.BubbleSize = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample </title></head>
<body>
<center>
	 <dotnet:Chart ID="Chart" runat="server" />
	  <br/>
	 <dotnet:Chart ID="Chart2" runat="server" />
	  <br/>
	 <dotnet:Chart ID="Chart3" runat="server" />
   </center>
</body>
</html>
