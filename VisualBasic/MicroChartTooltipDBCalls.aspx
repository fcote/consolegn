<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates showing MicroChart tooltips based on SubValues created using nested Database calls.

		Chart.Size = "600x350"
		Chart.Title = "Sales for 2002"
		Chart.ShowDateInTitle = False
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.LimeGreen, Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.ShadingEffectMode = ShadingEffectMode.Four
		Chart.YAxis.FormatString = "Currency"
		Chart.DefaultElement.DefaultSubValue.Visible = False

		' Style the title box & legend.
		Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
		Chart.TitleBox.Padding = 8
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Background2
		Chart.TitleBox.Background.Color = Color.LightSteelBlue
		Chart.LegendBox.Template = "%Value"
		Chart.LegendBox.DefaultEntry.Value = "<block fSize='10'>%YSum"

		' Get the live data and add it to the chart.
		Dim mySC As SeriesCollection = getLiveData()
		mySC(0).Name = "Total"
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.StartDate = New DateTime(2002, 1, 1, 0, 0, 0)
		de.EndDate = New DateTime(2002, 12, 31, 23, 59, 59)
		de.SqlStatement = "SELECT Name, sum(Total) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Orders.Name ORDER BY Orders.Name"
		Dim sc As SeriesCollection = de.GetSeries()

		For Each e As Element In sc(0).Elements
			' For each user, query the db for more data and construct an infoGrid for each element's tooltip.
			Dim s As Series = getMonthlyData(e.Name)
			e.ToolTip = constructInfoGrid(s)
		Next e
		Return sc
	End Function

	Function constructInfoGrid(ByVal s As Series) As String
		Dim str As String = "<Chart:Spacer width='25'><Chart:Spacer width='25'><block><block><row>"
		Dim max As Double = s.Calculate("", Calculation.Maximum).YValue * 1.1
		For Each e As Element In s.Elements
			str &= e.Name & "<block>" & e.YValue.ToString("C") & "<Chart:Bar value='" & e.YValue & "' max='" & max & "''><Chart:Sparkline values='" & e.SubValues.GetValues() & "' color='DarkGreen' colors='green,black'><hr>"
		Next e
		Return str
	End Function

	''' <summary>
	''' This method gets an element for each month and subValues grouped by days in each element.
	''' </summary>
	Function getMonthlyData(ByVal name As String) As Series
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.StartDate = New DateTime(2002, 1, 1, 0, 0, 0)
		de.EndDate = New DateTime(2002, 12, 31, 23, 59, 59)
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT Orders.OrderDate, Sum(Total) FROM Orders WHERE Orders.Name='" & name & "' AND OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
		de.DateGrouping = TimeInterval.Year
		de.SubValueDateGrouping = TimeInterval.Days
		Dim sc2 As SeriesCollection = de.GetSeries()

		Return sc2(0)
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<asp:Label ID='label1' runat="server" />
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
