<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(700)
   Chart.Height = Unit.Parse(300)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Sample Data"
   
   Dim mySC As SeriesCollection = getRandomData()
   Chart.SeriesCollection.Add(mySC)
   Chart.DefaultSeries.Type = SeriesType.Spline
   Chart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Horizontal
   
   Dim sChartArea As New ChartArea("S Chart")
   sChartArea.XAxis = New Axis()
   sChartArea.LegendBox = New LegendBox()
   sChartArea.LegendBox.Visible = False
   sChartArea.DefaultSeries.LegendEntry.Visible = False
   Chart.ExtraChartAreas.Add(sChartArea)
   
   
   Dim sChart As Series = StatisticalEngine.SChart(mySC)
   sChartArea.SeriesCollection.Add(sChart)
   
   Dim sXBARChartArea As New ChartArea("S XBAR")
   sXBARChartArea.XAxis = New Axis()
   
   Chart.ExtraChartAreas.Add(sXBARChartArea)
   sXBARChartArea.LegendBox = New LegendBox()
   sXBARChartArea.LegendBox.Visible = False
   
   Dim sXBARChart As Series = StatisticalEngine.SXBARChart(mySC)
   sXBARChartArea.SeriesCollection.Add(sXBARChart)
End Sub 'Page_Load


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(5)
   Dim a As Integer
   For a = 0 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 0 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   'SC[0].DefaultElement.Color = Color.FromArgb(255,99,49);
   'SC[1].DefaultElement.Color = Color.FromArgb(0,156,255);
   'SC[2].DefaultElement.Color = Color.FromArgb(49,255,49);
   'SC[3].DefaultElement.Color = Color.FromArgb(255,255,0);
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
