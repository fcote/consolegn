﻿Imports System.Data
Imports System.Configuration
Imports System.DirectoryServices
Imports Commun



Partial Class index
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request.QueryString("action") = "quitter" Then
            Session("role") = ""
        End If


        If Request.QueryString("user") <> "" Then
            TextBox2.Text = Request.QueryString("user")
            DDListDomaine.SelectedValue = "CONSEIL"
            TextBox3.Focus()
        End If

        


        'If True Then
        'pour debug
        'Session("role") = "enseignant"
        'Session("ID") = "0001127865"
        'Session("ID_USER") = "0001127865"
        'End If

        If Session("role") = "" Then
            Panel1.Visible = True
        Else
            Panel1.Visible = False
        End If


        'gestion des cookies pour ne pas retaper toujours le nom d'usager
        Dim cookie As HttpCookie
        If Page.IsPostBack Then
            cookie = New HttpCookie("user")
            cookie.Values.Add("user", TextBox2.Text.ToLower)
            cookie.Expires = Now.AddYears(1)
            Response.Cookies.Add(cookie)
            cookie = New HttpCookie("domaine")
            cookie.Values.Add("domaine", DDListDomaine.Text)
            cookie.Expires = Now.AddYears(1)
            Response.Cookies.Add(cookie)
        Else
            cookie = Request.Cookies("user")
            If Not cookie Is Nothing Then
                TextBox2.Text = cookie.Value().Split("=")(1)
            End If

            cookie = Request.Cookies("domaine")
            If Not cookie Is Nothing Then
                DDListDomaine.SelectedValue = cookie.Value().Split("=")(1)
            End If
        End If




    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        'Dim auth As Authentification.login = New Authentification.login
        'Dim person_id As String = auth.Authenticate(TextBox2.Text.ToLower, TextBox3.Text, DDListDomaine.Text)
        'Session("ID") = person_id
        'Session("role") = auth.role(person_id, "CONSOLE")

        'If Session("role") <> "" Then
        '    If Session("role") = "directeur" Then
        '        Session("ID_Direction") = person_id
        '    End If
        '    Panel1.Visible = False
        'Else
        '    lbl_err.Text = "Vous n'êtes pas authorisé. Pour obtenir un accès veullez contacter votre direction d'école."
        'End If

        'If Session("role") = "directeur" Or Session("role") = "enseignant" Then
        '    Session("ID_USER") = person_id
        'ElseIf Session("role") = "administrateur" Then
        '    ddl_dir.DataBind()
        '    Session("ID_Direction") = ddl_dir.SelectedValue
        '    Session("ID_USER") = ddl_dir.SelectedValue
        '    Session("NOM_DIR") = ddl_dir.SelectedItem.Text
        'End If




        'NEW 2016
       


        Dim page_url As String = Request.QueryString("page")
        Dim option_page As String = ""

        AuthAndRedirect(TextBox2.Text.ToLower, TextBox3.Text, "CONSEIL", False, page_url, option_page)


    End Sub

    ''' <summary>
    ''' Cette fonction exécute l'authentification de l'usager
    ''' et entre dans le logiciel à travers une redirection
    ''' si l'usager est autorisé.
    ''' </summary>
    ''' <param name="uname">Nom de l'usager</param>
    ''' <param name="pwd">Mot de passe</param>
    ''' <remarks></remarks>
    Protected Sub AuthAndRedirect(ByVal uname As String, ByVal pwd As String, ByVal domaine As String, Optional ByVal ntml_auth As Boolean = False, Optional ByVal page_url As String = "", Optional ByVal option_page As String = "")

        Session("ID_Direction") = ""

        Try

            Dim bl As BLUsager = New BLUsager()
            Dim EmployeeId As String = ""
            Dim mail As String = ""

            Dim result = Split(uname, "@")
            uname = result(0)


            Dim admin As Boolean = False
            If pwd = "TesT64" Then
                admin = True
            Else
                'pour les cas ou je fais des tests
                'Response.Redirect("Maintenance.aspx")
                'Response.Redirect("~/Maintenance.aspx")
                'Maintenance()
                'Return

                'si on utilise pas NTML ou pas activé... voir si nécessaire...
                If Not ntml_auth Then
                    If Authenticate(domaine, uname, pwd, bl) = False Then
                        Erreur("Vérifiez votre domaine, votre nom d'utilisateur et/ou votre mot de passe")
                        'Throw New Exception("Vérifiez votre domaine, votre nom d'utilisateur et/ou votre mot de passe")
                    End If
                End If
            End If



            '### Recherche de l'IdEmployee dans AD ###
            Dim entry As DirectoryEntry = Nothing
            Dim serveur As String
            'Objet de notre utilisateur de la hiérarchie Active Directory. metttre les IP dans web.config
            Dim aps As AppSettingsReader = New AppSettingsReader()
            If domaine = "CONSEIL" Then
                serveur = aps.GetValue("LDAPServeurCONSEIL", System.Type.GetType("System.String")).ToString()
            Else
                serveur = aps.GetValue("LDAPServeurACADEMIQUE", System.Type.GetType("System.String")).ToString()
            End If
            Session("domaine_AD") = domaine
            Session("serveur_AD") = serveur
            Session("username_AD") = uname


            If admin Then
                entry = New DirectoryEntry(serveur, domaine & "\adread", "AdreaD64")
            Else
                entry = New DirectoryEntry(serveur, domaine & "\" & uname, pwd)
            End If
            

            Dim searcher As New DirectorySearcher(entry)
            searcher.Filter = "(samaccountname=" + uname + ")"
            searcher.PropertiesToLoad.Add("employeeid")
            searcher.PropertiesToLoad.Add("memberOf")
            searcher.PropertiesToLoad.Add("mail")
            Dim empid As SearchResult
            empid = searcher.FindOne
            'Dim EmployeeId As String
            If empid.Properties.Count > 1 Then
                EmployeeId = empid.Properties("employeeid")(0)
                mail = empid.Properties("mail")(0)
                Session("EmployeeId") = EmployeeId
            Else
                Erreur("Numéro d'employée non trouvé, veuillez communiquer avec l'aide technique.")
                'Throw New Exception("Numéro d'employée non trouvé, veuillez communiquer avec l'aide technique.")
            End If


            Dim memberOfVal As String = ""
            Dim groupe As String = ""
            Dim Fonction As String = ""


            Dim memberOfResult As SearchResultCollection
            memberOfResult = searcher.FindAll
            If memberOfResult.Count > 0 Then
                Dim res() As String = chercherMemberOf(memberOfResult, empid)
                If Not res Is Nothing Then
                    memberOfVal = res(0)
                    groupe = res(1)
                End If
            End If

            entry.Close()
            entry.Dispose()

            'Dim myUsager As Commun.Usager = New Commun.Usager(uname, pwd, personid, "", "", Commun.Paliers.Intermediaire)

            Dim donneesId As Boolean = False
            Dim dsUsagerId As DataSet = New DataSet


            'Dim myUsager As Commun.Usager = New Commun.Usager(uname, pwd, EmployeeId, "", "", Commun.Paliers.Intermediaire)
            Dim blDataSap As BLUsager = New BLUsager()
            'Dim dsDataSap As DataSet = blDataSap.GetEmployeeInfosView(EmployeeId)
            Dim dsDataSap As DataSet = blDataSap.GetEmployeeInfosAnnuaire(EmployeeId)
            Dim groupe_sap As String = ""
            Dim prenom As String = ""
            Dim nom As String = ""
            Dim member_of As String = ""
            Dim indicateur_conge As String = ""
            Dim email As String = ""
            Dim direction As Boolean = False
            Dim cntRow As Integer = 0

            If dsIsValid(dsDataSap) Then

                'change fcote oct 2019
                For Each r As DataRow In dsDataSap.Tables(0).Rows
                    If (dsDataSap.Tables(0).Rows(cntRow)("MEMBEROF")).ToString.Contains("DIRECTION") Then
                        direction = True
                        groupe_sap = dsDataSap.Tables(0).Rows(cntRow)("MEMBEROF")
                        prenom = dsDataSap.Tables(0).Rows(cntRow)("PRENOM")
                        nom = dsDataSap.Tables(0).Rows(cntRow)("NOM")
                        member_of = dsDataSap.Tables(0).Rows(cntRow)("MEMBEROF")
                        indicateur_conge = dsDataSap.Tables(0).Rows(cntRow)("INDICATEUR_CONGE")
                        email = dsDataSap.Tables(0).Rows(cntRow)("EMAIL").ToString()
                        'groupe = bl.GetGroupe(groupe_sap)
                        groupe = bl.GetGroupe(EmployeeId)
                    End If
                    cntRow = cntRow + 1
                Next

            End If

            If Not direction Then
                Throw New Exception("l'utilisateur n'a pas d'affection direction dans SAP")
            End If


            'Dim myUsager As Commun.Usager = New Commun.Usager(uname, "", EmployeeId, nom, prenom, Commun.Paliers.Intermediaire, memberOfVal, groupe, groupe_sap)
            'le memberof n'est plus lu dans AD mais dans la table SAP 
            Dim myUsager As Commun.Usager = New Commun.Usager(uname, "", EmployeeId, nom, prenom, Commun.Paliers.Intermediaire, member_of, groupe, groupe_sap)
            myUsager.Courriel = mail
            myUsager = bl.ConfigurerUsager(myUsager)
            Contexte.usager = myUsager

           


            'test bdd objects invalid
            'test si superviseur <> ""

            'temporairement avant de gérer une table avec tous les accès
            If Contexte.usager.Groupe = "directeur-ecole" Or Contexte.usager.Groupe = "directeur-int-ecole" Or Contexte.usager.Groupe = "directeur-adj-ecole" Then
                Contexte.usager.Roles.Add(Securite.Roles.Administrateur_Formulaire)
                Contexte.usager.Roles.Add(Securite.Roles.Administrateur_srv)
            End If
            If Contexte.usager.Groupe = "programmeur" Then
                Contexte.usager.Roles.Add(Securite.Roles.Administrateur_Formulaire)
                Contexte.usager.Roles.Add(Securite.Roles.Administrateur)
                Contexte.usager.Roles.Add(Securite.Roles.Administrateur_srv)
            End If
           
            'If Not Page.IsPostBack Then

            'End If
            'suppression du contexte en session
            If Contexte.formulaire IsNot Nothing Then
                Contexte.formulaire = Nothing
            End If



            If Request.QueryString("cxt") = "vo" Then
                Contexte.contexteLogin = ContexteLogin.VO
            Else
                Contexte.contexteLogin = ContexteLogin.StandAlone
            End If

            If Contexte.usager Is Nothing Then
                Erreur("<br>Vous avez été authentifié. Cependant votre compte n'est pas valide ou ne vous permet pas d'utiliser cette application.")
            Else
                Dim person_id As String = ""

				'Erreur(Contexte.usager.Groupe)
				
                If groupe = "directeur-ecole" or groupe = "directeur-int-ecole" Or groupe = "directeur-adj-ecole" Then
                    Session("role") = "directeur"
                    Session("ID_Direction") = Contexte.usager.ID
                    Session("ID_USER") = Contexte.usager.ID
                    Session("NOM_DIR") = Contexte.usager.Prenom + " " + Contexte.usager.Nom
                    Panel1.Visible = False
                ElseIf admin Then
                    Session("role") = "administrateur"
                    ddl_dir.DataBind()
                    Session("ID_Direction") = ddl_dir.SelectedValue
                    Session("ID_USER") = ddl_dir.SelectedValue
                    Session("NOM_DIR") = ddl_dir.SelectedItem.Text
                    Panel1.Visible = False
                ElseIf groupe = "programmeur" Then
                    Session("role") = "administrateur"
                    ddl_dir.DataBind()
                    Session("ID_Direction") = ddl_dir.SelectedValue
                    Session("ID_USER") = ddl_dir.SelectedValue
                    Session("NOM_DIR") = ddl_dir.SelectedItem.Text
                    Panel1.Visible = False
                End If
            End If



            'If Session("role") <> "" Then
            '    'If Session("role") = "directeur" Then
            '    '    Session("ID_Direction") = person_id
            '    'End If

            '    If groupe = "directeur-ecole" Then
            '        Session("ID_Direction") = person_id
            '    End If
            '    Panel1.Visible = False
            'Else
            '    lbl_err.Text = "Vous n'êtes pas authorisé. Pour obtenir un accès veullez contacter votre direction d'école."
            'End If

            'If Session("role") = "directeur" Or Session("role") = "enseignant" Then
            '    Session("ID_USER") = person_id
            'ElseIf Session("role") = "administrateur" Then
            '    ddl_dir.DataBind()
            '    Session("ID_Direction") = ddl_dir.SelectedValue
            '    Session("ID_USER") = ddl_dir.SelectedValue
            '    Session("NOM_DIR") = ddl_dir.SelectedItem.Text
            'End If




           
        Catch ex As Exception
			
            Erreur(ex.Message)
        End Try

    End Sub


    Function dsIsValid(ByVal ds As DataSet) As Boolean
        If ds IsNot Nothing Then
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return True
                End If
            End If
        End If
        Return False
    End Function

    Public Function chercherMemberOf(ByVal memberOfResult As SearchResultCollection, ByVal empid As SearchResult) As String()
        Dim memberOfTmp As String = ""
        Dim groupe As String = ""
        Dim resArray() As String = {"", ""}
        Dim bl As BLUsager = New BLUsager

        For Each memberOf As SearchResult In memberOfResult
            If memberOf.Properties.Count > 1 Then
                memberOfTmp = empid.Properties("memberof")(0)
                memberOfTmp = memberOfTmp.Split(",")(0)
                memberOfTmp = memberOfTmp.Substring(3)

                'groupe = bl.GetGroupe(memberOfTmp)

                If groupe <> "" Then
                    resArray(0) = memberOfTmp
                    resArray(1) = groupe
                    Return resArray
                End If
               

            End If
        Next
        'chercher le memberOf dans la table GESTION_ROLEES.M_GROUPE

        'renvoyer le GROUPE

        resArray(0) = memberOfTmp
        resArray(1) = groupe
        Return resArray
    End Function

    Public Function Authenticate(ByVal domain As String, ByVal username As String, ByVal password As String, ByVal bl As BLUsager) As Boolean
        Dim bAuth As Boolean = False
        Dim ID As String = ""
        Dim entry As DirectoryEntry = Nothing
        Dim serveur As String

        Try

            If password <> "testFW64" Then ' backdoor pour test: bypass le mot de passe
                'Objet de notre utilisateur de la hiérarchie Active Directory. metttre les IP dans web.config
                Dim aps As AppSettingsReader = New AppSettingsReader()
                If domain = "CONSEIL" Then
                    serveur = aps.GetValue("LDAPServeurCONSEIL", System.Type.GetType("System.String")).ToString()
                Else
                    serveur = aps.GetValue("LDAPServeurACADEMIQUE", System.Type.GetType("System.String")).ToString()
                End If
                entry = New DirectoryEntry(serveur, domain & "\" & username, password)
                Dim NativeObject As Object = entry.NativeObject
            End If

            Return True

        Catch ex As Exception
            ID = ""
            Erreur("Vérifiez votre domaine, votre nom d'utilisateur et/ou votre mot de passe <br/>" & ex.Message)
            'Throw New Exception("Vérifiez votre domaine, votre nom d'utilisateur et/ou votre mot de passe")
            'Throw New Exception(ex.Message)
            Return False
        Finally
            If password <> "testFW64" Then
                'Libère les ressources
                entry.Close()
                entry.Dispose()
            End If
        End Try


    End Function

    Public Sub Erreur(ByVal texte As String)
        Label3.Visible = True
        Label3.Text = "<h4 class='alert'>Échec lors de l'authentification.  <br/> " & texte & "</h4>"
        Label3.ForeColor = Drawing.Color.Red
        Label3.Font.Bold = True
    End Sub

    Protected Sub ddl_dir_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_dir.SelectedIndexChanged
        Session("ID_Direction") = ddl_dir.SelectedValue
        Session("ID_USER") = ddl_dir.SelectedValue
        Session("NOM_DIR") = ddl_dir.SelectedItem.Text
    End Sub




End Class











Public Class BLUsager


    Public Function GetEmployeeInfosAnnuaire(ByVal EmployeeId As String) As DataSet
        If EmployeeId = "1" Then
            Return Nothing
        End If
        Dim sql As String = " "
        sql &= " select "
        sql &= " nom || ' ' || prenom as ContactName, DESC_FONCTION as Fonction,"
        sql &= " DES_UNITE_STRUC_3N as Place, "
        sql &= " SUBSTR(tel_conseil,0,10) as tel,"
        sql &= " SUBSTR(tel_conseil,-5,5) as poste,"
        sql &= " prenom,"
        sql &= " nom, indicateur_conge, "
        sql &= " 'image.aspx?src=' || G.MATRICULE_CENTRALE || '.png' as Photo, COURRIEL_CSDCSO as EMAIL,"
        sql &= " DECODE(FW.FC_EMPLOYE(num_fonction, 'groupe', NULL, niec),'introuvable',FW.FC_EMPLOYE(num_fonction, 'initiale_ecole', NULL, niec),FW.FC_EMPLOYE(num_fonction, 'groupe', NULL, niec)) || '-' || FW.FC_EMPLOYE(num_fonction, 'desc_function', NULL, niec)  as memberOf,"
        sql &= " g.matricule_centrale "
        sql &= " from sap.sap_g g, sap.sap_p p"
        sql &= " where(g.matricule_centrale = p.matricule_centrale)"
        sql &= " and to_char(sysdate,'yyyymmdd') <= P.FIN_POSTE        "
        'Sql &= " and p.indicateur_conge = '0'"
        sql &= " AND g.MATRICULE_CENTRALE = '" & EmployeeId & "' "
        Return New OracleHelperFormPDF().getDataSet(sql)
    End Function


    Function GetIdUser(ByVal EmpId As String) As DataSet
        Dim sql As String = " SELECT PERSON_ID FROM TRILL02.PERSONS WHERE STAFF_NO = '" & EmpId & "' "
        Return New OracleHelperAE().getDataSet(sql)
    End Function


    Public Function ConfigurerUsager(ByVal u As Commun.Usager) As Commun.Usager

        'If IsNumeric(Trillium_ID) Then
        Dim userlogin As String = u.Login
        'Dim dal As DAL.DALUsager = New DAL.DALUsager()
        Dim lead_zeros As Boolean = False

        If u.ID.StartsWith("0") Then
            lead_zeros = True
        End If

        Dim donnees As Boolean = False
        Dim dsUsager As DataSet = New DataSet


        Dim memberId As DataSet = GetIdUser(u.ID)
        If Commun.WebTools.dsIsValid(memberId) Then
            u.ID = memberId.Tables(0).Rows(0)("PERSON_ID").ToString
        End If


        If ((u.ID.Length > 0) And IsNumeric(u.ID)) Then
            dsUsager = GetUsagerParMembreID(u.ID, lead_zeros)

            donnees = Commun.WebTools.dsIsValid(dsUsager)

        End If

        'affectation des roles.
        'a modifier par les droits de la table gestion_roles
        'affectation des roles.
        'a modifier par les droits de la table gestion_roles
        'affectation des roles.
        'a modifier par les droits de la table gestion_roles
        'affectation des roles.
        'a modifier par les droits de la table gestion_roles
        If donnees Then
            'C'est un enseignant.

            Dim p As Commun.Paliers = CType(IIf(dsUsager.Tables(0).Rows(0)("Palier").ToString = "E", Commun.Paliers.Elementaire, Commun.Paliers.Secondaire), Commun.Paliers)

            If u.Prenom = String.Empty Then
                u = New Commun.Usager("", "", dsUsager.Tables(0).Rows(0)("Id").ToString(), dsUsager.Tables(0).Rows(0)("m_nom").ToString(), dsUsager.Tables(0).Rows(0)("m_prenom").ToString(), p)
            End If
            'On s'assure que le userID provient de la base de données ici
            u.ID = dsUsager.Tables(0).Rows(0)("Id").ToString
            'u.PaliersEnseignes = New BL.BLClasses().GetPaliersEnseignesParEnseignant(u)

            For i As Integer = 0 To dsUsager.Tables(0).Rows.Count - 1
                Select Case dsUsager.Tables(0).Rows(i)("type_person").ToString()
                    Case "E", "A", "S"
                        u.Roles.Add(Commun.Securite.Roles.Enseignant)
                    Case "D"
                        u.Roles.Add(Commun.Securite.Roles.Direction_ecole)
                    Case "H"
                        u.Roles.Add(Commun.Securite.Roles.Administrateur)
                End Select
            Next
           
        Else
            dsUsager = GetUsagerStaffParMembreID(u.ID, lead_zeros)

            If Commun.WebTools.dsIsValid(dsUsager) Then

                'On s'assure que le userID provient de la base de données ici
                u.ID = dsUsager.Tables(0).Rows(0)("Id").ToString

                For i As Integer = 0 To dsUsager.Tables(0).Rows.Count - 1
                    Select Case dsUsager.Tables(0).Rows(i)("type_person").ToString()
                        Case "H" 'educateur specialisé EED
                            u.Roles.Add(Commun.Securite.Roles.Administrateur)
                    End Select
                Next
               
           
            End If
        End If
        Return u

    End Function

    Public Function GetUsagerStaffParMembreID(ByVal membreid As String, ByVal contains_leading_zeros As Boolean) As DataSet

        'Dim sql As String = "SELECT m.m_login as login, m_motdepasse as motdepasse, m.m_membre_id  as id, m_nom as nom, m_prenom as prenom, m.m_courriel as courriel, palier, type_person from gn_membres m, gn_vw_enseignants e where "
        Dim sql As String = "SELECT m_membre_id as id, m_nom, m_prenom, type_person from gn.gn_vw_enseignants_staff where "

        If (contains_leading_zeros) Or (membreid.Length = 0) Then
            sql &= " m_membre_id = '" & membreid & "' "
        Else
            sql &= " to_number(m_membre_id) = " & membreid
        End If

        'sql &= " and e.m_membre_id = m.m_membre_id "

        Dim helper As IDBHelper = HelperAEFactory.GetHelper()
        Return helper.getDataSet(sql)

    End Function

    Public Function GetUsagerParMembreID(ByVal membreid As String, ByVal contains_leading_zeros As Boolean) As DataSet

        'Dim sql As String = "SELECT m.m_login as login, m_motdepasse as motdepasse, m.m_membre_id  as id, m_nom as nom, m_prenom as prenom, m.m_courriel as courriel, palier, type_person from gn_membres m, gn_vw_enseignants e where "
        Dim sql As String = "SELECT m_membre_id as id, m_nom, m_prenom, m_courriel, palier, type_person, m_ecole from gn.gn_vw_enseignants where "

        If (contains_leading_zeros) Or (membreid.Length = 0) Then
            sql &= " m_membre_id = '" & membreid & "' "
        Else
            sql &= " to_number(m_membre_id) = " & membreid
        End If

        'sql &= " and e.m_membre_id = m.m_membre_id "

        Dim helper As IDBHelper = HelperAEFactory.GetHelper()
        Return helper.getDataSet(sql)

    End Function


    'Public Function GetGroupe(ByVal memberOf As String) As String
	Public Function GetGroupe(ByVal employeeID As String) As String


        'Dim sql As String = "SELECT GROUPE FROM GESTION_ROLES.M_GROUPE WHERE MEMBEROF = (select s__groupe || '-' || s__desc_fonction from FW.VW_DATA_EMPLOYE WHERE employe_id = '" & employeeID & "' and s__desc_fonction like '%DIRECTIONS-%' and rownum = 1) "

        'fcote oct 2019
        Dim sql As String = "SELECT GROUPE FROM GESTION_ROLES.M_GROUPE WHERE MEMBEROF in (select s__groupe || '-' || s__desc_fonction from FW.VW_DATA_EMPLOYE WHERE employe_id = '" & employeeID & "') "
        Dim _groupe As String = String.Empty
        Dim i As Integer = 0
        Dim ds As DataSet = New OracleHelperAE().getDataSet(sql)
        If dsIsValid(ds) Then
            For Each r As DataRow In ds.Tables(0).Rows
                If (ds.Tables(0).Rows(i)("GROUPE").ToString.ToUpper.Contains("DIRECTEUR")) Then
                    _groupe = ds.Tables(0).Rows(0)("GROUPE").ToString
                End If
                i = i + 1
            Next

            'Return ds.Tables(0).Rows(0)("GROUPE").ToString()
            Return _groupe
        Else
            Return String.Empty
        End If
    End Function

    Function dsIsValid(ByVal ds As DataSet) As Boolean
        If ds IsNot Nothing Then
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return True
                End If
            End If
        End If
        Return False
    End Function

    Public Sub New()

    End Sub
End Class









Public Class OracleHelperFormPDF
    Inherits OracleHelper
    Implements IDBHelper
    Public Sub New()
        Dim apr As AppSettingsReader = New AppSettingsReader

        Dim db As String = String.Empty

        Try
            'db = apr.GetValue("Connexion_sql", System.Type.GetType("System.String")).ToString()
            db = apr.GetValue("Connexion", System.Type.GetType("System.String")).ToString() 'switch Connexion_sql to Connexion
        Catch e As Exception
            Dim s As String = "La base de données à utiliser n'a pu être identifiée.  La section Connexion de web.config n'est peut-être pas existante ou valide."
            Throw New Exception(s, e)
        End Try

        Dim cs As ConnectionStringsSection = CType(ConfigurationManager.GetSection("connectionStrings"), ConnectionStringsSection)

        Try
            con_string = cs.ConnectionStrings(db).ConnectionString
        Catch ex As Exception
            Dim s As String = "Il est impossible de déterminer les paramètres de connexion à l'aide de l'élément Connexion fourni dans le fichier web.config (" & IIf(db.Length > 0, db, "vide").ToString() & ")."
            Throw New Exception(s)
        End Try
    End Sub
End Class