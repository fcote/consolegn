<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using infoGrids in tooltips to show styled element details.

		' Setup the chart.
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.ShadingEffectMode = ShadingEffectMode.Three
		Chart.XAxis.SpacingPercentage = 20
		Chart.YAxis.FormatString = "Currency"

		' Style the titleBox and legend.
		Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
		Chart.TitleBox.Padding = 6
		Chart.TitleBox.Background.Color = Color.Chocolate ' AntiqueWhite;
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Background1
		Chart.TitleBox.Label.Color = Color.White
		Chart.LegendBox.DefaultEntry.LabelStyle.Color = Color.White
		Chart.LegendBox.DefaultEntry.LabelStyle.Font = New Font("Arial", 9, FontStyle.Bold)
		Chart.LegendBox.Template = "%Name%Value"

		' This defines the tooltip information details.
		Chart.DefaultElement.ToolTip = "<block fStyle='bold' hAlign='right'>Name: <block hAlign='left'> %Name<row><block fStyle='bold' hAlign='right'>Employee No: <block hAlign='left'> %EmployeeID<row><block fStyle='bold' hAlign='right'>Department: <block hAlign='left'>%Department<row><block fStyle='bold' hAlign='right'>Email: <block hAlign='left'>%email<row><block fStyle='bold' hAlign='right'>Phone: <block hAlign='left'>%PhoneNumber"

		Dim mySC As SeriesCollection = getLiveData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub



	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT * FROM Employees"

		'Set predefined data fields and some custom fields as attributes.
		de.DataFields = "xAxis=name,yAxis=salary=Total:,id=EmployeeID,department=Department,email,phone=PhoneNumber"
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
