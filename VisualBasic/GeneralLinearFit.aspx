<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Forecasting Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of GeneralLinear Forecasting engine in order to
   ' find the function of best fit from three functions spaces. The data used for which
   ' the functions are fit is a set of data which represents a FX exchange rate over a given
   ' period of time. We index the period by the number of days after the inital date and the
   ' three function spaces are the spaces spanned by the following basis elements:
   '
   ' 1) {(x - 2*x^2)
   ' 2) {(x - 2*x^2) (x^3 + 1)}
   ' 3) {(x - 2*x^2) (x^3 + 1), (10*cos(0.003*x))}
   '
   ' The Forecast Chart
   ForecastChart.Title = "Exchange"
   ForecastChart.TempDirectory = "temp"
   ForecastChart.Debug = True
   ForecastChart.Size = "800x800"
   ForecastChart.LegendBox.Template = "%icon %name"
   ForecastChart.YAxis.ScaleRange.ValueLow = 150
   
   ' The Forecast data
   Dim de As New DataEngine()
   de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   de.DateGrouping = TimeInterval.Days
   de.StartDate = New DateTime(1973, 3, 10, 0, 0, 0)
   de.EndDate = New DateTime(1984, 12, 10, 23, 59, 59)
   de.SqlStatement = "SELECT ID, Value AS q FROM Exchange WHERE Data >= #STARTDATE# AND Data <= #ENDDATE# ORDER BY Data "
   
   'Add a series
   Dim scForecast As SeriesCollection = de.GetSeries()
   ForecastChart.SeriesCollection.Add(scForecast)
   
   scForecast(0).Name = "Exchange"
   scForecast(0).Type = SeriesType.Spline
   
   
   ' Takes off the marker off the line and spline series.
   ForecastChart.DefaultSeries.DefaultElement.Marker = New ElementMarker(ElementMarkerType.None)
   ForecastChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   
   ' Generate a series of standard deviation for the given points
   Dim deviation As New Series()
   Dim i As Integer
   For i = 0 To (scForecast(0).Elements.Count) - 1
      Dim el As New Element()
      el.XValue = scForecast(0).Elements(i).XValue
      el.YValue = 1E-10
      deviation.Elements.Add(el)
   Next i
   
   ' Note that this line is necessary in order to clear the function basis set by previous
   ' example.
   '
   ForecastEngine.Options.Reset()
   
   ' Set the first model function
   '
   ' The first basis element: (x - 2*x^2)
   ForecastEngine.Options.AddSumOfPowerTerms(New Double() {1, - 2}, New Double() {1, 2})
   ' The second basis element: (1)
   ForecastEngine.Options.AddSumOfPowerTerms(New Double() {1}, New Double() {0})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel1 As New Series()
   generalLinearModel1 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel1.Name = "Model1"
   generalLinearModel1.Type = SeriesType.Spline
   'The next three lines display no to the chart the given function used
   ' to fit the curve
   generalLinearModel1.Elements(1500).SmartLabel.Text = "Function 1: %Function"
   generalLinearModel1.Elements(1500).ShowValue = True
   ForecastChart.SeriesCollection.Add(generalLinearModel1)
   
   ' Set the second model function ; we add x^3 function to the basis functions
   ForecastEngine.Options.AddSumOfPowerTerms(New Double() {1, 2}, New Double() {3, 0})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel2 As New Series()
   generalLinearModel2 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel2.Name = "Model2"
   generalLinearModel2.DefaultElement.Color = Color.FromArgb(255, 99, 49)
   generalLinearModel2.Type = SeriesType.Spline
   'The next three lines display no to the chart the given function used
   ' to fit the curve
   generalLinearModel2.Elements(600).SmartLabel.Text = "Function 2: %Function"
   generalLinearModel2.Elements(600).SmartLabel.Color = Color.FromArgb(255, 99, 49)
   generalLinearModel2.Elements(600).ShowValue = True
   ForecastChart.SeriesCollection.Add(generalLinearModel2)
   
   ' Set the third model function ; we add 10*cos(0.003*x)function to the basis functions
   ForecastEngine.Options.AddCosineSum(New Double() {10}, New Double() {0.003}, New Double() {0}, New Double() {1})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel3 As New Series()
   generalLinearModel3 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel3.Name = "Model3"
   generalLinearModel3.DefaultElement.Color = Color.FromArgb(0, 0, 0)
   generalLinearModel3.Type = SeriesType.Spline
   'The next three lines display no to the chart the given function used
   ' to fit the curve
   generalLinearModel3.Elements(1600).SmartLabel.Text = "Function 3: %Function"
   ' The label will have the same color as the element which he represents
   generalLinearModel3.Elements(1600).SmartLabel.Color = Color.FromArgb(0, 0, 0)
   generalLinearModel3.Elements(1600).ShowValue = True
   ForecastChart.SeriesCollection.Add(generalLinearModel3)
End Sub 'Page_Load 
		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="ForecastChart" runat="server"/>
			
			
		</div>
	</body>
</html>
