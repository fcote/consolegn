Imports System.Data
Imports System.Configuration
'Imports system.data.oracleclient
Imports System.Data.OracleClient
'Imports System.Data.SqlClient
Imports System.Threading


Public Interface IDBHelper
    Function getDataSet(ByVal sql As String) As DataSet
    Function getDataReader(ByVal sql As String) As Data.IDataReader
    Function getDataReader(ByVal sql As String, ByVal behavior As Data.CommandBehavior) As Data.IDataReader
    Function getConnection() As IDbConnection
    Function getErreur() As String
End Interface

Public Module HelperAEFactory

    'Ce module instancie des helpers pour la 
    'bonne connection.
    'Si on supportait diff�rentes db on pourrait
    'aussi les plugger ici.

    Public Function GetHelper() As IDBHelper
        Return New OracleHelperAE()
    End Function

End Module

'Public Module HelperPPFactory
'    Public Function GetHelper() As IDBHelper
'        'Return New OracleHelperPP()
'        Return New OracleHelperAE()
'    End Function
'End Module

'Public Class SQLHelper
'    Implements IDBHelper

'    Protected con_string As String = String.Empty
'    Protected erreur As String = String.Empty
'    Const MAX_ESSAIS_CONNECTION As Integer = 5
'    Public Function getDataSet(ByVal sql As String) As System.Data.DataSet Implements IDBHelper.getDataSet
'        Return _getDataSet(sql)
'    End Function
'    Private Function _getDataSet(ByVal sql As String) As System.Data.DataSet
'        Dim con As IDbConnection = _getConnection()

'        Dim cmd As IDbCommand = con.CreateCommand()

'        cmd.CommandType = Data.CommandType.Text
'        cmd.CommandText = sql

'        Dim adapter As IDbDataAdapter = _createDataAdapter()

'        adapter.SelectCommand = cmd

'        Dim ctr_essais As Integer
'        Dim reessayer As Boolean = True
'        Dim ds As DataSet = New DataSet()
'        Dim n_rows As Integer = 0
'        'Le Catch va sortir de la loop si besoin est
'        'Cette loop va attraper les erreurs de connection.
'        'Dim ge As GestionnaireDErreurs = New GestionnaireDErreurs()

'        While (reessayer)
'            Try
'                n_rows = adapter.Fill(ds)
'                reessayer = False
'            Catch ex As Exception
'                If ctr_essais > MAX_ESSAIS_CONNECTION Then
'                    'erreur = "C-002 - Maximum d'essais atteint. Le maximum d'essais de connection � la base de donn�es (" & MAX_ESSAIS_CONNECTION & ") a �t� atteint." & vbCrLf & "SQL: " & sql
'                    erreur = "C-002 - Maximum d'essais atteint. Le maximum d'essais de connection � la base de donn�es (" & MAX_ESSAIS_CONNECTION & ") a �t� atteint. SQL: " & sql
'                    reessayer = False
'                    Throw ex
'                Else
'                    Dim ch As CommonHelper = New CommonHelper()

'                    If ch.TestConnection(con) Then
'                        erreur = "C-003 - Avertissement: La connexion a �chou�. " & ex.Message
'                        ctr_essais += 1
'                    Else
'                        reessayer = False
'                        Throw ex
'                    End If
'                End If
'            End Try
'        End While

'        cmd.Dispose()

'        con.Dispose()

'        Return ds

'    End Function
'    Public Function getConnection() As System.Data.IDbConnection Implements IDBHelper.getConnection
'        Return _getConnection()
'    End Function
'    Private Function _getConnection() As IDbConnection
'        Dim maConnexionSQLsrv As System.Data.SqlClient.SqlConnection
'        maConnexionSQLsrv = New System.Data.SqlClient.SqlConnection(con_string)
'        Return maConnexionSQLsrv
'    End Function
'    Public Function getDataReader(ByVal sql As String) As IDataReader Implements IDBHelper.getDataReader
'        Return _getDataReader(sql)
'    End Function
'    Public Function getDataReader(ByVal sql As String, ByVal behavior As System.Data.CommandBehavior) As System.Data.IDataReader Implements IDBHelper.getDataReader
'        Return _getDataReader(sql, behavior)
'    End Function

'    Private Function _getDataReader(ByVal sql As String) As SqlDataReader
'        Return _getDataReader(sql, CommandBehavior.CloseConnection)
'    End Function

'    Private Function _getDataReader(ByVal sql As String, ByVal behavior As System.Data.CommandBehavior) As SqlDataReader

'        Dim con As SqlConnection = New SqlConnection()

'        con.ConnectionString = con_string
'        Dim cmd As SqlCommand = con.CreateCommand()
'        con.Open()

'        cmd.CommandType = Data.CommandType.Text
'        cmd.CommandText = sql

'        Dim dr As SqlDataReader = cmd.ExecuteReader(behavior)

'        Return dr

'    End Function
'    Private Function _createDataAdapter() As IDbDataAdapter
'        Return New SqlDataAdapter()
'    End Function
'    Public Function GetErreur() As String Implements IDBHelper.getErreur
'        Return erreur
'    End Function
'End Class


Public Class OracleHelper
    Implements IDBHelper

    Protected con_string As String = String.Empty
    Protected erreur As String = String.Empty
    Const MAX_ESSAIS_CONNECTION As Integer = 5

    Public Function GetErreur() As String Implements IDBHelper.getErreur
        Return erreur
    End Function

    Public Function getConnection() As System.Data.IDbConnection Implements IDBHelper.getConnection
        Return _getConnection()
    End Function

    Public Function getDataSet(ByVal sql As String) As System.Data.DataSet Implements IDBHelper.getDataSet
        Return _getDataSet(sql)
    End Function

    Private Function _getDataReader(ByVal sql As String) As OracleDataReader
        Return _getDataReader(sql, CommandBehavior.CloseConnection)
    End Function

    Private Function _getDataReader(ByVal sql As String, ByVal behavior As System.Data.CommandBehavior) As OracleDataReader

        Dim con As OracleConnection = New OracleConnection()

        con.ConnectionString = con_string
        Dim cmd As OracleCommand = con.CreateCommand()
        con.Open()

        cmd.CommandType = Data.CommandType.Text
        cmd.CommandText = sql

        Dim dr As OracleDataReader = cmd.ExecuteReader(behavior)

        Return dr

    End Function

    Public Function getDataReader(ByVal sql As String) As IDataReader Implements IDBHelper.getDataReader
        Return _getDataReader(sql)
    End Function

    Public Function getDataReader(ByVal sql As String, ByVal behavior As System.Data.CommandBehavior) As System.Data.IDataReader Implements IDBHelper.getDataReader
        Return _getDataReader(sql, behavior)
    End Function


    'Public Class TimeoutWrapper

    '    Public Shared Sub Invoke(ByVal timeout As TimeSpan, ByVal action As Action)
    '        Invoke(timeout, action, Nothing)
    '    End Sub

    '    Public Shared Sub Invoke(ByVal timeout As TimeSpan, ByVal action As Action, ByVal abort As Action)

    '        Dim threadToKill As Thread = Nothing
    '        Dim wrappedAction As Action = Sub()
    '                                          threadToKill = Thread.CurrentThread
    '                                          action()
    '                                      End Sub
    '        Dim result As IAsyncResult = wrappedAction.BeginInvoke(Nothing, Nothing)
    '        If (result.AsyncWaitHandle.WaitOne(timeout, True)) Then
    '            wrappedAction.EndInvoke(result)
    '        Else
    '            If Not threadToKill Is Nothing Then
    '                Try
    '                    threadToKill.Abort()
    '                Catch

    '                End Try
    '            End If
    '            If Not abort Is Nothing Then
    '                abort()
    '            End If
    '            Throw New TimeoutException()
    '        End If
    '    End Sub
    'End Class
    Public Function fillDS(ByVal adapter As IDbDataAdapter, ByVal ds As DataSet) As Integer
        Return adapter.Fill(ds)
    End Function


    Private Function _getDataSet(ByVal sql As String) As System.Data.DataSet
        Dim con As IDbConnection = _getConnection()
        Dim cmd As IDbCommand = con.CreateCommand()

        cmd.CommandType = Data.CommandType.Text
        cmd.CommandText = sql
        Dim adapter As IDbDataAdapter = _createDataAdapter()
        adapter.SelectCommand = cmd


        'Dim ctr_essais As Integer
        Dim reessayer As Boolean = True
        Dim ds As DataSet = New DataSet()
        Dim n_rows As Integer = 0


        'Modification 22/08/2012 pour �viter d'�tre bloqu� dans le try lors du fill
        'A TESTER...
        'Try
        '    Dim endAt As DateTime = DateTime.Now.AddMinutes(1)
        '    Dim timeout As TimeSpan = New TimeSpan(0, 0, 0, 5)
        '    While DateTime.Now < endAt
        '        Try
        '            TimeoutWrapper.Invoke(timeout, Function() n_rows = fillDS(adapter, ds))
        '            Exit Try
        '        Catch ex As TimeoutException
        '            '{ /* Do something */ }
        '        End Try
        '    End While

        'Catch ex As Exception
        '    cmd.Dispose()
        '    con.Dispose()
        '    Return ds
        'End Try

        Try
            n_rows = adapter.Fill(ds)
            reessayer = False
        Catch ex As Exception
            cmd.Dispose()
            con.Dispose()
            Return ds
        End Try

        cmd.Dispose()
        con.Dispose()

        Return ds

    End Function

    Private Function _getConnection() As IDbConnection
        Dim con As OracleConnection = New OracleConnection()

        con.ConnectionString = con_string

        Return (con)
    End Function

    Private Function _createDataAdapter() As IDbDataAdapter
        Return New OracleDataAdapter()
    End Function

End Class

Public Class OracleHelperAE
    Inherits OracleHelper
    Implements IDBHelper

    Public Sub New()

        Dim apr As AppSettingsReader = New AppSettingsReader

        Dim db As String = String.Empty

        Try
            db = apr.GetValue("Connexion", System.Type.GetType("System.String")).ToString()
        Catch e As Exception
            Dim s As String = "La base de donn�es � utiliser n'a pu �tre identifi�e.  La section Connexion de web.config n'est peut-�tre pas existante ou valide."
            Throw New Exception(s, e)
        End Try

        Dim cs As ConnectionStringsSection = CType(ConfigurationManager.GetSection("connectionStrings"), ConnectionStringsSection)

        Try
            con_string = cs.ConnectionStrings(db).ConnectionString
        Catch ex As Exception
            Dim s As String = "Il est impossible de d�terminer les param�tres de connexion � l'aide de l'�l�ment Connexion fourni dans le fichier web.config (" & IIf(db.Length > 0, db, "vide").ToString() & ")."
            Throw New Exception(s)
        End Try

    End Sub

End Class

'Public Class OracleHelperPP
'    Inherits OracleHelper
'    Implements IDBHelper

'    Public Sub New()

'        Dim apr As AppSettingsReader = New AppSettingsReader

'        Dim db As String = String.Empty

'        Try
'            db = apr.GetValue("ConnexionPP", System.Type.GetType("System.String")).ToString()
'        Catch e As Exception
'            Dim s As String = "La base de donn�es � utiliser n'a pu �tre identifi�e.  La section Connexion de web.config n'est peut-�tre pas existante ou valide."
'            Throw New Exception(s, e)
'        End Try

'        Dim cs As ConnectionStringsSection = CType(ConfigurationManager.GetSection("connectionStrings"), ConnectionStringsSection)

'        Try
'            con_string = cs.ConnectionStrings(db).ConnectionString
'        Catch ex As Exception
'            Dim s As String = "Il est impossible de d�terminer les param�tres de connexion � l'aide de l'�l�ment Connexion fourni dans le fichier web.config (" & IIf(db.Length > 0, db, "vide").ToString() & ")."
'            Throw New Exception(s)
'        End Try

'    End Sub

'End Class

Public Class OracleHelperFormPDF
    Inherits OracleHelper
    Implements IDBHelper
    Public Sub New()
        Dim apr As AppSettingsReader = New AppSettingsReader

        Dim db As String = String.Empty

        Try
            'db = apr.GetValue("Connexion_sql", System.Type.GetType("System.String")).ToString()
            db = apr.GetValue("Connexion", System.Type.GetType("System.String")).ToString() 'switch Connexion_sql to Connexion
        Catch e As Exception
            Dim s As String = "La base de donn�es � utiliser n'a pu �tre identifi�e.  La section Connexion de web.config n'est peut-�tre pas existante ou valide."
            Throw New Exception(s, e)
        End Try

        Dim cs As ConnectionStringsSection = CType(ConfigurationManager.GetSection("connectionStrings"), ConnectionStringsSection)

        Try
            con_string = cs.ConnectionStrings(db).ConnectionString
        Catch ex As Exception
            Dim s As String = "Il est impossible de d�terminer les param�tres de connexion � l'aide de l'�l�ment Connexion fourni dans le fichier web.config (" & IIf(db.Length > 0, db, "vide").ToString() & ")."
            Throw New Exception(s)
        End Try
    End Sub
End Class


'Public Class SQLHelperEED
'    Inherits SQLHelper
'    Implements IDBHelper

'    Public Sub New()

'        Dim apr As AppSettingsReader = New AppSettingsReader

'        Dim db As String = String.Empty

'        Try
'            db = apr.GetValue("Connexion_sql", System.Type.GetType("System.String")).ToString()
'        Catch e As Exception
'            Dim s As String = "La base de donn�es � utiliser n'a pu �tre identifi�e.  La section Connexion de web.config n'est peut-�tre pas existante ou valide."
'            Throw New Exception(s, e)
'        End Try

'        Dim cs As ConnectionStringsSection = CType(ConfigurationManager.GetSection("connectionStrings"), ConnectionStringsSection)

'        Try
'            con_string = cs.ConnectionStrings(db).ConnectionString
'        Catch ex As Exception
'            Dim s As String = "Il est impossible de d�terminer les param�tres de connexion � l'aide de l'�l�ment Connexion fourni dans le fichier web.config (" & IIf(db.Length > 0, db, "vide").ToString() & ")."
'            Throw New Exception(s)
'        End Try

'    End Sub

'End Class

'Public Class SQLHelperFORMPDF
'    Inherits SQLHelper
'    Implements IDBHelper

'    Public Sub New()

'        Dim apr As AppSettingsReader = New AppSettingsReader

'        Dim db As String = String.Empty

'        Try
'            db = apr.GetValue("Connexion_sql", System.Type.GetType("System.String")).ToString()
'        Catch e As Exception
'            Dim s As String = "La base de donn�es � utiliser n'a pu �tre identifi�e.  La section Connexion de web.config n'est peut-�tre pas existante ou valide."
'            Throw New Exception(s, e)
'        End Try

'        Dim cs As ConnectionStringsSection = CType(ConfigurationManager.GetSection("connectionStrings"), ConnectionStringsSection)

'        Try
'            con_string = cs.ConnectionStrings(db).ConnectionString
'        Catch ex As Exception
'            Dim s As String = "Il est impossible de d�terminer les param�tres de connexion � l'aide de l'�l�ment Connexion fourni dans le fichier web.config (" & IIf(db.Length > 0, db, "vide").ToString() & ")."
'            Throw New Exception(s)
'        End Try

'    End Sub

'End Class


Public Class CommonHelper

    Public erreur As String

    Public Function GetErreur() As String
        Return erreur
    End Function

    Public Function TestConnection(ByVal con As IDbConnection) As Boolean

        Dim est_ouverte As Boolean = (con.State = ConnectionState.Open)

        Try
            If Not est_ouverte Then
                con.Open()
                Return True
            End If
        Catch ex As Exception
            'Dim ge As GestionnaireDErreurs = New GestionnaireDErreurs()
            'ge.EcrireTracePile = False
            'ge.EcrireContexte = False
            erreur = "C-001 - �chec de connection. Impossible d'ouvrir la connection" & ex.Message
            Return False
        Finally
            If Not (con Is Nothing) And (con.State <> ConnectionState.Closed) And Not est_ouverte Then
                con.Close()
            End If

        End Try
        Return False
    End Function

End Class
