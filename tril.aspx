<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="tril.aspx.vb" Inherits="tril" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <asp:Label ID="Label1" runat="server" Font-Names="Berlin Sans FB Demi" Font-Size="XX-Large"
            Text="Trillium: renseignements enseignants" Width="100%"></asp:Label><br />
        <br />
        &nbsp;<telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource1" GridLines="None" AllowFilteringByColumn="True" AllowSorting="True" PageSize="15" ShowGroupPanel="True" Skin="Office2007">
            <MasterTableView DataSourceID="SqlDataSource1" GroupsDefaultExpanded="False" 
                AutoGenerateColumns="False">
                <Columns>
                    <telerik:GridBoundColumn DataField="Identifiant" HeaderText="Identifiant" 
                        SortExpression="Identifiant" UniqueName="Identifiant">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Pr�nom" HeaderText="Pr�nom" 
                        SortExpression="Pr�nom" UniqueName="Pr�nom">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Nom" HeaderText="Nom" SortExpression="Nom" 
                        UniqueName="Nom">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="N� employ�" HeaderText="N� employ�" 
                        SortExpression="N� employ�" UniqueName="N� employ�">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PERSON_ID" HeaderText="PERSON_ID" 
                        SortExpression="PERSON_ID" UniqueName="PERSON_ID">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="courriel" HeaderText="courriel" 
                        SortExpression="courriel" UniqueName="courriel">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Poste" HeaderText="Poste" 
                        SortExpression="Poste" UniqueName="Poste">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Affectation" DataType="System.Decimal" 
                        HeaderText="Affectation" SortExpression="Affectation" UniqueName="Affectation">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="START_DATE" DataType="System.DateTime" 
                        HeaderText="START_DATE" SortExpression="START_DATE" UniqueName="START_DATE">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="END_DATE" DataType="System.DateTime" 
                        HeaderText="END_DATE" SortExpression="END_DATE" UniqueName="END_DATE">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ecole" HeaderText="Ecole" 
                        SortExpression="Ecole" UniqueName="Ecole">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SCHOOL_CODE" HeaderText="SCHOOL_CODE" 
                        SortExpression="SCHOOL_CODE" UniqueName="SCHOOL_CODE">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings AllowDragToGroup="True">
            </ClientSettings>
        </telerik:RadGrid>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CS_Trillium %>"
            ProviderName="<%$ ConnectionStrings:CS_Trillium.ProviderName %>" SelectCommand="select up.user_profile &quot;Identifiant&quot;, p.LEGAL_FIRST_NAME &quot;Pr�nom&quot;, p.LEGAL_SURNAME &quot;Nom&quot;, 
p.staff_no &quot;N� employ�&quot;, p.person_id ,-- pt.email_account &quot;courriel&quot;,
FC_ENSEIGNANT_CONTACT(p.person_id,'Internet', '') &quot;courriel&quot;,
ss.staff_type_name &quot;Poste&quot;, ss.fte &quot;Affectation&quot;, ss.start_date, ss.end_date , s.school_name &quot;Ecole&quot;, ss.school_code
from persons p 
left outer JOIN user_profiles up 
    ON p.person_id = up.person_id
left outer join school_staff ss
    on p.person_id = ss.person_id 
left outer join schools s
    on s.school_code = ss.school_code   
where school_year = fc_val_parametres('ANNEESCOLAIRE')
order by p.LEGAL_FIRST_NAME,p.person_id ">
        </asp:SqlDataSource>

 </asp:content>
