<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">
 'This sample demonstrates how to get the metafile of the chart legendbox.
Dim myChart As New dotnetCHARTING.Chart()
Dim filePathChart As String = ""
Dim filePathLegend As String = ""


Sub Page_Load(sender As [Object], e As EventArgs)
   
   'Add data to the chart
   CreateChart()
   
   Dim legendMetafile As System.Drawing.Imaging.Metafile = myChart.GetLegendMetafile()
   
        'Save the metafile
   filePathLegend = myChart.FileManager.SaveImage(legendMetafile)
   
        Dim chartMetafile As System.Drawing.Imaging.Metafile = myChart.GetChartMetafile()
   
   'Save the metafile
        filePathChart = myChart.FileManager.SaveImage(chartMetafile)
   
   'Check the temp directory for writeEmbed.js and creates it if doesn't exist.
   CreateFileJS()
End Sub 'Page_Load
 
Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
            s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
                e.Name = "Element " + b.ToString()
         e.YValue = 25 + myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData

Sub CreateChart()
        myChart.ImageFormat = ImageFormat.Swf
   myChart.Size = "600x480"
   myChart.TempDirectory = "temp"
   myChart.DisableBrowserCache = False
   myChart.LegendBox.Position = LegendBoxPosition.None
   
   
   ' Set the title.
   myChart.Title = "My Chart"
   
   ' Set the x axis label
   myChart.ChartArea.XAxis.Label.Text = "X Axis Label"
   
   ' Set the y axis label
   myChart.ChartArea.YAxis.Label.Text = "Y Axis Label"
   
   ' Set the bar shading effect
   myChart.ShadingEffect = True
   
   ' Add the random data.
   myChart.SeriesCollection.Add(getRandomData())
End Sub 'CreateChart
 
    Sub CreateFileJS()
        Dim tempPath As String = MapPath("temp")
        Dim di As New DirectoryInfo(tempPath)
        Dim fiArr As FileInfo() = di.GetFiles("writeEmbed.js")
        If fiArr.Length < 1 Then
            Dim fout As New FileStream(tempPath + "\writeEmbed.js", FileMode.Create, FileAccess.Write)
            Dim data As String = "function WriteEmbed(stringValue){document.write(stringValue);}"
            Dim adData(data.Length) As Byte
            adData = ASCIIEncoding.ASCII.GetBytes(data)
            fout.Write(adData, 0, adData.Length)
            fout.Close()
        End If
    End Sub 'CreateFileJS

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Legend Metafile Sample</title>
<script src="temp/writeEmbed.js" type="text/javascript"></script>
</head>
<body>
<font size="2" face="Arial">This chart image can be placed freely in your page 
and treated as a regular image file:<br/>
<%
    Response.Write("<script type=""text/javascript"">WriteEmbed('<embed height=""480"" width=""600"" src=""")
    Response.Write(filePathChart.Replace("\", "/"))
    Response.Write(""" type=""application/swf""/>');")
    Response.Write("</script>")
%><br/><br/>
The legend below is a separate image file which can be independently placed in 
the page.<br/><br/></font>
<table border="0" style="border-collapse: collapse" id="table1" cellpadding="4">
	<tr>
		<td><%
		        Response.Write("<script type=""text/javascript"">WriteEmbed('<embed height=""70"" width=""100"" src=""")
		        Response.Write(filePathLegend.Replace("\", "/"))
		        Response.Write(""" type=""application/swf""/>');")
		        Response.Write("</script>")
%>
</td>
		<td><font size="2" face="Arial">This feature is useful for custom layouts but it 
is also a powerful addition for multiple chart alignment as chart width is not 
affected by differing legend size requirements on the chart surface.&nbsp; In 
		this sample the legend is added to a table so this text can adjoin it.</font></td>
	</tr>
</table>
<p>&nbsp;</p>
</body>
</html>