<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates how the chart navigation can be controlled with buttons within the chart.

		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() {Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.DefaultSeries.Type = SeriesType.AreaLine
		Chart.LegendBox.Position = LegendBoxPosition.ChartArea

		' Enable the Navigator.
		Chart.Navigator.Enabled = True
		Chart.Navigator.NavigationBar.Visible = False
		Chart.XAxis.Viewport.ScaleRange = New ScaleRange(New DateTime(2009, 2, 1), New DateTime(2009, 2, 5))

		' The control ID is specified so it can be referenced in javascript.
		Chart.Navigator.ControlID = "slPlugin"

		' Setup annotations to control the chart through JavaScript methods.
		Dim an As Annotation = New Annotation("Max Slow")
		an.Background.Color = Color.LightBlue
		an.DefaultCorner = BoxCorner.Round
		an.Padding = 5
		an.Position = New Point(80, 220)
		an.Hotspot.URL = "Javascript:maxslow();"
		an.Hotspot.ToolTip = "Maximize slowly"
		Chart.Annotations.Add(an)

		Dim an2 As Annotation = New Annotation("<")
		an2.Background.Color = Color.LightBlue
		an2.DefaultCorner = BoxCorner.Round
		an2.Padding = 5
		an2.Position = New Point(40, 220)
		an2.Hotspot.URL = "Javascript:goback();"
		an2.Hotspot.ToolTip = "Page Left"
		Chart.Annotations.Add(an2)

		Dim an3 As Annotation = New Annotation(">")
		an3.Background.Color = Color.LightBlue
		an3.DefaultCorner = BoxCorner.Round
		an3.Padding = 5
		an3.Position = New Point(60, 220)
		an3.Hotspot.URL = "Javascript:goforward();"
		an3.Hotspot.ToolTip = "Page Right"
		Chart.Annotations.Add(an3)


		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2009, 1, 1)

		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 54
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)

		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<script type="text/javascript">

	var chartID = "slPlugin";

	function maxslow() {
		var control = document.getElementById(chartID);
		control.Content.Chart.NavigateToVisibleRangePerc(0, 1, 2000);
	}

	function goback() {
		var control = document.getElementById(chartID);
		control.Content.Chart.PageLeft();
	}

	function goforward() {
		var control = document.getElementById(chartID);
		control.Content.Chart.PageRight();
	}

</script>

<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
