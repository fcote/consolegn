<%@ Page Language="VB" debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs )

	'set global properties
    Chart.Title="Item sales"
    Chart.ChartArea.XAxis.Label.Text="Days"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.DateGrouping = TimeInterval.Days
    
	Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
	Chart.DefaultSeries.StartDate= New System.DateTime(2002,3,1,0,0,0)
    Chart.DefaultSeries.EndDate = New System.DateTime(2002,3,20,23,59,59)
    Chart.OverlapFooter=false
    
    'Add a series
    Chart.Series.Name="Items"
    Chart.Series.SqlStatement= "SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
    Chart.SeriesCollection.Add()
    
   
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Date Grouping By Days Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
