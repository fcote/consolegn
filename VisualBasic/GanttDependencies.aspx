<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using Gantt dependencies.

		Chart.TempDirectory = "temp"
		Chart.Debug = True

		Chart.Type = ChartType.ComboHorizontal
		Chart.Size = "600x300"
		Chart.Title = "Project Alpha"
		Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
		Chart.YAxis.DefaultTick.Label.Font = New Font("Arial", 12)

		' The Series.Line object defines gantt dependency line styles.
		Chart.DefaultSeries.Line.Color = Color.Black
		Chart.DefaultSeries.Line.EndCap = LineCap.ArrowAnchor
		Chart.DefaultSeries.Line.Width = 3
		Chart.DefaultSeries.Line.AnchorCapScale = 2

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection

		Dim e1 As Element = New Element("Preparations")
		e1.YDateTimeStart = New DateTime(2008, 2, 5)
		e1.YDateTime = e1.YDateTimeStart.AddDays(10)
		e1.Complete = 50

		Dim e2 As Element = New Element("Execution")
		e2.YDateTimeStart = New DateTime(2008, 2, 15)
		e2.YDateTime = e2.YDateTimeStart.AddDays(5)

		Dim e3 As Element = New Element("Cleanup")
		e3.YDateTimeStart = New DateTime(2008, 2, 19)
		e3.YDateTime = e3.YDateTimeStart.AddDays(5)

		Dim e4 As Element = New Element("Presentation")
		e4.YDateTimeStart = New DateTime(2008, 3, 1)
		e4.YDateTime = e4.YDateTimeStart.AddDays(5)

		' Gantt Dependencies are defined with the element's Parent property similar to Organizational charts.
		e2.Parent = e1
		e3.Parent = e2
		e4.Parent = e3

		Return New SeriesCollection(New Series("", e1, e2, e3,e4))


	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
