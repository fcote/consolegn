<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sms.aspx.vb" Inherits="sms" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Le Gestionnaire de Notes</title>
</head>
   <body >
      <form id="Form1" runat="server">
          <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
          </telerik:RadScriptManager>
	  <div>
	  Salut!
          <telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource1" GridLines="None" ShowGroupPanel="False">
           </telerik:RadGrid><asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SMS_00AConnectionString %>"
              SelectCommand="SELECT distinct AD_Site_Name0, SYS.Netbios_Name0, SYS.User_Name0, CSYS.Manufacturer0, CSYS.Model0, pcbios.Description0, pcbios.SerialNumber0, pcbios.Version0&#13;&#10;FROM v_R_System SYS&#13;&#10;RIGHT JOIN  v_GS_COMPUTER_SYSTEM CSYS on SYS.ResourceID = CSYS.ResourceID&#13;&#10;right join v_GS_PC_BIOS pcbios on CSYS.ResourceID =  pcbios.ResourceID&#13;&#10;group by AD_Site_Name0, SYS.Netbios_Name0, SYS.User_Name0, CSYS.Manufacturer0, CSYS.Model0, pcbios.Description0, pcbios.SerialNumber0, pcbios.Version0&#13;&#10;">
          </asp:SqlDataSource>
	  </div>
      </form>
   </body>
</HTML>
