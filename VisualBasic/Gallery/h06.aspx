<%@ Page Language="VB" Debug="true" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   ' Set the chart type
   Chart.Type = ChartType.Gauges
   ' Set the size
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   ' Set the temp directory
   Chart.TempDirectory = "temp"
   ' Debug mode. ( Will show generated errors if any )
   Chart.Debug = True
   Chart.Title = "My Chart"
   
   ' Set a default gauge face background color.
   Chart.DefaultSeries.Background.Color = Color.White
   
   
   Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   
   
   'Chart.DefaultSeries.Type = SeriesType.Line;
   Chart.DefaultElement.ErrorOffset = 25
   
   
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 1
      Dim s As New Series()
      s.Name = "Series " & a
      Dim b As Integer
      For b = 0 To 0
         Dim e As New Element()
         e.Name = "Element " & b
         'e.XValue = (a+1)*4+b;
         e.YValue = 20 + myR.Next(50) +(2 - a) * 80
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   'SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
   'SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
