<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates how a detailed Navigator chart can be controled by hovering over microCharts.

		chart.Size = "600x350"

		chart.TempDirectory = "temp"
		chart.Debug = True
		chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		chart.DefaultSeries.Type = SeriesType.Line
		chart.DefaultElement.Marker.Visible = False
		chart.LegendBox.Position = LegendBoxPosition.ChartArea
		chart.YAxis.Scale = Scale.Range
		chart.Navigator.ControlID = "slPlugin"
		chart.Navigator.Enabled = True
		chart.DefaultSeries.LegendEntry.Value = ""

		'Disable navigator navigation.
		chart.Navigator.NavigationBar.Visible = False
		chart.Navigator.EnableXScrollbar = False
		chart.Navigator.NavigationBar.Visible = False
		chart.Navigator.PreviewAreaNavigationOptons = 0
		chart.ChartArea.NavigatorOptions = 0

		Dim mySC As SeriesCollection = getSalesVsCostDaily()

		' Add the random data.
		chart.SeriesCollection.Add(mySC)

		Dim mySC2 As SeriesCollection = getSalesVsCost()
		Dim microCharts As ArrayList = getGridTextMontly(mySC2(0))

		Dim i As Integer = 0
		For Each o As Object In microCharts
            Dim micro As String = CStr(o)
            Label1.Text = Label1.Text & getChartTag(micro, i) & "<br/>"
            i = i + 1
		Next o
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2010, 1, 1)

		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 14
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)

		Return SC
	End Function

	Function getChartTag(ByVal labelText As String, ByVal i As Integer) As String
		Dim c As Chart = New Chart()
		c.TempDirectory = "temp"
		c.Margin = "0"
		c.ObjectChart = New dotnetCHARTING.Label(labelText)
		If i > 0 Then
			Return "<img src='" & c.FileManager.SaveImage() & "' onMouseOver=""Javascript:updateMonth('" & New DateTime(2007, i, 1).ToString() & "');"">"
		Else
			Return "<img src='" & c.FileManager.SaveImage() & "'>"
		End If

	End Function

	Function getGridTextMontly(ByVal s As Series) As ArrayList
		Dim sb As StringBuilder = New StringBuilder()
		Dim result As ArrayList = New ArrayList()
		' Define some block styles
		Dim headerStyle As String = "<block fStyle='bold' fSize='10' hAlign='Center'>"
		Dim layout As String = "<Chart:Spacer size='90x1'><Chart:Spacer size='80x1'><Chart:Spacer size='80x1'><row>"
		' Setup header row

		Dim head As String = headerStyle & "Month" & headerStyle & "Total<Chart:Scale min='0' max='50000' value='$%Value' width='80'>" & headerStyle & " Daily"
		result.Add(head)

		'row for each element(Month)
		Dim i As Integer = 0
		For Each e As Element In s.Elements
			Dim thisElement As String = ""
			Dim daily As String = getMonthValues(i + 1)
		   ' thisElement += layout;
			' Add name and value
			thisElement &= ("<block fStyle='Bold' vAlign='Center'>" & e.Name & "<block hAlign='Center' vAlign='Center' hAligh='Right'>" & e.YValue.ToString("C"))
			' Add bar and sparkline
			thisElement &= ("<Chart:Bar min='0' max='50000' value='" & e.YValue & "' width='80' shading='7'><Chart:Sparkline values='" & daily & "' height='25' width='80'>")
			i += 1
			result.Add(thisElement)
		Next e
		Return result
	End Function
	Function getMonthValues(ByVal month As Integer) As String
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.StartDate = New System.DateTime(2007, month, 1, 0, 0, 0)
		de.EndDate = de.StartDate.AddMonths(1)
		de.SqlStatement = "SELECT OrderDate,Total FROM Purchases WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
		de.DateGrouping = TimeInterval.Month
		Dim sc As SeriesCollection = de.GetSeries()
		Return sc(0).GetYValueList()
	End Function
	Function getSalesVsCost() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.StartDate = New System.DateTime(2007, 1, 1, 0, 0, 0)
		de.EndDate = New System.DateTime(2007, 12, 31, 23, 59, 59)
		de.DateGrouping = TimeInterval.Year
		de.SqlStatement = "SELECT OrderDate,Total,CostOfGoods FROM Purchases WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
		de.DataFields = "XAxis=OrderDate,YAxis=Total,YAxis=CostOfGoods"
		Return de.GetSeries()
	End Function
	Function getSalesVsCostDaily() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.StartDate = New System.DateTime(2007, 1, 1, 0, 0, 0)
		de.EndDate = New System.DateTime(2007, 12, 31, 23, 59, 59)
		de.DateGrouping = TimeInterval.Days
		de.SqlStatement = "SELECT OrderDate,Total,CostOfGoods FROM Purchases WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
		de.DataFields = "XAxis=OrderDate,YAxis=Total,YAxis=CostOfGoods"
		Return de.GetSeries()
	End Function


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
	<style type="text/css">
	div, p, td
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: x-small;
	}
	.style1
	{
		text-align: right;
	}
</style>
	<script type="text/javascript">

		var chartID = "slPlugin";

		// For more information on this code and a client side API reference, see this tutorial: 
		// http://www.dotnetcharting.com/documentation/v6_0/Silverlight_Navigator_API.html

		// Allows getting the month names
		Date.prototype.getMonthName = function () {
			var m = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
			return m[this.getMonth()];
		}

		// This function is called by the static chart and controls the silverlight chart movement.
		function updateMonth(date) {
			var control = document.getElementById(chartID);

			var leftDate = new Date();
			leftDate.setTime(Date.parse(date));
			leftDate.setHours(-6);
			var tmpDate = new Date();
			tmpDate.setTime(Date.parse(date));
			tmpDate.setDate(1);
			tmpDate.setMonth(tmpDate.getMonth() + 1);
			tmpDate.setHours(-6);
			control.Content.Chart.NavigateToVisibleRange(leftDate, tmpDate, 180);

		}

		function zoomOut() {
			var control = document.getElementById(chartID);
			control.Content.Chart.NavigateToVisibleRangePerc(0, 1, 320);
		}

	</script>
</head>
<body>
	<div align="center">
		<table>
			<tr>
				<td colspan="2">This sample demonstrates how a detailed Navigator chart can be controled by hovering over microCharts.<br />
				<strong>Try it:</strong> Move your mouse over the micro chart on the left.
				</td>
			</tr>
			<tr>
				<td>
				<div id="chartDiv" onMouseOut="Javascript:zoomOut();">
					<asp:Label ID="Label1" runat="server" /></div>
				</td>
				<td>
					<dnc:Chart ID="chart" runat="server" />
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
