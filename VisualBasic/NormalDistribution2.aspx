<%@ Page Language="VB" debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   ' This sample demonstrates the use of NormalDistribution
   ' within StatisticalEngine.
   ' The Distribution Chart
   DistributionChart.Title = "Distribution"
   DistributionChart.TempDirectory = "temp"
   DistributionChart.Debug = True
   DistributionChart.Size = "600x300"
   DistributionChart.LegendBox.Template = "%icon %name"
   'DistributionChart.XAxis.ScaleRange.ValueLow =50;
   DistributionChart.YAxis.Percent = True
   DistributionChart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   DistributionChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   DistributionChart.DefaultSeries.DefaultElement.Marker.Visible = False
   DistributionChart.XAxis.Interval = 1
   DistributionChart.YAxis.FormatString = "0.000000"
   
   
   
   Dim normalDistVector() As Double = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10}
   
   Dim sampledata As New Series("Sample1")
   Dim i As Integer
   For i = 0 To normalDistVector.Length - 1
      Dim el As New Element()
      el.XValue = normalDistVector(i)
      sampledata.Elements.Add(el)
   Next i
   ' The second parameter of this method is the standard deviation of the normal probability distribution
   Dim normalDistribution As Series = StatisticalEngine.NormalDistribution(sampledata, 1)
   normalDistribution.Type = SeriesType.AreaSpline
   normalDistribution.DefaultElement.ShowValue = True
   normalDistribution.DefaultElement.SmartLabel.Text = "%YValue"
   normalDistribution.DefaultElement.SmartLabel.Color = Color.Black
   DistributionChart.SeriesCollection.Add(normalDistribution)
End Sub 'Page_Load
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="DistributionChart" runat="server"/>			
		</div>
	</body>
</html>
