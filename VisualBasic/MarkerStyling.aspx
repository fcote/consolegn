<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Size = "1000x500"
   Chart.Title = "marker settings based on population and state capitals (Red marker)."
   Chart.TempDirectory = "temp"
   Chart.TitleBox.Position = TitleBoxPosition.Full
   Chart.ChartArea.Background = New Background(Color.FromArgb(142, 195, 236), Color.FromArgb(63, 137, 200), 90)
   Chart.Mapping.ZoomCenterPoint = New PointF( 38,- 96.5F)
   Chart.Mapping.ZoomPercentage = 180
   Chart.Type = ChartType.Map
   Chart.Mapping.MapLayerCollection.Add("../../Images/MapFiles/states.shp")
   Chart.Mapping.MapLayerCollection(0).DefaultShape.Background.Color = Color.PapayaWhip
   
   Dim layer As MapLayer = MapDataEngine.LoadLayer("../../Images/MapFiles/cities.SHP")
   
   ' set different marker settings based on population and state capitals.
   Dim shape As Shape
   For Each shape In  layer.Shapes
      
      
      Dim population As Integer = Convert.ToInt32(shape("POP"))
      Dim capital As String = CStr(shape("CAPITAL"))
      If capital = "N" Then
         shape.Marker.Type = ElementMarkerType.Circle
         shape.Marker.Color = Color.Blue
      Else
         shape.Marker.Type = ElementMarkerType.Triangle
         shape.Marker.Color = Color.Red
      End If
      
      If population < 10000 Then
         shape.Marker.Size = 1
      Else
         If population < 100000 Then
            shape.Marker.Size = 4
         Else
            If population < 500000 Then
               shape.Marker.Size = 7
            Else
               If population < 2000000 Then
                  shape.Marker.Size = 10
               Else
                  If population < 3000000 Then
                     shape.Marker.Size = 15
                  Else
                     If population < 5000000 Then
                        shape.Marker.Size = 20
                     Else
                        shape.Marker.Size = 25
                     End If
                  End If 
               End If
            End If
         End If
      End If
   Next shape
   Chart.Mapping.MapLayerCollection.Add(layer)
End Sub 'Page_Load

</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
