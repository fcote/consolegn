<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(500)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Adding ranged ticks."
   Chart.LegendBox.Visible = False
   Chart.ShadingEffect = True
   Chart.ShadingEffectMode = ShadingEffectMode.Two
   
   ' * INTRO *
   ' This sample will demonstrate usage of ranged ticks
   
   Dim at As New AxisTick(5, 15)
   at.Label.Color = Color.Red
   at.Label.Text = "5 to 15 unit Tick"
   ' Add it to the axis.
   Chart.YAxis.ExtraTicks.Add(at)
   
   Dim at2 As New AxisTick(35, 20)
   at2.Label.Color = Color.Red
   at2.Label.Text = "25 Unit Range"
   ' Add it to the axis.
   Chart.YAxis.ExtraTicks.Add(at2)
   
   Dim at3 As New AxisTick(10, 30)
   at3.Label.Color = Color.Red
   at3.Label.Text = "10 to 30 range"
   ' Add it to the axis.
   Chart.YAxis.ExtraTicks.Add(at3)
   
   
   Dim at4 As New AxisTick(New DateTime(2005, 1, 1), New DateTime(2005, 3, 1))
   at4.Label.Color = Color.Red
   at4.Label.Text = "Q1"
   Chart.XAxis.ExtraTicks.Add(at4)
   
   Dim at5 As New AxisTick(New DateTime(2005, 4, 1), New DateTime(2005, 6, 1))
   at5.Label.Color = Color.Red
   at5.Label.Text = "Q2"
   Chart.XAxis.ExtraTicks.Add(at5)
   
   Dim at6 As New AxisTick(New DateTime(2005, 7, 1), New DateTime(2005, 9, 1))
   at6.Label.Color = Color.Red
   at6.Label.Text = "Q3"
   Chart.XAxis.ExtraTicks.Add(at6)
   
   Dim at7 As New AxisTick(New DateTime(2005, 10, 1), New DateTime(2005, 12, 1))
   at7.Label.Color = Color.Red
   at7.Label.Text = "Q4"
   Chart.XAxis.ExtraTicks.Add(at7)
   
   Dim at8 As New AxisTick(New DateTime(2005, 1, 1), New DateTime(2005, 6, 1))
   at8.Label.Color = Color.Red
   at8.Label.Text = "First Half"
   Chart.XAxis.ExtraTicks.Add(at8)
   
   Dim at9 As New AxisTick(New DateTime(2005, 7, 1), New DateTime(2005, 12, 1))
   at9.Label.Color = Color.Red
   at9.Label.Text = "Second Half"
   Chart.XAxis.ExtraTicks.Add(at9)
   
   Dim at10 As New AxisTick(New DateTime(2005, 1, 1), New DateTime(2005, 12, 1))
   at10.Label.Color = Color.Red
   at10.Label.Text = "Fiscal Year 2005"
   Chart.XAxis.ExtraTicks.Add(at10)
   
   
   ' 3. GET DATA
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. For information on acquiring 
   ' database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   ' Add the random data.
   Chart.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load
 



Function getRandomData() As SeriesCollection
   
   Dim myR As New Random()
   Dim SC As New SeriesCollection()
   Dim dt As New DateTime(2005, 1, 1)
   
   Dim s As New Series()
   Dim b As Integer
   For b = 0 To 11
      
      Dim e As New Element()
      e.XDateTime = dt
      dt = dt.AddMonths(1)
      e.YValue = myR.Next(50)
      s.Elements.Add(e)
   Next b
   
   ' give each series element its own color
   s.PaletteName = Palette.Two
   
   SC.Add(s)
   
   Return SC
End Function 'getRandomData

        </script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
