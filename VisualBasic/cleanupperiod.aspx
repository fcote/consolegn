<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

Function  getRandomData() As SeriesCollection

	Dim SC As   New SeriesCollection()
	Dim myR As  New Random()
	Dim a As Integer
	Dim b As Integer
	Dim s As  Series
	Dim e As Element
	For a = 1 To 4
	
		s = new Series()
		s.Name = "Series " & a.ToString()
		For b = 1 To 5
		    e = new Element()
			e.Name = "E " & b.ToString()
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	Return SC
End Function


Sub Page_Load(sender As Object,e As EventArgs)

	'Set clean up priod(only delete the auto generated files, starts with dnc-...)
	Chart.CleanupPeriod=5
	
	' Set the title.
	Chart.Title="My Chart"

	' Set the x axis label
	Chart.ChartArea.XAxis.Label.Text="X Axis Label"

	' Set the y axis label
	Chart.ChartArea.YAxis.Label.Text="Y Axis Label"

	' Set the directory where the images will be stored.
	Chart.TempDirectory="temp"

	' Set the bar shading effect
	Chart.ShadingEffect = true

	' Set he chart size.
	Chart.Width = Unit.Parse(600)
	Chart.Height = Unit.Parse(350)
	
	Chart.Debug=true

	' Add the random data.
	Chart.SeriesCollection.Add(getRandomData())
    
    
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Cleanup Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>

</html>
