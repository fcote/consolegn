<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>

<script runat="server">

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + CStr(a)
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + CStr(b)
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   
   Return SC
End Function 'getRandomData



Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.TempDirectory = "temp"
   
   ' Set this property in order to get error messages when the chart has problems:
   Chart.Debug = True
   
   
   ' If there is a problem with the data engine, we can also debug it with the following:
   ' We setup a dataengine object and cause an error.
   Dim de As New DataEngine()
   de.ConnectionString = "error causing connection string"
   de.SqlStatement = "error causing statement"
   Dim sc As SeriesCollection = de.GetSeries()
   
   ' Now assign the error message to a label on the page.
   Label.Text = "DataEngine ErrorMessage: " + de.ErrorMessage
   
   
   Chart.YAxis.Scale = Scale.Time
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Debugging Sample</title>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" /><br>
			<asp:Label id="Label" runat="server" />
		</div>
	</body>
</html>
