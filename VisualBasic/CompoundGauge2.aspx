<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using compounded gauges.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.Mentor = False
		Chart.Type = ChartType.Gauges
		Chart.OverlapFooter = true
		Chart.Size = "500x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.Background.Color = Color.Transparent

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()
		Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
		Chart.ChartArea.ClearColors()

		Dim digital As Series = Series.FromYValues(mySC(0)(0).YValue)
		digital.XAxis = New Axis()
		digital.XAxis.DefaultTick.Label.Text = ""
		digital.GaugeType = GaugeType.Bars
		digital.GaugeBorderBox.Padding = 2
		mySC(0).Background.Color = Color.White
		digital.GaugeBorderBox.Position = New Rectangle(180,270,130,20)
		digital.GaugeBorderBox.Shadow.Color = Color.Empty
		digital.LegendEntry.Visible = False
		Chart.SeriesCollection.Add(digital)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 1
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(14150)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body bgcolor="#00cc33" background="../../images/back038.jpg">
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
