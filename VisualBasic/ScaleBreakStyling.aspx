<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)

Chart.Type = ChartType.Combo
Chart.Size = "600x350"
Chart.TempDirectory = "temp"
Chart.Debug = True
Chart.Title = "Scale Break Styling ( Gap, ZigZag, Line )"


' This sample demonstrates ....
' *DYNAMIC DATA NOTE* 
' This sample uses random data to populate the chart. To populate 
' a chart with database data see the following resources:
' - Classic samples folder
' - Help File > Data Tutorials
' - Sample: features/DataEngine.aspx
Dim mySC As SeriesCollection = getRandomData()

' Page link related code.
Dim scaleBreakStyleOption As String = HttpContext.Current.Request.QueryString("ScaleBreakStyle")
If scaleBreakStyleOption Is Nothing Or scaleBreakStyleOption = "" Then
   scaleBreakStyleOption = "Gap"
End If 
Chart.YAxis.ScaleBreakStyle = CType([Enum].Parse(GetType(dotnetCHARTING.ScaleBreakStyle), scaleBreakStyleOption, True), dotnetCHARTING.ScaleBreakStyle)
Chart.YAxis.SmartScaleBreak = True 'For disabling scale break, set to false
Chart.DefaultAxis.AlternateGridBackground.Color = Color.Empty
Chart.DefaultAxis.DefaultTick.GridLine.Color = Color.Empty


' Add the random data.
Chart.SeriesCollection.Add(mySC)

End Sub 'Page_Load
Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   SC(0).Elements(1).YValue = 1000
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
	<div style="text-align:center">
		<a href="?ScaleBreakStyle=Gap">Gap style scale break</a> | <a href="?ScaleBreakStyle=ZigZag">ZigZag style scale break</a> | <a href="?ScaleBreakStyle=Line">Line style scale break</a><br>
		</div>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
