<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates different projections available for map rendering along with and ecw image layer.
   Chart.Title = "No Projection"
   Chart.Mapping.ZoomPercentage = 100
   LoadDefaults(Chart, 3)
   
   Chart1.Title = "Mercator Projection"
   Chart1.Mapping.Projection.Type = ProjectionType.Mercator
   Chart1.Mapping.Projection.Parameters = "-60"
   LoadDefaults(Chart1,2)
   
   Chart2.Title = "Lambert Conic Projection"
   Chart2.Mapping.Projection.Type = ProjectionType.LambertConic
	Chart2.Mapping.Projection.Parameters = "-10,-20,-32,49"
   LoadDefaults(Chart2, 1)
End Sub 'Page_Load
 

Sub LoadDefaults(c As Chart, unique As Integer)
   c.Type = ChartType.Map
   c.Size = "800x400"
   
   c.TempDirectory = "temp"
   Dim layer As MapLayer = MapDataEngine.LoadLayer("../../Images/MapFiles/worldmap.ecw", "../../Images/MapFiles/worldmapProj" + Convert.ToString(unique) + ".jpg")
   c.Debug = True
   c.Mapping.MapLayerCollection.Add(layer)
   c.Mapping.MapLayerCollection.Add("../../Images/MapFiles/states.shp")
   c.Mapping.MapLayerCollection.Add("../../Images/MapFiles/europe.shp")
   c.Mapping.DefaultShape.Line.Color = Color.White
End Sub 'LoadDefaults
</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
			<dotnet:Chart id="Chart1" runat="server" >
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" >
			</dotnet:Chart>

		</div>
	</body>
</html>
