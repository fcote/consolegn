<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates styling element borders and outlines.

		Chart.TempDirectory = "temp"
		Chart.Debug = True

		Chart.Type = ChartType.Combo
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.Use3D = True

		Chart.DefaultElement.Transparency = 20

		' Set the default element outline style.
		Chart.DefaultElement.ShapeType = ShapeType.Chat
		Chart.DefaultElement.Outline.Color = Color.Red
		Chart.DefaultElement.Outline.Width = 2
		Chart.DefaultElement.Outline.DashStyle = DashStyle.Dash

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data and specify the series types.
		Chart.SeriesCollection.Add(mySC)
		mySC(0).Type = SeriesType.Column
		mySC(1).Type = SeriesType.BubbleShape
		mySC(2).Type = SeriesType.Bar
		mySC(3).Type = SeriesType.BarSegmented
		mySC(4).Type = SeriesType.Cone
		mySC(5).Type = SeriesType.Cylinder
		mySC(6).Type = SeriesType.Pyramid

	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 7
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 4
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = 10+myR.Next(50)
				e.BubbleSize = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
