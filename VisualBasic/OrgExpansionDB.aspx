<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">


	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates interactive organizational chart expansion with the AJAX zoomer and live data.

		' Apply some styling settings.
		Chart.Size = "1000x550"
		Chart.Title = ".netCHARTING Sample"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Type = ChartType.Organizational
		Chart.Zoomer.Enabled = True
		Chart.ChartArea.Padding = 35

		' Define some different Header styles.
		Dim headerLabel1 As String = "<Chart:Spacer size='50,1'><Chart:Spacer size='90,1'><block><row> %Name <block><Chart:Image src='../../images/addW.png'>"
		Dim headerLabel2 As String = "<Chart:Spacer size='50,1'><Chart:Spacer size='90,1'><block><row> %Name <block><Chart:Image src='../../images/removeW.png'>"
		Dim headerLabel3 As String = "<Chart:Spacer size='50,1'><Chart:Spacer size='90,1'><block><row> %Name <block><block>"

		' TitleBox Customization
		Chart.TitleBox.Label.Text = " Acme Company's Organizational Chart"
		Chart.TitleBox.Label.Color = Color.White
		Chart.TitleBox.Label.Shadow.Color = Color.FromArgb(105, 0, 0, 0)
		Chart.TitleBox.Label.Shadow.Depth = 2
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Two
		Chart.TitleBox.Background.Color = Color.SteelBlue

		' Node connecting line styling.
		Chart.DefaultSeries.Line.Color = Color.Gray
		Chart.DefaultSeries.Line.Width = 3

		' Style the default annotation.
		Chart.DefaultElement.Annotation = New Annotation()
		Chart.DefaultElement.Annotation.Padding = 5
		Chart.DefaultElement.Annotation.DynamicSize = False
		Chart.DefaultElement.Annotation.DefaultCorner = BoxCorner.Round
		Chart.DefaultElement.Annotation.Background.ShadingEffectMode = ShadingEffectMode.Seven
		Chart.DefaultElement.Annotation.HeaderBackground.ShadingEffectMode = ShadingEffectMode.Two
		Chart.DefaultElement.Annotation.HeaderLabel.Shadow.Depth = 3
		Chart.DefaultElement.Annotation.HeaderBackground.Color = Color.Green
		Chart.DefaultElement.Annotation.HeaderLabel = New dotnetCHARTING.Label(headerLabel3, New Font("Arial", 11, FontStyle.Bold), Color.White)
		Chart.DefaultElement.Annotation.Label.Text = "<block fSize='12'>%Name %id"
		Chart.DefaultElement.Annotation.Label.Text = "<img:../../images/Org%picture><block halign='right'> %Department <br> %Email <br> %phone <br> Office #%office "

		Dim lastClicked As Integer = 1

		' This sample will maintain a comma delimited list of node IDs that are expanded.
		' By default the main node is expanded showing 3
		Dim expandedList As String = "1"

		' Get the query string
		If Page.Request.QueryString.Count > 0 Then
			Dim query As String = Page.Request.QueryString("id")
			If Not query Is Nothing AndAlso query.Length > 0 Then
				lastClicked = Integer.Parse(query)
			End If

			query = Page.Request.QueryString("list")
			If Not query Is Nothing AndAlso query.Length > 0 Then
				expandedList = query.ToString()
			End If
		End If

		' Each node's URL will append its ID to the expandedList.
		' The ID of the clicked node is also passed so the last clicked node can be highlighted.
		Chart.DefaultElement.Annotation.URL = "?id=%id&list=" & expandedList & ",%id"
		Chart.DefaultElement.Annotation.HeaderLabel.Text = headerLabel1

		' Get the organizational nodes.
		Dim mySC As SeriesCollection = getLiveData()

		' By passing the comma delimited list of IDs to the trim method, it will return only the nodes that should appear on the chart.
		' When the addAttributes parameter is true, the trim method will also mark some nodes with custom attributes describing them.
		Dim [partial] As Series = mySC(0).Trim(expandedList, True)

		Dim listArray As String() = expandedList.Split(New String() { "," }, StringSplitOptions.RemoveEmptyEntries)


		' The following loop will iterate the elements and modify them based on custom attributes added by the trim method and other conditions.  
		For Each el As Element In [partial].Elements
			'If node was clicked last, set a different color
			If lastClicked = el.InstanceID AndAlso lastClicked <> 1 Then
				If el.Annotation Is Nothing Then
				el.Annotation = New Annotation()
				End If
				el.Annotation.Background.Color = Color.FromArgb(255, 255, 246)

			End If

			' If node is expanded, use a remove icon and pass an expansion list without this element's ID.
			If isInList(el.InstanceID, listArray) AndAlso el.InstanceID <> 1 Then
				If el.Annotation Is Nothing Then
				el.Annotation = New Annotation()
				End If
				el.Annotation.URL = "?id=%id&list=" & removeFromList(el.InstanceID, listArray)
				el.Annotation.HeaderLabel.Text = headerLabel2
			End If

			' If it's the root node, or the node has no children to expand, don't link it or show expand/contract icon.
			If el.CustomAttributes("NodeType") = "Root" OrElse el.CustomAttributes("NodeType") = "NoChildren" Then
				If el.Annotation Is Nothing Then
				el.Annotation = New Annotation()
				End If
				el.Annotation.URL = ""
				el.Annotation.HeaderLabel.Text = headerLabel3
			End If
		Next el

		' Add the data.
		Chart.SeriesCollection.Add([partial])

		addExpandCollapseAllButtons(1, mySC(0))
	End Sub

	Function removeFromList(ByVal id As Integer, ByVal list As String()) As String
		Dim newList As String = ""
		For i As Integer = 0 To list.Length - 1
			Dim lid As String = list(i)
			If Integer.Parse(lid) <> id Then
				newList &= lid
				If i < list.Length - 1 Then
				newList &= ","
				End If
			End If
		Next i
		Return newList
	End Function

	Function isInList(ByVal id As Integer, ByVal list As String()) As Boolean
		For Each lid As String In list
			If Integer.Parse(lid) = id Then
			Return True
			End If
		Next lid
		Return False
	End Function

	Function getLiveData() As SeriesCollection
		' Obtain series data from the database. Note this was created by adding PID (parentid)
		' to an existing table only, then populating the parentid based on the hiearchy in place.
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.SqlStatement = "SELECT * FROM Employees"
		de.DataFields = "InstanceID=ID,InstanceParentID=PID,Name=Name,office,Department,Email,Phone,Picture"
		Return de.GetSeries()
	End Function

	' Add buttons to expand and collapse all nodes.
	Sub addExpandCollapseAllButtons(ByVal root As Integer, ByVal s As Series)
		Dim expandAllList As String = ""
		For Each e As Element In s.Elements
			expandAllList &= e.InstanceID & ","
		Next e

		Dim a3 As Annotation = New Annotation("", "Expand all nodes", "?list=" & expandAllList)
		a3.Position = New Point(15, 38)
		a3.Label = New dotnetCHARTING.Label("Expand All", New Font("Arial", 10, FontStyle.Bold), Color.White)
		a3.DefaultCorner = BoxCorner.Round
		a3.Size = New Size(110, 25)
		a3.CornerSize = 4
		a3.Background.Color = Color.LimeGreen
		a3.Background.ShadingEffectMode = ShadingEffectMode.Three
		Chart.Annotations.Add(a3)

		Dim a4 As Annotation = New Annotation("", "Expand all nodes", "?list=1")
		a4.Position = New Point(130, 38)
		a4.Label = New dotnetCHARTING.Label("Collapse All", New Font("Arial", 10, FontStyle.Bold), Color.White)
		a4.DefaultCorner = BoxCorner.Round
		a4.Size = New Size(100, 25)
		a4.CornerSize = 4
		a4.Background.Color = Color.LimeGreen
		a4.Background.ShadingEffectMode = ShadingEffectMode.Three
		Chart.Annotations.Add(a4)
	End Sub

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
