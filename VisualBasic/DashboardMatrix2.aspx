<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        ' This sample demonstrates a database driven digital dashboards utilizing a number of chart types including InfoGrids and MicroCharts.

        Chart.TempDirectory = "temp"
        'Chart.Palette = new Color[] { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
        Chart.ShadingEffectMode = ShadingEffectMode.Seven
        Chart.Type = ChartType.Combo
        Chart.Size = "500x300"
        Chart.ShowDateInTitle = False
        Chart.Title = "2007 Sales"
        Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
        Chart.DefaultFormatString = "Currency"
        Chart.TitleBox.CornerTopLeft = BoxCorner.Round
        Chart.TitleBox.CornerTopRight = BoxCorner.Round
        Chart.DefaultAxis.TickNumberMaximum = 7
        Chart.DefaultLegendBox.DefaultEntry.Name = "<block fStyle='Bold' fSize='9'>%Name"

        Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Five
        Chart.TitleBox.Background.Color = Color.White
        Dim mySC As SeriesCollection = getSalesVsCost()
        mySC(0).Name = "Income"
        mySC(1).Name = "Cost"

        ' Add the random data.
        Chart.SeriesCollection.Add(mySC)

        Dim cArea As ChartArea = New ChartArea(withTrend(mySC(0) - mySC(1), ""))
        cArea.SeriesCollection(0).Name = "Net Profit"
        Chart.ExtraChartAreas.Add(cArea)

        ' new legendbox for the second chart area.
        cArea.LegendBox = New LegendBox()
        cArea.LegendBox.DefaultCorner = BoxCorner.Round
        cArea.LegendBox.Position = New Point(83, 165)
        cArea.LegendBox.Background.ShadingEffectMode = ShadingEffectMode.Five
        cArea.LegendBox.Background.Color = Color.White
        Chart2.TempDirectory = "temp"
        Chart2.LabelChart.Text = getGridTextMontly(mySC(0))

        Chart3.TempDirectory = "temp"
        Chart3.LabelChart.Font = New Font("Arial", 10)
        Chart3.LabelChart.Text = getGridTextKPIs()

    End Sub


    Function getGridTextKPIs() As String
        ' Bullet target values
        Dim avCTarget As Single = 90
        Dim avGTarget As Single = 190
        Dim avNTarget As Single = 100

        ' Define some block styles
        Dim headerStyle As String = "<block fStyle='bold' fSize='10' hAlign='Center'>"
        Dim extras As String = " width='135' shading='7'"
        Dim sb As StringBuilder = New StringBuilder()

        ' Header row
        sb.Append(headerStyle & "KPI" & headerStyle & "Value<Chart:Scale min='0' max='200' value='%Value%' " & extras & ">" & headerStyle & " Over Time")
        sb.Append("<hr>")

        ' Row for each KPI
        Dim avC As SeriesCollection = getAvCostOverTime()
        Dim avCVal As Single = CSng(avC(0).Calculate("", Calculation.Average).YValue)
        Dim avCPOT As Integer = CInt(Fix(avCVal / avCTarget * 100))
        sb.Append("Average Cost / Day <block hAlign='Center'> " & avCVal.ToString("C") & "<Chart:Bullet values='" & avCPOT & ",100,60,80' min='0' max='200'" & extras & "><Chart:SparkLine values='" & avC(0).GetYValueList() & "'" & extras & ">")

        Dim avG As SeriesCollection = getAvGrossOverTime()
        Dim avGVal As Single = CSng(avG(0).Calculate("", Calculation.Average).YValue)
        Dim avGPOT As Integer = CInt(Fix(avGVal / avGTarget * 100))
        sb.Append("<row>Average Gross / Day <block hAlign='Center'> " & avGVal.ToString("C") & "<Chart:Bullet values='" & avGPOT & ",100,60,80' min='0' max='200'" & extras & "><Chart:SparkLine values='" & avG(0).GetYValueList() & "'" & extras & ">")

        Dim avN As SeriesCollection = New SeriesCollection(avG(0) - avC(0))
        Dim avNVal As Single = CSng(avN(0).Calculate("", Calculation.Average).YValue)
        Dim avNPOT As Integer = CInt(Fix(avNVal / avNTarget * 100))
        sb.Append("<row>Average Net / Day <block hAlign='Center'> " & avNVal.ToString("C") & "<Chart:Bullet values='" & avNPOT & ",100,60,80' min='0' max='200'" & extras & "><Chart:SparkLine values='" & avN(0).GetYValueList() & "'" & extras & ">")

        Return sb.ToString()
    End Function

    Function getGridTextMontly(ByVal s As Series) As String
        Dim sb As StringBuilder = New StringBuilder()

        ' Define some block styles
        Dim headerStyle As String = "<block fStyle='bold' fSize='10' hAlign='Center'>"

        ' Setup header row
        sb.Append(headerStyle & "Month" & headerStyle & "Total<Chart:Scale min='0' max='50000' value='$%Value' width='80'>" & headerStyle & " Daily")
        sb.Append("<hr>")

        'row for each element(Month)
        Dim i As Integer = 0
        For Each e As Element In s.Elements
            Dim daily As String = getMonthValues(i + 1)
            ' Add name and value
            sb.Append("<block fStyle='Bold' vAlign='Center'>" & e.Name & "<block hAlign='Center' vAlign='Center' hAligh='Right'>" & e.YValue.ToString("C"))
            ' Add bar and sparkline
            sb.Append("<Chart:Bar  min='0' max='50000' value='" & e.YValue & "' width='80' shading='7'><Chart:Sparkline values='" & daily & "' height='25' width='80'>")

            If i < s.Elements.Count - 1 Then
                sb.Append("<hr>")
            End If
            i = i + 1
        Next e
        Return sb.ToString()
    End Function

    Function getMonthValues(ByVal month As Integer) As String
        Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
        de.StartDate = New System.DateTime(2002, month, 1, 0, 0, 0)
        de.EndDate = de.StartDate.AddMonths(1)
        de.DateGrouping = TimeInterval.Month

        de.SqlStatement = "SELECT OrderDate,Sum(Quantity) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"

        Dim sc As SeriesCollection = de.GetSeries()
        Return sc(0).GetYValueList()
    End Function

    ' Takes a series and returns a series collection that includes a trend series as well.
    Function withTrend(ByVal s As Series, ByVal name As String) As SeriesCollection
        Dim result As SeriesCollection = New SeriesCollection()

        Dim trend As Series = (New SeriesCollection(s)).Calculate(name, Calculation.TrendLineLinear)
        trend.Type = SeriesType.Line
        trend.DefaultElement.Marker.Visible = False
        trend.LegendEntry.Visible = False
        trend.DefaultElement.Color = Color.Black
        trend.Line.EndCap = LineCap.ArrowAnchor
        trend.Line.AnchorCapScale = 2
        trend.Line.Width = 2

        If trend.Elements.Count > 0 Then
            trend.YAxis = New Axis()
            s.YAxis = trend.YAxis
            Dim am As AxisMarker = New AxisMarker("", Color.Red, trend(0).YValue)
            am.LegendEntry.Visible = False
            am.Line.DashStyle = DashStyle.Dash
            s.YAxis.Markers.Add(am)

        End If
        result.Add(s, trend)
        Return result
    End Function

    Function getSalesVsCost() As SeriesCollection
        Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
        de.StartDate = New System.DateTime(2007, 1, 1, 0, 0, 0)
        de.EndDate = New System.DateTime(2007, 12, 31, 23, 59, 59)
        de.DateGrouping = TimeInterval.Year
        de.SqlStatement = "SELECT OrderDate,Total,CostOfGoods FROM Purchases WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
        de.DataFields = "XAxis=OrderDate,YAxis=Total,YAxis=CostOfGoods"
        Return de.GetSeries()
    End Function

    Function getAvCostOverTime() As SeriesCollection
        Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
        de.StartDate = New System.DateTime(2007, 1, 1, 0, 0, 0)
        de.EndDate = New System.DateTime(2007, 12, 31, 23, 59, 59)
        de.DateGrouping = TimeInterval.Year
        de.DateGroupingCalculation = Calculation.Average
        de.SqlStatement = "SELECT OrderDate,CostOfGoods FROM Purchases WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
        Return de.GetSeries()
    End Function

    Function getAvGrossOverTime() As SeriesCollection
        Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
        de.StartDate = New System.DateTime(2007, 1, 1, 0, 0, 0)
        de.EndDate = New System.DateTime(2007, 12, 31, 23, 59, 59)
        de.DateGrouping = TimeInterval.Year
        de.DateGroupingCalculation = Calculation.Average
        de.SqlStatement = "SELECT OrderDate,Total FROM Purchases WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
        Return de.GetSeries()
    End Function
























</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <table style="width: 800px">
            <tr>
                <td>
                    <dotnet:Chart ID="Chart" runat="server" />
                    <dotnet:Chart ID="Chart3" runat="server" />
                </td>
                <td>
                    <dotnet:Chart ID="Chart2" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
