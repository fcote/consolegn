<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates several features of the Column microChart.

		Chart.TempDirectory = "temp"
		Chart.Background.Color = Color.FromArgb(244, 244, 244)
		Chart.LabelChart.Alignment = StringAlignment.Near

		Dim s As String = "<block fStyle='Bold'>Column Type Options<block><hr>"
		s &= "Plain Column Chart <Chart:Column values='5,8,10,7,3,2,7,4,5,6,3,2' >"

		' Zero values are always shown so they can be used as placeholders.
		s &= "<row>With 0 values <Chart:Column values='0,0,0,10,7,3,2,7,4,5,0,0,0' >"

		' Negative values are colored in red by default.
		s &= "<row>With (-) & (+) values <Chart:Column values='10,7,3,2,7,4,-5,-6,-3,-2' >"
		s &= "<row>With (-) & (+) & 0 values <Chart:Column values='10,7,3,2,7,4,0,0,0,-5,-6,-3,-2' >"

		' When a column microChart width is not specified it is determined automatically. 
		s &= "<hr><block fStyle='Bold'>Sizing<block><hr>"
		s &= "Height specified <Chart:Column values='5,8,10,7,3' height='25'>"

		' When only the height is specified, the column widths are based on the height and chart width is determined by the column width and number of elements shown.
		s &= "<row>Height specified <Chart:Column values='5,8,10,7,3' height='30'>"
		s &= "<row>Height specified<br>  -width is dynamic <Chart:Column values='5,8,10,7,3,3,5' height='30'>"
		s &= "<row>Height & Width specified <Chart:Column values='5,8,10,7,3' height='30' width='30'>"
		s &= "<row>Height & Width specified <Chart:Column values='5,8,10,7,3' height='30' width='70'>"


		s &= "<hr><block fStyle='Bold'>AxisMarker<block><hr>"

		' A reference line can be used by specifying a value in the AxisMarker parameter.
		s &= "Column with a AxisMarker line <Chart:Column values='5,8,10,7,3,2,7,4,5,6,3,2' AxisMarker='5'>"

		' A portion of the y axis can be marked by using two values in the AxisMarker parameter.
		s &= "<row>Column with a range AxisMarker <Chart:Column values='5,8,10,7,3,2,7,4,5,6,3,2' AxisMarker='3,7'>"

		' Specifying colors.
		s &= "<hr><block fStyle='Bold'>Coloring<block><hr>"
		s &= "Specifying colors: <Chart:Column values='5,8,10,7,3,2,7,4,-5,-6,-3,-2' AxisMarker='5,8' colors='DarkCyan,DeepPink,Yellow'>"

		Chart.ObjectChart = New dotnetCHARTING.Label(s)
	End Sub


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
