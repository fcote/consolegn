<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender as Object ,e as EventArgs )


	Chart.TempDirectory = "temp"
	Chart.Debug = true
	Chart2.TempDirectory = "temp"
	Chart2.Debug = true
	Chart.Title = "Without minimum interval setting"
	Chart2.Title = "Minimum interval set to 1"
	
	' This sample will demonstrate how minimum intervals can be used.

	' 1. SET A MINIMUM INTERVAL (For Chart2)
	Chart2.YAxis.MinimumInterval = 1
	

	' 2. ADD DATA
	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Chart.SeriesCollection.Add(getRandomData())
	Chart2.SeriesCollection.Add(getRandomData())
    
End Sub


Function  getRandomData() As SeriesCollection
	
		
	Dim SC As SeriesCollection  = new SeriesCollection()
	Dim myR as Random  = new Random()
	Dim a,b As Integer
	Dim s As Series 
	Dim e As Element	
	
	For a = 0 To 3
	
		s = new Series()
		s.Name = "Series " & a
		For  b = 0 To 3
		
			e = new Element()
			e.Name = "Element " & b
			e.YValue = myR.Next(4)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
				<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
