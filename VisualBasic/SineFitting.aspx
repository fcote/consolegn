<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Forecasting Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of GeneralLinear Forecasting engine in order to
   ' find the function of best fit from three functions spaces. The data used for which
   ' the fuctions are fit is a set of data which represents a FX exchange rate over a given
   ' period of time. We index the period by the number of days after the inital date and the
   ' eights function spaces are the spaces spanned by the following basis elements:
   '	
   ' 1) {(1)}
   ' 2) {(1), (sin 0.01*x)}
   ' 3) {(1), (sin 0.01*x), (sin 0.02*x)}
   ' 4) {(1), (sin 0.01*x), (sin 0.02*x), (sin 0.03*x)}
   ' 5) {(1), (sin 0.01*x), (sin 0.02*x), (sin 0.03*x), (sin 0.04*x)}
   ' 6) {(1), (sin 0.01*x), (sin 0.02*x), (sin 0.03*x), (sin 0.04*x), (sin 0.05*x)}
   ' 7) {(1), (sin 0.01*x), (sin 0.02*x), (sin 0.03*x), (sin 0.04*x), (sin 0.05*x), (sin 0.06*x)}
   ' 8) {(1), (sin 0.01*x), (sin 0.02*x), (sin 0.03*x), (sin 0.04*x), (sin 0.05*x), (sin 0.06*x), (sin 0.07*x)}
   '
   ' The Forecast Chart
   ForecastChart.Title = "Fitting Sum of Sine Terms"
   ForecastChart.TempDirectory = "temp"
   ForecastChart.Debug = True
   ForecastChart.Size = "1000x800"
   ForecastChart.LegendBox.Template = "%icon %name"
   ForecastChart.YAxis.ScaleRange.ValueLow = 210
   ForecastChart.XAxis.ScaleRange.ValueLow = 3200
   ForecastChart.PaletteName = Palette.Three
   
   ' The Forecast data
   Dim de As New DataEngine()
   de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   de.DateGrouping = TimeInterval.Days
   de.StartDate = New DateTime(1983, 10, 10, 0, 0, 0)
   de.EndDate = New DateTime(1984, 12, 10, 23, 59, 59)
   de.SqlStatement = "SELECT ID, Value AS q FROM Exchange WHERE Data >= #STARTDATE# AND Data <= #ENDDATE# ORDER BY Data "
   
   'Add a series
   Dim scForecast As SeriesCollection = de.GetSeries()
   ForecastChart.SeriesCollection.Add(scForecast)
   
   scForecast(0).Name = "FX Exchange Rate"
   scForecast(0).Type = SeriesType.Spline
   
  ' Takes off the marker off the line and spline series.

	 
   ForecastChart.DefaultSeries.DefaultElement.Marker = New ElementMarker(ElementMarkerType.None)
   ForecastChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   
   ' Generate a series of standard deviation for the given points
   Dim deviation As New Series()
   Dim i As Integer
   For i = 0 To (scForecast(0).Elements.Count) - 1
      Dim el As New Element()
      el.XValue = scForecast(0).Elements(i).XValue
      el.YValue = 1E-10
      deviation.Elements.Add(el)
   Next i
   
   ' Declare a new serie for the ChiSquare elements
   Dim chiSquareSeries As New Series()
   
   ' Note that this line is necessary in order to clear the function basis set by previous
   ' example.
   '
   ForecastEngine.Options.Reset()
   
   ' Set the first model function
   '
   ' The second basis element: (1)
   ForecastEngine.Options.AddConstantTerm(100)
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinear As New Series()
   generalLinear = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinear.Name = "Constant Term"
   generalLinear.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinear)
   
   ' Set the second model function ; we add 10*(sin 0.01*x) function to the basis functions
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.01}, New Double() {0}, New Double() {1})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel2 As New Series()
   generalLinearModel2 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel2.Name = "1st Sine"
   generalLinearModel2.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel2)
   
   ' Set the third model function ; we add 10*(sin 0.02*x) function to the basis functions
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.02}, New Double() {0}, New Double() {1})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel3 As New Series()
   generalLinearModel3 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel3.Name = "2nd Sine Sum"
   generalLinearModel3.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel3)
   
   ' We add 10*(sin 0.03*x) function to the basis functions
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.03}, New Double() {0}, New Double() {1})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel4 As New Series()
   generalLinearModel4 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel4.Name = "3rd Sine Sum"
   generalLinearModel4.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel4)
   
   ' We add 10*(sin 0.04*x) function to the basis functions
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.04}, New Double() {0}, New Double() {1})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel5 As New Series()
   generalLinearModel5 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel5.Name = "4th Sine Sum"
   generalLinearModel5.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel5)
   
   ' We add 10*(sin 0.05*x) function to the basis functions
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.05}, New Double() {0}, New Double() {1})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel6 As New Series()
   generalLinearModel6 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel6.Name = "5th Sine Sum"
   generalLinearModel6.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel6)
   
   ' We add 100 10*(sin 0.06*x) function to the basis functions
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.06}, New Double() {0}, New Double() {1})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel7 As New Series()
   generalLinearModel7 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel7.Name = "6th Sine Sum"
   generalLinearModel7.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel7)
   
   ' We add 10*(sin 0.07*x)  function to the basis functions
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.07}, New Double() {0}, New Double() {1})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel8 As New Series()
   generalLinearModel8 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel8.Name = "7th Sine Sum"
   generalLinearModel8.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel8)
   
   ' Add more sine functions
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.08}, New Double() {0}, New Double() {1})
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.09}, New Double() {0}, New Double() {1})
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.1}, New Double() {0}, New Double() {1})
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.11}, New Double() {0}, New Double() {1})
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.12}, New Double() {0}, New Double() {1})
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.13}, New Double() {0}, New Double() {1})
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.14}, New Double() {0}, New Double() {1})
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.15}, New Double() {0}, New Double() {1})
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.16}, New Double() {0}, New Double() {1})
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.17}, New Double() {0}, New Double() {1})
   ForecastEngine.Options.AddSineSum(New Double() {10}, New Double() {0.18}, New Double() {0}, New Double() {1})
   ' Generate a new series which will draw the function of best from the function spacce available.
   Dim generalLinearModel9 As New Series()
   generalLinearModel9 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel9.Name = "Sum of Sine"
   generalLinearModel9.DefaultElement.Color = Color.FromArgb(0, 0, 0)
   generalLinearModel9.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel9)
End Sub 'Page_Load
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="ForecastChart" runat="server"/>
		</div>
	</body>
</html>
