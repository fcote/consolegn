﻿<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="index.aspx.vb" Inherits="index" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div><br/>
        <span lang="fr-ca" style="font-family: 'Berlin Sans FB Demi'; font-size: xx-large">Console du Gestionnaire de notes</span>
    </div>
	<asp:Label ID="Label3" runat="server" Text="" Width="100%"></asp:Label>
	 
    <asp:Panel ID="Panel1" runat="server">
        <asp:Label ID="lbl_err" runat="server" ForeColor="Red" Width="100%"></asp:Label>
        <br />
        <table align="center" cellpadding="5" cellspacing="5">
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Domaine:" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DDListDomaine" runat="server" Width="165px">
                        <asp:ListItem Selected="True" Value="ACADEMIQUE">ACADEMIQUE</asp:ListItem>
                        <asp:ListItem>CONSEIL</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" 
                Text="Nom d'usager :" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server" Width="160px"></asp:TextBox>
                </td>
                <td>
                    <img alt="information" border="0" 
                src="images/information.png" 
                
                        title="votre nom d'usager est le même que votre adresse courriel. Exemple: Jean Dupont -&gt; jdupont ou jdupont@csdccs.edu.on.ca" /></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" 
                Text="Mot de passe :" Width="100%"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server" 
                TextMode="Password" Width="160px">qdaxyi</asp:TextBox>
                </td>
                <td>
                    <img alt="information" border="0" 
                src="images/information.png" 
                title="Votre mot de passe est le même que celui pour accéder à vos courriels" /></td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="OK" Width="80px" />
                </td>
                <td>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <br />
    
    
    <ul>
   <% If Session("role") = "administrateur" Or Session("role") = "directeur" Then%> 
    <li><a href="ConfigurationAssiduite.aspx" style="font-family: 'Berlin Sans FB Demi'; font-size: x-large">Configuration de l'assiduité</a></li>
    <li>-----------</li>
    <li><a href="gestion.aspx" style="font-family: 'Berlin Sans FB Demi'; font-size: x-large">Gestion des enseignants</a></li><br/><br/>
   <% End If%>
   <% If Session("role") = "administrateur" Or Session("role") = "directeur" Or Session("role") = "enseignant" Then%> 
   <li><a href="gn_rapport.aspx" style="font-family: 'Berlin Sans FB Demi'; font-size: x-large" onclick="grayOut(true, null);">Rapport notes évaluations</a></li><br/>
    <li><a href="gn_eleve.aspx" style="font-family: 'Berlin Sans FB Demi'; font-size: x-large" onclick="grayOut(true, null);">Notes par élève</a></li><br/>
    <%--<li><a href="gn.aspx" style="font-family: 'Berlin Sans FB Demi'; font-size: x-large" onclick="grayOut(true, null);">Notes par cours</a></li><br/>--%>
    <li><a href="gn2.aspx" style="font-family: 'Berlin Sans FB Demi'; font-size: x-large" onclick="grayOut(true, null);">Notes par cours/évaluations</a></li><br/>
    <li><a href="ee.aspx" style="font-family: 'Berlin Sans FB Demi'; font-size: x-large">Élèves en échec</a></li><br/>
    <%End If%>
    </ul>
   
    <% If Session("role") = "administrateur" Then%>
    <span lang="fr-ca" style="font-family: 'Berlin Sans FB Demi'; font-size: xx-large">Console administration</span>
    <ul>
    <li><a href="tril.aspx" style="font-family: 'Berlin Sans FB Demi'; font-size: x-large">Trillium - information des utilisateurs</a></li>
    <li><a href="trilele.aspx" style="font-family: 'Berlin Sans FB Demi'; font-size: x-large">Trillium - information des élèves</a></li>
    <li><a href="trilret.aspx" style="font-family: 'Berlin Sans FB Demi'; font-size: x-large">Trillium - Retard élèves</a></li>
    <li><a href="pp.aspx" style="font-family: 'Berlin Sans FB Demi'; font-size: x-large">Portail des parents - information des comptes</a></li>
    <li><a href="sms.aspx" style="font-family: 'Berlin Sans FB Demi'; font-size: x-large">SMS - Ordinateurs dans les écoles</a></li>
    </ul>
    Accéder en tant que:
    <asp:DropDownList ID="ddl_dir" runat="server" 
            DataSourceID="SqlDataSource5" DataTextField="NOM" 
            DataValueField="M_MEMBRE_ID" AutoPostBack="True">
    </asp:DropDownList><br/>
    <asp:SqlDataSource ID="SqlDataSource5" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand=" 
            SELECT M_NOM || ', ' || M_PRENOM || ' - ' || M_ECOLE AS NOM, M_MEMBRE_ID 
            FROM GN_VW_ENSEIGNANTS WHERE TYPE_PERSON = 'D' ORDER BY M_NOM, M_PRENOM ">
    </asp:SqlDataSource>
    <%End If%>
    
    
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    
 </asp:content>
