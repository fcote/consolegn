<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender as Object,e as EventArgs )


	Chart.Type = ChartType.Combo 'Horizontal
	Chart.Width = New Unit(600)
	Chart.Height = New Unit(350)
	Chart.TempDirectory = "temp"
	Chart.Debug = true
	
	
	' This sample will demonstrate how to use the glass effect. This effect works best on box objects.
	
	' Legendbox effect
	Chart.LegendBox.Background.Color = Color.Orange
	Chart.LegendBox.Background.GlassEffect = true
	
	' Titlebox effect
	Chart.Title = "This is the  Title box " & vbCrLf & "with a glass effect."
	Chart.TitleBox.Padding = 10
	Chart.TitleBox.Background.Color = Color.Orange
	Chart.TitleBox.Background.GlassEffect = true
		
	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	' Add the random data.
	Chart.SeriesCollection.Add(getRandomData())
    
End Sub

Function  getRandomData() As SeriesCollection

	Dim SC As SeriesCollection = new SeriesCollection()
	Dim myR As Random  = new Random()
	Dim a,b As Integer
	Dim s as Series
	Dim e as Element
	
	For a = 0 To 3
	
		s = new Series()
		s.Name = "Series " & a
		For b = 0 To 3
		
			e = new Element()
			e.Name = "Element " & b
			'e.YValue = -25 & myR.Next(50)
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
