<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender as Object ,e as EventArgs )


	Chart.TempDirectory = "temp"
	Chart.Debug = true
	Chart2.TempDirectory = "temp"
	Chart2.Debug = true
	Chart.Title = "Multiple Units"	
	Chart2.Title = "Custom Start Times"	
	
	' This sample will demonstrate how to use the advanced TimeInterval features with axis scales.
	
	' 2. SETUP AXIS RANGE AND INTERVAL (For Chart)
	' First we set the x and y axis ranges.
	Chart.XAxis.ScaleRange = new ScaleRange (new DateTime(2005,1,5),new DateTime(2005,9,5))
	Chart.YAxis.ScaleRange = new ScaleRange (new DateTime(2005,1,4,23,30,0),new DateTime(2005,1,5,0,30,0))

	' This code demonstrates a (2 month) interval.
	Chart.XAxis.TimeInterval = TimeInterval.Month
	Chart.XAxis.TimeIntervalAdvanced.Multiplier = 2
	Chart.XAxis.Label.Text = "2 Month Interval"'
	
	' This code demonstrates a (5 Minute) interval.
	Chart.YAxis.TimeInterval = TimeInterval.Minutes
	Chart.YAxis.TimeIntervalAdvanced.Multiplier = 5
	Chart.YAxis.Label.Text = "5 Minute Interval"
		
	' 3. SETUP AXIS RANGE AND INTERVAL (For Chart2)
	' First we set the x and y axis ranges.
	Chart2.XAxis.ScaleRange = new ScaleRange (new DateTime(2005,4,5),new DateTime(2005,6,5))
	

	' This code demonstrates a (week starting on tuesday) interval.
	Chart2.XAxis.TimeInterval = TimeInterval.Week
	Chart2.XAxis.TimeIntervalAdvanced.StartDayOfWeek = CInt(DayOfWeek.Tuesday)
	' Set a format string to include the day part of the date.
	Chart2.XAxis.FormatString = "D"
	Chart2.XAxis.Label.Text = "Interval is week starting on tuesday"
	
	' This code demonstrates a (quarter starting on Feb 15th) interval.
	Chart2.YAxis.ScaleRange = new ScaleRange (new DateTime(2004,9,1),new DateTime(2006,1,5))
	Chart2.YAxis.TimeInterval = TimeInterval.Quarter
	' When specifying a start date, the year part is not important, just month and day. 
	' The date can be specific to the millisecond as the start time.
	Chart2.YAxis.TimeIntervalAdvanced.Start = new DateTime(2005,2,15)
	' Set a format string to include the day part of the date.
	Chart2.YAxis.FormatString = "d"
	Chart2.YAxis.Label.Text = "Interval is quarter \nstarting on Feb 15th."
	

	' 4. ADD DATA
	' *DYNAMIC DATA NOTE* 
	' This sample uses a static element to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	
	' This element will be used by all the chart.
	Dim el as Element  = new Element()
	el.XDateTime = new DateTime(2005,5,5)
	el.YDateTime = new DateTime(2005,1,5)
		
	Chart.SeriesCollection.Add(new Series("",el))
	Chart2.SeriesCollection.Add(new Series("",el))

End Sub

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>

		</div>
	</body>
</html>
