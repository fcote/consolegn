<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        ' This sample demonstrates showing this chart in tooltips of each element and based on SubValues created using SubValueDateGrouping property.

		' Common chart settings.
		Chart.Size = "600x350"
		Chart.Title = "<block hAlign='left'>2002 Sales<br>  Hover over bars for details."
		Chart.ShowDateInTitle = False
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.YAxis.FormatString = "Currency"
		Chart.DefaultElement.DefaultSubValue.Visible = False
		Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
		Chart.TitleBox.Padding = 8
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Background2
		Chart.TitleBox.Background.Color = Color.LightSteelBlue
		Chart.ShadingEffectMode = ShadingEffectMode.Three

		' See if this chart is to stream for tooltip.
		Dim stream As String = HttpContext.Current.Request.QueryString("stream")
		If Not stream Is Nothing AndAlso stream = "true" Then
			' Specify setting for the streamed version of this chart.
            Chart.Title = "<block hAlign='left'>" + HttpContext.Current.Request.QueryString("startDate") + " 2002 Sales"
			Chart.UseFile = False
			Chart.Background.Color = Color.Transparent

			' Parse the values passed in the query strying and add them as elements to the chart.
			Dim vals As String = HttpContext.Current.Request.QueryString("vals")
			If Not vals Is Nothing AndAlso vals.Length > 0 Then
				Dim valsAr As String() = vals.Split(New String() { "," }, StringSplitOptions.RemoveEmptyEntries)
				Dim s As Series = New Series()
				Dim i As Integer = 1
				For Each val As String In valsAr
					Dim el As Element = New Element(i.ToString())
					Try
						el.YValue = CDbl(Single.Parse(val))
					Catch
					End Try
					s.Elements.Add(el)
					i += 1
				Next val
				Chart.SeriesCollection.Add(s)
			End If
		Else
			' if not streaming, show the main data. 

			Chart.ChartArea.Label.Text = "Hover bars for details or click bars for a full sized detail chart."

			' Tooltip definition.
			Chart.DefaultElement.ToolTip = "<Chart:Image src='FullChartTooltipsDBGrouping.aspx?startDate=%Name&vals=%SubValueList&stream=true' size='300x200'>"
			Chart.DefaultElement.URL = "?startDate=%Name&vals=%SubValueList&stream=true"
			Chart.DefaultElement.URLTarget = "_blank"
            Chart.SeriesCollection.Add(getLiveData())

		End If
	End Sub

	Function getLiveData() As Series
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.StartDate = New DateTime(2002, 1, 1, 0, 0, 0)
		de.EndDate = New DateTime(2002, 12, 31, 23, 59, 59)
		de.SqlStatement = "SELECT OrderDate,Total,Name,Quantity FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#"
		de.DataFields = "xAxis=OrderDate,yAxis=Total,Quantity"
		de.DateGrouping = TimeInterval.Year
		de.SubValueDateGrouping = TimeInterval.Days
		Return de.GetSeries()(0)
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
