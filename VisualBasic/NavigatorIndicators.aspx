<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script type="text/C#" runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using the navigator to show a finance chart indicators and volume that utilizes finance bars for improved performance.

		chart.Size = "640x600"
		chart.TempDirectory = "temp"
		chart.Debug = True
		chart.Palette = New Color() { Color.FromArgb(255, 47, 107, 220), Color.FromArgb(255, 30, 156, 255) }

		' The chart area legend can dynamically update based on mouse position and visible range.
		chart.LegendBox.Position = LegendBoxPosition.ChartArea
		chart.LegendBox.Template = "%ICON%NAME%VALUE"
		chart.DefaultElement.Marker.Visible = False
		' Setup main chart axis settings
		chart.YAxis.Scale = Scale.Range
		chart.YAxis.FormatString = "Currency"

		' A starting view
		chart.XAxis.Viewport.ScaleRange = New ScaleRange(New DateTime(2010, 1, 1), DateTime.Now)

		' Enable the navigator.
		chart.Navigator.Enabled = True

		' Database start date.
		Dim startDate As DateTime = New System.DateTime(2000, 12, 1, 0, 0, 0)
		' startDate = new System.DateTime(1980, 12, 1, 0, 0, 0);


		' ---- Add the main Data with moving average and bollinger bands.
		chart.SeriesCollection.Add(getPriceAndIndicators(startDate))

		' ---- Stochastic
		' Setup stochastic chart area
		Dim ca3 As ChartArea = New ChartArea()
		ca3.HeightPercentage = 25
		ca3.YAxis.ScaleRange = New ScaleRange(0, 100)
		ca3.YAxis.Scale = Scale.Normal
		ca3.YAxis.Markers.Add(New AxisMarker("", Color.FromArgb(30, Color.Green), 0, 20))
		ca3.YAxis.Markers.Add(New AxisMarker("", Color.FromArgb(30, Color.Red), 80, 100))
		' Specify the exact ticks to use.
		ca3.YAxis.ClearValues = True
		ca3.YAxis.ExtraTicks.Add(AxisTickCollection.FromValues(0, 20, 50, 80, 100))
		chart.ExtraChartAreas.Add(ca3)

		' Add the stochastic data.
		ca3.SeriesCollection.Add(getStochasticData(startDate))

		' ---- Volume
		Dim ca As ChartArea = New ChartArea()
		ca.HeightPercentage = 25
		ca.YAxis.DefaultTick.Label.Text = "<" & "%Value/1000000> M"

		Dim mySC2 As SeriesCollection = getVolumeData(startDate)
		ca.SeriesCollection.Add(mySC2)

		chart.ExtraChartAreas.Add(ca)

	End Sub

	Function getPriceAndIndicators(ByVal startDate As DateTime) As SeriesCollection
		Dim result As SeriesCollection = New SeriesCollection()
		Dim connectionString As String = ConfigurationSettings.AppSettings("DNCConnectionString")
        Dim SqlStatement As String = "SELECT * FROM MSFT WHERE TransDate >= #STARTDATE# "
		Dim de As DataEngine = New DataEngine(connectionString, SqlStatement)
		de.ChartObject = chart
        de.DataFields = "XDateTime=TransDate,YValue=AdjClose"
		de.StartDate = startDate
		Dim mySC As SeriesCollection = de.GetSeries()

		If mySC.Count > 0 Then
			' Apply price line settings.
			mySC(0).Type = SeriesType.Line
			mySC(0).Line.Width = 2
			mySC(0).LegendEntry.Marker.Visible = False
			mySC(0).DefaultElement.Marker.Visible = False
			mySC(0).Name = "MSFT"
			mySC(0).LegendEntry.Value = "%PercentageChange"

			' Calculate simple moving average
			Dim ma As Series = FinancialEngine.SimpleMovingAverage(mySC(0), ElementValue.YValue, 5)
			ma.Name = "Moving Average"
			ma.Line.Width = 1
			ma.DefaultElement.Color = Color.Purple
			ma.DefaultElement.Transparency = 50
			ma.Type = SeriesType.Line

			' Get Bollinger Bands as two series.
			Dim bands As SeriesCollection = StatisticalEngine.BollingerBands(ma, 2, 20)

			' Take one of the bollinger band series and use the values of the second series to set element.YValueStart of the first series.
			' Only the first series will be added to the chart as it will contain the info of both.
			Dim BBans As Series = bands(0)
			BBans.DefaultElement.Color = Color.Red
			BBans.LegendEntry.Value = ""
			BBans.Type = SeriesType.AreaLine
			BBans.DefaultElement.Transparency = 70
			BBans.Name = "Bollinger Bands"
			For i As Integer = 0 To bands(0).Elements.Count - 1
				BBans(i).YValueStart = Math.Round(bands(1)(i).YValue, 2)
				BBans(i).YValue = Math.Round(BBans(i).YValue, 2)
			Next i

			result.Add(BBans)
			result.Add(mySC(0))
			result.Add(ma)
		End If


		Return result
	End Function

	Function getStochasticData(ByVal startDate As DateTime) As SeriesCollection
		Dim msftFull As Series = getFullMSFT(startDate)

		Dim kFast As Series = FinancialEngine.KFastStochastic(msftFull, 10)
		kFast.Type = SeriesType.Line
		kFast.Line.Width = 1
		kFast.DefaultElement.Color = Color.Blue
		kFast.LegendEntry.Value = ""
		kFast.DefaultElement.SmartLabel.Text = "%YValue%"

		Dim dStochastic As Series = FinancialEngine.DStochastic(msftFull, 10, 4, 5)
		dStochastic.Type = SeriesType.Line
		dStochastic.Line.Width = 1
		dStochastic.DefaultElement.Color = Color.Red
		dStochastic.LegendEntry.Value = ""
		dStochastic.DefaultElement.SmartLabel.Text = "%YValue%"

		Dim result As SeriesCollection = New SeriesCollection()
		result.Add(kFast, dStochastic)
		Return result
	End Function

	Function getVolumeData(ByVal startDate As DateTime) As SeriesCollection
		Dim connectionString As String = ConfigurationSettings.AppSettings("DNCConnectionString")
        Dim SqlStatement As String = "SELECT TransDate,Volume FROM MSFT WHERE TransDate >= #STARTDATE# "
		Dim de2 As DataEngine = New DataEngine(connectionString, SqlStatement)
		de2.ChartObject = chart
		de2.StartDate = startDate
        de2.DataFields = "XDateTime=TransDate,YValue=Volume"
		Dim mySC As SeriesCollection = de2.GetSeries()

		' Apply volume series settings.
		If mySC.Count > 0 Then
			mySC(0).Type = SeriesTypeFinancial.Bar

			' In order to improve the performance of this chart, the volume series will utilize the finance bars instead of full columns.
			For Each el As Element In mySC(0).Elements
				' This applies the y value to high and 0 to low so that the finance bars appear as a column series would.
				el.High = el.YValue
				el.Low = 0
			Next el

			mySC(0).Name = "Volume"
			' This ensures the chart area does not calculate values for ranges in the chart area legend.
			mySC(0).LegendEntry.Value = ""
			' The legend in the chart area will show the element's high value which is the same as the y value of a column series.
			mySC(0).DefaultElement.SmartLabel.Text = "%High"
		End If
		Return mySC
	End Function

	Function getFullMSFT(ByVal startDate As DateTime) As Series
		Dim connectionString As String = ConfigurationSettings.AppSettings("DNCConnectionString")

		'///////////// Add the main Data with moving average and bollinger bands.
        Dim SqlStatement As String = "SELECT * FROM MSFT WHERE TransDate >= #STARTDATE# "
        Dim de As DataEngine = New DataEngine(connectionString, SqlStatement)
		de.ChartObject = chart
        de.DataFields = "XDateTime=TransDate,Open=OpenPrice,Close=ClosePrice,High=HighPrice,Low=LowPrice,Volume=Volume"
		de.StartDate = startDate

		Return de.GetFinancialSeries()(0)
	End Function


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="chart" runat="server" />
		<br />
		<asp:Label ID="Label1" runat="server" />
	</div>
</body>
</html>
