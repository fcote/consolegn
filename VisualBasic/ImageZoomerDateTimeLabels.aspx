<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)


	Chart.Width = 600
	Chart.Height = 400
	Chart.TempDirectory = "temp"
	Chart.Title = "Sales totals"
	Chart.Debug = True

	' Enable Scrolling
	Chart.XAxis.Viewport.TimeInterval = New TimeIntervalAdvanced(New TimeSpan(30, 0, 0, 0))
	Chart.Zoomer.Enabled = True

	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
	Chart.LegendBox.Template = "%Icon%Name"
	Chart.DefaultElement.Marker.Size = 5
	Chart.DefaultSeries.DefaultElement.Transparency = 30

	Chart.DefaultSeries.Type = SeriesType.AreaLine
	Chart.YAxis.Scale = Scale.Stacked
	Chart.YAxis.Label.Text = "Products sold"

	' Modify the x axis labels.
	Chart.XAxis.Scale = Scale.Time
	Chart.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Dynamic
	Chart.XAxis.TimeScaleLabels.DayFormatString = "o"
	Chart.XAxis.TimeScaleLabels.MonthFormatString = "MMM"
	Chart.XAxis.TimeInterval = TimeInterval.Day
	Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month)

	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Dim mySC As SeriesCollection = getRandomData()

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)

End Sub

Function getRandomData() As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random()

	For a As Integer = 0 To 3
		Dim s As Series = New Series()
Dim startDT As DateTime = New DateTime(2008,1,1)
		s.Name = "Product " & (a+1).ToString()
		For b As Integer = 0 To 99
			Dim e As Element = New Element()
							 e.XDateTime = startDT
							  startDT = startDT.AddDays(1)

			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)
		Return SC
End Function
		</script>
	</head>
	<body>
		<center>
 <dotnet:Chart id="Chart"  runat="server"/>
		</center>
	</body>
</html>
