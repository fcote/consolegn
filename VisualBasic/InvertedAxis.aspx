<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + CStr(a)
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + CStr(b)
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   
   Return SC
End Function 'getRandomData



Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Use3D = True
   
   ' First we get our data.
   Dim sc As SeriesCollection = getRandomData()
   
   ' In order to invert the axes we will simulate this with 2 steps. 
   ' 1. We will negate the y values of our data.
   Dim s As Series
   For Each s In  sc
      Dim el As Element
      For Each el In  s.Elements
         el.YValue = - el.YValue
      Next el 
   Next s ' Now add the data
   Chart.SeriesCollection.Add(sc)
   
   ' 2. We will now change the string tick labels of the y axis so they look like positives when they are negative.
   Chart.YAxis = Chart.YAxis.Calculate("Y values",New ChangeValueDelegate(AddressOf NegateAxis))
   
   
   ' More: You can try different series types by changing the following line
   Chart.DefaultSeries.Type = SeriesType.Cylinder
End Sub 'Page_Load
 



Function NegateAxis(val As String) As String
   ' Convert the string number to a double and make it negative.
   Dim myNum As Double = - Convert.ToDouble(val)
   
   Return myNum.ToString()
End Function 'NegateAxis


</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Inverted Axis</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
