<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

		' This sample demonstrates multiple shape grouping.
		Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
			Chart.Type = ChartType.Map
			Chart.Size = "850x440"
			Chart.Title = "World Regions"
			Chart.TempDirectory = "temp"
			Chart.Debug=True
			Chart.TitleBox.Position = TitleBoxPosition.Full
			Chart.LegendBox.Visible = False

			Dim layer As MapLayer = MapDataEngine.LoadLayer("../../images/MapFiles/world.shp")
			Chart.Mapping.DefaultShape.Line.Color = Color.Transparent

			Chart.Mapping.ZoomCenterPoint = New PointF(13, 0)
			Chart.Mapping.ZoomPercentage = 101


			Dim gr As dotnetCHARTING.Mapping.Group = New dotnetCHARTING.Mapping.Group("REGION", "Europe")
			Dim gr2 As dotnetCHARTING.Mapping.Group = New dotnetCHARTING.Mapping.Group("REGION", "Asia")
			Dim gr3 As dotnetCHARTING.Mapping.Group = New dotnetCHARTING.Mapping.Group("REGION", "North America")
			Dim gr4 As dotnetCHARTING.Mapping.Group = New dotnetCHARTING.Mapping.Group("REGION", "Antarctica")
			Dim gr5 As dotnetCHARTING.Mapping.Group = New dotnetCHARTING.Mapping.Group("REGION", "Latin America")
			Dim gr6 As dotnetCHARTING.Mapping.Group = New dotnetCHARTING.Mapping.Group("REGION", "Australia")
			Dim gr7 As dotnetCHARTING.Mapping.Group = New dotnetCHARTING.Mapping.Group("REGION", "NorthAfrica")
			Dim gr8 As dotnetCHARTING.Mapping.Group = New dotnetCHARTING.Mapping.Group("REGION", "Sub Saharan Africa")

			gr.DefaultShape.Background.Color = Color.FromArgb(245,106,13)
			gr2.DefaultShape.Background.Color = Color.FromArgb(254,210,29)
			gr3.DefaultShape.Background.Color = Color.FromArgb(126, 177, 27)
			gr4.DefaultShape.Background.Color = Color.Transparent
			gr5.DefaultShape.Background.Color = Color.FromArgb(61, 55, 108)
			gr6.DefaultShape.Background.Color = Color.FromArgb(50, 96, 182)
			gr7.DefaultShape.Background.Color = Color.FromArgb(52,130,114)
			gr8.DefaultShape.Background.Color = Color.FromArgb(52, 130, 114)


			layer.Groups.Add(gr)
			layer.Groups.Add(gr2)
			layer.Groups.Add(gr3)
			layer.Groups.Add(gr4)
			layer.Groups.Add(gr5)
			layer.Groups.Add(gr6)
			layer.Groups.Add(gr7)
			layer.Groups.Add(gr8)
			Chart.Mapping.MapLayerCollection.Add(layer)


			Dim lb As LegendBox = New LegendBox()
			lb.ClearColors()
			lb.Position = New Point(50, 270)
			lb.DefaultEntry.Background.ShadingEffectMode = ShadingEffectMode.One
			Dim le As LegendEntry = New LegendEntry("Europe", "", Color.FromArgb(245, 106, 13))
			lb.ExtraEntries.Add(New LegendEntry("Europe", "", Color.FromArgb(245, 106, 13)))
			lb.ExtraEntries.Add(New LegendEntry("Asia", "", Color.FromArgb(254, 210, 29)))
			lb.ExtraEntries.Add(New LegendEntry("North America", "", Color.FromArgb(126, 177, 27)))
			lb.ExtraEntries.Add(New LegendEntry("Latin America", "", Color.FromArgb(61, 55, 108)))
			lb.ExtraEntries.Add(New LegendEntry("Australia", "", Color.FromArgb(50, 96, 182)))
			lb.ExtraEntries.Add(New LegendEntry("Africa", "", Color.FromArgb(52, 130, 114)))
			Chart.ExtraLegendBoxes.Add(lb)

		End Sub

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
