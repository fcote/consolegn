<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using AreaLine series to draw four quadrants on the chart.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.Type = ChartType.Combo
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"

		Chart.DefaultSeries.Type = SeriesType.Marker
		Chart.YAxis.ScaleRange = New ScaleRange(0, 100)
		Chart.XAxis.ScaleRange = New ScaleRange(0, 100)

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		Chart.SeriesCollection.Add(getQuadrantAreas())
		' Add the random data.
		Chart.SeriesCollection.Add(mySC)


	End Sub

	Function getQuadrantAreas() As SeriesCollection
		Dim s As Series = New Series()
		s.Type = SeriesType.AreaLine
		Dim e1 As Element = New Element()
		e1.YValue = 50
		e1.YValueStart = 0
		e1.XValue = 0
		Dim e2 As Element = New Element()
		e2.YValue = 50
		e2.YValueStart = 0
		e2.XValue = 50
		s.Elements.Add(e1, e2)

		Dim s2 As Series = New Series()
		s2.Type = SeriesType.AreaLine
		Dim e3 As Element = New Element()
		e3.YValue = 100
		e3.YValueStart = 50
		e3.XValue = 0
		Dim e4 As Element = New Element()
		e4.YValue = 100
		e4.YValueStart = 50
		e4.XValue = 50
		s2.Elements.Add(e3, e4)

		Dim s3 As Series = New Series()
		s3.Type = SeriesType.AreaLine
		Dim e5 As Element = New Element()
		e5.YValue = 100
		e5.YValueStart = 50
		e5.XValue = 50
		Dim e6 As Element = New Element()
		e6.YValue = 100
		e6.YValueStart = 50
		e6.XValue = 100
		s3.Elements.Add(e5, e6)

		Dim s4 As Series = New Series()
		s4.Type = SeriesType.AreaLine
		Dim e7 As Element = New Element()
		e7.YValue = 50
		e7.YValueStart = 0
		e7.XValue = 50
		Dim e8 As Element = New Element()
		e8.YValue = 50
		e8.YValueStart = 0
		e8.XValue = 100
		s4.Elements.Add(e7, e8)

		s.DefaultElement.Marker.Visible = False
		s2.DefaultElement.Marker.Visible = False
		s3.DefaultElement.Marker.Visible = False
		s4.DefaultElement.Marker.Visible = False
		s.DefaultElement.Transparency = 70
		s2.DefaultElement.Transparency = 70
		s3.DefaultElement.Transparency = 70
		s4.DefaultElement.Transparency = 70
		s.LegendEntry.Visible = False
		s2.LegendEntry.Visible = False
		s3.LegendEntry.Visible = False
		s4.LegendEntry.Visible = False

		Dim sc As SeriesCollection = New SeriesCollection()
		sc.Add(s, s2, s3, s4)

		Return sc


	End Function

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 4
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 4
				Dim e As Element = New Element()
				e.YValue = myR.Next(100)
				e.XValue = myR.Next(100)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
