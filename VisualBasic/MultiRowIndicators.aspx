<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using multiple rows of indicator lights on a chart.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(0, 156, 255), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49) }

		Chart.Type = ChartType.Gauges
		Chart.DefaultSeries.GaugeType = GaugeType.IndicatorLight
		Chart.Size = "230x400"
		Chart.Title = ".netCHARTING Sample"
		Chart.LegendBox.Visible = False

		Chart.DefaultElement.Marker.Size = 35
		Chart.DefaultAxis.DefaultTick.Label.Font = New Font("Arial", 12, FontStyle.Italic)
		Chart.DefaultSeries.GaugeBorderBox.ClearColors()
		Chart.DefaultElement.SmartLabel.Type = LabelType.Digital
		Chart.DefaultElement.SmartLabel.Color = Color.White
		Chart.DefaultElement.SmartLabel.Font = New Font("Arial", 15, FontStyle.Bold)
		Chart.DefaultElement.ShowValue = True
		Chart.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center

		Dim sc As SmartColor = New SmartColor(Color.Red, New ScaleRange(0, 0))
		sc.LegendEntry.Visible = False
		Chart.SmartPalette.Add("Series 1", sc)
		Chart.SmartPalette.Add("*", New SmartColor(Color.Orange, New ScaleRange(0, 50)))

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		mySC(1).Name = "Change"
		mySC(1).DefaultElement.ShowValue = False
		mySC(1).DefaultElement.ForceMarker = True

		mySC(0).XAxis = New Axis()
		mySC(0).DefaultElement.SmartLabel.Alignment = LabelAlignment.Center
		mySC(0).DefaultElement.ShowValue = True

		For Each el As Element In mySC(1).Elements
			If el.YValue > 40 AndAlso el.YValue < 60 Then
				el.Marker = New ElementMarker("../../images/minusSign.png")
			ElseIf el.YValue >= 60 Then
				el.Color = Chart.Palette(0)
				el.Marker = New ElementMarker("../../images/upArrow.png")
			ElseIf el.YValue <= 40 Then
				el.Color = Chart.Palette(3)
				el.Marker = New ElementMarker("../../images/downArrow.png")
			End If
		Next el

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(3)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 2
			Dim s As Series = New Series("Series " & a.ToString())
			s.XAxis = New Axis()
			s.XAxis.ClearValues = True
			s.DefaultElement.ShowValue = True

			For b = 1 To 7
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(100)
				If myR.Next(3) = 2 AndAlso a = 1 Then
					e.YValue = 0
				End If
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
