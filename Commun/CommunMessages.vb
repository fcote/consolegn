Imports Microsoft.VisualBasic
Imports System.data
Imports System.configuration

Namespace Messagerie

    Public Class CommunMessages

        Public Class StatutsMail
            Public Const Normal As String = "X"
            Public Const Efface As String = "E"
            Public Const Retire As String = "R"
        End Class

        'Public Const CONN_STRING As String = "data source=SPTRDV; user id=gn; Password=gn4000;Pooling=false"
        'Public Const CONN_STRING_PP As String = "Data Source=test;User ID=portailedu;password=portailedu;pooling=false;"

        'Public Shared ReadOnly Property CONN_STRING_PP() As String
        '    Get
        '        Dim apr As AppSettingsReader = New AppSettingsReader

        '        Dim db As String = String.Empty

        '        Try
        '            Return apr.GetValue("ConnOracle", System.Type.GetType("System.String")).ToString()
        '        Catch e As Exception
        '            Dim s As String = "La base de donn�es � utiliser n'a pu �tre identifi�e.  La section Connexion de web.config n'est peut-�tre pas existante ou valide."
        '            Throw New Exception(s, e)
        '        End Try
        '    End Get
        'End Property

        'Public Shared ReadOnly Property CONN_STRING() As String
        '    Get
        '        Dim apr As AppSettingsReader = New AppSettingsReader

        '        Dim db As String = String.Empty

        '        Try
        '            db = apr.GetValue("Connexion", System.Type.GetType("System.String")).ToString()
        '        Catch e As Exception
        '            Dim s As String = "La base de donn�es � utiliser n'a pu �tre identifi�e.  La section Connexion de web.config n'est peut-�tre pas existante ou valide."
        '            Throw New Exception(s, e)
        '        End Try

        '        Dim cs As ConnectionStringsSection = CType(ConfigurationManager.GetSection("connectionStrings"), ConnectionStringsSection)

        '        Try
        '            Return cs.ConnectionStrings(db).ConnectionString
        '        Catch ex As Exception
        '            Dim s As String = "Il est impossible de d�terminer les param�tres de connexion � l'aide de l'�l�ment Connexion fourni dans le fichier web.config (" & IIf(db.Length > 0, db, "vide") & ")."
        '            Throw New Exception(s)
        '        End Try
        '    End Get
        'End Property


        Public Const DATE_FORMAT As String = "dd-MM-yyyy tt HH:mm"

        Public Enum Typesdevue
            Inbox
            Envoyes
        End Enum

        Public Shared Function genererNouvelleFenetre(ByVal dataitemindex As String, ByVal mode As String, ByVal source As String, ByVal parentApp As String, ByVal enregistrer As Boolean) As String

            Dim js As String = "var child = window.open('msg.aspx?dii=" & dataitemindex & "&mode=" & mode & "&source=" & source & "&parentapp=" & parentApp & "', '', 'toolbar=no,width=750,height=700');return false;"

            Return js

        End Function

        Public Shared Function genererFenetreContacts(ByVal parentApp As String, ByVal enregistrer As Boolean) As String

            Dim js As String = String.Empty

            If parentApp = "AE" Then
                js = "var child = window.open('ChoisirContacts_AE.aspx', '', 'toolbar=no,width=1500,height=1024,scrollbars=yes,resizable=yes');"
            Else
                js = "var child = window.open('ChoisirContacts_PP.aspx', '', 'toolbar=no,width=750,height=700,scrollbars=yes,resizable=yes');"
            End If

            Return js

        End Function

    End Class

End Namespace
