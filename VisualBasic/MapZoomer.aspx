<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

		' This sample demonstrates using the AJAX zoomer with maps.
		Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
			Chart.Type = ChartType.Map
			Chart.Size = "640x480"
			Chart.Title = "Chart Sample: Right-Click to zoom."
			Chart.TempDirectory = "temp"
			Chart.Debug=True
			Chart.TitleBox.Position = TitleBoxPosition.Full
			Chart.LegendBox.Visible = False

			Chart.Zoomer.Enabled = True

			Dim layer As MapLayer = MapDataEngine.LoadLayer("../../images/MapFiles/primusa.shp")

			layer.PaletteName = Palette.FiveColor11

			Chart.Mapping.MapLayerCollection.Add(layer)

		End Sub

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
