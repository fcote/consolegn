<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Combo
   Chart.Size = "1200x500"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample: Image Bars"
   Chart.LegendBox.Visible = False
   Chart.DefaultAxis.SpacingPercentage = 3
   
   ' This sample demonstrates how to load and set image bar template from the cache.
   ' Add the random data.
   Dim mySC As SeriesCollection = getRandomData()
   
   
	mySC(0).ImageBarTemplate = "../../images/ImageBarTemplates/book2"
	mySC(1).ImageBarTemplate = "../../images/ImageBarTemplates/book"
	mySC(2).ImageBarTemplate = "../../images/ImageBarTemplates/bottle"
	mySC(3).ImageBarTemplate = "../../images/ImageBarTemplates/building"
	mySC(4).ImageBarTemplate = "../../images/ImageBarTemplates/candle"
	mySC(5).ImageBarTemplate = "../../images/ImageBarTemplates/chimney"
	mySC(6).ImageBarTemplate = "../../images/ImageBarTemplates/chocolate"
	mySC(7).ImageBarTemplate = "../../images/ImageBarTemplates/clock"
	mySC(8).ImageBarTemplate = "../../images/ImageBarTemplates/coins"
	mySC(9).ImageBarTemplate = "../../images/ImageBarTemplates/daisy"
	mySC(10).ImageBarTemplate = "../../images/ImageBarTemplates/disks"
	mySC(11).ImageBarTemplate = "../../images/ImageBarTemplates/envelope"
	mySC(12).ImageBarTemplate = "../../images/ImageBarTemplates/hat"
	mySC(13).ImageBarTemplate = "../../images/ImageBarTemplates/lipstick"
	mySC(14).ImageBarTemplate = "../../images/ImageBarTemplates/log"
	mySC(15).ImageBarTemplate = "../../images/ImageBarTemplates/man"
	mySC(16).ImageBarTemplate = "../../images/ImageBarTemplates/man_nolegs"
	mySC(17).ImageBarTemplate = "../../images/ImageBarTemplates/man_resized"
	mySC(18).ImageBarTemplate = "../../images/ImageBarTemplates/man_tiled"
	mySC(19).ImageBarTemplate = "../../images/ImageBarTemplates/palm"
	mySC(20).ImageBarTemplate = "../../images/ImageBarTemplates/pen"
	mySC(21).ImageBarTemplate = "../../images/ImageBarTemplates/pine"
	mySC(22).ImageBarTemplate = "../../images/ImageBarTemplates/rose"
	mySC(23).ImageBarTemplate = "../../images/ImageBarTemplates/screw"
	mySC(24).ImageBarTemplate = "../../images/ImageBarTemplates/star"
	mySC(25).ImageBarTemplate = "../../images/ImageBarTemplates/test_tube"
	mySC(26).ImageBarTemplate = "../../images/ImageBarTemplates/tire"
	mySC(27).ImageBarTemplate = "../../images/ImageBarTemplates/black_pencil"
	mySC(28).ImageBarTemplate = "../../images/ImageBarTemplates/blue_pencil"
	mySC(29).ImageBarTemplate = "../../images/ImageBarTemplates/candy"
   
   
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 30
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 1
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = myR.Next(50) + 50
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   Return SC
End Function 'getRandomData
</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server">
			</dotnet:Chart>			

		</div>
	</body>
</html>
