<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample is used to provide a dynamic chart image for sample EmbeddedLegendCharts.aspx

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.SmallChartMode = True
		Chart.ShadingEffectMode = ShadingEffectMode.Three
		Chart.UseFile = False
		Chart.Type = ChartType.Gauges

		Chart.DefaultSeries.GaugeType = GaugeType.Horizontal
		Chart.DefaultSeries.Type = SeriesType.BarSegmented
		Chart.Size = "100x20"
		Chart.DefaultSeries.GaugeBorderBox.ClearColors()
		Chart.DefaultSeries.GaugeBorderBox.Position = New Rectangle(0, 0, 100, 18)
		Chart.Background.Color = Color.Transparent
		Chart.DefaultSeries.GaugeBorderBox.Background.Color = Color.Transparent
		Chart.ChartArea.Background.Color = Color.Transparent
		Dim value As Double = 0

		If Not Request.QueryString("value") Is Nothing Then
			Dim query As String = Request.QueryString("value")
			value = Double.Parse(query)
			If Not Request.QueryString("max") Is Nothing Then
				Dim maxQuery As String = Request.QueryString("max")
				Dim max As Double = Double.Parse(maxQuery)
				Chart.YAxis.Maximum = max
			End If

			Dim el As Element = New Element()
			el.YValue = value
			Chart.YAxis.ClearValues = True
			Chart.SeriesCollection.Add(New Series("", el))

		Else
			Chart.Visible = False
			Label1.Text = "This chart is designed to display as a sub-chart within EmbeddedLegendCharts.aspx. Please view that page instead."
		End If
	End Sub

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
	<asp:Label ID="Label1" runat="server" />
</body>
</html>
