<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   '  This sample demonstrates image bars, title box customization,
   '  image background and axis label outline usage.
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Size = "500x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Construction Housing Starts"
   
   
   ' TitleBox Customization
   Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   Chart.TitleBox.Background = New Background(Color.White, Color.LightGray, 90)
   Chart.TitleBox.Label.OutlineColor = Color.LightGray
   Chart.TitleBox.CornerTopLeft = BoxCorner.Round
   Chart.TitleBox.CornerTopRight = BoxCorner.Round
   
   
   ' The following line sets the image template.
   Chart.DefaultSeries.ImageBarTemplate = "../../Images/ImageBarTemplates/screw"
   
   ' Background image for illusion of screws sinking into a wood board
   Chart.Background = New Background("../../Images/woodboard.jpg", BackgroundMode.Image)
   
   ' Enhance label visibility with an outline  
   Chart.XAxis.TickLabel.OutlineColor = Color.Orange
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 1
      Dim s As New Series()
      s.Name = "Total " + a.ToString()
      Dim b As Integer
      For b = 1 To 6
         Dim e As New Element()
         e.Name = "Street " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   
   
   Return SC
End Function 'getRandomData

</script>



<html xmlns="http://www.w3.org/1999/xhtml"><head><title>.netCHARTING Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
