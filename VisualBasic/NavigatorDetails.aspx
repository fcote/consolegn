<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using a regular chart grouped by month to send date info to the navigator and control its movement.

		Chart.Size = "600x300"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Mentor = False
		Chart.TitleBox.ClearColors()
		Chart.LegendBox.Visible = False
		Chart.ChartArea.Label.Text = "Click a bar to view the daily values for the month"

        Chart.DefaultElement.Hotspot.Attributes.Custom.Add("OnClick", "Javascript:updateMonth('%XValue');")

		Dim mySC As SeriesCollection = getLiveData(TimeInterval.Months)
		mySC(0).PaletteName = Palette.Bright

		' Add the data.
		Chart.SeriesCollection.Add(mySC)

		Chart1.Size = "700x350"
		Chart1.TempDirectory = "temp"
		Chart1.PaletteName = Palette.Bright
		Chart1.Debug = True
		Chart1.LegendBox.Position = LegendBoxPosition.ChartArea

		' Enable the Navigator
		Chart1.Navigator.Enabled = True

		' The control ID is specified so it can be referenced in javascript.
		Chart1.Navigator.ControlID = "slPlugin"

		' Specify the start date range.
		Chart1.XAxis.Viewport.ScaleRange = New ScaleRange(New DateTime(2002, 1, 1), New DateTime(2002, 2, 1))

		'Disable navigator navigation.
		Chart1.Navigator.NavigationBar.Visible = False
		Chart1.Navigator.EnableXScrollbar = False
		Chart1.Navigator.NavigationBar.Visible = False
		Chart1.Navigator.PreviewAreaNavigationOptons = 0
		Chart1.ChartArea.NavigatorOptions = 0

		Dim mySC2 As SeriesCollection = getLiveData1(TimeInterval.Days)

		' Add the data.
		Chart1.SeriesCollection.Add(mySC2)

		For Each el As Element In mySC2(0).Elements
			' Color each element based on the month
			el.Color = Chart1.Palette(el.XDateTime.Month - 1)

			' Offset the elements so they appear in the middle of the day
			el.XDateTime = el.XDateTime.AddHours(12)
		Next el
	End Sub


	Function getLiveData(ByVal grouping As TimeInterval) As SeriesCollection
		Dim low As DateTime = New DateTime(2002, 1, 1)
        Dim high As DateTime = New DateTime(2002, 12, 31, 23, 59, 59)
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.StartDate = low
		de.EndDate = high
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.DateGrouping = grouping
		de.SqlStatement = "SELECT OrderDate,Total FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#"
		de.DataFields = "XDateTime=OrderDate,YValue=Total"
		Return de.GetSeries()
	End Function
	Function getLiveData1(ByVal grouping As TimeInterval) As SeriesCollection
		Dim low As DateTime = New DateTime(2002, 1, 1)
        Dim high As DateTime = New DateTime(2002, 12, 31, 23, 59, 59)
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.StartDate = low
		de.EndDate = high
		de.ChartObject = Chart1 ' Necessary to view any errors the dataEngine may throw.
		de.DateGrouping = grouping
		de.SqlStatement = "SELECT OrderDate,Total FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#"
		de.DataFields = "XDateTime=OrderDate,YValue=Total"
		Return de.GetSeries()
	End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<style type="text/css">
	div, p
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: x-small;
	}
</style>
<script type="text/javascript">

	// For more information on this code and a client side API reference, see this tutorial:
	// http://www.dotnetcharting.com/documentation/v6_0/Silverlight_Navigator_API.html

	var chartID = "slPlugin";

	// Allows getting the month names
	Date.prototype.getMonthName = function () {
		var m = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		return m[this.getMonth()];
	}

	// This function is called by the static chart and controls the silverlight chart movement.
	function updateMonth(date) {
		var control = document.getElementById(chartID);

		var leftDate = new Date();
		leftDate.setTime(Date.parse(date));
		leftDate.setDate(leftDate.getDate() + 1);
		leftDate.setHours(-6);

		var tmpDate = new Date();
		tmpDate.setTime(Date.parse(date));
		tmpDate.setDate(1);
		tmpDate.setMonth(tmpDate.getMonth() + 1);
		tmpDate.setHours(-6);

		control.Content.Chart.NavigateToVisibleRange(leftDate, tmpDate, 700);
		document.getElementById('myDiv').innerHTML = "<strong>Details for " + tmpDate.getMonthName() + " " + tmpDate.getYear() + "</strong>";
	}

</script>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
		<table style="width: 700">
			<tr>
				<td>
					<hr />
				</td>
			</tr>
			<tr>
				<td>
					<div id="myDiv">
						<strong>Details for January 2002 </strong>
					</div>
				</td>
			</tr>
		</table>
		<dnc:Chart ID="Chart1" runat="server" />
	</div>
</body>
</html>
