<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<%@ Import Namespace="dotnetCHARTING"%>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Financial DM Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of the DM+ and DM- indicators.
   
   ' First we declare a chart of type FinancialChart	
   ' The Financial Chart
   FinancialChart.Title = "Directional Movement Chart"
   FinancialChart.TempDirectory = "temp"
   FinancialChart.Debug = True
   FinancialChart.ShadingEffect = True
   FinancialChart.LegendBox.Template = "%icon %name"
   FinancialChart.Size = "800X400"
   FinancialChart.XAxis.Scale = Scale.Time
   ' For financial indicators the time scale is inverted (i.e. the first element of the series is the newest)
   FinancialChart.XAxis.InvertScale = True
   FinancialChart.XAxis.FormatString = "MMM d"
   FinancialChart.XAxis.TimeInterval = TimeInterval.Day
   
   FinancialChart.YAxis.Label.Text = "Price (USD)"
   FinancialChart.YAxis.FormatString = "currency"
   FinancialChart.YAxis.ScaleRange.ValueLow = 16.0
   FinancialChart.YAxis.ScaleRange.ValueHigh = 32.0
   FinancialChart.YAxis.Scale = Scale.Range
   
   ' Here we load data samples from the FinancialCompany table from within chartsample.mdb
   Dim priceDataEngine As New DataEngine()
   priceDataEngine.ChartType = ChartType.Financial
   priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   priceDataEngine.DateGrouping = TimeInterval.Day
   priceDataEngine.StartDate = New DateTime(2001, 10, 1)
   priceDataEngine.EndDate = New DateTime(2001, 12, 31)
   priceDataEngine.SqlStatement = "SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate"
  priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice"
   
   Dim sc As SeriesCollection = priceDataEngine.GetSeries()
   Dim prices As Series = Nothing
   If sc.Count > 0 Then
      prices = sc(0)
   Else
      Return
   End If 
   prices.DefaultElement.ToolTip = "L:%Low-H:%High"
   prices.DefaultElement.SmartLabel.Font = New Font("Arial", 6)
   prices.DefaultElement.SmartLabel.Text = "O:%Open-C:%Close"
   prices.Type = SeriesTypeFinancial.Bar
   
   Dim cp As New CalendarPattern(TimeInterval.Day, TimeInterval.Week, "0000001")
   cp.AdjustmentUnit = TimeInterval.Day
   prices.Trim(cp, ElementValue.XDateTime)
   prices.Name = "Prices"
   FinancialChart.SeriesCollection.Add(prices)
   
   ' Financial Series DMI Signal
   FinancialChart.DefaultSeries.DefaultElement.Marker = New ElementMarker(ElementMarkerType.None)
   'FinancialChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;
   FinancialChart.DefaultSeries.Type = SeriesType.Line
   
   ' Here we display the financial simple moving average for the prices series over five days
   Dim movingAverage As Series = FinancialEngine.SimpleMovingAverage(prices, ElementValue.High, 10)
   FinancialChart.SeriesCollection.Add(movingAverage)
   
   ' Create a new chart area for DM+ and DM-.
   Dim dmChartArea As New ChartArea("Directional Movement")
   dmChartArea.HeightPercentage = 20
   dmChartArea.YAxis = New Axis()
   dmChartArea.YAxis.ScaleRange.ValueLow = 0
   dmChartArea.YAxis.ScaleRange.ValueHigh = 0.8
   FinancialChart.ExtraChartAreas.Add(dmChartArea)
   
   ' Here we display the DM+
   Dim plusDM As Series = FinancialEngine.PlusDirectionalMovement(prices)
   plusDM.Type = SeriesType.Spline
   plusDM.DefaultElement.Color = Color.FromArgb(0, 255, 0)
   If plusDM.Elements.Count > 0 Then
      dmChartArea.SeriesCollection.Add(plusDM)
   Else
      Console.WriteLine("The series plusDM is empty")
   End If 
   
   ' Here we display the DM-
   Dim minusDM As Series = FinancialEngine.MinusDirectionalMovement(prices)
   minusDM.Type = SeriesType.Spline
   minusDM.DefaultElement.Color = Color.FromArgb(0, 0, 255)
   If minusDM.Elements.Count > 0 Then
      dmChartArea.SeriesCollection.Add(minusDM)
   Else
      Console.WriteLine("The series minusDM is empty")
   End If 
   ' Here we display the DMI Signal. For calcualating the DMI Signal we are using the 
   ' Simple Moving Average method(method =1) over a period of 10 days.
   Dim dmiSignal As Series = FinancialEngine.DMISignal(prices, 1, 10)
   dmiSignal.Type = SeriesType.Marker
   dmiSignal.LegendEntry.Visible = False
   If dmiSignal.Elements.Count > 0 Then
      ' We add the movingAverage series to dmiSignal in order to represents
      ' dmiSignal on to the FinancialChart. We did this because the YValues of the
      ' dmiSignal take the values of: 1, 0, or -1 representing a buy, no action or sell
      ' signal.
      FinancialChart.SeriesCollection.Add((Series.Add(dmiSignal, movingAverage)))
   Else
      Console.WriteLine("The series dmiSignal is empty")
   End If 
End Sub 'Page_Load
		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="FinancialChart" runat="server"/>
		</div>
	</body>
</html>