<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   ' This sample will demonstrate how axis ticks can show data totals.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Setup the y axis and chart colors.
   Chart.YAxis.Scale = Scale.FullStacked
   Chart.PaletteName = Palette.Three
   
   ' Create the shadow axis and add it.
   Dim shadow As Axis = Chart.YAxis.Calculate("Totals")
   shadow.Orientation = dotnetCHARTING.Orientation.Right
   shadow.ClearValues = True
   Chart.AxisCollection.Add(shadow)
   
   ' Do some calculations
   Dim sum As Double = mySC.Calculate("", Calculation.Sum).Calculate("", Calculation.Sum).YValue
   Dim s1Sum As Double = mySC(0).Calculate("", Calculation.Sum).YValue
   Dim s2Sum As Double = mySC(1).Calculate("", Calculation.Sum).YValue
   Dim s3Sum As Double = mySC(2).Calculate("", Calculation.Sum).YValue
   Dim runningSum As Double = 0
   Dim currentValue As Double = 0
   
   currentValue = s1Sum * 100 / sum
   
   ' Create the axis ticks and set their colors based on the chart palette.
   Dim tickA As New AxisTick(runningSum, currentValue + runningSum)
   tickA.Label.Text = currentValue.ToString("0") + "%"
   tickA.Label.Color = Chart.Palette(0)
   tickA.Line.Color = Chart.Palette(0)

   
   runningSum += currentValue
   currentValue = s2Sum * 100 / sum
   
   Dim tickB As New AxisTick(runningSum, currentValue + runningSum)
   tickB.Label.Text = currentValue.ToString("0") + "%"
   tickB.Label.Color =Chart.Palette(1)
   tickB.Line.Color = Chart.Palette(1)
   
   runningSum += currentValue
   currentValue = s3Sum * 100 / sum
   
   Dim tickC As New AxisTick(runningSum, currentValue + runningSum)
   tickC.Label.Text = currentValue.ToString("0") + "%"
   tickC.Label.Color = Chart.Palette(2)
   tickC.Line.Color = Chart.Palette(2)
   
   ' Add the extra ticks.
   shadow.ExtraTicks.Add(tickA, tickB, tickC)
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(2)
   Dim a As Integer
   For a = 0 To 2
      Dim s As New Series()
      s.Name = "Series " +Convert.ToString(a + 1)
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " +Convert.ToString(b + 1)
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
