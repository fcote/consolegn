<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">
 ' This sample demonstrates how to iterate a layer of shape files.
Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Map
   Chart.Size = "1200x700"
   Chart.Title = "Election 2004 results by state"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.TitleBox.Position = TitleBoxPosition.Full
   Chart.ChartArea.Background = New Background(Color.FromArgb(142, 195, 236), Color.FromArgb(63, 137, 200), 90)
   Dim layer As MapLayer = MapDataEngine.LoadLayer("../../../images/MapFiles/primusa.shp")
   layer.ImportData(CreateDataTable(), "STATE_ABBR", "Code")
   Chart.Mapping.DefaultShape.Background.Color = Color.Red
   Chart.Mapping.DefaultShape.Label.Text = "%STATE_ABBR" + ControlChars.Lf + "%Seats2004"
   Chart.Mapping.DefaultShape.Label.OutlineColor = Color.White
   Chart.Mapping.DefaultShape.Label.Font = New Font("Arial", 9)
   Chart.Mapping.DefaultShape.Label.Hotspot.ToolTip = "%STATE_NAME"
   
   Chart.Mapping.MapLayerCollection.Add(layer)
   
   'Set different colors to each state
   Dim shape As Shape
   For Each shape In  layer.Shapes
      Dim code As String = CStr(shape("STATE_ABBR"))
      If code = "WA" Or code = "OR" Or code = "CA" Or code = "MN" Or code = "WI" Or code = "MI" Or code = "PA" Or code = "IL" Or code = "NY" Or code = "MA" Or code = "NH" Or code = "MA" Or code = "RI" Or code = "NJ" Or code = "ME" Or code = "DL" Or code = "MD" Or code = "DE" Or code = "HI" Or code = "VT" Or code = "ME" Or code = "CT" Then
         
         shape.Background.Color = Color.Blue
      End If 
   Next shape
End Sub 'Page_Load

Private Function CreateDataTable() As DataTable
   Dim dt As New DataTable()
   Dim connString As String = "Provider=Microsoft.Jet.OLEDB.4.0;user id=admin;password=;data source=" + Server.MapPath("../../../database/chartsample.mdb")
   Dim conn As New OleDbConnection(connString)
   Dim adapter As New OleDbDataAdapter()
   Dim selectQuery As String = "SELECT [Code],[Seats2004] FROM Locations"
   adapter.SelectCommand = New OleDbCommand(selectQuery, conn)
   adapter.Fill(dt)
   Return dt
End Function 'CreateDataTable


		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
