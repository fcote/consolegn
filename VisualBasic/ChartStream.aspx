<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">
 'This sample demonstrates how to get the chart memory stream
Dim myChart As New dotnetCHARTING.Chart()
Dim filePath As String = ""

Sub Page_Load(sender As [Object], e As EventArgs)
   
   'Add data to the chart
   CreateChart()
   'Get the chart memoryStream
   Dim st As System.IO.MemoryStream = myChart.GetChartStream()
   'Save the memoryStream to the file and return the file's relative path 
   'and set it to src attribute of embed element.
   filePath = myChart.FileManager.SaveImage(st)
   
   'Check the temp directory for writeEmbed.js and creates it if does not exist.
   CreateFileJS()
End Sub 'Page_Load
 
Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = 25 + myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData

Sub CreateChart()
   
   myChart.Size = "600x350"
   myChart.TempDirectory = "temp"
   myChart.FileName = "chartStream"
   myChart.DisableBrowserCache = False
   myChart.ImageFormat = ImageFormat.Swf
   
   
   ' Set the title.
   myChart.Title = "My Chart"
   
   ' Set the x axis label
   myChart.ChartArea.XAxis.Label.Text = "X Axis Label"
   
   ' Set the y axis label
   myChart.ChartArea.YAxis.Label.Text = "Y Axis Label"
   
   ' Set the bar shading effect
   myChart.ShadingEffect = True
   
   ' Add the random data.
   myChart.SeriesCollection.Add(getRandomData())
End Sub 'CreateChart 
Sub CreateFileJS()
   Dim tempPath As String = MapPath("temp")
   Dim di As New DirectoryInfo(tempPath)
   Dim fiArr As FileInfo() = di.GetFiles("writeEmbed.js")
   If fiArr.Length < 1 Then
      Dim fout As New FileStream(tempPath + "\writeEmbed.js", FileMode.Create, FileAccess.Write)
      Dim data As String = "function WriteEmbed(stringValue){document.write(stringValue);}"
      Dim adData(data.Length) As Byte
      adData = ASCIIEncoding.ASCII.GetBytes(data)
      fout.Write(adData, 0, adData.Length)
      fout.Close()
   End If
End Sub 'CreateFileJS
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Chart Stream Sample</title>
<script src="temp/writeEmbed.js" type="text/javascript"></script>
</head>
<body>
<center>
<%
Response.Write("<script type=""text/javascript"">WriteEmbed('<embed height=""480"" width=""600"" src=""")
Response.Write(filePath.Replace("\", "/"))
Response.Write(""" type=""application/swf""/>');")
Response.Write("</script>")
%>
</center>
</body>
</html>