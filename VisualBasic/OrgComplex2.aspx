<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates setting up an organizational chart with invisible nodes.

		' Setup the chart
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.Type = ChartType.Organizational
		Chart.Size = "550x400"
		Chart.ChartArea.Label.Text = "The blue and green rows also contain an invisible " & Constants.vbLf & "node in the middle to enable a more complex configuration."
		Chart.LegendBox.Padding = 5
		Chart.LegendBox.DefaultEntry.LabelStyle.Font = New Font("Arial", 10, FontStyle.Bold)
		Chart.LegendBox.ClearColors()
		Chart.DefaultSeries.Line.Color = Color.Gray
		Chart.DefaultSeries.Line.Width = 3

		' Style the default annotation node.
		Chart.DefaultElement.Annotation = New Annotation()
		Chart.DefaultElement.Annotation.Padding = 5
		Chart.DefaultElement.Annotation.Background.ShadingEffectMode = ShadingEffectMode.Three
		Chart.DefaultElement.Annotation.Label.Font = New Font("Arial", 10, FontStyle.Bold)
		Chart.DefaultElement.Annotation.Size = New Size(95, 60)

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Chart.SeriesCollection.Add(getData())

		' Add a custom legend box.
		Chart.DefaultSeries.LegendEntry.Visible = False
		Chart.LegendBox.ExtraEntries.Add(New LegendEntry("Up for Reelection", "", Color.Orange))
		Chart.LegendBox.ExtraEntries.Add(New LegendEntry("Perminent position", "", Color.Red))
		Chart.LegendBox.Visible = True
		Chart.LegendBox.Position = New Point(360, 60)

	End Sub

	Function getData() As SeriesCollection
		Dim p1 As Element = New Element("Secretary of State")
		Dim vp1 As Element = New Element("Chief of Staff")
		Dim vpE As Element = New Element("")
		Dim vp3 As Element = New Element("Executive Secretariat")
		Dim mm1 As Element = New Element("Inspector General")
		Dim mm2 As Element = New Element("Policy Planning Staff")
		Dim mm3 As Element = New Element("Office of Civil Rights")
		Dim mm4 As Element = New Element("Legal Advisor")
		Dim mm5 As Element = New Element("Legislative Affairs")
		Dim mm6 As Element = New Element("Intelligence and Research")
		Dim mm7 As Element = New Element("Resource Management")
		Dim mm8 As Element = New Element("Chief of Protocol")
		Dim mmE As Element = New Element("")

		mmE.Parent = vpE
		vp1.Parent = p1
		vpE.Parent = p1
		vp3.Parent = p1
		mm1.Parent = vpE
		mm2.Parent = vpE
		mm3.Parent = vpE
		mm4.Parent = vpE
		mm5.Parent = mmE
		mm6.Parent = mmE
		mm7.Parent = mmE
		mm8.Parent = mmE

		colorAnnotation(vp3, Color.Orange)
		colorAnnotation(vp1, Color.Orange)
		colorAnnotation(mm1, Color.Orange)
		colorAnnotation(mm2, Color.Red)
		colorAnnotation(mm4, Color.Red)

		Dim Depts2 As Series = New Series("Low Level", mm5, mm6, mm7, mm8)
		Dim Depts1 As Series = New Series("Deparment Head", mm1, mm2, mmE, mm3, mm4)
		Dim vpS As Series = New Series("Top Leaders", vp1, vpE, vp3)
		Dim President As Series = New Series("", p1)

		Depts1.DefaultElement.Annotation = New Annotation()
		Depts1.DefaultElement.Annotation.Background.Color = Chart.Palette(0)
		Depts2.DefaultElement.Annotation = New Annotation()
		Depts2.DefaultElement.Annotation.Background.Color = Chart.Palette(2)
		vpS.DefaultElement.Annotation = New Annotation()
		vpS.DefaultElement.Annotation.Background.Color = Chart.Palette(3)

		Dim scSolo As SeriesCollection = New SeriesCollection(President, vpS, Depts1, Depts2)
		Return scSolo
	End Function

	Sub colorAnnotation(ByVal e As Element, ByVal c As Color)
		e.Annotation = New Annotation()
		e.Annotation.Line.Color = c
		e.Annotation.Line.Width = 4
	End Sub

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
