<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using breadcrumbs of organizational nodes.

		' Setup the chart
		Chart.Size = "900x400"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Type = ChartType.Organizational
		Chart.MarginTop = 40
		Chart.ChartArea.Label.Text = "<block hAlign='center'>Click on a node to view the breadcrumbs leading up to it."
		Chart.ChartArea.Padding = 35
		Chart.DefaultSeries.Line.Color = Color.Gray
		Chart.DefaultSeries.Line.Width = 3
		Chart.ChartArea.Background.ShadingEffectMode = ShadingEffectMode.Background1
		Chart.ChartArea.Background.Color = Color.LightGoldenrodYellow

		' Style the default node annotation.
		Chart.DefaultElement.Annotation = New Annotation()
		Chart.DefaultElement.Annotation.Padding = 5
		Chart.DefaultElement.Annotation.Size = New Size(70, 25)
		Chart.DefaultElement.Annotation.DefaultCorner = BoxCorner.Square
		Chart.DefaultElement.Annotation.Line = New Line(Color.Gray, 2,DashStyle.Dash)
		Chart.DefaultElement.Annotation.Label.Text = "<block fSize='12'>%Name"
		Chart.DefaultElement.Annotation.URL = "?id=%id"

		Dim crumbID As Integer = -1

		' Get the query string
		If Page.Request.QueryString.Count > 0 Then
			Dim query As String = Page.Request.QueryString("id")
			If Not query Is Nothing AndAlso query.Length > 0 Then
				crumbID = Integer.Parse(query)
			End If
		End If

		' Get the live data
		Dim myS As Series = getLiveData()(0)

		' If a node was clicked, show the breadcrumbs.
		If crumbID <> -1 Then
			' Get a comma delimited string of breadcrumbs leading up to this element.
			Dim crumbList As String = myS.GetElementBreadcrumbs(crumbID, "%Name", ",")

			' Split the crumbs into an array
			Dim crumbArray As String() = crumbList.Split(New String() { "," }, StringSplitOptions.RemoveEmptyEntries)

			' Draw an annotation for each crumb.
			Dim i As Integer = 0
			For Each crumb As String In crumbArray
				Dim an As Annotation = New Annotation(crumb)
				an.DynamicSize = False
				an.Background.ShadingEffectMode = ShadingEffectMode.Background2
				an.DefaultCorner = BoxCorner.Square
				an.Size = New Size(80, 25)
				an.Position = New Point(8 + (i * 95), 10)
				Chart.Annotations.Add(an)
				i += 1
			Next crumb
		End If

		' Modify the styling for the clicked Node.
		For Each el As Element In myS.Elements
			If el.InstanceID = crumbID Then
				el.Annotation = New Annotation()
				el.Annotation.Line.DashStyle = DashStyle.Solid
			End If
		Next el

		' Add the data.
		Chart.SeriesCollection.Add(myS)
	End Sub

	Function getLiveData() As SeriesCollection
		' Obtain series data from the database. Note this was created by adding PID (parentid)
		' to an existing table only, then populating the parentid based on the hiearchy in place.
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.SqlStatement = "SELECT * FROM Employees"
		de.DataFields = "InstanceID=ID,InstanceParentID=PID,Name=Name,office,Department,Email,Phone,Picture"
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
