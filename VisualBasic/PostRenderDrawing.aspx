<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Post Render Drawing Image</title>
		<script runat="server">




Sub Page_Load(sender As [Object], e As EventArgs)
   'dotnetCHARTING.Chart Chart = new dotnetCHARTING.Chart();
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
   Dim s As Series = getRandomData()
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(s)
   
   'Get a bitmap of the chart.
   Dim bmp As Bitmap = Chart.GetChartBitmap()
   
   ' Create a graphics object from the bitmap
   Dim g As Graphics = Graphics.FromImage(bmp)
   
   
   ' After the chart is generated, the element's positions are available.
   ' Lets iterate the elements we have added and make an array of these positions. 
   Dim points(s.Elements.Count-1) As PointF
   Dim i As Integer
   For i = 0 To s.Elements.Count-1
      points(i) = s.Elements(i).Point
   Next i 
   ' Draw a curve connecting the points.
   g.DrawCurve(Pens.Red, points)
   
   ' Draw a string on the first element
   g.DrawString("First Element", New Font("Arial", 10), Brushes.Black, points(0))
   
   g.Dispose()
   
   
   'Save the image using the FileManager.
   Chart.FileManager.SaveImage(bmp)
End Sub 'Page_Load
 

Function getRandomData() As Series
   Dim myR As New Random()
   Dim s As New Series("My Series")
   Dim b As Integer
   For b = 1 To 4
      Dim e As New Element("Element " + b.ToString(), myR.Next(50))
      s.Elements.Add(e)
   Next b
   Return s
End Function 'getRandomData

        </script>
	</head>
	<body>
		<div style="text-align:center">
           <dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px" Depth="13px" Use3D="True">
			</dotnet:Chart>
		</div>
	</body>
</html>
