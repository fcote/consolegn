<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

	Chart.Type = ChartType.Combo 'Horizontal;
	Chart.Size = "600x350"
	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Title = ".netCHARTING Sample"


	' This sample demonstrates how to label the total values of column stacks.

	Chart.DefaultElement.ShowValue = True
	Chart.YAxis.Scale = Scale.Stacked
	Chart.YAxis.Maximum = 200

	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Dim mySC As SeriesCollection = getRandomData()

	' Clear the secondary alignment options of element labels.
	For Each ser As Series In mySC
		ser.DefaultElement.SmartLabel.AlignmentSecondary.Clear()
		For Each el As Element In ser.Elements
			el.SmartLabel.AlignmentSecondary.Clear()
		Next el
	Next ser

	'Chart.DefaultElement.SmartLabel.Text = "<img:../../images/us.png>";

	' Create a total series that is invisible. Just used to place the totals labels.
	Dim total As Series = mySC.Calculate("Total",Calculation.Sum)
	total(0).SmartLabel.Text = "<img:../../images/browserO.png>"
	total(1).SmartLabel.Text = "<img:../../images/browserIE.png>"
	total(2).SmartLabel.Text = "<img:../../images/browserS.png>"
	total(3).SmartLabel.Text = "<img:../../images/browserN.png>"
	total.Type = SeriesType.Marker

	total.DefaultElement.Marker.Type = ElementMarkerType.None

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)
	Chart.SeriesCollection.Add(total)

End Sub

Function getRandomData() As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random(7)
	For a As Integer = 1 To 4
		Dim s As Series = New Series()
		s.Name = "Series " & a.ToString()
		For b As Integer = 1 To 4
			Dim e As Element = New Element()
			e.Name = "Element " & b.ToString()
			e.YValue = myR.Next(20,50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	Return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
