<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object ,e As EventArgs )

	'set global properties
    Chart.Title="Sales"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
    Chart.DateGrouping = TimeInterval.Quarters
    
    'Add a series
    Chart.Series.Name="Order"
    Chart.Series.SqlStatement= "SELECT OrderDate,Sum(Total) FROM Orders Group By OrderDate ORDER BY OrderDate"
    Chart.SeriesCollection.Add()
    
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Date Grouping By Quarters Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
