<%@ Page Language="vb" debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	Chart.Title = "This chart will refresh every 15 seconds with new data"

	'Enable AJAX zoom / scroll / refresh control
	Chart.Zoomer.Enabled = True

	'Disable section based rendering
	Chart.Zoomer.UseSections = False

	'Set reload option
	Chart.Zoomer.ReloadPeriod = New TimeSpan(0,0,15)
	AddHandler Chart.Zoomer.ReloadChart, AddressOf ZoomerReloadChart

	Chart.LegendBox.Background.Color = Color.FromArgb(245,245,245)
	Chart.TitleBox.Background.Color = Color.SteelBlue
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Two
	Chart.TitleBox.Label.Color = Color.White

	Chart.ShadingEffect = True
	Chart.ShadingEffectMode = ShadingEffectMode.Seven
	Chart.PaletteName = dotnetCHARTING.Palette.Bright
	Chart.DefaultElement.ShowValue=True
	Chart.Type = ChartType.Combo
	Chart.Size = "600x350"
	Chart.TempDirectory="temp"
	Chart.Debug = True
	Chart.DefaultSeries.Type = SeriesType.Bar

	' Set the x axis label
	Chart.ChartArea.XAxis.Label.Text="X Axis Label"

	' Set the y axis label
	Chart.ChartArea.YAxis.Label.Text="Y Axis Label"

	' Add the random data.
	Chart.SeriesCollection.Add(getRandomData(1))
	
	message.Text = "This page was created at "+ DateTime.Now.ToString()
End Sub
Public Shared Sub ZoomerReloadChart(ByVal chart As Chart)
	Dim mySC As SeriesCollection = getRandomData(DateTime.Now.Millisecond)
	chart.SeriesCollection.Clear()
	chart.SeriesCollection.Add(mySC)
End Sub
Private Shared Function getRandomData(ByVal seed As Integer) As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random(seed)
	For a As Integer = 1 To 4
		Dim s As Series = New Series()
		s.Name = "Series " & a
		For b As Integer = 1 To 5
			Dim e As Element = New Element()
			e.Name = "E " & b
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	Return SC
End Function
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Reload Period Sample</title></head>
<body>
<center>
 <dotnet:Chart id="Chart"  runat="server"/>
 <asp:Label ID="message" Runat="server"/>
</center>
</body>
</html>
