<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of NormalDistribution method from the StatisticalEngine. This method return the normal distribution for a series when the standard deviation is known. The mean is calculated using the arithmetic mean of the XValues of the given series
   ' The Probability Chart
   ProbabilityChart.Title = "Standard Normal distribution"
   ProbabilityChart.TempDirectory = "temp"
   ProbabilityChart.Debug = True
   ProbabilityChart.Size = "600x400"
   ProbabilityChart.LegendBox.Template = "%icon %name"
   ProbabilityChart.XAxis.Scale = Scale.Normal
   ProbabilityChart.XAxis.Maximum = 5
   ProbabilityChart.YAxis.ScaleRange.ValueHigh = 0.5
   ProbabilityChart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   ProbabilityChart.DefaultSeries.DefaultElement.Marker.Visible = True
   ProbabilityChart.DefaultSeries.Type = SeriesType.Spline
   ProbabilityChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   
   
   Dim sprobability As New SeriesCollection()
   
   ' Generate the sample data.
   Dim sampledata1 As New Series("Sample Data")
   sampledata1.Elements.Add(New Element("", 0.0, 0.03125))
   sampledata1.Elements.Add(New Element("", 1.0, 0.15625))
   sampledata1.Elements.Add(New Element("", 2.0, 0.3125))
   sampledata1.Elements.Add(New Element("", 3.0, 0.3125))
   sampledata1.Elements.Add(New Element("", 4.0, 0.15625))
   sampledata1.Elements.Add(New Element("", 5.0, 0.003125))
   sprobability.Add(sampledata1)
   
   ' Add the series collection to the chart
   ProbabilityChart.SeriesCollection.Add(sprobability)
   
   ' The second parameter of this method is the standard deviation of the normal probability distribution
   Dim normalDistribution As Series = StatisticalEngine.NormalDistribution(sampledata1, 1)
   normalDistribution.Type = SeriesType.Spline
   ProbabilityChart.SeriesCollection.Add(normalDistribution)
End Sub 'Page_Load 

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="ProbabilityChart" runat="server"/>
		</div>
	</body>
</html>
