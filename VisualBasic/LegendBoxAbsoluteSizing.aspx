<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates how to specify a legend box size and position.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.Type = ChartType.Combo
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom

		Chart.LegendBox.Position = New Rectangle(New Point(40,40),New Size(130, 80))

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

		mySC(0).LegendEntry.LabelStyle.Font = New Font("Arial", 12)
		mySC(2).LegendEntry.LabelStyle.Font = New Font("Arial", 17)


		Dim lb As LegendBox = New LegendBox()
		lb.Position = New Rectangle(New Point(40,140),New Size(250, 50))


		Dim lb2 As LegendBox = New LegendBox()
		lb2.Position = New Rectangle(New Point(340, 100), New Size(90, 170))

		Dim lb3 As LegendBox = New LegendBox()
		lb3.Position = New Rectangle(New Point(40, 240), New Size(90, 80))


		lb.ExtraEntries.Add(getEntries())
		lb2.ExtraEntries.Add(getEntries())
		lb3.ExtraEntries.Add(getEntries())
		Chart.ExtraLegendBoxes.Add(lb,lb2,lb3)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 4
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 4
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getEntries() As LegendEntryCollection
		Dim lec As LegendEntryCollection = New LegendEntryCollection()
		For i As Integer = 0 To 4
			Dim le As LegendEntry = New LegendEntry()
			le.Name = "Element: " & i
			le.Value = (CInt(Fix(234 * i))).ToString()
			lec.Add(le)
		Next i
		Return lec

	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
