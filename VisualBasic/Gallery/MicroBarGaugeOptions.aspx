<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates several features of the BarGauge microChart.

		Chart.TempDirectory = "temp"
		Chart.Background.Color = Color.FromArgb(244, 244, 244)
		Chart.LabelChart.Alignment = StringAlignment.Near

		Dim s As String = "<block fStyle='Bold'>BarGauge Type Options<block><hr>"
		s &= "Plain BarGauge: <Chart:BarGauge value='10'>"

		' Setting a scale maximum value
		s &= "<row>BarGauge & scale max <Chart:BarGauge value='10' max='20'><hr>"

		' Specifying a color
		s &= "<row>BarGauge with Color <Chart:BarGauge value='10' color='LightSteelBlue' ><hr>"

		' Bar Gauge Shading Effect Modes.
		s &= "<row>Bar Shading 1 <Chart:BarGauge value='10' shading='1' color='LightSteelBlue'>"
		s &= "<row>Bar Shading 2 <Chart:BarGauge value='10' shading='2' color='PeachPuff'>"
		s &= "<row>Bar Shading 3 <Chart:BarGauge value='10' shading='3' color='DarkGreen'>"
		s &= "<row>Bar Shading 4 <Chart:BarGauge value='10' shading='4' color='DarkRed'>"
		s &= "<row>Bar Shading 5 <Chart:BarGauge value='10' shading='5' color='DodgerBlue'>"
		s &= "<row>Bar Shading 6 <Chart:BarGauge value='10' shading='6' color='LightSteelBlue'>"
		s &= "<row>Bar Shading 7 <Chart:BarGauge value='10' shading='7' color='LightSteelBlue'><hr>"

		' Specifying a size
		s &= "<row>BarGauge with size set<Chart:BarGauge value='10' max='20' width='150' height='15' color='Bisque'>"

		Chart.ObjectChart = New dotnetCHARTING.Label(s)
	End Sub


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
