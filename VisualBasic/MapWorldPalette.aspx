<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>

	<script runat="server">

		' This sample demonstrates using a palette for a layer and zooming.
		Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
			Chart.Type = ChartType.Map
			Chart.Size = "650x540"
			Chart.Title = "Mapping Sample"
			Chart.TempDirectory = "temp"
			Chart.Debug = True
			Chart.TitleBox.Position = TitleBoxPosition.Full
			Chart.LegendBox.Visible = False

			Dim layer As MapLayer = MapDataEngine.LoadLayer("../../images/MapFiles/world.shp")
			Chart.Mapping.DefaultShape.Line.Color = Color.Gray
			Chart.Mapping.ZoomCenterPoint = New PointF(3, 18)
			Chart.Mapping.ZoomPercentage = 351

			layer.PaletteName = Palette.FiveColor1
			Chart.Mapping.MapLayerCollection.Add(layer)
		End Sub

	</script>

</head>
<body>
	<div style="text-align: center">
		<dotnet:Chart ID="Chart" runat="server">
		</dotnet:Chart>
	</div>
</body>
</html>
