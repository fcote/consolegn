﻿Imports Oracle.DataAccess.Client
Imports System.Data
'Imports CECCE.StudioPedagogique.Data

'Imports connection

Partial Class gn2
    Inherits System.Web.UI.Page


    Public Chrono As System.Diagnostics.Stopwatch = New System.Diagnostics.Stopwatch()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Response.Write("ID=" & Session("ID_Direction"))
        If Not Page.IsPostBack Then
            '    Session("ID_Enseignant") = DropDownList1.SelectedValue
        End If

        If Not Page.IsPostBack Then
            If Session("role") = "administrateur" Or Session("role") = "directeur" Then
                SqlDataSource7.SelectCommand = " SELECT DISTINCT gne2.m_ecole as ecole, gne2.nom_ecole " & _
                        "FROM GN_VW_ENSEIGNANTS GNE2 " & _
                        "WHERE GNE2.M_MEMBRE_ID = :ID_USER"
            Else
                SqlDataSource7.SelectCommand = " select ecole from SP_ACCES_CONSOLE where login = :ID_USER"
            End If
            DropDownList4.databind()

            Dim sql1 As String = ""
            'sql1 &= "SELECT DISTINCT GNE1.M_NOM || ', ' || GNE1.M_PRENOM || ' - ' || GNE1.M_ECOLE AS NOM, GNE1.M_MEMBRE_ID "
            'sql1 &= "FROM GN_VW_ENSEIGNANTS GNE1 "
            'sql1 &= "LEFT OUTER JOIN GN_VW_ENSEIGNANTS GNE2 "
            'sql1 &= "ON GNE2.M_MEMBRE_ID = '" & Session("ID_USER") & "'  "
            'sql1 &= "WHERE (GNE1.TYPE_PERSON = 'E' OR GNE1.TYPE_PERSON = 'A') "
            'sql1 &= "AND GNE1.M_ECOLE = '" & DropDownList4.SelectedValue & "'  "
            'sql1 &= "ORDER BY NOM "
			
			
			sql1 &= "SELECT DISTINCT p.legal_surname || ', ' || p.legal_first_name || ' - ' || ss.school_code AS NOM,"
			sql1 &= "	p.person_id M_MEMBRE_ID, "
			sql1 &= "	p.legal_surname M_NOM, "
			sql1 &= "	p.legal_first_name m_prenom, "
			sql1 &= "	ss.school_code m_ecole, "
			sql1 &= "	DECODE (ss.staff_type_name, 'Teacher', 'E', 'Teacher Spec', 'E', 'Principal', 'D', 'Vice Principal', 'D', "
			sql1 &= "	'Principal A', 'D', 'Vice Principal A', 'D', 'A') type_person, "
			sql1 &= "	ss.teacher_flag, "
			sql1 &= "	SUBSTR ( DECODE (o.school_type_code, 'ConEd', 'Sec', o.school_type_code), 1, 1) palier, "
			sql1 &= "	s.school_name nom_ecole, "
			sql1 &= "	ss.school_year anneescolaire "
			sql1 &= "  FROM trill02.persons p, "
			sql1 &= "	school_staff ss, "
			sql1 &= "	organizations o, "
			sql1 &= "	schools s "
			sql1 &= "  WHERE p.person_id  = ss.person_id			   "
            sql1 &= "  AND ss.school_year = " & DropDownList16.SelectedValue & " "
            sql1 &= "  AND s.school_code     = ss.school_code "
            sql1 &= "  AND o.organization_id = s.default_school_bsid "
            sql1 &= "  AND o.board_bsid      = 'B67318' "
            sql1 &= "  AND s.active_flag     = 'x'  "
            sql1 &= " AND (DECODE (ss.staff_type_name, 'Teacher', 'E', 'Teacher Spec', 'E', 'Principal', 'D', 'Vice Principal', 'D', 'Principal A', 'D', 'Vice Principal A', 'D', 'A') = 'E' OR DECODE (ss.staff_type_name, 'Teacher', 'E', 'Teacher Spec', 'E', 'Principal', 'D', 'Vice Principal', 'D', 'Principal A', 'D', 'Vice Principal A', 'D', 'A') = 'A')  "
            sql1 &= " AND ss.school_code = '" & DropDownList4.SelectedValue & "'   "
            sql1 &= " ORDER BY NOM  "

            'Dim valueeeee As String = DropDownList16.SelectedValue
            'valueeeee = ""




            SqlDataSource1.SelectCommand = sql1
            DropDownList1.DataBind()

            load_data(True, True)

        Else
            load_data(False, False)
        End If


    End Sub


    Sub load_data(ByVal init As Boolean, ByVal init2 As Boolean)

        If (init) Then
            Dim sql2 As String = ""
            '_OLD plus utilisé
            sql2 &= "SELECT DISTINCT ecole || codecours || cours_section || classe_code || strand_code || report_period AS uniqueid, "
            sql2 &= "semester, ecole, codecours, classe_code, 'Semestre ' || semester|| ' -> ' || 'Cours '|| matiere || '(' || codecours || ')'|| ' -> ' || "
            sql2 &= "classe_code || ' -> ' || 'Nb évaluations ' || pkg_sp.fc_nbr_evaluation (ecole,anneescolaire,classe_code,codecours,report_period,strand_code) AS Liste_Classes "
            sql2 &= "FROM gn_vw_classes_sec c, organizations o, schools s, school_class_term t "
            sql2 &= "WHERE c.anneescolaire = " & DropDownList16.SelectedValue & " "
            'sql2 &="AND c.membre_id = :M_MEMBRE_ID AND s.default_school_bsid = o.organization_id AND s.school_code = ecole " 
            sql2 &= "AND c.membre_id = '" & DropDownList1.SelectedValue & "' AND s.default_school_bsid = o.organization_id AND s.school_code = ecole "
            sql2 &= "AND t.school_code = s.school_code AND t.school_year = c.anneescolaire AND t.class_code = c.classe_code "
            sql2 &= "ORDER BY semester, ecole, codecours, classe_code "

            'new 2015-2016

            sql2 = ""
            'parfois problème avec classe_code
            'sql2 &= " SELECT DISTINCT ecole || codecours || cours_section || classe_code || strand_code || report_period AS uniqueid, "
            sql2 &= " SELECT DISTINCT ecole || codecours || cours_section || strand_code || report_period AS uniqueid, "
            sql2 &= "   semester, ecole, codecours, classe_code, 'Semestre ' || semester|| ' -> ' || 'Cours '|| matiere || '(' || codecours || ')'|| ' -> ' || "
            sql2 &= " classe_code || ' -> ' || 'Nb évaluations ' || pkg_sp.fc_nbr_evaluation (ecole,anneescolaire,classe_code,codecours,report_period,strand_code) AS Liste_Classes "
            sql2 &= " FROM (SELECT DISTINCT sc.reporting_teacher membre_id,"
            sql2 &= " sc.class_code classe_code,"
            sql2 &= " sc.school_year anneescolaire,"
            sql2 &= " sc.school_code ecole,"
            sql2 &= " DECODE (SUBSTRB(sc.class_code, 1,4),'COOP', 'COURS_COOP', cc.course_code ) codecours,"
            sql2 &= " DECODE (SUBSTRB(sc.class_code, 1,4),'COOP', 'Cours COOP', cc.course_title) description,"
            sql2 &= " DECODE (SUBSTRB(sc.class_code, 1,4),'COOP', 'Cours COOP', cc.course_title) matiere,"
            sql2 &= " '00' NIVEAU,"
            sql2 &= " '00' report_period,"
            sql2 &= " 'x' subject_code,"
            sql2 &= " 'xxx' strand_code,"
            sql2 &= " DECODE (SUBSTRB(sc.class_code, 1,4),'COOP', '00', course_section) cours_section"
            sql2 &= " FROM school_classes sc,"
            sql2 &= " course_codes cc,"
            sql2 &= " course_code_section ccs"
            sql2 &= " WHERE sc.school_code IN"
            sql2 &= " (SELECT school_code"
            sql2 &= " FROM schools"
            sql2 &= " WHERE default_school_bsid IN"
            sql2 &= " (SELECT organization_id"
            sql2 &= " FROM organizations"
            sql2 &= " WHERE board_bsid                                           = 'B67318'"
            sql2 &= " AND DECODE(school_type_code,'ConEd','Sec',school_type_code)= 'Sec'"
            sql2 &= " )"
            sql2 &= " AND active_flag = 'x'"
            sql2 &= " )"
            sql2 &= " AND sc.school_year  = '" & DropDownList16.SelectedValue & "' "
            sql2 &= " AND ccs.school_code = sc.school_code"
            sql2 &= " AND ccs.school_year = sc.school_year"
            sql2 &= " AND ccs.class_code  = sc.class_code"
            sql2 &= " AND cc.school_code  = ccs.school_code"
            sql2 &= " AND cc.school_year  = ccs.school_year"
            sql2 &= " AND cc.course_code  = ccs.course_code"
            sql2 &= " AND ccs.builder_set_code = 'working') c, organizations o, schools s, school_class_term t "
            sql2 &= " WHERE c.anneescolaire = '" & DropDownList16.SelectedValue & "' "
            sql2 &= " AND c.membre_id = '" & DropDownList1.SelectedValue & "' AND s.default_school_bsid = o.organization_id AND s.school_code = ecole "
            sql2 &= " AND t.school_code = s.school_code AND t.school_year = c.anneescolaire AND t.class_code = c.classe_code "
            sql2 &= " ORDER BY semester, ecole, codecours, classe_code "






            SqlDataSource2.SelectCommand = sql2
            DropDownList2.DataBind()
        End If

        Dim sql3 As String = ""
        sql3 &= "SELECT   m.m_prenom as Prénom, m.m_nom as nom, p.gender sexe, pkg_sp.fc_eleve_pei (m.m_membre_id, m.m_anneescolaire, m.m_ecole ) Pei, "
        sql3 &= "DECODE ('2', '0', (SELECT b_niveau_bulletin FROM sp_baremes WHERE b_niveau_rendement = pkg_sp.fc_niveau_a_afficher (ROUND (NVL (m_note_ajustee, m_note ), 1), " & DropDownList16.SelectedValue & ",DECODE ('2', '0', 'E', '1', 'S' ) )   "
        sql3 &= "AND b_anneescolaire = " & DropDownList16.SelectedValue & "   AND b_niveau = DECODE ('2', '0', 'E', '1', 'S')   "
        sql3 &= "AND b_type_eval = 'G'),'1', ROUND (NVL (m_note_ajustee, m_note), 1), '2', ROUND (NVL (m_note_ajustee, m_note), 1) ) AS note_bulletin, "
        sql3 &= "ROUND (m_comp1_moyenne, 1) CC, ROUND (m_comp2_moyenne, 1) HP, ROUND (m_comp3_moyenne, 1) CO, ROUND (m_comp4_moyenne, 1) MA, "
        sql3 &= "pkg_sp.fc_niveau_a_afficher(ROUND (m_comp1_moyenne, 1)," & DropDownList16.SelectedValue & ",'S') AS CC_N, "
        sql3 &= "pkg_sp.fc_niveau_a_afficher(ROUND (m_comp2_moyenne, 1)," & DropDownList16.SelectedValue & ",'S') AS HP_N, "
        sql3 &= "pkg_sp.fc_niveau_a_afficher(ROUND (m_comp3_moyenne, 1)," & DropDownList16.SelectedValue & ",'S') AS CO_N, "
        sql3 &= "pkg_sp.fc_niveau_a_afficher(ROUND (m_comp4_moyenne, 1)," & DropDownList16.SelectedValue & ",'S') AS MA_N, "
        sql3 &= "ROUND (m_note_examen, 1) AS Examen, pkg_sp.fc_niveau_a_afficher(ROUND (m_note_examen, 1)," & DropDownList16.SelectedValue & ",'S') AS Exam_N, "
        sql3 &= "ROUND (m_note_semestre, 1) AS Semestre, pkg_sp.fc_niveau_a_afficher(ROUND (m_note_semestre, 1)," & DropDownList16.SelectedValue & ",'S') AS Semestre_N, "
        sql3 &= "ROUND (m_note, 1) AS moyenne, m_note_semestre_ajustement AS ajustement, ROUND (NVL (m_note_ajustee, m_note), 1) AS moyenne_ajustee, "
        sql3 &= "pkg_sp.fc_niveau_a_afficher (ROUND (m_note, 1)," & DropDownList16.SelectedValue & ",'S') AS moyenne_n, "
        sql3 &= "pkg_sp.fc_niveau_a_afficher(ROUND (NVL (m_note_ajustee, m_note), 1)," & DropDownList16.SelectedValue & ",'S') AS moyenne_ajustee_N, "
        sql3 &= "m_a_quitte "
        sql3 &= "FROM sp_classes c, trill02.persons p, sp_eleves_resultats m "
        sql3 &= "WHERE m.m_ecole ='" & Session("m_ecole") & "' "
        'problème avec classe_code...
        'sql3 &= "AND m.m_anneescolaire = " & DropDownList16.SelectedValue  & " AND m.m_classe_code = '" & Session("m_classe_code") & "'  AND m.m_codecours =  '" & Session("m_codecours") & "'  "
        sql3 &= "AND m.m_anneescolaire = " & DropDownList16.SelectedValue & " AND m.m_codecours =  '" & Session("m_codecours") & "'  "
        sql3 &= "AND m.m_periode_ref = '" & Session("m_periode_ref") & "'  AND m.m_strand_code = '" & Session("m_strand_code") & "'  AND m.m_membre_id = p.person_id AND c.c_ecole = m.m_ecole "
        sql3 &= "AND c.c_anneescolaire = m.m_anneescolaire AND c.c_classe_code = m.m_classe_code AND c.c_codecours = m.m_codecours AND c.c_periode_ref = m.m_periode_ref "
        sql3 &= "AND c.c_strand_code = m.m_strand_code ORDER BY m_a_quitte, m_nom, m_prenom"
        SqlDataSource3.SelectCommand = sql3
        'RadGrid1.DataBind()


        If (init2) Then
            Dim sql6 As String = ""
            sql6 &= "SELECT   e_evaluation_id as eval_id, e_date AS d_date,"
            sql6 &= " e_description, e_date || ' ' || e_description as description"
            sql6 &= " FROM sp_evaluations LEFT JOIN sp_grilles ON e_evaluation_id = evaluation_id"
            sql6 &= " WHERE e_ecole = '" & Session("m_ecole") & "' "
            sql6 &= " AND e_anneescolaire = " & DropDownList16.SelectedValue & ""
            'probleme avec certain classe_code
            'sql6 &= " AND e_classe_code = '" & Session("m_classe_code") & "' "
            sql6 &= " AND e_codecours = '" & Session("m_codecours") & "' "
            sql6 &= " AND e_strand_code = '" & Session("m_strand_code") & "' "
            sql6 &= " ORDER BY d_date ASC, e_description ASC"
            SqlDataSource6.SelectCommand = sql6
            DropDownList3.DataBind()
        End If


        Dim sql4 As String = ""
        sql4 &= "SELECT  "
        sql4 &= "TO_CHAR (e_date, 'dd-MM-yy')as e_date , e_description , e_moyenne, "
        sql4 &= "e_ponderation, e_examen, e_date_affichage, "
        sql4 &= "pkg_sp.fc_niveau_a_afficher (e_moyenne," & DropDownList16.SelectedValue & ",'S') AS moyenne_gr,ROUND (e_moyenne, 1) || ' / ' "
        sql4 &= "|| pkg_sp.fc_niveau_a_afficher (e_moyenne, " & DropDownList16.SelectedValue & ", 'S') AS moyenne_aff, "
        sql4 &= "e_comp1_style, e_comp2_style, e_comp3_style, e_comp4_style, pkg_sp.fc_status_evaluation (e_evaluation_id) AS statut, "
        sql4 &= "(SELECT COUNT (n_membre_id) FROM sp_notes, sp_eleves_resultats  WHERE(n_evaluation_id = e_evaluation_id) "
        sql4 &= "AND m_membre_id = n_membre_id AND m_codecours = e_codecours AND m_classe_code = e_classe_code AND m_ecole = e_ecole "
        sql4 &= "AND m_periode_ref = e_periode_ref AND m_anneescolaire = e_anneescolaire AND m_a_quitte = 'N') AS no_eleves, "
        sql4 &= "(SELECT COUNT (n_membre_id) FROM sp_notes, sp_eleves_resultats WHERE n_abs = 'N' "
        sql4 &= "AND n_comp1_sur100 IS NULL AND n_comp2_sur100 IS NULL AND n_comp3_sur100 IS NULL AND n_comp4_sur100 IS NULL "
        sql4 &= "AND m_membre_id = n_membre_id AND m_codecours = e_codecours AND m_classe_code = e_classe_code AND m_ecole = e_ecole "
        sql4 &= "AND m_periode_ref = e_periode_ref AND m_anneescolaire = e_anneescolaire AND m_a_quitte = 'N' AND n_evaluation_id = e_evaluation_id) AS no_vides, "
        sql4 &= "(SELECT COUNT (n_membre_id) FROM sp_notes, sp_eleves_resultats WHERE(n_evaluation_id = e_evaluation_id) "
        sql4 &= "AND n_abs = 'O' AND m_membre_id = n_membre_id AND m_codecours = e_codecours AND m_classe_code = e_classe_code "
        sql4 &= "AND m_ecole = e_ecole AND m_periode_ref = e_periode_ref AND m_anneescolaire = e_anneescolaire AND m_a_quitte = 'N') AS no_absents "
        sql4 &= "FROM sp_evaluations LEFT JOIN sp_grilles ON e_evaluation_id = evaluation_id WHERE e_ecole = '" & Session("m_ecole") & "'  "
        'problème avec certain classe_code
        'sql4 &= "AND e_anneescolaire = " & DropDownList16.SelectedValue  & " AND e_classe_code = '" & Session("m_classe_code") & "'  "
        sql4 &= "AND e_anneescolaire = " & DropDownList16.SelectedValue & "  "
        sql4 &= "AND e_codecours = '" & Session("m_codecours") & "'  AND e_strand_code = '" & Session("m_strand_code") & "'  ORDER BY e_date ASC, e_description ASC "
        SqlDataSource4.SelectCommand = sql4
        'RadGrid2.DataBind()

        Session("ID_Evaluation") = DropDownList3.SelectedValue
        load_data_rg3()
        'RadGrid3.DataBind()


        RadGrid1.ExportSettings.FileName = "Moyenne_" & Session("m_codecours") & "_" & Date.Today
        RadGrid1.ExportSettings.IgnorePaging = True
        RadGrid1.ExportSettings.OpenInNewWindow = True
        RadGrid1.MasterTableView.CommandItemSettings.RefreshText = "Rafraîchir"

        RadGrid2.ExportSettings.FileName = "Moyenne_évaluation_" & Date.Today
        RadGrid2.ExportSettings.IgnorePaging = True
        RadGrid2.ExportSettings.OpenInNewWindow = True
        RadGrid2.MasterTableView.CommandItemSettings.RefreshText = "Rafraîchir"

        RadGrid3.ExportSettings.FileName = "Notes_élèves_évaluation_" & Date.Today
        RadGrid3.ExportSettings.IgnorePaging = True
        RadGrid3.ExportSettings.OpenInNewWindow = True
        RadGrid3.MasterTableView.CommandItemSettings.RefreshText = "Rafraîchir"
    End Sub

    Sub load_data_rg3()
        Dim sql5 As String = ""
        sql5 &= "SELECT  m_prenom as Prenom, m_nom as nom, m_a_quitte, "
        sql5 &= "pkg_sp.fc_niveau_a_afficher (n_comp1_sur100," & DropDownList16.SelectedValue & ",'S') AS CC, "
        sql5 &= "pkg_sp.fc_niveau_a_afficher (n_comp2_sur100," & DropDownList16.SelectedValue & ",'S') AS HP, "
        sql5 &= "pkg_sp.fc_niveau_a_afficher (n_comp3_sur100," & DropDownList16.SelectedValue & ",'S') AS CO, "
        sql5 &= "pkg_sp.fc_niveau_a_afficher (n_comp4_sur100," & DropDownList16.SelectedValue & ",'S') AS MA, "
        sql5 &= "pkg_sp.fc_niveau_a_afficher (n_note, " & DropDownList16.SelectedValue & ", 'S') AS note, "
        sql5 &= "ROUND (n_note, 1) as note_sur_100, n_abs as abs FROM sp_eleves_resultats, sp_notes LEFT JOIN sp_documents_note "
        'sql5 &= "ON dn_evaluation_id = n_evaluation_id AND dn_membre_id = n_membre_id WHERE n_evaluation_id = '" & DropDownList3.SelectedValue & "' "
        sql5 &= "ON dn_evaluation_id = n_evaluation_id AND dn_membre_id = n_membre_id WHERE n_evaluation_id = '" & Session("ID_Evaluation") & "' "

        'problème avec certain classe_code
        'sql5 &= "AND m_ecole =  '" & Session("m_ecole") & "'   AND m_periode_ref =  '" & Session("m_periode_ref") & "'  AND m_classe_code =  '" & Session("m_classe_code") & "'  AND m_codecours =  '" & Session("m_codecours") & "'  "
        sql5 &= "AND m_ecole =  '" & Session("m_ecole") & "'   AND m_periode_ref =  '" & Session("m_periode_ref") & "'  AND m_codecours =  '" & Session("m_codecours") & "'  "
        sql5 &= "AND m_strand_code =  '" & Session("m_strand_code") & "'  AND m_anneescolaire = " & DropDownList16.SelectedValue & " AND m_membre_id = n_membre_id "
        sql5 &= "AND (   dn_date_remise = (SELECT MAX (dn_date_remise) FROM sp_documents_note n2 WHERE(n2.dn_evaluation_id = n_evaluation_id) "
        sql5 &= "AND n2.dn_membre_id = n_membre_id) OR dn_date_remise IS NULL ) ORDER BY m_nom, m_prenom "


        'sql5 = "SELECT  m_prenom as Prenom, m_nom as nom, m_a_quitte, pkg_sp.fc_niveau_a_afficher (n_comp1_sur100," & DropDownList16.SelectedValue  & ",'S') AS CC, pkg_sp.fc_niveau_a_afficher (n_comp2_sur100," & DropDownList16.SelectedValue  & ",'S') AS HP, pkg_sp.fc_niveau_a_afficher (n_comp3_sur100," & DropDownList16.SelectedValue  & ",'S') AS CO, pkg_sp.fc_niveau_a_afficher (n_comp4_sur100," & DropDownList16.SelectedValue  & ",'S') AS MA, pkg_sp.fc_niveau_a_afficher (n_note, " & DropDownList16.SelectedValue  & ", 'S') AS note, ROUND (n_note, 1) as note_sur_100, n_abs as abs FROM sp_eleves_resultats, sp_notes LEFT JOIN sp_documents_note ON dn_evaluation_id = n_evaluation_id AND dn_membre_id = n_membre_id WHERE n_evaluation_id = '430628' AND m_ecole =  '206'   AND m_periode_ref =  '00'  AND m_classe_code =  'PSE4U1-01'  AND m_codecours =  'PSE4U1'  AND m_strand_code =  'xxx'  AND m_anneescolaire = " & DropDownList16.SelectedValue  & " AND m_membre_id = n_membre_id AND (   dn_date_remise = (SELECT MAX (dn_date_remise) FROM sp_documents_note n2 WHERE(n2.dn_evaluation_id = n_evaluation_id) AND n2.dn_membre_id = n_membre_id) OR dn_date_remise IS NULL ) ORDER BY m_nom, m_prenom "

        SqlDataSource5.SelectCommand = sql5
        'RadGrid3.DataBind()
    End Sub
    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged


        'faire un test sur ecole
        ' si sec
        'alors on cache les RG ele
        ' si ele
        'alors on cache les RG sec

        Session("ID_Enseignant") = DropDownList1.SelectedValue
        Session("ID_Evaluation") = ""
        'DropDownList2.Enabled = True

        Session("m_ecole") = ""
        Session("m_classe_code") = ""
        Session("m_codecours") = ""
        Session("m_periode_ref") = ""
        Session("m_strand_code") = ""

        'RadGrid1.DataSource = Nothing
        'RadGrid1.DataBind()
        load_data(True, True)

    End Sub

    Protected Sub DropDownList2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList2.SelectedIndexChanged
        ' Session("ID_Enseignant") = DropDownList1.SelectedValue
        Session("ID_Evaluation") = ""
        Dim unique_id As String = DropDownList2.SelectedValue

        Dim sql As String
        'parfois problème avec classe_code
        'sql = "select * from gn_vw_classes_sec where (ecole || codecours || cours_section || classe_code || strand_code || report_period) = '" & unique_id & "' "
        sql = "select * from gn_vw_classes_sec where (ecole || codecours || cours_section || strand_code || report_period) = '" & unique_id & "' "
        Dim ds As DataSet = New OracleHelperAE().getDataSet(sql.ToString)


        If ds.Tables(0).Rows.Count > 0 Then
            Session("m_ecole") = ds.Tables(0).Rows(0)("ECOLE").ToString
            Session("m_classe_code") = ds.Tables(0).Rows(0)("CLASSE_CODE").ToString
            Session("m_codecours") = ds.Tables(0).Rows(0)("CODECOURS").ToString
            Session("m_periode_ref") = ds.Tables(0).Rows(0)("REPORT_PERIOD").ToString ' A VÉRIFIER
            Session("m_strand_code") = ds.Tables(0).Rows(0)("STRAND_CODE").ToString
        End If

        load_data(False, True)


    End Sub


    Protected Sub DropDownList2_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList2.DataBound
        DropDownList2_SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub DropDownList3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList3.SelectedIndexChanged
        Session("ID_Evaluation") = DropDownList3.SelectedValue
        load_data_rg3()
        RadGrid3.DataBind()
    End Sub
    Protected Sub DropDownList16_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim sql1 As String = ""
        'sql1 &= "SELECT DISTINCT GNE1.M_NOM || ', ' || GNE1.M_PRENOM || ' - ' || GNE1.M_ECOLE AS NOM, GNE1.M_MEMBRE_ID "
        'sql1 &= "FROM GN_VW_ENSEIGNANTS GNE1 "
        'sql1 &= "LEFT OUTER JOIN GN_VW_ENSEIGNANTS GNE2 "
        'sql1 &= "ON GNE2.M_MEMBRE_ID = '" & Session("ID_USER") & "'  "
        'sql1 &= "WHERE (GNE1.TYPE_PERSON = 'E' OR GNE1.TYPE_PERSON = 'A') "
        'sql1 &= "AND GNE1.M_ECOLE = '" & DropDownList4.SelectedValue & "'  "
        'sql1 &= "ORDER BY NOM "


        sql1 &= "SELECT DISTINCT p.legal_surname || ', ' || p.legal_first_name || ' - ' || ss.school_code AS NOM,"
        sql1 &= "	p.person_id M_MEMBRE_ID, "
        sql1 &= "	p.legal_surname M_NOM, "
        sql1 &= "	p.legal_first_name m_prenom, "
        sql1 &= "	ss.school_code m_ecole, "
        sql1 &= "	DECODE (ss.staff_type_name, 'Teacher', 'E', 'Teacher Spec', 'E', 'Principal', 'D', 'Vice Principal', 'D', "
        sql1 &= "	'Principal A', 'D', 'Vice Principal A', 'D', 'A') type_person, "
        sql1 &= "	ss.teacher_flag, "
        sql1 &= "	SUBSTR ( DECODE (o.school_type_code, 'ConEd', 'Sec', o.school_type_code), 1, 1) palier, "
        sql1 &= "	s.school_name nom_ecole, "
        sql1 &= "	ss.school_year anneescolaire "
        sql1 &= "  FROM trill02.persons p, "
        sql1 &= "	school_staff ss, "
        sql1 &= "	organizations o, "
        sql1 &= "	schools s "
        sql1 &= "  WHERE p.person_id  = ss.person_id			   "
        sql1 &= "  AND ss.school_year = " & DropDownList16.SelectedValue & " "
        sql1 &= "  AND s.school_code     = ss.school_code "
        sql1 &= "  AND o.organization_id = s.default_school_bsid "
        sql1 &= "  AND o.board_bsid      = 'B67318' "
        sql1 &= "  AND s.active_flag     = 'x'  "
        sql1 &= " AND (DECODE (ss.staff_type_name, 'Teacher', 'E', 'Teacher Spec', 'E', 'Principal', 'D', 'Vice Principal', 'D', 'Principal A', 'D', 'Vice Principal A', 'D', 'A') = 'E' OR DECODE (ss.staff_type_name, 'Teacher', 'E', 'Teacher Spec', 'E', 'Principal', 'D', 'Vice Principal', 'D', 'Principal A', 'D', 'Vice Principal A', 'D', 'A') = 'A')  "
        sql1 &= " AND ss.school_code = '" & DropDownList4.SelectedValue & "'   "
        sql1 &= " ORDER BY NOM  "






        SqlDataSource1.SelectCommand = sql1
        DropDownList1.DataBind()


        load_data(True, True)
        'DropDownList2_SelectedIndexChanged(sender, e)



        'Session("ID_Evaluation") = DropDownList3.SelectedValue
        'load_data_rg3()
        'RadGrid3.DataBind()
    End Sub


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Chrono.Start()
    End Sub

    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

    'End Sub

    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        Chrono.Stop()
        'Response.Write("Temps: " & Chrono.ElapsedMilliseconds().ToString())

        ' Label1.Text = Chrono.ElapsedMilliseconds().ToString() & "ms"
    End Sub

    Protected Sub DropDownList16_SelectedIndexChanged1(sender As Object, e As System.EventArgs)

        Dim sql1 As String = ""
        'sql1 &= "SELECT DISTINCT GNE1.M_NOM || ', ' || GNE1.M_PRENOM || ' - ' || GNE1.M_ECOLE AS NOM, GNE1.M_MEMBRE_ID "
        'sql1 &= "FROM GN_VW_ENSEIGNANTS GNE1 "
        'sql1 &= "LEFT OUTER JOIN GN_VW_ENSEIGNANTS GNE2 "
        'sql1 &= "ON GNE2.M_MEMBRE_ID = '" & Session("ID_USER") & "'  "
        'sql1 &= "WHERE (GNE1.TYPE_PERSON = 'E' OR GNE1.TYPE_PERSON = 'A') "
        'sql1 &= "AND GNE1.M_ECOLE = '" & DropDownList4.SelectedValue & "'  "
        'sql1 &= "ORDER BY NOM "


        sql1 &= "SELECT DISTINCT p.legal_surname || ', ' || p.legal_first_name || ' - ' || ss.school_code AS NOM,"
        sql1 &= "	p.person_id M_MEMBRE_ID, "
        sql1 &= "	p.legal_surname M_NOM, "
        sql1 &= "	p.legal_first_name m_prenom, "
        sql1 &= "	ss.school_code m_ecole, "
        sql1 &= "	DECODE (ss.staff_type_name, 'Teacher', 'E', 'Teacher Spec', 'E', 'Principal', 'D', 'Vice Principal', 'D', "
        sql1 &= "	'Principal A', 'D', 'Vice Principal A', 'D', 'A') type_person, "
        sql1 &= "	ss.teacher_flag, "
        sql1 &= "	SUBSTR ( DECODE (o.school_type_code, 'ConEd', 'Sec', o.school_type_code), 1, 1) palier, "
        sql1 &= "	s.school_name nom_ecole, "
        sql1 &= "	ss.school_year anneescolaire "
        sql1 &= "  FROM trill02.persons p, "
        sql1 &= "	school_staff ss, "
        sql1 &= "	organizations o, "
        sql1 &= "	schools s "
        sql1 &= "  WHERE p.person_id  = ss.person_id			   "
        sql1 &= "  AND ss.school_year = " & DropDownList16.SelectedValue & " "
        sql1 &= "  AND s.school_code     = ss.school_code "
        sql1 &= "  AND o.organization_id = s.default_school_bsid "
        sql1 &= "  AND o.board_bsid      = 'B67318' "
        sql1 &= "  AND s.active_flag     = 'x'  "
        sql1 &= " AND (DECODE (ss.staff_type_name, 'Teacher', 'E', 'Teacher Spec', 'E', 'Principal', 'D', 'Vice Principal', 'D', 'Principal A', 'D', 'Vice Principal A', 'D', 'A') = 'E' OR DECODE (ss.staff_type_name, 'Teacher', 'E', 'Teacher Spec', 'E', 'Principal', 'D', 'Vice Principal', 'D', 'Principal A', 'D', 'Vice Principal A', 'D', 'A') = 'A')  "
        sql1 &= " AND ss.school_code = '" & DropDownList4.SelectedValue & "'   "
        sql1 &= " ORDER BY NOM  "






        SqlDataSource1.SelectCommand = sql1
        DropDownList1.DataBind()


        load_data(True, True)
        'DropDownList2_SelectedIndexChanged(sender, e)



        'Session("ID_Evaluation") = DropDownList3.SelectedValue
        'load_data_rg3()
        'RadGrid3.DataBind()
    End Sub
End Class
