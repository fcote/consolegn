<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script type="text/C#" runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using the navigator to show a finance chart with volume that utilizes finance bars for improved performance.

		chart.Size = "640x500"
		chart.TempDirectory = "temp"
		chart.Debug = True
		chart.Palette = New Color() { Color.FromArgb(255, 47, 107, 220), Color.FromArgb(255, 30, 156, 255) }

		' The chart area legend can dynamically update based on mouse position and visible range.
		chart.LegendBox.Position = LegendBoxPosition.ChartArea
		chart.LegendBox.Template = "%ICON%NAME%VALUE"

		chart.DefaultElement.Marker.Visible = False

		' Setup main chart axis settings
		chart.YAxis.Scale = Scale.Range
		chart.YAxis.FormatString = "Currency"

		' A starting view
		chart.XAxis.Viewport.ScaleRange = New ScaleRange(New DateTime(2010, 5, 1), DateTime.Now)

		' Enable the navigator
		chart.Navigator.Enabled = True

		' Database start date.
		Dim startDate As DateTime = New System.DateTime(2000, 12, 1, 0, 0, 0)

		Dim mySC As SeriesCollection = getPriceData(startDate)
		' Add the data.
		chart.SeriesCollection.Add(mySC)

		' --- Setup the volume area.
		Dim mySC2 As SeriesCollection = getVolumeData(startDate)
		Dim ca As ChartArea = New ChartArea()
		ca.HeightPercentage = 33

		' This culture setting is used so that the division in the axis tick below works.
		' With some culture settings, the value may get formatted in a way that internally wont be recognized as a number to divide.
		ca.YAxis.CultureName = "en-US"
		ca.YAxis.DefaultTick.Label.Text = "<" & "%Value/1000000> M"
		ca.SeriesCollection.Add(mySC2)

		chart.ExtraChartAreas.Add(ca)
	End Sub

	Function getPriceData(ByVal startDate As DateTime) As SeriesCollection
		Dim connectionString As String = ConfigurationSettings.AppSettings("DNCConnectionString")
        Dim SqlStatement As String = "SELECT TransDate,AdjClose FROM MSFT WHERE TransDate >= #STARTDATE# "
		Dim de As DataEngine = New DataEngine(connectionString, SqlStatement)
		de.ChartObject = chart
        de.DataFields = "XDateTime=TransDate,YValue=AdjClose"
		de.StartDate = startDate
		Dim mySC As SeriesCollection = de.GetSeries()

		' Apply price line settings.
		If mySC.Count > 0 Then
			mySC(0).Type = SeriesType.Line
			mySC(0).Line.Width = 2
			mySC(0).Name = "MSFT"
			mySC(0).LegendEntry.Value = "%PercentageChange"
		End If
		Return mySC
	End Function
	Function getVolumeData(ByVal startDate As DateTime) As SeriesCollection
		Dim connectionString As String = ConfigurationSettings.AppSettings("DNCConnectionString")
        Dim SqlStatement As String = "SELECT TransDate,Volume FROM MSFT WHERE TransDate >= #STARTDATE# "
		Dim de2 As DataEngine = New DataEngine(connectionString, SqlStatement)
		de2.ChartObject = chart
		de2.StartDate = startDate
        de2.DataFields = "XDateTime=TransDate,YValue=Volume"
		Dim mySC As SeriesCollection = de2.GetSeries()

		' Apply volume series settings.
		If mySC.Count > 0 Then
			mySC(0).Type = SeriesTypeFinancial.Bar

			' In order to improve the performance of this chart, the volume series will utilize the finance bars instead of full columns.
			For Each el As Element In mySC(0).Elements
				' This applies the y value to high and 0 to low so that the finance bars appear as a column series would.
				el.High = el.YValue
				el.Low = 0
			Next el

			mySC(0).Name = "Volume"
			' This ensures the chart area does not calculate values for ranges in the chart area legend.
			mySC(0).LegendEntry.Value = ""
			' The legend in the chart area will show the element's high value which is the same as the y value of a column series.
			mySC(0).DefaultElement.SmartLabel.Text = "%High"
		End If
		Return mySC
	End Function


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="chart" runat="server" />
		<br />
		<asp:Label ID="Label1" runat="server" />
	</div>
</body>
</html>
