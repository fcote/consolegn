<%@ Page Language="vb" debug="true" trace="false" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml" %>

<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   'This sample demonstrates how to use an xml string to generate a chart.
   'set Title 
   Chart.Title = "sales report"
   
   ' Set the x axis label
   Chart.ChartArea.XAxis.Label.Text = "X Axis Label"
   
   ' Set the y axis label
   Chart.ChartArea.YAxis.Label.Text = "Y Axis Label"
   
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   'Adding series programatically
   Chart.Series.Name = "Sales"
   Chart.Series.Data = GetXmlString()
   Chart.Series.DataFields = "xaxis=Name,yaxis=Total"
   Chart.SeriesCollection.Add()
End Sub 'Page_Load

Function GetXmlString() As String

	Dim xmlString As New StringBuilder("<?xml version='1.0' standalone='yes'?>")
	xmlString.Append("<NewDataSet>")
  	xmlString.Append("<OrderTable>")
    xmlString.Append("<Name>Aida</Name>")
    xmlString.Append("<Total>3759</Total>")
xmlString.Append("</OrderTable>")
xmlString.Append("<OrderTable>")
     xmlString.Append("<Name>Ain</Name>")
    xmlString.Append("<Total>3510</Total>")
xmlString.Append("</OrderTable>")
xmlString.Append("<OrderTable>")
xmlString.Append("<Name>Ali</Name>")
    xmlString.Append("<Total>5478</Total>")
xmlString.Append("</OrderTable>")
xmlString.Append("<OrderTable>")
     xmlString.Append("<Name>Barbi</Name>")
    xmlString.Append("<Total>3029</Total>")
xmlString.Append("</OrderTable>")
xmlString.Append("<OrderTable>")
xmlString.Append("<Name>Bard</Name>")
    xmlString.Append("<Total>2743</Total>")
xmlString.Append("</OrderTable>")
xmlString.Append("<OrderTable>")
     xmlString.Append("<Name>Bo</Name>")
    xmlString.Append("<Total>2331</Total>")
xmlString.Append("</OrderTable>")
xmlString.Append("<OrderTable>")
xmlString.Append("<Name>David</Name>")
    xmlString.Append("<Total>1954</Total>")
xmlString.Append("</OrderTable>")
xmlString.Append("<OrderTable>")
     xmlString.Append("<Name>Joe</Name>")
    xmlString.Append("<Total>7661</Total>")
xmlString.Append("</OrderTable>")
xmlString.Append("<OrderTable>")
xmlString.Append("<Name>John</Name>")
    xmlString.Append("<Total>7026</Total>")
xmlString.Append("</OrderTable>")
xmlString.Append("<OrderTable>")
     xmlString.Append("<Name>Kei</Name>")
    xmlString.Append("<Total>6408</Total>")
  xmlString.Append("</OrderTable>")
  xmlString.Append("<OrderTable>")
     xmlString.Append("<Name>Moe</Name>")
    xmlString.Append("<Total>6287</Total>")
  xmlString.Append("</OrderTable>")
xmlString.Append("<OrderTable>")
     xmlString.Append("<Name>Pearl</Name>")
    xmlString.Append("<Total>4105</Total>")
  xmlString.Append("</OrderTable>")
xmlString.Append("</NewDataSet>")
Return xmlString.ToString()
End Function 'GetXmlString
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>XmlString Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
