<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>



<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   'set global properties
   Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   Chart.Title = "Sales for 2002"
   Chart.XAxis.Label.Text = "Months"
   Chart.YAxis.Label.Text = "Sales (USD)"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Mentor=false
   Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   
   Chart.DateGrouping = TimeInterval.Year
   Chart.Use3D = True
   
   Dim b As Axis = Chart.YAxis.Calculate("Sales (CAD)", New ChangeValueDelegate(AddressOf MyFunction))
   b.Orientation = dotnetCHARTING.Orientation.Right
   Chart.AxisCollection.Add(b)
   
   'Add a series
   Dim de As New DataEngine()
   de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   de.StartDate = New DateTime(2002, 1, 1, 0, 0, 0)
   de.EndDate = New DateTime(2002, 12, 31, 23, 59, 59)
   de.DateGrouping = TimeInterval.Year
   de.SqlStatement = "SELECT OrderDate, Total FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
   Dim sc As SeriesCollection = de.GetSeries()
   
   SetDataToDataGrid(sc)
   
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load

Public Sub SetDataToDataGrid(sc As SeriesCollection)
   Dim dt As New DataTable()
   Dim dataCol As New DataColumn()
   dataCol.ColumnName = "Months"
   dt.Columns.Add(dataCol)
   Dim j As Integer
   For j = 0 To sc.Count - 1
      Dim sr As dotnetCHARTING.Series = sc(j)
      sr.Name = "Sales"
      dataCol = New DataColumn()
      dataCol.ColumnName = "Sales (USD)"
      dt.Columns.Add(dataCol)
      
      dataCol = New DataColumn()
      dataCol.ColumnName = "Sales (CAD)"
      dt.Columns.Add(dataCol)
      
      Dim dr As DataRow = Nothing
      Dim k As Integer
      For k = 0 To sr.Elements.Count - 1
         If j = 0 Then
            dr = dt.NewRow()
            dr(j) = sr.Elements(k).Name
            dt.Rows.Add(dr)
         End If
         
         If sr.Elements(k).YValue.Equals(Double.NaN) Then
            dt.Rows(k)((j + 1)) = Chart.EmptyElementText
            dt.Rows(k)((j + 2)) = Chart.EmptyElementText
         Else
            dt.Rows(k)((j + 1)) = String.Format("{0:C}", sr.Elements(k).YValue)
            dt.Rows(k)((j + 2)) =(sr.Elements(k).YValue * 1.36).ToString("c")
         End If 
      Next k
   Next j
   
   Dim dv As New DataView(dt)
   chartdatagrid.DataSource = dv
   chartdatagrid.DataBind()
End Sub 'SetDataToDataGrid
 
Public Shared Function MyFunction(value As String) As String
   Dim dValue As Double = Convert.ToDouble(value)
   dValue = dValue * 1.36
   Return Convert.ToString(dValue)
End Function 'MyFunction
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>.netCHARTING Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
  <asp:DataGrid id="chartdatagrid" Width="50%" Font-Name="Arial" HeaderStyle-BackColor="skyblue"
BackColor="lightblue" runat="server"/>
</div>
</body>

</html>
