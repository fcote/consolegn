<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   ' Set the title.
   Chart.Title = "My Chart"
   
   'Enable streaming
   Chart.UseFile = False
   
   ' Change the shading mode
   Chart.ShadingEffectMode = ShadingEffectMode.Two
   
   ' Set the x axis label
   Chart.ChartArea.XAxis.Label.Text = "X Axis Label"
   
   ' Set the y axis label
   Chart.ChartArea.YAxis.Label.Text = "Y Axis Label"
   
   ' Set he chart size.
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   
   ' Add the random data.
   Chart.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 5
         Dim e As New Element()
         e.Name = "E " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   Return SC
End Function 'getRandomData
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Streaming Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
