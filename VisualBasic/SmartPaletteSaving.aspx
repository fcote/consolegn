<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Size = "700x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample"
   
   
   ' This sample demonstrates how to save a custom palette to the xml file.
   Chart.ChartArea.Label.Text = "This palette is saved to an xml file 'myColors.xml'" + ControlChars.Lf + " and is loaded in sample" + ControlChars.Lf + " SmartPaletteLoading.aspx"
   
   Dim an As New Annotation("Go to the sample that loads the myColors.xml file")
   
   an.URL = "SmartPaletteLoading.aspx"
   an.Label.Color = Color.Blue
   an.Label.Font = New Font("Arial", 8, FontStyle.Underline)
   an.Position = New PointF(590, 130)
   Chart.Annotations.Add(an)
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' here we'll use two different methods just for demonstration purposes. One will not have any effect.
   ' Calling this method from the series collection will generate the nameValuePairs for Series only.
   Dim ncp As SmartPalette = mySC.GetSmartPalette(New Color() {Color.Red, Color.Green, Color.Orange})
   ncp = mySC.GetSmartPalette(Palette.Three)
   
   ' Now get the element colors from series.  Both name color pairs cant be used because 
   ' element colors will override the series colors so this one wont be used, just demonstrated. 
   Dim ncp2 As SmartPalette = mySC(0).GetSmartPalette(Palette.Three)
   
   ' This will also work:
   Dim ncp3 As SmartPalette = mySC(0).GetSmartPalette(Chart.Palette)
   
   ' Save the series colors.
   ncp.SaveState("temp/myColors.xml")
   
   ' Use them on a chart.
   Chart.SmartPalette = ncp
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
