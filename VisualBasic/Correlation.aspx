<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of LeastSquaresRegressionLineX and LeastSquaresRegressionLineY from
   ' within Correlation class
   ' The Correlation Chart
   CorrelationChart.Title = "Correlation"
   CorrelationChart.TempDirectory = "temp"
   CorrelationChart.Debug = True
   CorrelationChart.Size = "600x400"
   CorrelationChart.LegendBox.Template = "%icon %name"
   CorrelationChart.XAxis.Scale = Scale.Normal
   CorrelationChart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   CorrelationChart.DefaultSeries.DefaultElement.Marker.Visible = False
   CorrelationChart.DefaultSeries.Type = SeriesType.Spline
   
   ' Generate the sample data.
   Dim scorrelation As New SeriesCollection()
   Dim sampledata1 As New Series("Sample 1")
   Dim b As Integer
   For b = 1 To 9
      Dim el As New Element()
      el.YValue = - Math.Sin(b) - b
      el.XValue = b
      sampledata1.Elements.Add(el)
   Next b
   scorrelation.Add(sampledata1)
   CorrelationChart.SeriesCollection.Add(scorrelation)
   
   ' Here we create a series which describe the regression line of Y on X using the 
   ' method of least squares
   CorrelationChart.SeriesCollection.Add(StatisticalEngine.LeastSquaresEstimateY("EstimateY", sampledata1))
   
   ' Here we create a series which describe the regression line of X on Y using the 
   ' method of least squaresLeastSquaresEstimatex
   Dim estimateX As Series = StatisticalEngine.LeastSquaresEstimateX("EstimateX", sampledata1)
   estimateX.DefaultElement.Color = Color.FromArgb(255, 99, 49)
   CorrelationChart.SeriesCollection.Add(estimateX)
End Sub 'Page_Load 

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="CorrelationChart" runat="server"/>			
		</div>
	</body>
</html>
