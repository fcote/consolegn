<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Residuals Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of EstimateX and EstimateY from
   ' within Correlation class
   ' The Correlation Chart
   CorrelationChart.Title = "Correlation"
   CorrelationChart.TempDirectory = "temp"
   CorrelationChart.Debug = True
   CorrelationChart.Size = "1000x400"
   CorrelationChart.LegendBox.Template = "%icon %name"
   CorrelationChart.XAxis.Scale = Scale.Normal
   
   Dim scorrelation As New SeriesCollection()
   Dim sampledata1 As New Series("Sample 1")
   Dim b As Integer
   For b = 1 To 9
      Dim el As New Element()
      el.YValue = - Math.Sin(b) * b
      el.XValue = b
      sampledata1.Elements.Add(el)
   Next b
   scorrelation.Add(sampledata1)
   scorrelation(0).Type = SeriesType.Spline
   
   CorrelationChart.SeriesCollection.Add(scorrelation)
   ' Takes off the marker off the line and spline series.
   CorrelationChart.DefaultSeries.DefaultElement.Marker = New ElementMarker(ElementMarkerType.None)
   
   
   
   ' Here we create a series which will hold 
   ' the estimated value of the Y variable when the X variable is known
   Dim estYPoints As New Series()
   Dim i As Integer
   For i = 1 To 9
      Dim estY As Element = StatisticalEngine.EstimateY(sampledata1, i)
      estYPoints.Elements.Add(estY)
   Next i
   estYPoints.Name = "EstYPoints"
   estYPoints.DefaultElement.Color = Color.FromArgb(255, 99, 49)
   estYPoints.DefaultElement.Marker = New ElementMarker(ElementMarkerType.Circle)
   estYPoints.Type = SeriesType.Marker
   CorrelationChart.SeriesCollection.Add(estYPoints)
   
   
   ' Here we create a series which will hold 
   ' the estimated value of the X variable when the Y variable is known
   Dim estXPoints As New Series()
   Dim j As Integer
   For j = 1 To 9
      Dim estX As Element = StatisticalEngine.EstimateX(sampledata1, - j)
      estXPoints.Elements.Add(estX)
   Next j
   estXPoints.Name = "EstXPoints"
   estXPoints.DefaultElement.Marker = New ElementMarker(ElementMarkerType.Triangle)
   estXPoints.Type = SeriesType.Marker
   CorrelationChart.SeriesCollection.Add(estXPoints)
   
   Dim residualsChartArea As New ChartArea("Residuals")
   residualsChartArea.HeightPercentage = 20
   CorrelationChart.ExtraChartAreas.Add(residualsChartArea)
   
   ' Here we create a series which display the residuals
   ' in accordance with the regression line constructed using LeastSquaresRegressionLineX 
   Dim residuals As Series = StatisticalEngine.Residuals(sampledata1)
   residuals.Type = SeriesType.Spline
   residuals.Name = "Residuals"
   residuals.DefaultElement.Color = Color.FromArgb(125, 255, 49)
   residualsChartArea.SeriesCollection.Add(residuals)
   
   ' Here we calculate the residual Average Of the data sample
   Dim residualAvg As Element = StatisticalEngine.ResidualsAverage(sampledata1)
   residualAvg.XValue = 1
   Dim resAvg As New Series()
   resAvg.Type = SeriesType.Marker
   resAvg.DefaultElement.Marker = New ElementMarker(ElementMarkerType.Circle, 5, Color.FromArgb(255, 99, 49))
   resAvg.Name = "ResidualAverage"
   If residualAvg.YValue = 0 Then
      Console.WriteLine("The residuals average is 0.")
   Else
      resAvg.Elements.Add(residualAvg)
      resAvg.DefaultElement.Color = Color.FromArgb(99, 255, 49)
      residualsChartArea.SeriesCollection.Add(resAvg)
   End If
End Sub 'Page_Load 


		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="CorrelationChart" runat="server"/>
		</div>
	</body>
</html>
