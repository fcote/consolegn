<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using a stacked pie ring to indicate which pie elements belong to which series or group.

		Chart.TempDirectory = "temp"
		Chart.Debug = True

		Chart.Type = ChartType.PiesNested
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"

		Chart.LegendBox.Visible = False

		Chart.DefaultElement.ShowValue = True
		Chart.DefaultElement.SmartLabel.Text = "%Name"

		Chart.DefaultElement.Transparency = 75
		Chart.SpacingPercentageNested = 0


		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		Chart.SeriesCollection.Add(getProcessed(mySC))

		Chart.YAxis.Scale = Scale.Stacked



	End Sub

	Function getProcessed(ByVal sc As SeriesCollection) As SeriesCollection
		Dim s1 As Series = New Series()
		Dim s2 As Series = New Series()
		Dim i As Integer = 1
		Dim s As Series
		For Each s In sc
			s1.Elements.Add(s.Elements)
			Dim sumE As Element = s.Calculate("Group " & i.ToString(), Calculation.Sum)
			s2.Elements.Add(sumE)
			i += 1
		Next s

		s1.DefaultElement.SmartLabel.OutlineColor = Color.White
		s2.DefaultElement.ShowValue = True
		s2.DefaultElement.SmartLabel.Font = New Font("Arial", 10, FontStyle.Bold)
		s2.DefaultElement.SmartLabel.Text = "%Name"
		s1.DefaultElement.BubbleSize = 50
		s2.DefaultElement.BubbleSize = 5
		s2.DefaultElement.Transparency = 0
		Return New SeriesCollection(s2, s1)

	End Function

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(5)
        Dim SC As SeriesCollection = New SeriesCollection()
        Dim a As Integer
        Dim b As Integer
		For a = 1 To 3
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 2
				Dim e As Element = New Element("Element " & (b + (a * 2) - 2).ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
