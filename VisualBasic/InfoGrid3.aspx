<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates how to set up an infoGrid using MicroCharts and data from a Series object.

		Chart.Margin = "10"
		Chart.TempDirectory = "temp"

		Chart.ObjectChart = New dotnetCHARTING.Label(getGridText(getRandomData()))

	End Sub

	Function getGridText(ByVal sc As SeriesCollection) As String
		Dim sb As StringBuilder = New StringBuilder()
		sb.Append("<block fStyle='Bold' vAlign='Center'>Series<Chart:Scale min='0' max='100' value='%Value%'>")
		For Each s As Series In sc
			sb.Append("<row>" & s.Name & "<Chart:BarFull values='" & s.GetYValueList() & "'  shading='7'>")
		Next s

		Return sb.ToString()
	End Function

	Function getRandomData() As SeriesCollection
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim myR As Random = New Random()
		For a As Integer = 0 To 6
			Dim s As Series = New Series()
			s.Name = "Series " & a
			For b As Integer = 0 To 3
				Dim e As Element = New Element()
				e.Name = "Element " & b
				'e.YValue = -25 + myR.Next(50);
				e.YValue = myR.Next(1000)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
