<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="VB" Debug="true" Trace="false" EnableSessionState="true" Description="dotnetCHARTING Component" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Paging Elements</title>
		<script runat="server">
Dim currentPage As Integer = 1
Dim pageSize As Integer = 4

Sub Page_Load(sender As [Object], e As EventArgs)
   'set global properties
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(700)
   Chart.Height = Unit.Parse(300)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Item sales"
   Chart.TitleBox.Position = TitleBoxPosition.Right
   Chart.TitleBox.ClearColors()
   Chart.XAxis.Label.Text = "Year 2002"
   
   
   ' Get the page from the query string.
   If Not (Request.QueryString("Page") Is Nothing) Then
      currentPage = Convert.ToInt32(Request.QueryString("Page"))
   End If 
   'set global database properties
   Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   Chart.DefaultSeries.StartDate = New System.DateTime(2002, 1, 1, 0, 0, 0)
   Chart.DefaultSeries.EndDate = New System.DateTime(2002, 12, 31, 23, 59, 59)
   Chart.DefaultSeries.SqlStatement = "SELECT Name,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Name"   
   
   'Add a series
   Chart.Series.Name = "Items 1"
   Chart.Series.Limit = currentPage.ToString() + "x" + pageSize.ToString()
   Chart.Series.LimitMode = dotnetCHARTING.LimitMode.Top
   Chart.Series.ShowOther = False
   Chart.SeriesCollection.Add()
   
   'Add a series
   Chart.Series.Name = "Other"
   Chart.Series.Limit = currentPage.ToString() + "x" + pageSize.ToString()
   Chart.Series.LimitMode = dotnetCHARTING.LimitMode.ExcludeTop
   Chart.Series.ShowOther = False
   'Chart.Series.Visible=false;
   Chart.SeriesCollection.Add()
  
   
   ' Create an annotation button          
   Dim an2 As New Annotation(" < Previous")
   setAnnotation(an2)
   
   an2.Position = New Point(49, 20)
   an2.CornerTopLeft = BoxCorner.Cut
   an2.CornerTopRight = BoxCorner.Square
   an2.Size = New Size(85, 16)
   Chart.Annotations.Add(an2)
   
   ' Activate it
   If currentPage > 1 Then
      an2.URL = "PagingElements.aspx?page=" + Convert.ToString((currentPage - 1))
   Else
      an2.Label.Color = Color.Gray
   End If 
  
   AddHandler Chart.PostDataProcessing, AddressOf OnPostDataProcessing
End Sub 'Page_Load
 

Sub setAnnotation(an As Annotation)
   ' Set default annotation properties
   an.DefaultCorner = BoxCorner.Square
   an.Size = New Size(66, 16)
   an.Background = New Background(Color.FromArgb(241, 250, 180), Color.White, 90)
   an.Label.Font = New Font("Verdana", 7.7F, FontStyle.Bold)
   an.CornerTopRight = BoxCorner.Cut
   an.Padding = 2
End Sub 'setAnnotation
 

Sub OnPostDataProcessing(sender As [Object])
   Dim totalElements As Integer = Chart.SeriesCollection(0).TotalElements
   Dim totalPage As Integer = totalElements / pageSize
   
   If totalElements Mod pageSize <> 0 Then 
    totalPage = totalPage +1
    End If
   
   'Create the next button annotation
   Dim an As New Annotation("Next >")
   setAnnotation(an)
   an.Position = New Point(224, 20)
   
   ' Activate it
   If totalPage > currentPage Then
      an.URL = "PagingElements.aspx?page=" + Convert.ToString((currentPage + 1))
   Else
      an.Label.Color = Color.Gray
   End If 
   ' 	
   Dim an2 As New Annotation("(Page " + currentPage.ToString() + " of " + totalPage.ToString() + ")")
   
   
   setAnnotation(an2)
   an2.Size = New Size(90, 16)
   an2.Position = New Point(134, 20)
   an2.CornerTopRight = BoxCorner.Square
   an2.Label.Font = New Font("Arial", 7.7F)
   an2.Label.Color = Color.DarkBlue
   
   Chart.Annotations.Add(an, an2)
   
   Dim otherElements As ElementCollection = Chart.SeriesCollection(1).Elements
   Dim otherList As New StringBuilder()
   Dim el As Element
   For Each el In  otherElements
      otherList.Append((el.Name + ", "))
   Next el 
   If otherList.Length > 0 Then
      otherList.Insert(0, "Others:" + ControlChars.Lf)
      otherList.Remove(otherList.Length - 2, 2)
      
      Dim an3 As New Annotation(otherList.ToString())
      an3.Position = New Point(580, 60)
      an3.Size = New Size(110, 60)
      an3.CornerTopLeft = BoxCorner.Square
      an3.Label.Font = New Font("Arial", 7.7F)
      an3.Label.Color = Color.Black
      
      Chart.Annotations.Add(an, an3)
   End If 
   Chart.SeriesCollection.RemoveAt(1)
End Sub 'OnPostDataProcessing 


		</script>
	<style type="text/css">
      body.myBody  
      {
       border-top-width:0px;
       border-left-width:0px;
     
     }
     </style>
	</head>
	<body class="myBody">
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" />
		</div>
	</body>
</html>
