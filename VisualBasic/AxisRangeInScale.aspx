<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using an axis marker with IncludeInAxisScale set to true. The axis min/max snap to the marker range automatically.

		SetChart(Chart)
		SetChart(Chart1)

		Chart1.Type = ChartType.Combo
		Chart1.DefaultSeries.Type = SeriesType.Marker
		Chart1.LegendBox.Position = New Point(55, 85)
		Chart1.Annotations(0).Position = New Point(55,25)
		Chart1.DefaultElement.Marker.Size = 20
	End Sub


	Sub SetChart(ByVal c As Chart)
		c.Type = ChartType.Gauges
		c.Size = "600x350"
		c.LegendBox.Position = New Point(10, 70)
		c.TempDirectory = "temp"
		c.Debug = True
		c.Palette(0) = Color.DodgerBlue
		c.DefaultBox.DefaultCorner = BoxCorner.Round
		c.ShadingEffectMode = ShadingEffectMode.Three

		' Style the chart area.
		c.ChartArea.Background.ShadingEffectMode = ShadingEffectMode.Background2
		c.ChartArea.Background.Color = Color.GreenYellow

		' Add an annotation.
		Dim an As Annotation = New Annotation("Axis min/max will snap to range AxisMarkers with AxisMarker.IncludeInAxisScale = true ")
		an.Position = New Point(10, 10)
		an.Size = New Size(190, 50)
		c.Annotations.Add(an)

		' Style the gauge.
		c.DefaultSeries.GaugeBorderBox.Background.Color = Color.OldLace
		c.YAxis.DefaultTick.Label.Font = New Font("Arial", 14, FontStyle.Bold)

		' Add axis markers.
		Dim am As AxisMarker = New AxisMarker("Warning", Color.Red, 80, 100)
		am.IncludeInAxisScale = True
		Dim am2 As AxisMarker = New AxisMarker("Warning", Color.Red, -20, 0)
		am2.IncludeInAxisScale = True

		c.YAxis.Markers.Add(am, am2)

		' Add some data
		Dim s As Series = Series.FromYValues(15)
		c.SeriesCollection.Add(s)
	End Sub

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />

		 <dnc:Chart ID="Chart1" runat="server" />
	</div>
</body>
</html>
