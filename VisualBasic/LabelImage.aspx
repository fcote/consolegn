<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using an image within a label.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.Type = ChartType.Combo
		Chart.Size = "550x500"
		Chart.Title = ".netCHARTING Sample"
		Chart.DefaultSeries.Type = SeriesType.Bar
		Chart.DefaultElement.ShowValue = True
		'Chart.LegendBox.Visible = false;
		Chart.LegendBox.Template = "%Name%Chart%Value"
		Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom
		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		Dim path As String = ""
		For i As Integer = 0 To mySC.Count - 1
			mySC(i).DefaultElement.Color = Chart.Palette(i)
			path = getSmallChart(mySC(i))
			mySC(i).LegendEntry.CustomAttributes.Add("Chart", "<img:" & path & ">")

		Next i


		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 2
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 4
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function


	Function getSmallChart(ByVal s As Series) As String
		Dim c As Chart = New Chart()
		c.SmallChartMode = True
		c.ChartArea.ClearColors()
		c.DefaultElement.Marker.Size = 2
		c.Size = "100,30"
		c.Background.Color = Color.Transparent
		c.DefaultAxis.Line.Color = Color.Empty
		c.DefaultSeries.Type = SeriesType.Line
		c.SeriesCollection.Add(s)
		c.TempDirectory = "temp"
		c.DefaultAxis.ClearValues = True
		Return c.FileManager.SaveImage()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
