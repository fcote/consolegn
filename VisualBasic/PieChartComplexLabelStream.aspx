<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample is used to provide a dynamic chart image for sample PieChartComplexLabel.aspx

		Chart.TempDirectory = "temp"
		Chart.Debug = True

		Chart.UseFile = False
		Chart.Type = ChartType.Combo
		Chart.Size = "20x20"
		Chart.SmallChartMode = True
		Chart.Use3D = True
		Chart.Depth = 0
		Chart.Background.Color = Color.Transparent

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim col As Color = Color.Green
		If Not Request.QueryString("color") Is Nothing Then
			Dim query As String = Request.QueryString("color")
			Dim cc As ColorConverter = New ColorConverter()
			If query.Length = 6 Then
				col = CType(cc.ConvertFromInvariantString("#" & query), Color)
			Else
				col = CType(cc.ConvertFromInvariantString(query), Color)
			End If
			'col = Color.FromArgb( col);

			Chart.LegendBox.Visible = False
			Chart.ChartArea.ClearColors()
			Chart.DefaultAxis.Clear()
			Chart.ShadingEffectMode = ShadingEffectMode.Three
			Dim el As Element = New Element("", 10)
			el.Color = col

			Chart.YAxis.ScaleRange = New ScaleRange(0, 10)
			Chart.XAxis.SpacingPercentage = 0
			Chart.DefaultAxis.ClearValues = True


			' Add the random data.
			Chart.SeriesCollection.Add(New Series("", el))
		Else
			Chart.Visible = False
			Label1.Text = "This chart is designed to display as a sub-chart within PieChartComplexLabel.aspx. Please view that page instead."
		End If
	End Sub



</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
	<asp:Label ID="Label1" runat="server" />
</body>
</html>
