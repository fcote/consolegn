<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>dotnetCHARTING Sample</title>
</head>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	'Enable zooming with just one property
	Chart.Zoomer.Enabled = True

	'limit the number of elements in view port
	Chart.XAxis.Viewport.ElementCount =10

	Chart.Debug = True
	Chart.TempDirectory = "temp"
	Chart.ShadingEffect = True
	Chart.YAxis.Label.Text = "Salary / Year"
	Chart.YAxis.FormatString = "currency"
	Chart.XAxis.Label.Text = "Employee Code"
	Chart.PaletteName = dotnetCHARTING.Palette.Bright
	Chart.Type = ChartType.Combo
	Chart.Size = "600x350"
	Chart.Title = "A chart with zooming, right click on the chart to access zooming"
	Chart.DefaultSeries.Type = SeriesType.Spline
	Chart.LegendBox.Position = LegendBoxPosition.None
	Chart.YAxis.Scale = Scale.Stacked
	Chart.DefaultElement.ShowValue = True
	Chart.DefaultSeries.DefaultElement.ToolTip = "%name of %SeriesName"
	Dim mySC As SeriesCollection= getRandomData()
	Chart.SeriesCollection.Add(mySC)
	Dim Mean As Element = mySC(0).Calculate("",Calculation.Mean)
	Dim am As AxisMarker = New AxisMarker("Mean",New Line(Color.Green,1), Mean.YValue)
	am.Label.Alignment = StringAlignment.Near
	Chart.YAxis.Markers.Add(am)

End Sub

 Function getRandomData() As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random(1)
	For a As Integer = 1 To 1
		Dim s As Series = New Series()
		s.Name = "Series " & a
		For b As Integer = 1 To 99
			Dim e As Element = New Element()
			e.Name = "Emp # " & b
			e.YValue = myR.Next(25000,100000)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC

 End Function
</script>

<body>
<center>
	 <dotnet:Chart ID="Chart" runat="server" />
</center>
</body>
</html>
