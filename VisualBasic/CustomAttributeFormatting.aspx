<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load( sender As Object ,e As EventArgs )


	Chart.Type = ChartType.Combo'Horizontal
	Chart.Size = "600x350"
	Chart.TempDirectory = "temp"
	Chart.Debug = true
	Chart.Title = ".netCHARTING Sample"
	
	Chart.DefaultElement.ToolTip = "%Name"
	' This sample demonstrates formatting custom element attributes through tokens.
	
	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Dim mySC As SeriesCollection = getRandomData()

    ' Add an attribute.
    mySC(0)(0).CustomAttributes.Add("attribute","555.555")
    mySC(0)(0).ShowValue = true

    ' Specify inline formatting for it.
    Chart.DefaultElement.SmartLabel.Text = "<%attribute,Currency>"
	
    ' Add the random data.
	Chart.SeriesCollection.Add(mySC)

End Sub

Function  getRandomData() As SeriesCollection

	Dim SC As New SeriesCollection()
	Dim myR As New Random(1)
	Dim a,b AS Integer
	for   a = 1 To  5 
	
		Dim  s As Series= new Series()
		s.Name = "Series " + a.ToString()
		For  b = 1 To 5 
		
			Dim e AS  Element= new Element()
			e.Name = "Element " + b.ToString()
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
