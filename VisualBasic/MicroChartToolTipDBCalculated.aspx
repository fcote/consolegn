<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates showing MicroChart tooltips based on SubValues created using the SeriesCollection.Calculate Method.

		' Setup the chart.
		Chart.Size = "600x350"
		Chart.Title = "<block hAlign='left'>2002 Sales<br>   Hover over bars for details"
		Chart.ShowDateInTitle = False
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.ShadingEffectMode = ShadingEffectMode.Four
		Chart.YAxis.FormatString = "Currency"

		' Style the title box & legend.
		Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
		Chart.TitleBox.Padding = 8
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Background2
		Chart.TitleBox.Background.Color = Color.LightGreen
		Chart.LegendBox.Template = "%Value"
		Chart.LegendBox.DefaultEntry.Value = "<block fSize='10'>%YSum"

		' Get the data; A series for each person with month elements.
		Dim mySC As SeriesCollection = getLiveData()

		' Transpose so it's a Series for each month with an element for each user.
		mySC.Transpose()

		' Calculate down to a single series with an element for each user and sub values for each month.
		Dim s As Series = mySC.Calculate("", Calculation.Sum, ElementValue.YValue)

		' Create a tooltip preview showing monthly details for each user element.
		Chart.DefaultElement.ToolTip = "<block fStyle='bold'>2002 Sales by %Name<hr>Total: %YValue<hr><Chart:Column values='%SubValueList' width='100' height='50' color='LimeGreen'><row><Chart:ScaleB values='Jan,Dec' width='100'>"

		' Hide the subValues.
		Chart.DefaultElement.DefaultSubValue.Visible = False

		' Add the random data.
		Chart.SeriesCollection.Add(s)


	End Sub

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.StartDate = New DateTime(2002, 1, 1, 0, 0, 0)
		de.EndDate = New DateTime(2002, 12, 31, 23, 59, 59)
		de.SqlStatement = "SELECT OrderDate,Total,Name FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY Name"
		de.DateGrouping = TimeInterval.Year
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
