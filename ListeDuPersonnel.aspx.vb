﻿
Imports System.Data
Imports System.DirectoryServices
Imports System.DirectoryServices.Protocols



Partial Class ListeDuPersonnel
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            cherche_user_groupe(DropDownList1.SelectedValue)
        End If


    End Sub


    Public Shared Function ServerCallback(ByVal conn As LdapConnection, ByVal certificate As System.Security.Cryptography.X509Certificates.X509Certificate) As Boolean
        'À FAIRE: AJOUTER LE CODE POUR VÉRIFIER LE CERTIFICAT
        Return True
    End Function

    Function cherche_user_groupe(ByVal group As String) As Array 'Element

        Dim distinguishedname As String = ""
        Dim entry As DirectoryEntry = Nothing

        Dim domaine As String = "CONSEIL"
        Dim serveur As String
        Dim aps As AppSettingsReader = New AppSettingsReader()

        If domaine = "CONSEIL" Then
            serveur = aps.GetValue("LDAPServeurCONSEIL", System.Type.GetType("System.String")).ToString()
        Else
            serveur = aps.GetValue("LDAPServeurACADEMIQUE", System.Type.GetType("System.String")).ToString()
        End If

        Try
            entry = New DirectoryEntry(serveur, domaine & "\adread", "AdreaD64")
            Dim NativeObject As Object = entry.NativeObject

        Catch ex As Exception
            entry.Close()
            entry.Dispose()
        End Try



        'Dim con As LdapConnection

        Dim con As System.DirectoryServices.Protocols.LdapConnection = New LdapConnection(New LdapDirectoryIdentifier("172.29.241.1:636"))

        con.AuthType = System.DirectoryServices.Protocols.AuthType.Basic

        'If secure Then
        'Secure
        con.SessionOptions.SecureSocketLayer = True
        con.SessionOptions.VerifyServerCertificate = New VerifyServerCertificateCallback(AddressOf ServerCallback)
        'End If
        con.Bind(New System.Net.NetworkCredential("adread", "AdreaD64", "CONSEIL"))

        Dim request As SearchRequest
        request = New SearchRequest("OU=Utilisateurs,OU=" & group.ToUpper & ",OU=SERVICES,OU=ADMINISTRATIF,DC=csdccs,DC=edu,DC=on,DC=ca", "CN=*", System.DirectoryServices.Protocols.SearchScope.Subtree)
        Dim sres As SearchResponse = CType(con.SendRequest(request), SearchResponse)
        Dim max As Integer = sres.Entries.Count


        For i As Integer = 0 To max - 1
            Dim entrySearch As SearchResultEntry = sres.Entries(i)

            Dim dn As String = entrySearch.DistinguishedName
            Dim cn As String = getValue(entrySearch.Attributes, "cn")
            Dim desc As String = getValue(entrySearch.Attributes, "description").ToString


            'If IsArray(desc) Then
            '    desc = desc(0)
            'End If

            Dim tel As String = getValue(entrySearch.Attributes, "telephoneNumber")

            If tel.Length > 4 Then
                tel = tel.Substring(tel.Length - 4)
            End If

            Dim Name As String = getValue(entrySearch.Attributes, "displayName")

            Response.Write("<div style='color:blue;'><b>" & Name & "</b></div>")
            Response.Write("<i>" & desc & "</i><br>")
            Response.Write("Poste " & tel & "<br>")
            Response.Write("<br>")
            'créer requête (delete * where .... + insert)
            'ou check and update or insert 
           


        Next



        Return Nothing

    End Function

    Private Function getValue(ByVal att As SearchResultAttributeCollection, ByVal attributeName As String) As String

        If attributeName.Length = 0 Then
            Return String.Empty
        End If

        If att(attributeName) Is Nothing Then
            Return String.Empty
        Else
            If att(attributeName).Count > 0 Then
                'Return att(attributeName).Item(0).ToString
                If Not att(attributeName).Item(0).GetType.FullName = "System.String" Then
                    Return Encoding.Default.GetString(att(attributeName).Item(0))
                Else
                    Return att(attributeName).Item(0).ToString
                End If
            Else
                Return String.Empty
            End If
        End If

    End Function
End Class
