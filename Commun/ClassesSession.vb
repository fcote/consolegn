Imports Microsoft.VisualBasic
Imports System.text
Imports System.Web

Public Interface IChangeValidatingPage
    ReadOnly Property ClientSetModifie() As String
    ReadOnly Property ClientValidation() As String
    Property ValiderChangement() As Boolean
    Sub ResetClientModifie()
End Interface

Public Class PresentationDeNotes
    Public Nom As String
    Public Comp1 As Enums.PresentationDeNotes
    Public Comp2 As Enums.PresentationDeNotes
    Public Comp3 As Enums.PresentationDeNotes
    Public Comp4 As Enums.PresentationDeNotes
    Public comp5 As Enums
    Public Moyenne As Enums.PresentationDeNotes

    Public Property Comp(ByVal i As Integer) As Enums.PresentationDeNotes
        Get
            Select Case i
                Case 1
                    Return Comp1
                Case 2
                    Return Comp2
                Case 3
                    Return Comp3
                Case 4
                    Return Comp4
                Case Else
                    Throw New Exception("Les comp�tences sont num�rot�es de 1 � 4 seulement.")
            End Select
        End Get
        Set(ByVal value As Enums.PresentationDeNotes)
            Select Case i
                Case 1
                    Comp1 = value
                Case 2
                    Comp2 = value
                Case 3
                    Comp3 = value
                Case 4
                    Comp4 = value
                Case Else
                    Throw New Exception("Les comp�tences sont num�rot�es de 1 � 4 seulement.")
            End Select
        End Set
    End Property

End Class

Public Class PreferencesUsager

    Private _preferences(Constantes.MAX_PREFS) As String

    Public Sub New(ByVal prefs() As String)
        _preferences = prefs
    End Sub

    Public Sub New()

    End Sub

    Public Property Preferences() As String()
        Get
            Return _preferences
        End Get
        Set(ByVal value As String())
            _preferences = value
        End Set
    End Property


End Class

<Serializable()> _
Public Class Formulaire '�tait Formulaire
    Implements System.Runtime.Serialization.ISerializable


    Private _id_form As String
    Private _nom_formulaire As String
    Private _nom_table_formulaire As String
    Private _nom_pdf As String
    Private _FLAGELEVE As String
    Private _form_name As String
    Private _description_formulaire As String

    'nouveau
    Private _type_form As String
    Private _id_niec As String
    Private _id_form_use As String
    Private _id_createur As String
    Private _id_employe_destinataire As String

    Private _affichage As String
    Private _action As String

    Private _id_table_form_param As String
    Private _status As String
    Private _message As String
    Private _print_form As Boolean

    Private _id_control As String
    Private _isPublic As Boolean


    'utilisation
    'contexte. formulaire.key = "key"
    '_statusByKey = contexte. formulaire.status

    ' Terminer
    Private _key As String
    Public _id_formByKey As New Dictionary(Of String, String)
    Private _nom_formulaireByKey As New Dictionary(Of String, String)
    Private _nom_table_formulaireByKey As New Dictionary(Of String, String)
    Private _nom_pdfByKey As New Dictionary(Of String, String)
    Public _FLAGELEVEByKey As New Dictionary(Of String, String)
    Private _form_nameByKey As New Dictionary(Of String, String)
    Private _description_formulaireByKey As New Dictionary(Of String, String)
    Public _type_formByKey As New Dictionary(Of String, String)
    Public _id_niecByKey As New Dictionary(Of String, String)
    Private _id_form_useByKey As New Dictionary(Of String, String)
    Public _id_createurByKey As New Dictionary(Of String, String)
    Public _id_employe_destinataireByKey As New Dictionary(Of String, String)
    Private _affichageByKey As New Dictionary(Of String, String)
    Private _actionByKey As New Dictionary(Of String, String)
    Public _id_table_form_paramByKey As New Dictionary(Of String, String)
    Public _statusByKey As New Dictionary(Of String, String)
    Public _messageByKey As New Dictionary(Of String, String)
    Private _print_formByKey As New Dictionary(Of String, Boolean)
    Public _id_controlByKey As New Dictionary(Of String, String)
    Private _isPublicByKey As New Dictionary(Of String, Boolean)

    'Private FORMULAIRE_VIDE ' name='FORMULAIRE_VIDE'  value='" & formulaire_vide & "' />"
    'Private ID_EMPLOYE ' name='ID_EMPLOYE'  value='" & id_employe_destinataire & "' />"
    'Private FORM_TABLE ' name='FORM_TABLE'  value='" & nom_table_form & "' />"
    'Private ID ' name='ID'  value='" & id_table_form & "' />"
    'Private TYPE_FORM ' name='TYPE_FORM'  value='HTML'"

    Public Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
        _id_form = info.GetString("_id_form")
        _nom_formulaire = info.GetString("_nom_formulaire")
        _nom_table_formulaire = info.GetString("_nom_table_formulaire")
        _nom_pdf = info.GetString("_nom_pdf")
        _FLAGELEVE = info.GetString("_FLAGELEVE")

        _description_formulaire = info.GetString("_description_formulaire")
        'nouveau
        _form_name = info.GetString("_form_name")
        _type_form = info.GetString("_type_form")
        _id_niec = info.GetString("_id_niec")
        _id_form_use = info.GetString("_id_form_use")
        _id_createur = info.GetString("_id_createur")
        _id_employe_destinataire = info.GetString("_id_employe_destinataire")
        _id_table_form_param = info.GetString("_id_table_form_param")
        _affichage = info.GetString("_affichage")
        _action = info.GetString("_action")
        _status = info.GetString("_status")
        _message = info.GetString("_message")
        _print_form = info.GetBoolean("_print_form")
        _id_control = info.GetString("_id_control")
        _isPublic = info.GetBoolean("_isPublic")


        initForm()


       
    End Sub
    Public Sub initForm()

        _id_formByKey.Add("", "")
        _nom_formulaireByKey.Add("", "")
        _nom_table_formulaireByKey.Add("", "")
        _nom_pdfByKey.Add("", "")
        _FLAGELEVEByKey.Add("", "")
        _form_nameByKey.Add("", "")
        _description_formulaireByKey.Add("", "")
        _type_formByKey.Add("", "HTML")
        _id_niecByKey.Add("", "")
        _id_form_useByKey.Add("", "")
        _id_createurByKey.Add("", "")
        _id_employe_destinataireByKey.Add("", "")
        _affichageByKey.Add("", "")
        _actionByKey.Add("", "")
        _id_table_form_paramByKey.Add("", "")
        _statusByKey.Add("", "")
        _messageByKey.Add("", "")
        _print_formByKey.Add("", Nothing)
        _id_controlByKey.Add("", "")
        _isPublicByKey.Add("", Nothing)

    End Sub

    Public Function FindKey(ByVal key As String) As Boolean
        If Not _id_formByKey Is Nothing Then
            Return _id_formByKey.ContainsKey(key)
        End If
        Return False
    End Function

    Public Sub InitKey(ByVal key As String)
        'If Contexte.formulaire.id_formByKey Is Nothing Then
        '    initForm()
        'End If

        If _id_formByKey Is Nothing Then
            initForm()
        End If

        If Not _id_formByKey.ContainsKey(key) Then
            _id_formByKey.Add(key, "")
        End If

        If Not _nom_formulaireByKey.ContainsKey(key) Then
            _nom_formulaireByKey.Add(key, "")
        End If
        If Not _nom_table_formulaireByKey.ContainsKey(key) Then
            _nom_table_formulaireByKey.Add(key, "")
        End If
        If Not _nom_pdfByKey.ContainsKey(key) Then
            _nom_pdfByKey.Add(key, "")
        End If
        If Not _FLAGELEVEByKey.ContainsKey(key) Then
            _FLAGELEVEByKey.Add(key, "")
        End If
        If Not _description_formulaireByKey.ContainsKey(key) Then
            _description_formulaireByKey.Add(key, "")
        End If
        If Not _form_nameByKey.ContainsKey(key) Then
            _form_nameByKey.Add(key, "")
        End If
        If Not _type_formByKey.ContainsKey(key) Then
            _type_formByKey.Add(key, "HTML")
        End If
        If Not _id_niecByKey.ContainsKey(key) Then
            _id_niecByKey.Add(key, "")
        End If
        If Not _id_form_useByKey.ContainsKey(key) Then
            _id_form_useByKey.Add(key, "")
        End If


        If Not _id_createurByKey.ContainsKey(key) Then
            _id_createurByKey.Add(key, "")
        End If
        If Not _id_employe_destinataireByKey.ContainsKey(key) Then
            _id_employe_destinataireByKey.Add(key, "")
        End If
        If Not _id_table_form_paramByKey.ContainsKey(key) Then
            _id_table_form_paramByKey.Add(key, "")
        End If
        If Not _affichageByKey.ContainsKey(key) Then
            _affichageByKey.Add(key, "")
        End If
        If Not _actionByKey.ContainsKey(key) Then
            _actionByKey.Add(key, "")
        End If
        If Not _statusByKey.ContainsKey(key) Then
            _statusByKey.Add(key, "")
        End If
        If Not _messageByKey.ContainsKey(key) Then
            _messageByKey.Add(key, "")
        End If
        If Not _print_formByKey.ContainsKey(key) Then
            _print_formByKey.Add(key, Nothing)
        End If
        If Not _id_controlByKey.ContainsKey(key) Then
            _id_controlByKey.Add(key, "")
        End If
        If Not _isPublicByKey.ContainsKey(key) Then
            _isPublicByKey.Add(key, Nothing)
        End If


    End Sub

    Public Sub New(ByVal id_form As String, ByVal nom_formulaire As String, ByVal nom_table_formulaire As String, ByVal nom_pdf As String, ByVal FLAGELEVE As String, ByVal form_name As String, ByVal description_formulaire As String)
        _id_form = id_form
        _nom_formulaire = nom_formulaire
        _nom_table_formulaire = nom_table_formulaire
        _nom_pdf = nom_pdf
        _FLAGELEVE = FLAGELEVE
        _form_name = form_name
        _description_formulaire = description_formulaire
        _isPublic = False

        initForm()
    End Sub

    Public Sub New(ByVal id_form As String, ByVal nom_formulaire As String, ByVal nom_table_formulaire As String, ByVal nom_pdf As String, ByVal FLAGELEVE As String, ByVal form_name As String, ByVal description_formulaire As String, ByVal isPublic As Boolean)
        _id_form = id_form
        _nom_formulaire = nom_formulaire
        _nom_table_formulaire = nom_table_formulaire
        _nom_pdf = nom_pdf
        _FLAGELEVE = FLAGELEVE
        _form_name = form_name
        _description_formulaire = description_formulaire
        _isPublic = isPublic


        initForm()
    End Sub

    Public Sub New()
        _id_form = ""
        _nom_formulaire = ""
        _nom_table_formulaire = ""
        _nom_pdf = ""
        _FLAGELEVE = ""
        _form_name = ""
        _description_formulaire = ""
        _isPublic = False


        initForm()
    End Sub

    Public Sub New(ByVal id_form As String)
        _id_form = ""
        _nom_formulaire = ""
        _nom_table_formulaire = ""
        _nom_pdf = ""
        _FLAGELEVE = ""
        _form_name = ""
        _description_formulaire = ""
        _isPublic = False

        InitKey(id_form + ",0")
        _key = id_form + ",0"

    End Sub

    Public Property print_form() As Boolean
        Get
            Return _print_form
        End Get
        Set(ByVal value As Boolean)
            _print_form = value
        End Set
    End Property

    Public Property isPublic() As Boolean
        Get
            Return _isPublic
        End Get
        Set(ByVal value As Boolean)
            _isPublic = value
        End Set
    End Property

    Public Property id_form() As String
        Get
            Return _id_form
        End Get
        Set(ByVal value As String)
            _id_form = value
        End Set
    End Property

    Public Property nom_formulaire() As String
        Get
            Return _nom_formulaire
        End Get
        Set(ByVal value As String)
            _nom_formulaire = value
        End Set
    End Property

    Public Property nom_table_formulaire() As String
        Get
            Return _nom_table_formulaire
        End Get
        Set(ByVal value As String)
            _nom_table_formulaire = value
        End Set
    End Property

    Public Property nom_pdf() As String
        Get
            Return _nom_pdf
        End Get
        Set(ByVal value As String)
            _nom_pdf = value
        End Set
    End Property

    Public Property FLAGELEVE() As String
        Get
            Return _FLAGELEVE
        End Get
        Set(ByVal value As String)
            _FLAGELEVE = value
        End Set
    End Property



    Public Property description_formulaire() As String
        Get
            Return _description_formulaire
        End Get
        Set(ByVal value As String)
            _description_formulaire = value
        End Set
    End Property
    'Nouveau
    Public Property form_name() As String
        Get
            Return _form_name
        End Get
        Set(ByVal value As String)
            _form_name = value
        End Set
    End Property
    Public Property type_form() As String
        Get
            Return _type_form
        End Get
        Set(ByVal value As String)
            _type_form = value
        End Set
    End Property
    Public Property id_niec() As String
        Get
            Return _id_niec
        End Get
        Set(ByVal value As String)
            _id_niec = value
        End Set
    End Property
    Public Property id_form_use() As String
        Get
            Return _id_form_use
        End Get
        Set(ByVal value As String)
            _id_form_use = value
        End Set
    End Property
    Public Property id_createur() As String
        Get
            Return _id_createur
        End Get
        Set(ByVal value As String)
            _id_createur = value
        End Set
    End Property
    Public Property id_employe_destinataire() As String
        Get
            Return _id_employe_destinataire
        End Get
        Set(ByVal value As String)
            _id_employe_destinataire = value
        End Set
    End Property
    Public Property id_table_form_param() As String
        Get
            Return _id_table_form_param
        End Get
        Set(ByVal value As String)
            _id_table_form_param = value
        End Set
    End Property
    Public Property affichage() As String
        Get
            Return _affichage
        End Get
        Set(ByVal value As String)
            _affichage = value
        End Set
    End Property
    Public Property action() As String
        Get
            Return _action
        End Get
        Set(ByVal value As String)
            _action = value
        End Set
    End Property
    Public Property status() As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            _status = value
        End Set
    End Property
    Public Property message() As String
        Get
            Return _message
        End Get
        Set(ByVal value As String)
            _message = value
        End Set
    End Property
    Public Property id_control() As String
        Get
            Return _id_control
        End Get
        Set(ByVal value As String)
            _id_control = value
        End Set
    End Property

    Public Property key() As String
        Get
            Return _key
        End Get
        Set(ByVal value As String)
            InitKey(value)
            _key = value
        End Set
    End Property



    ''' <summary>
    ''' '''''''''''''''''''''''''''''''''''''''''''''
    ''' </summary>
    ''' <param name="info"></param>
    ''' <param name="context"></param>
    ''' <remarks></remarks>
    Public Property print_formByKey() As Boolean
        Get
            Return _print_formByKey(_key)
        End Get
        Set(ByVal value As Boolean)
            _print_formByKey(_key) = value
        End Set
    End Property

    Public Property isPublicByKey() As Boolean
        Get
            Return _isPublicByKey(_key)
        End Get
        Set(ByVal value As Boolean)
            _isPublicByKey(_key) = value
        End Set
    End Property

    Public Property id_formByKey() As String
        Get
            Return _id_formByKey(_key)
        End Get
        Set(ByVal value As String)
            _id_formByKey(_key) = value
            '_id_formByKey.Add(_key, value)
        End Set
    End Property

    Public Property nom_formulaireByKey() As String
        Get
            Return _nom_formulaireByKey(_key)
        End Get
        Set(ByVal value As String)
            _nom_formulaireByKey(_key) = value
        End Set
    End Property

    Public Property nom_table_formulaireByKey() As String
        Get
            Return _nom_table_formulaireByKey(_key)
        End Get
        Set(ByVal value As String)
            _nom_table_formulaireByKey(_key) = value
        End Set
    End Property

    Public Property nom_pdfByKey() As String
        Get
            Return _nom_pdfByKey(_key)
        End Get
        Set(ByVal value As String)
            _nom_pdfByKey(_key) = value
        End Set
    End Property

    Public Property FLAGELEVEByKey() As String
        Get
            Return _FLAGELEVEByKey(_key)
        End Get
        Set(ByVal value As String)
            _FLAGELEVEByKey(_key) = value
        End Set
    End Property



    Public Property description_formulaireByKey() As String
        Get
            Return _description_formulaireByKey(_key)
        End Get
        Set(ByVal value As String)
            _description_formulaireByKey(_key) = value
        End Set
    End Property
    'Nouveau
    Public Property form_nameByKey() As String
        Get
            Return _form_nameByKey(_key)
        End Get
        Set(ByVal value As String)
            _form_nameByKey(_key) = value
        End Set
    End Property
    Public Property type_formByKey() As String
        Get
            Return _type_formByKey(_key)
        End Get
        Set(ByVal value As String)
            _type_formByKey(_key) = value
        End Set
    End Property
    Public Property id_niecByKey() As String
        Get
            Return _id_niecByKey(_key)
        End Get
        Set(ByVal value As String)
            _id_niecByKey(_key) = value
        End Set
    End Property
    Public Property id_form_useByKey() As String
        Get
            Return _id_form_useByKey(_key)
        End Get
        Set(ByVal value As String)
            _id_form_useByKey(_key) = value
        End Set
    End Property
    Public Property id_createurByKey() As String
        Get
            Return _id_createurByKey(_key)
        End Get
        Set(ByVal value As String)
            _id_createurByKey(_key) = value
        End Set
    End Property
    Public Property id_employe_destinataireByKey() As String
        Get
            Return _id_employe_destinataireByKey(_key)
        End Get
        Set(ByVal value As String)
            _id_employe_destinataireByKey(_key) = value
        End Set
    End Property
    Public Property id_table_form_paramByKey() As String
        Get
            Return _id_table_form_paramByKey(_key)
        End Get
        Set(ByVal value As String)
            _id_table_form_paramByKey(_key) = value
        End Set
    End Property
    Public Property affichageByKey() As String
        Get
            Return _affichageByKey(_key)
        End Get
        Set(ByVal value As String)
            _affichageByKey(_key) = value
        End Set
    End Property
    Public Property actionByKey() As String
        Get
            Return _actionByKey(_key)
        End Get
        Set(ByVal value As String)
            _actionByKey(_key) = value
        End Set
    End Property
    Public Property statusByKey() As String
        Get
            Return _statusByKey(_key)
        End Get
        Set(ByVal value As String)
            _statusByKey(_key) = value
        End Set
    End Property
    Public Property messageByKey() As String
        Get
            Return _messageByKey(_key)
        End Get
        Set(ByVal value As String)
            _messageByKey(_key) = value
        End Set
    End Property
    Public Property id_controlByKey() As String
        Get
            Return _id_controlByKey(_key)
        End Get
        Set(ByVal value As String)
            _id_controlByKey(_key) = value
        End Set
    End Property

    ''' <summary>
    ''' '''''''''''''''''''''''''''''''''''''''''''
    ''' 
    ''' </summary>
    ''' <param name="info"></param>
    ''' <param name="context"></param>
    ''' <remarks></remarks>


    Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData
        info.AddValue("_id_form", _id_form)
        info.AddValue("_nom_formulaire", _nom_formulaire)
        info.AddValue("_nom_table_formulaire", _nom_table_formulaire)
        info.AddValue("_nom_pdf", _nom_pdf)
        info.AddValue("_FLAGELEVE", _FLAGELEVE)
        info.AddValue("_form_name", _form_name)
        info.AddValue("_description_formulaire", _description_formulaire)
        'nouveau
        info.AddValue("_type_form", _type_form)
        info.AddValue("_id_niec", _id_niec)
        info.AddValue("_id_form_use", _id_form_use)
        info.AddValue("_id_createur", _id_createur)
        info.AddValue("_id_employe_destinataire", _id_employe_destinataire)
        info.AddValue("_id_table_form_param", _id_table_form_param)
        info.AddValue("_affichage", _affichage)
        info.AddValue("_action", _action)
        info.AddValue("_status", _status)
        info.AddValue("_message", _message)
        info.AddValue("_print_form", _print_form)
        info.AddValue("_id_control", _id_control)
    End Sub

    Public Overrides Function ToString() As String
        'Pas besoin?
        'Dim info As System.Text.StringBuilder = New System.Text.StringBuilder("")

        'info.AppendLine("Id: " & _id)
        'info.AppendLine("Description: " & _desc)
        'info.AppendLine("Date: " & _date)
        'info.AppendLine("Pond�ration: " & _pond)

        'Return info.ToString()
        Return ""
    End Function

End Class


<Serializable()> _
Public Class Usager
    Implements System.Runtime.Serialization.ISerializable

    Private _login As String
    Private _ID As String
    Private _prenom As String
    Private _nom As String
    Private _courriel As String = String.Empty
    Private _palier As Paliers
    Private _motdepasse As String
    Private _preferences As PreferencesUsager = New PreferencesUsager
    Private _paliersEnseignes As Boolean()
    Private _role As Securite.RolesCollection = New Securite.RolesCollection()
    Private _titulaire As Boolean
    Private _admin As Boolean
    Private _ID_administration As String = ""
    Private _ecole As String
    Private _memberOf As String 'Exemple: SRI-Programmeur
    Private _groupe As String 'Exemple: SRI
    Private _groupe_sap As String 'Exemple: SRI-Programmeur
    Private _fonction As String 'Exemple: Programmeur

    Public Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
        _login = info.GetValue("_login", System.Type.GetType("System.String")).ToString()
        _ID = info.GetValue("_ID", System.Type.GetType("System.String")).ToString()
        _prenom = info.GetValue("_prenom", System.Type.GetType("System.String")).ToString()
        _nom = info.GetValue("_nom", System.Type.GetType("System.String")).ToString()
        '_palier = CType(info.GetValue("_palier", System.Type.GetType("System.Int32")), Commun.Paliers)
        _motdepasse = info.GetValue("_motdepasse", System.Type.GetType("System.String")).ToString()
        SetPreferences(CType(info.GetValue("_preferences", System.Type.GetType("System.String[]")), String()))
        _paliersEnseignes = CType(info.GetValue("_paliersEnseignes", System.Type.GetType("System.Boolean[]")), Boolean())
        _role.SetValue(CType(info.GetValue("_role", System.Type.GetType("System.Int32")), System.Int32))
        _titulaire = CType(info.GetValue("_titulaire", System.Type.GetType("System.Boolean")), System.Boolean)
        _courriel = info.GetValue("_courriel", System.Type.GetType("System.String")).ToString()
        _admin = CType(info.GetValue("_admin", System.Type.GetType("System.Boolean")), System.Boolean)
        'modification Jr car bug ???
        '_ecole = info.GetValue("_ecole", System.Type.GetType("System.String")).ToString()
    End Sub

    Public Sub New(ByVal login As String, ByVal motDePasse As String, ByVal id As String, ByVal nom As String, ByVal prenom As String, ByVal palier As Paliers, Optional ByVal memberOf As String = "", Optional ByVal groupe As String = "", Optional ByVal groupe_sap As String = "")
        _login = login
        _ID = id
        _prenom = prenom
        _nom = nom
        _palier = palier
        _motdepasse = motDePasse
        _memberOf = memberOf
        _groupe = groupe
        _groupe_sap = groupe_sap ' concat�nation des champs groupe et desc_function de la table SAP.GROUPE_SAP
        _ID_administration = ""
    End Sub

    Public Sub New(ByVal id As String, ByVal nom As String, ByVal prenom As String, ByVal courriel As String)
        _ID = id
        _prenom = prenom
        _nom = nom
        _courriel = courriel
    End Sub

    Public Sub SetPreferences(ByVal prefs() As String)
        _preferences = New PreferencesUsager(prefs)
    End Sub

    Public ReadOnly Property Preferences() As PreferencesUsager
        Get
            Return _preferences
        End Get
    End Property

    Public WriteOnly Property PaliersEnseignes() As Boolean()
        Set(ByVal value As Boolean())
            _paliersEnseignes = value
        End Set
    End Property

    Public ReadOnly Property Administrateur() As Boolean
        Get
            Return _role.Contains(Securite.Roles.Administrateur)
        End Get
    End Property

    Public ReadOnly Property MemberOf() As String
        Get
            Return _memberOf
        End Get
    End Property

    Public ReadOnly Property Groupe() As String
        Get
            Return _groupe
        End Get
    End Property

    Public ReadOnly Property Groupe_sap() As String
        Get
            Return _groupe_sap
        End Get
    End Property

    Public ReadOnly Property Fonction() As String
        Get
            Return _fonction
        End Get
    End Property
    
    Public ReadOnly Property Palier() As Paliers
        Get
            Return _palier
        End Get
    End Property

    Public ReadOnly Property Roles() As Securite.RolesCollection
        Get
            Return _role
        End Get
    End Property

    Public ReadOnly Property Login() As String
        Get
            Return _login
        End Get
    End Property

    Public Property ID() As String
        Get
            Return _ID
        End Get
        Set(ByVal value As String)
            _ID = value
        End Set
    End Property
    Public Property Ecole() As String
        Get
            Return _ecole
        End Get
        Set(ByVal value As String)
            _ecole = value
        End Set
    End Property

    Public Property ID_administration() As String ' to_test
        Get ' to_test
            Return _ID_administration ' to_test
        End Get ' to_test
        Set(ByVal value As String) ' to_test
            _ID_administration = value ' to_test
        End Set ' to_test
    End Property ' to_test

    Public Property Nom() As String
        Get
            Return _nom
        End Get
        Set(ByVal value As String)
            _nom = value
        End Set
    End Property

    Public Property Prenom() As String
        Get
            Return _prenom
        End Get
        Set(ByVal value As String)
            _prenom = value
        End Set
    End Property

    Public ReadOnly Property MotDePasse() As String
        Get
            Return _motdepasse
        End Get
    End Property

    Public Property Courriel() As String
        Get
            Return _courriel
        End Get
        Set(ByVal value As String)
            _courriel = value
        End Set
    End Property

    Public Function EnseigneAuPalier(ByVal p As Paliers) As Boolean
        If _paliersEnseignes Is Nothing Then
            Return False
        Else
            Return _paliersEnseignes(p)
        End If
    End Function

    Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData
        info.AddValue("_login", _login)
        info.AddValue("_ID", _ID)
        info.AddValue("_prenom", _prenom)
        info.AddValue("_nom", _nom)
        info.AddValue("_palier", CInt(_palier))
        info.AddValue("_motdepasse", _motdepasse)
        info.AddValue("_preferences", _preferences.Preferences, System.Type.GetType("System.String[]"))
        info.AddValue("_paliersEnseignes", _paliersEnseignes, System.Type.GetType("System.Boolean[]"))
        info.AddValue("_role", _role.GetValue, System.Type.GetType("System.Int32"))
        info.AddValue("_titulaire", _titulaire)
        info.AddValue("_courriel", _courriel)
        info.AddValue("_admin", _admin)
        'rajouter Jr 5/04/2011
        info.AddValue("_ecole", _ecole)
    End Sub

    Public Property EstTitulaire() As Boolean
        Get
            Return _titulaire
        End Get
        Set(ByVal value As Boolean)
            _titulaire = value
        End Set
    End Property

    Public Overrides Function ToString() As String

        Dim info As System.Text.StringBuilder = New System.Text.StringBuilder("")

        info.AppendLine("Login: " & _login)
        info.AppendLine("ID: " & _ID)
        info.AppendLine("Prenom: " & _prenom)
        info.AppendLine("Nom: " & _nom)
        info.AppendLine("Palier: " & _palier.ToString())
        info.AppendLine("Courriel: " & _courriel.ToString())
        'info.AppendLine("_motdepasse: " & _motdepasse)

        Return info.ToString()

    End Function

End Class

<Serializable()> _
Public Class Evaluation
    Implements System.Runtime.Serialization.ISerializable

    Private _id As String
    Private _desc As String
    Private _date As DateTime
    Private _pond As Integer

    Public Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
        _id = info.GetString("_id")
        _desc = info.GetString("_desc")
        _date = info.GetDateTime("_date")
        _pond = CInt(info.GetString("_pond"))
    End Sub

    Public Sub New(ByVal id As String, ByVal description As String, ByVal sdate As DateTime, ByVal ponderation As Integer)
        _id = id
        _desc = description
        _date = sdate
        _pond = ponderation
    End Sub

    Public Property ID() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _desc
        End Get
        Set(ByVal value As String)
            _desc = value
        End Set
    End Property

    Public Property DateDeLEvaluation() As DateTime
        Get
            Return _date
        End Get
        Set(ByVal value As DateTime)
            _date = value
        End Set
    End Property

    Public Property Ponderation() As Integer
        Get
            Return _pond
        End Get
        Set(ByVal value As Integer)
            _pond = value
        End Set
    End Property

    Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData
        info.AddValue("_id", _id)
        info.AddValue("_desc", _desc)
        info.AddValue("_date", _date)
        info.AddValue("_pond", _pond)
    End Sub

    Public Overrides Function ToString() As String
        Dim info As System.Text.StringBuilder = New System.Text.StringBuilder("")

        info.AppendLine("Id: " & _id)
        info.AppendLine("Description: " & _desc)
        info.AppendLine("Date: " & _date)
        info.AppendLine("Pond�ration: " & _pond)

        Return info.ToString()
    End Function

End Class

<Serializable()> _
Public Class Eleve
    Implements System.Runtime.Serialization.ISerializable

    Private _id As String
    Private _nom As String
    Private _prenom As String
    'Private _classe As UneClasse
    'Private _enseignant As Usager

    Public Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
        _id = info.GetString("_id")
        _nom = info.GetString("_nom")
        _prenom = info.GetString("_prenom")
        '_classe = info.GetValue("_classe", System.Type.GetType("UneClasse"))
        '_enseignant = info.GetValue("_enseignant", System.Type.GetType("Usager"))
    End Sub

    'Public Sub New(ByVal id As String, ByVal nom As String, ByVal prenom As String, ByVal classe As UneClasse, ByVal enseignant As Usager)
    Public Sub New(ByVal id As String, ByVal nom As String, ByVal prenom As String)
        _id = id
        _nom = nom
        _prenom = prenom
        '_classe = classe
        '_enseignant = enseignant
    End Sub

    Public Property ID() As String
        Get
            Return _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property

    Public Property Nom() As String
        Get
            Return _nom
        End Get
        Set(ByVal value As String)
            _nom = value
        End Set
    End Property

    Public Property Prenom() As String
        Get
            Return _prenom
        End Get
        Set(ByVal value As String)
            _prenom = value
        End Set
    End Property

    'Public Property Classe() As UneClasse
    '    Get
    '        Return _classe
    '    End Get
    '    Set(ByVal value As UneClasse)
    '        _classe = value
    '    End Set
    'End Property

    'Public Property Enseignant() As Usager
    '    Get
    '        Return _enseignant
    '    End Get
    '    Set(ByVal value As Usager)
    '        _enseignant = value
    '    End Set
    'End Property

    Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData
        info.AddValue("_id", _id)
        info.AddValue("_nom", _nom)
        info.AddValue("_prenom", _prenom)
        'info.AddValue("_classe", _classe)
        'info.AddValue("_enseignant", _enseignant)
    End Sub

    Public Overrides Function ToString() As String
        Dim info As System.Text.StringBuilder = New System.Text.StringBuilder("")
        info.AppendLine("ID: " & _id)
        info.AppendLine("Nom: " & _nom)
        info.AppendLine("Prenom: " & _prenom)
        'info.AppendLine("Classe: " & vbCrLf & "[" & _classe.ToString() & "]")
        'info.AppendLine("Enseignant: " & vbCrLf & "[" & _enseignant.ToString() & "]")

        Return info.ToString()
    End Function
End Class

<Serializable()> _
Public Class CAP
    Implements System.Runtime.Serialization.ISerializable

    Public Enum EtatsCAP
        E1_Vide
        E2E3E4_DiagnostiqueObjectifsSuivi
        E5_Archiv�
    End Enum

    Private _description As String = String.Empty
    Private _id As Long = 0
    Private _etat As EtatsCAP = EtatsCAP.E1_Vide
    Private _actif As Boolean = False

    Public Sub New()

    End Sub

    Public Sub New(ByVal description As String, ByVal id As Long, ByVal etat As EtatsCAP, ByVal actif As Boolean)
        _description = description
        _etat = etat
        _id = id
        _actif = actif
    End Sub

    Public Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
        _description = info.GetString("desc")
        _etat = CType([Enum].Parse(EtatsCAP.E1_Vide.GetType(), info.GetString("etat")), EtatsCAP)
        _id = info.GetInt64("id")
        _actif = info.GetBoolean("actif")
    End Sub

    Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData
        info.AddValue("desc", _description)
        info.AddValue("etat", _etat)
        info.AddValue("id", _id)
        info.AddValue("actif", _actif)
    End Sub

    Public Property Actif() As Boolean
        Get
            Return _actif
        End Get
        Set(ByVal value As Boolean)
            _actif = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    Public Property ID() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            _id = value
        End Set
    End Property

    Public Property Etat() As EtatsCAP
        Get
            Return _etat
        End Get
        Set(ByVal value As EtatsCAP)
            _etat = value
        End Set
    End Property

End Class

<Serializable()> _
Public Class UneClasse
    Implements System.Runtime.Serialization.ISerializable

    Private _codecours As String = String.Empty
    Private _classecode As String = String.Empty
    Private _anneescolaire As String = String.Empty
    Private _matiere As String = String.Empty
    Private _niveau As String = String.Empty
    Private _etape As String = "0"
    Private _domaine As String = String.Empty
    Private _nomdomaine As String = String.Empty
    Private _ecole As String = String.Empty
    Private _nomecole As String = String.Empty
    Private _nomecolecourt As String = String.Empty
    Private _treepath As String = String.Empty
    Private _palier As Paliers = Paliers.Elementaire
    Private _section As String = String.Empty
    Private _description As String = String.Empty
    'ajout Jr 26/01/09
    Private _periode_bull As String = String.Empty

    Public Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
        _codecours = info.GetValue("_codecours", System.Type.GetType("System.String")).ToString()
        _classecode = info.GetValue("_classecode", System.Type.GetType("System.String")).ToString()
        _anneescolaire = info.GetValue("_anneescolaire", System.Type.GetType("System.String")).ToString()
        _matiere = info.GetValue("_matiere", System.Type.GetType("System.String")).ToString()
        _niveau = info.GetValue("_niveau", System.Type.GetType("System.String")).ToString()
        _etape = info.GetValue("_etape", System.Type.GetType("System.String")).ToString()
        _domaine = info.GetValue("_domaine", System.Type.GetType("System.String")).ToString()
        _nomdomaine = info.GetValue("_nomdomaine", System.Type.GetType("System.String")).ToString()
        _ecole = info.GetValue("_ecole", System.Type.GetType("System.String")).ToString()
        _nomecole = info.GetValue("_nomecole", System.Type.GetType("System.String")).ToString()
        _nomecolecourt = info.GetValue("_nomecolecourt", System.Type.GetType("System.String")).ToString()
        _treepath = info.GetValue("_treepath", System.Type.GetType("System.String")).ToString()
        _palier = CType(info.GetValue("_palier", System.Type.GetType("System.Int32")).ToString(), Paliers)
        _section = info.GetValue("_section", System.Type.GetType("System.String")).ToString()
        _description = info.GetValue("_description", System.Type.GetType("System.String")).ToString()
        'ajout Jr 26/01/09
        _periode_bull = info.GetValue("_periode_bull", System.Type.GetType("System.String")).ToString()
    End Sub

    'modif ajout de periode_bull
    Public Sub New(ByVal treepath As String, ByVal nom_ecole As String, ByVal ecole As String, ByVal codecours As String, ByVal classecode As String, ByVal anneescolaire As String, ByVal matiere As String, ByVal niveau As String, ByVal etape As String, ByVal domaine As String, ByVal nom_domaine As String, ByVal palier As Paliers, ByVal section As String, Optional ByVal periode_bull As String = "")
        _codecours = codecours
        _classecode = classecode
        _anneescolaire = anneescolaire
        _matiere = matiere
        _niveau = niveau
        _etape = etape
        _domaine = domaine
        _nomecole = nom_ecole
        _ecole = ecole
        _treepath = treepath
        _nomdomaine = nom_domaine
        _palier = palier
        _section = section
        'ajout Jr 26/01/09
        _periode_bull = periode_bull
    End Sub

    Public Sub New()

    End Sub

    Public Sub New(ByVal annee_scolaire As String, ByVal ecole As String)
        _ecole = ecole
        _anneescolaire = annee_scolaire
    End Sub

    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    Public ReadOnly Property Section() As String
        Get
            Return _section
        End Get
    End Property

    Public Property Ecole() As String
        Get
            Return _ecole
        End Get
        Set(ByVal value As String)
            _ecole = value
        End Set
    End Property

    Public ReadOnly Property CodeCours() As String
        Get
            Return _codecours
        End Get
    End Property

    Public ReadOnly Property ClasseCode() As String
        Get
            Return _classecode
        End Get
    End Property

    Public Property AnneeScolaire() As String
        Get
            Return _anneescolaire
        End Get
        Set(ByVal value As String)
            _anneescolaire = value
        End Set
    End Property

    Public ReadOnly Property Niveau() As String
        Get
            Return _niveau
        End Get
    End Property

    Public ReadOnly Property Matiere() As String
        Get
            Return _matiere
        End Get
    End Property

    Public Property Etape() As String
        Get
            Return _etape
        End Get
        Set(ByVal value As String)
            _etape = value
        End Set
    End Property

    Public ReadOnly Property Domaine() As String
        Get
            Return _domaine
        End Get
    End Property

    Public ReadOnly Property NomDomaine() As String
        Get
            Return _nomdomaine
        End Get
    End Property

    Public Property NomEcole() As String

        Get
            Return _nomecole
        End Get
        Set(ByVal value As String)
            _nomecole = value
        End Set
    End Property

    Public Property NomEcoleCourt() As String
        Get
            Return _nomecolecourt
        End Get
        Set(ByVal value As String)
            _nomecolecourt = value
        End Set
    End Property

    Public Property Palier() As Paliers
        Get
            Return _palier
        End Get
        Set(ByVal value As Paliers)
            _palier = value
        End Set
    End Property
    'ajout Jr 26/01/09
    Public Property Periode_bull() As String
        Get
            Return _periode_bull
        End Get
        Set(ByVal value As String)
            _periode_bull = value
        End Set
    End Property

    Public ReadOnly Property TreePath() As String
        Get
            Return _treepath
        End Get
    End Property

    Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData
        info.AddValue("_codecours", _codecours)
        info.AddValue("_classecode", _classecode)
        info.AddValue("_anneescolaire", _anneescolaire)
        info.AddValue("_matiere", _matiere)
        info.AddValue("_niveau", _niveau)
        info.AddValue("_etape", _etape)
        info.AddValue("_domaine", _domaine)
        info.AddValue("_nomdomaine", _nomdomaine)
        info.AddValue("_ecole", _ecole)
        info.AddValue("_nomecole", _nomecole)
        info.AddValue("_nomecolecourt", _nomecolecourt)
        info.AddValue("_treepath", _treepath)
        info.AddValue("_palier", _palier)
        info.AddValue("_section", _section)
        info.AddValue("_description", _description)
        'ajout Jr 26/01/09
        info.AddValue("_periode_bull", _periode_bull)
    End Sub

    Public Overrides Function ToString() As String
        Dim info As StringBuilder = New StringBuilder("")

        info.AppendLine("Code cours: " & _codecours)
        info.AppendLine("Classe code: " & _classecode)
        info.AppendLine("Ann�e scolaire: " & _anneescolaire)
        info.AppendLine("Matiere: " & _matiere)
        info.AppendLine("Niveau: " & _niveau)
        info.AppendLine("Niveau: " & _section)
        info.AppendLine("�tape: " & _etape)
        info.AppendLine("Domaine: " & _domaine)
        info.AppendLine("Nomdomaine: " & _nomdomaine)
        info.AppendLine("�cole: " & _ecole)
        info.AppendLine("Nom de l'�cole: " & _nomecole)
        info.AppendLine("Tree path: " & _treepath)
        info.AppendLine("Palier: " & _palier.ToString())
        'ajout Jr 26/01/09
        info.AppendLine("P�riode du bulletin: " & _periode_bull.ToString())
        Return info.ToString()

    End Function

    Public Function Serialize(Optional ByVal separator As String = "|") As String

        Dim info As StringBuilder = New StringBuilder()

        info.Append(_codecours & separator)
        info.Append(_classecode & separator)
        info.Append(_anneescolaire & separator)
        info.Append(_matiere & separator)
        info.Append(_niveau & separator)
        info.Append(_section & separator)
        info.Append(_etape & separator)
        info.Append(_domaine & separator)
        info.Append(_nomdomaine & separator)
        info.Append(_ecole & separator)
        info.Append(_nomecole & separator)
        info.Append(_nomecolecourt & separator)
        info.Append(_treepath & separator)
        'ajout Jr 26/01/09
        info.Append(_periode_bull & separator)
        info.Append(_palier.ToString())

        Return info.ToString()

    End Function

    Public Sub Deserialize(ByVal serializedinfo As String, Optional ByVal separator As String = "|")

        If serializedinfo.Length = 0 Then
            Throw New Exception("L'information s�rializ�e ne peut pas �tre vide.")
        End If

        Dim props() As String = serializedinfo.Split(separator.ToCharArray(0, 1))

        _codecours = props(0)
        _classecode = props(1)
        _anneescolaire = props(2)
        _matiere = props(3)
        _niveau = props(4)
        _section = props(5)
        _etape = props(6)
        _domaine = props(7)
        _nomdomaine = props(8)
        _ecole = props(9)
        _nomecole = props(10)
        _nomecolecourt = props(11)
        _treepath = props(12)
        'ajout Jr 26/01/09
        _periode_bull = props(13)
        'modif Jr 26/01/09
        '_palier = CType(IIf(props(13).Length > 0, [Enum].Parse(System.Type.GetType("Commun.Paliers"), props(13)), Paliers.Elementaire), Paliers)<
        _palier = CType(IIf(props(14).Length > 0, [Enum].Parse(System.Type.GetType("Commun.Paliers"), props(14)), Paliers.Elementaire), Paliers)


    End Sub

End Class

Partial Public Class Contexte

    Private Class ValeursContexte
        Public Const vclasse As String = "classe"
        Public Const veleve As String = "eleve"
        Public Const vevaluation As String = "evaluation"
        Public Const vusager As String = "usager"
        Public Const vcontextelogin As String = "contextelogin"
        Public Const vcap As String = "cap"
        Public Const vformulaire As String = "formulaire"
    End Class

    Public Shared Property cap() As CAP
        Get
            If HttpContext.Current.Session Is Nothing Then
                Return Nothing
            End If
            If HttpContext.Current.Session(ValeursContexte.vclasse) Is Nothing Then
                Return Nothing
            Else
                Return CType(HttpContext.Current.Session(ValeursContexte.vcap), CAP)
            End If
        End Get
        Set(ByVal value As CAP)
            HttpContext.Current.Session(ValeursContexte.vcap) = value
        End Set
    End Property

    Public Shared Property classe() As UneClasse
        Get
            If HttpContext.Current.Session Is Nothing Then
                Return Nothing
            End If
            If HttpContext.Current.Session(ValeursContexte.vclasse) Is Nothing Then
                Return Nothing
            Else
                Return CType(HttpContext.Current.Session(ValeursContexte.vclasse), UneClasse)
            End If
        End Get
        Set(ByVal value As UneClasse)
            HttpContext.Current.Session(ValeursContexte.vclasse) = value
        End Set
    End Property

    Public Shared Property eleve() As Eleve
        Get
            If HttpContext.Current.Session Is Nothing Then
                Return Nothing
            End If
            If HttpContext.Current.Session(ValeursContexte.veleve) Is Nothing Then
                Return Nothing
            Else
                Return CType(HttpContext.Current.Session(ValeursContexte.veleve), Eleve)
            End If
        End Get
        Set(ByVal value As Eleve)
            HttpContext.Current.Session(ValeursContexte.veleve) = value
        End Set
    End Property

    Public Shared Property evaluation() As Evaluation
        Get
            If HttpContext.Current.Session Is Nothing Then
                Return Nothing
            End If
            If HttpContext.Current.Session(ValeursContexte.vevaluation) Is Nothing Then
                Return Nothing
            Else
                Return CType(HttpContext.Current.Session(ValeursContexte.vevaluation), Evaluation)
            End If
        End Get
        Set(ByVal value As Evaluation)
            HttpContext.Current.Session(ValeursContexte.vevaluation) = value
        End Set
    End Property

    Public Shared Property usager() As Usager
        Get
            If HttpContext.Current.Session Is Nothing Then
                Return Nothing
            End If
            If HttpContext.Current.Session(ValeursContexte.vusager) Is Nothing Then
                Return Nothing
            Else
                Return CType(HttpContext.Current.Session(ValeursContexte.vusager), Usager)
            End If
        End Get
        Set(ByVal value As Usager)
            HttpContext.Current.Session(ValeursContexte.vusager) = value
        End Set
    End Property

    'Public Shared Property contexteLogin() As Commun.ContexteLogin
    '    Get
    '        If HttpContext.Current.Session Is Nothing Then
    '            Return Nothing
    '        End If
    '        If HttpContext.Current.Session(ValeursContexte.vcontextelogin) Is Nothing Then
    '            Return Nothing
    '        Else
    '            Return CType(HttpContext.Current.Session(ValeursContexte.vcontextelogin), ContexteLogin)
    '        End If
    '    End Get
    '    Set(ByVal value As ContexteLogin)
    '        HttpContext.Current.Session(ValeursContexte.vcontextelogin) = value
    '    End Set
    'End Property

    Public Shared Property formulaire() As Formulaire
        Get
            If HttpContext.Current.Session Is Nothing Then
                Return Nothing
            End If
            If HttpContext.Current.Session(ValeursContexte.vformulaire) Is Nothing Then
                Return Nothing
            Else
                Return CType(HttpContext.Current.Session(ValeursContexte.vformulaire), Formulaire)
            End If
        End Get
        Set(ByVal value As Formulaire)
            HttpContext.Current.Session(ValeursContexte.vformulaire) = value
        End Set
    End Property

    Public Shared Function TesteCAP() As Boolean
        Return Not (cap Is Nothing)
    End Function

    Public Shared Function TesteClasse() As Boolean
        Return Not (classe Is Nothing)
    End Function

    Public Shared Function TesteEleve() As Boolean
        Return Not (eleve Is Nothing)
    End Function
    Public Shared Function TesteEvaluation() As Boolean
        Return Not (evaluation Is Nothing)
    End Function

    Public Shared Function TesteUsager() As Boolean
        Return Not (usager Is Nothing)
    End Function

    Public Shared Function TesteFormulaire() As Boolean
        Return Not (formulaire Is Nothing)
    End Function


    'Public Shared Function TesteFormulaireListe() As Boolean
    '    Return Not (formulaireListe Is Nothing)
    'End Function

    'Public Shared Function GetStatus() As String
    '    Dim sb As StringBuilder = New StringBuilder("")

    '    sb.AppendLine("USAGER")
    '    If Contexte.TesteUsager Then
    '        sb.AppendLine(Contexte.usager.ToString())
    '    Else
    '        sb.AppendLine("Nothing")
    '    End If
    '    sb.AppendLine()

    '    sb.AppendLine("FORMULAIRE")
    '    If Contexte.TesteFormulaire Then
    '        sb.AppendLine(Contexte.formulaire.ToString())
    '    Else
    '        sb.AppendLine("Nothing")
    '    End If
    '    sb.AppendLine()

    '    sb.AppendLine("CLASSE")
    '    If Contexte.TesteClasse Then
    '        sb.AppendLine(Contexte.classe.ToString())
    '    Else
    '        sb.AppendLine("Nothing")
    '    End If
    '    sb.AppendLine()

    '    sb.AppendLine("ELEVE")
    '    If Contexte.TesteEleve Then
    '        sb.AppendLine(Contexte.eleve.ToString())
    '    Else
    '        sb.AppendLine("Nothing")
    '    End If
    '    sb.AppendLine()


    '    sb.AppendLine("EVALUATION")
    '    If Contexte.TesteEvaluation Then
    '        sb.AppendLine(Contexte.evaluation.ToString())
    '    Else
    '        sb.AppendLine("Nothing")
    '    End If
    '    sb.AppendLine()


    '    sb.AppendLine("CONTEXTE LOGIN")
    '    sb.AppendLine(Contexte.contexteLogin.ToString())
    '    sb.AppendLine()

    '    Return sb.ToString()

    'End Function

End Class

Public Class Enums

    Public Enum TextModes
        Freetext = 0
        Preset
        Empty
    End Enum

    Public Enum TypesDeNotes
        globale
        analytique
        nonEvaluee
        pourcentage
    End Enum

    Public Enum Competences
        CC = 1
        HP
        CO
        MA
    End Enum

    Public Enum PresentationDeNotes
        globale = TypesDeNotes.globale
        analytique = TypesDeNotes.analytique
        defaut
        pourcentage
        globale_et_pourcentage
    End Enum

End Class

Public Class NoStrand

    Private Const no_strand_1 As String = "xxx"
    Private Const no_strand_2 As String = "999"

    Public Sub New()

    End Sub

    Public Shared Operator =(ByVal strand1 As NoStrand, ByVal strand2 As String) As Boolean
        Return (strand2 = no_strand_1) Or (strand2 = no_strand_2)
    End Operator

    Public Shared Operator <>(ByVal strand1 As NoStrand, ByVal strand2 As String) As Boolean
        Return (strand2 <> no_strand_1) And (strand2 <> no_strand_2)
    End Operator

    Public Shared Operator =(ByVal strand2 As String, ByVal strand1 As NoStrand) As Boolean
        Return (strand2 = no_strand_1) Or (strand2 = no_strand_2)
    End Operator

    Public Shared Operator <>(ByVal strand2 As String, ByVal strand1 As NoStrand) As Boolean
        Return (strand2 <> no_strand_1) And (strand2 <> no_strand_2)
    End Operator

End Class

Public Class ConfigCache

    Public Shared Property NormeMinistere() As String
        Get
            If HttpContext.Current.Cache("NORME") Is Nothing Then
                Return String.Empty
            Else
                Return CStr(HttpContext.Current.Cache("NORME"))
            End If
        End Get
        Set(ByVal value As String)
            AjouterAuCache("NORME", value)
        End Set
    End Property

    Public Shared Property AnneeScolaire() As String
        Get
            If HttpContext.Current.Cache("ANNEESCOLAIRE") Is Nothing Then
                Return String.Empty
            Else
                Return HttpContext.Current.Cache("ANNEESCOLAIRE").ToString()
            End If
        End Get
        Set(ByVal value As String)
            AjouterAuCache("ANNEESCOLAIRE", value)
        End Set
    End Property

    Private Shared Sub AjouterAuCache(ByVal cle As String, ByVal valeur As Object)
        If Not HttpContext.Current.Cache(cle) Is Nothing Then
            HttpContext.Current.Cache(cle) = valeur
        Else
            HttpContext.Current.Cache.Add(cle, valeur, Nothing, DateTime.MaxValue, New TimeSpan(30, 0, 0, 0), Caching.CacheItemPriority.Low, Nothing)
        End If
    End Sub

End Class

Namespace Statuts
    Public Structure StatClasse
        Public Evaluations As StatEvaluations
        Public Bulletins As StatBulletins
        Public Moyenne As Double
    End Structure

    Public Structure StatEvaluations
        Public ToutesCompletees As Boolean
        Public Compte As Boolean
    End Structure

    Public Structure StatBulletins
        Public CommentairesCompletes As Boolean
        Public HabiletesCompletees As Boolean
    End Structure
End Namespace