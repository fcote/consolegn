<%@ Page Language="vb" Debug="true" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>

	<script runat="server">

		' This sample demonstrates using hotspots and annotations with manually added map points.
		Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
			' Setup the chart.
			Chart.Type = ChartType.Map
			Chart.Size = "850x550"
			Chart.Title = "<block fSize='11'>Click on a point of interest for more information"
			Chart.TempDirectory = "temp"
			Chart.Debug = True
			Chart.LegendBox.Visible = False
			Chart.ChartArea.Background.ShadingEffectMode = ShadingEffectMode.Background2

			' Style the title box.
			Chart.TitleBox.Position = TitleBoxPosition.Full
			Chart.TitleBox.Label.Color = Color.White
			Chart.TitleBox.Label.GlowColor = Color.Black
			Chart.TitleBox.Background.Color = Color.DeepSkyBlue
			Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Background1
			Chart.TitleBox.Position = TitleBoxPosition.Full
			Chart.TitleBox.CornerTopRight = BoxCorner.Round
			Chart.TitleBox.CornerTopLeft = Chart.TitleBox.CornerTopRight

			Chart.Mapping.ZoomPercentage = 150

			' By default the ID is -1, meaning; no location is selected.
			Dim id As Integer = -1
			If Page.Request.QueryString.Count > 0 Then
				Dim query As String = Page.Request.QueryString("id")
				id = Integer.Parse(query)
			End If

			' Create a layer for the map.
			Dim layer As MapLayer = MapDataEngine.LoadLayer("../../images/MapFiles/primusa.shp")
			layer.DefaultShape.Line.Color = Color.Black
			layer.DefaultShape.Background.Color = Color.FromArgb(240, 240, 240)

			' Create a layer for the map points representing stores.
			Dim stores As MapLayer = New MapLayer()

			' Create default annotation settings.
			Dim defaultAn As Annotation = New Annotation()
			defaultAn.DynamicSize = False
			defaultAn.DefaultCorner = BoxCorner.Round
			defaultAn.Background.ShadingEffectMode = ShadingEffectMode.Background2
			defaultAn.Background.Color = Color.DeepSkyBlue

			' By default the annotations will be null, unless the query string specifies the id 1-3.
			' Create the annotations.
			Dim an1 As Annotation = New Annotation("<img:../../images/PetStoreLogo.png> Pet Store<br>Chicago IL<br>(312) 555-2345<row><block fColor='blue' url='http://dotnetcharting.com'>Pet Store Website", defaultAn)
			If id <> 1 Then
			an1 = Nothing
			End If

			Dim an2 As Annotation = New Annotation("<img:../../images/PetStoreLogo.png> Pet Store<br>Wisconsin Dells WI<br>(608) 555-2345<row><block fColor='blue' url='http://dotnetcharting.com'>Pet Store Website", defaultAn)
			If id <> 2 Then
			an2 = Nothing
			End If

			Dim an3 As Annotation = New Annotation("<img:../../images/PetStoreLogo.png> Pet Store<br>Las Vegas NV<br>(702) 555-2345<row><block fColor='blue' url='http://dotnetcharting.com'>Pet Store Website", defaultAn)
			If id <> 3 Then
			an3 = Nothing
			End If

			' Add the points along with markers, hotspots and annotations.
	        stores.AddLatLongPoint(New PointF(41.75F, -87.99F), New ElementMarker(ElementMarkerType.FivePointStar, 20, Color.Red), New dotnetCHARTING.Hotspot("?id=1", ""), an1)
	        stores.AddLatLongPoint(New PointF(43.62F, -89.77F), New ElementMarker(ElementMarkerType.FivePointStar, 20, Color.Red), New dotnetCHARTING.Hotspot("?id=2", ""), an2)
	        stores.AddLatLongPoint(New PointF(36.17F, -115.139F), New ElementMarker(ElementMarkerType.FivePointStar, 20, Color.Red), New dotnetCHARTING.Hotspot("?id=3", ""), an3)

			' Add both layers to the map.
			Chart.Mapping.MapLayerCollection.Add(layer, stores)
			Chart.GetChartBitmap()
		End Sub
	</script>

</head>
<body>
	<div style="text-align: center">
		<dotnet:Chart ID="Chart" runat="server">
		</dotnet:Chart>
	</div>
</body>
</html>
