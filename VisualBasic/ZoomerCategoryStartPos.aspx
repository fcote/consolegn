<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates specifying a starting position and range of a category axis viewport using the AJAX zoomer.

		Chart.Size = "600x550"
		Chart.Title = "Specifying a start position for category zooming."
		Chart.TempDirectory = "temp"
		Chart.Debug = True

		' Main settings: Line width and color.
		Dim lW As Integer = 2
		Dim baseColor As Color = Color.Maroon

		' Enable Zoomer
		Chart.Zoomer.Enabled = True

		' Specify the zoomer viewport referring to elements by their index.
		Chart.XAxis.Viewport.ScaleRange = New ScaleRange(2, 3)

		' All the code hereafter is mainly for visual styling purposes.

		' Element Styling.
		Chart.DefaultElement.ShowValue = True
		Chart.DefaultElement.SmartLabel.Font = New Font("Arial", 11)
		Chart.DefaultElement.SmartLabel.Color = Color.Black
		Chart.DefaultElement.Transparency = 10


		' Styling Settings.
		Chart.Palette = New Color() { Color.FromArgb(255, 99, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(49, 255, 49), Color.FromArgb(0, 156, 255) }
		Chart.ShadingEffectMode = ShadingEffectMode.Two
		Chart.DefaultShadow.Color = Color.FromArgb(100, Color.Gray)
		Chart.DefaultShadow.Depth = 7
		Chart.ChartArea.Background.Color = Color.FromArgb(246, 246, 253)
		Chart.ChartArea.Line.Width = lW
		Chart.ChartArea.Line.Color = baseColor

		' Axis Settings
		Chart.DefaultAxis.AlternateGridBackground.Color = Color.Empty
		Chart.DefaultAxis.Line.Color = Color.Transparent
		Chart.DefaultAxis.DefaultTick.Line.Color = baseColor
		Chart.DefaultAxis.DefaultTick.Label.Font = New Font("Arial", 8, FontStyle.Bold)
		Chart.YAxis.DefaultTick.Label.Font = New Font("Arial", 12, FontStyle.Bold)
		Chart.YAxis.TickNumberMaximum = 8
		Chart.DefaultAxis.DefaultTick.Label.Color = baseColor
		Chart.DefaultAxis.DefaultTick.GridLine.Width = lW
		Chart.DefaultAxis.DefaultTick.Line.Width = lW
		Chart.DefaultAxis.DefaultTick.Line.Length = 6

		'TitleBox Settings
		Chart.TitleBox.Position = TitleBoxPosition.Full ' WithLegend;
		Chart.TitleBox.CornerTopRight = BoxCorner.Round
		Chart.TitleBox.CornerTopLeft = BoxCorner.Round
		Chart.TitleBox.Padding = 7
		Chart.TitleBox.Line.Width = lW
		Chart.TitleBox.Line.Color = baseColor
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Four
		Chart.TitleBox.Background.Color = baseColor
		Chart.TitleBox.Label.Color = Color.White
		Chart.TitleBox.Label.GlowColor = Color.Black
		Chart.TitleBox.Label.Font = New Font("Tahoma", 11, FontStyle.Bold)

		'LegendBox Settings
		Chart.LegendBox.ClearColors()
		Chart.LegendBox.Background.Color = Color.FromArgb(246, 246, 253)
		Chart.LegendBox.Position = New Point(55, 53)
		Chart.LegendBox.LabelStyle.Font = New Font("Arial", 10, FontStyle.Bold)


		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		For a As Integer = 1 To 2
			Dim s As Series = New Series("Series " & a.ToString())
			For b As Integer = 1 To 16
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
