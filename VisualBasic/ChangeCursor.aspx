<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
		<script runat="server">



Sub Page_Load(sender As [Object], e As EventArgs)
   'This sample demonstrates how the cursor can be changed based on hovering over hotspots.
   ' FullScreen
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Title = "Cursor Changes on bars (IE Only)"
   Chart.Palette = New Color() {Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255)}
   
   
   Chart.DefaultElement.Hotspot.Attributes.Custom.Add("OnMouseOver", "scss('pointer');")
   Chart.DefaultElement.Hotspot.Attributes.Custom.Add("OnMouseOut", "scss('default');")
   
   ' Add the random data.
   Chart.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   Return SC
End Function 'getRandomData


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Change Cursor sample</title>

<script language=javascript>
function scss( tid)
{
     myImg = document.getElementById("ChartArea");
     myImg.style.cursor = tid;
}
</script>
</head>
	<body>
		<div align="center" ID="ChartArea">
			<dotnet:Chart id="Chart" runat="server" />
		</div>
	</body>
</html>
