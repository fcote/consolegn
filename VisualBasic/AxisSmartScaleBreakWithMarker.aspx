<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Smart Scale Breaks With Markers"
   
   ' This sample will demonstrate how smart breaks can be used with limit markers.	
   ' Setup the y axis.
   Chart.YAxis.Label.Text = "Bandwidth Usage"
   Chart.YAxis.DefaultTick.Label.Text = "%Value MB"
   Chart.YAxis.SmartScaleBreak = True
   Chart.YAxis.Maximum = 550
   
   ' Setup the legend box.
   Chart.LegendBox.HeaderEntry.Visible = True
   Chart.LegendBox.HeaderEntry.Value = "(MB)"
   Chart.LegendBox.HeaderEntry.Name = "Site"
   
   ' Create an axis marker and add it..
   Dim am As New AxisMarker("Limit (530 MB)", New Line(Color.Red), 530)
   
   ' This makes sure the axis range encompases the axis marker.
   am.IncludeInAxisScale = True
   am.Label.Alignment = StringAlignment.Near
   
   Chart.YAxis.Markers.Add(am)
   
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Add a divider line to the last series entry.
   mySC(3).LegendEntry.DividerLine.Color = Color.Black
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Website " +Convert.ToString(a + 1)
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Month " +Convert.ToString(b + 1)
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
