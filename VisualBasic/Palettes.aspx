<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>

<script runat="server">

Private Sub Page_Load(sender As Object, e As System.EventArgs)
   'This sample demonstrates different palette settings.
   Dim name As String
   For Each name In  [Enum].GetNames(GetType(dotnetCHARTING.Palette))
    If name = "FiveColor1" Then
   	Exit For
   End If
   
      Dim chartObject As New dotnetCHARTING.Chart()
      chartObject.Type = ChartType.Pies
      chartObject.Title = "Palette Name: " + name
      chartObject.TempDirectory = "temp"
      chartObject.Debug = True
      chartObject.ShadingEffect = True
      chartObject.LegendBox.Template = "%icon %name"
      chartObject.Size = "800X600"
      chartObject.PaletteName = CType([Enum].Parse(GetType(dotnetCHARTING.Palette), name), dotnetCHARTING.Palette)
      chartObject.SeriesCollection.Add(getRandomData())
      Controls.Add(chartObject)
   Next name 
End Sub 'Page_Load

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim s As New Series()
   Dim b As Integer
   For b = 1 To 50
      Dim e As New Element()
      e.Name = "Element " + b.ToString()
      e.YValue = 3
      s.Elements.Add(e)
   Next b
   SC.Add(s)
   Return SC
End Function 'getRandomData
</script>
	</head>
	<body>
		<center>
		</center>
	</body>
</html>
