<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates creating an organizational chart designer.

		' Load the chart state that contains nodes info.
		Chart.LoadState("temp/orgState.xml")

		' Set some styling properties.
		Chart.Type = ChartType.Organizational
		Chart.Size = "800x450"
		Chart.Title = ".netCHARTING Organizational Chart Designer"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.Black }
		Chart.DefaultSeries.Line.Width = 3

		' Apply title box styling.
		Chart.TitleBox.Label.Color = Color.White
		Chart.TitleBox.Label.Shadow.Color = Color.FromArgb(105, 0, 0, 0)
		Chart.TitleBox.Label.Shadow.Depth = 2
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Two
		Chart.TitleBox.Background.Color = Color.SteelBlue
		Chart.TitleBox.Position = TitleBoxPosition.Full
		Chart.TitleBox.Padding = 8

		' Set default annotation style.
		Chart.DefaultElement.Annotation = New Annotation()
		Chart.DefaultElement.Annotation.Label.Text = "<Chart:Spacer width='80'><Chart:Spacer width='10'><Chart:Spacer width='15'><Chart:Spacer width='5'><Chart:Spacer width='15'><row>%Name <Chart:Spacer width='20'><Chart:Image src='../../images/add2.png' url='?id=%id&a=add'><Chart:Spacer width='5'><Chart:Image src='../../images/delete2.png' url='?id=%id&a=remove'>"
		Chart.DefaultElement.Annotation.Padding = 8
		Chart.DefaultElement.Annotation.Background.ShadingEffectMode = ShadingEffectMode.Background2
		Chart.DefaultElement.Annotation.Background.Color = Color.DeepSkyBlue
		Chart.DefaultElement.Annotation.DefaultCorner = BoxCorner.Round
		Chart.DefaultElement.Annotation.Size = New Size(170, 50)

		' If no data is stored in the xml, add a starting node.
		If Chart.SeriesCollection.Count = 0 Then
			Chart.SeriesCollection.Add(getInitialData())
		End If

		' Check the querystring to determine actions.
		If Page.Request.QueryString.Count > 0 Then
			' Action
			Dim query As String = Page.Request.QueryString("a")
			' Node ID
			Dim queryID As String = Page.Request.QueryString("id")

			' If action is 'add' place an input form on the page.
			If Not queryID Is Nothing AndAlso Not query Is Nothing AndAlso query = "add" Then
				label1.Text = "<Form method=""get"" ><input type=""hidden"" name=""a"" value=""adding""><input type=""hidden"" name=""id"" value=""" & queryID & """><input type=""text"" name=""name""><input type=submit value=""add""></form>"
			ElseIf Not queryID Is Nothing AndAlso Not query Is Nothing AndAlso query = "adding" Then
				Dim name As String = Page.Request.QueryString("name")
				If name.Trim().Length > 0 Then
					' Add a new node based on the form input.
					Dim el As Element = New Element(Page.Request.QueryString("name"))
					el.InstanceID = getNextID(Chart.SeriesCollection(0))
					el.InstanceParentID = Integer.Parse(queryID)
					Chart.SeriesCollection(0).AddElements(el)
				End If
			ElseIf Not queryID Is Nothing AndAlso Not query Is Nothing AndAlso query = "remove" AndAlso queryID <> "1" Then
				' Remove a node from the data.
				For Each el As Element In Chart.SeriesCollection(0).Elements
					If el.InstanceID = Integer.Parse(queryID) Then
						Chart.SeriesCollection(0).Elements.Remove(el)
						Exit For
					End If
				Next el
			End If
		End If

		' Specify the content of the main node.
		For Each el As Element In Chart.SeriesCollection(0).Elements
			If el.InstanceID = 1 Then
				el.Annotation = New Annotation()
				el.Annotation.Label.Text = "<Chart:Spacer width='80'><Chart:Spacer width='10'><Chart:Spacer width='15'><Chart:Spacer width='5'><Chart:Spacer width='15'><row>%Name <Chart:Spacer width='20'><Chart:Image src='../../images/add2.png' url='?id=%id&a=add'><Chart:Spacer width='5'><Chart:Spacer size='10x5'>"
			End If
		Next el

		'Chart.SaveState("temp/orgState.xml");
	End Sub
	Sub Page_UnLoad()
		Chart.SaveState("temp/orgState.xml")
	End Sub

	' Get a new unused ID.
	Function getNextID(ByVal s As Series) As Integer
		Dim maxID As Integer = 0
		For Each e As Element In s.Elements
			If e.InstanceID > maxID Then
			maxID = e.InstanceID
			End If
		Next e
		Return maxID + 1
	End Function

	Function getInitialData() As SeriesCollection
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim s As Series = New Series("")
		Dim e As Element = New Element("President")
		e.InstanceID = 1
		s.AddElements(e)
		SC.Add(s)
		Return SC
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<asp:Label ID="label1" runat="server" />
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
