<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   Chart.DefaultSeries.Type = SeriesType.Spline
   
   Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom
Chart.LegendBox.Template="%name %icon"
   
   ' This sample will demonstrates how to use and attach axis markers to axes and elements.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' 1. SINGLE VALUES
   Dim am1 As New AxisMarker("Marker at 45", New Line(Color.Orange), 45)
   Chart.YAxis.Markers.Add(am1)
   
   Dim am2 As New AxisMarker("Text marker at 'Element 0'", New Line(Color.Green), "Element 0")
   Chart.XAxis.Markers.Add(am2)
   
   ' 2. RANGE MARKERS
   Dim am3 As New AxisMarker("20-30", New Background(Color.FromArgb(100, Color.LightGreen)), 20, 30)
   Chart.YAxis.Markers.Add(am3)
   
   Dim am4 As New AxisMarker("'Element 1'-'Element 2'", New Background(Color.FromArgb(100, Color.LightBlue)), "Element 1", "Element 2")
   am4.Label.LineAlignment = StringAlignment.Near
   Chart.XAxis.Markers.Add(am4)
   
   ' 3.ELEMENT MARKERS
   mySC(0).Elements(3).AxisMarker = New AxisMarker("Attached to element 3", New Line(Color.Blue), 0)
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData

        </script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
