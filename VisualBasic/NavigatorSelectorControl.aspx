<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using the Navigator to select time ranges and pass them to a page with more charts.

		chart.Size = "700x115"
		chart.TempDirectory = "temp"
		chart.Debug = True
		chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		chart.DefaultSeries.Type = SeriesType.Line
		chart.DefaultElement.Marker.Visible = False
		' Enable the Navigator
		chart.Navigator.Enabled = True

		' Hide the chart area and legend box.
		chart.ChartArea.Visible = False
		chart.LegendBox.Visible = False

		' The control ID is specified so it can be referenced in javascript.
		chart.Navigator.ControlID = "slPlugin"

		' Specify the start date range.
		chart.XAxis.ScaleRange = New ScaleRange(New DateTime(2002, 1, 1), New DateTime(2003, 1, 20))


		Dim mySC As SeriesCollection = getLiveData(New DateTime(2002, 1, 1), New DateTime(2003, 1, 20))

		' Add the random data.
		chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2002, 1, 1)

		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 19
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)

		Return SC
	End Function
	Function getLiveData(ByVal low As DateTime, ByVal high As DateTime) As SeriesCollection
        Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.StartDate = low
		de.EndDate = high
		de.ChartObject = chart ' Necessary to view any errors the dataEngine may throw.
		de.DateGrouping = TimeInterval.Days
		de.SqlStatement = "SELECT OrderDate,Total,Name FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#"
		de.DataFields = "XDateTime=OrderDate,YValue=Total,SplitBy=Name"
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<script type="text/javascript">

    var chartID = "slPlugin";

    // For more information on this code and a client side API reference, see this tutorial: 
    // http://www.dotnetcharting.com/documentation/v6_0/Silverlight_Navigator_API.html

    // This function registers the location update handler when the silverlight chart loads.
    function initEvents() {
        var control = document.getElementById("slPlugin");
        if (control) {
	try {
            // When this handler is set, it will automatically call the event to set the initial location.
            control.Content.Chart.SetLocationUpdateHandler("locationUpdated");
            clearInterval(initID);
	}
	catch(err) {}
	}
     }

     function locationUpdated(lowVal, highVal) {
         document.getElementById('myFrame').src = "SelectorCharts.aspx?low=" + lowVal + "&high="+highVal;
     }

     // Try to register the location update handler with the chart until it is succeeds.
     var initID = setInterval("initEvents()", 1000);

    </script>
<body>
	<div align="center">
		<dnc:Chart ID="chart" runat="server" />

			 <iframe id="myFrame" src="SelectorCharts.aspx?low=1/1/2002&high=1/20/2003" width="700" height="600"/>
	</div>
</body>
</html>
