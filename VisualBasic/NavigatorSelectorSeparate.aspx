<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

		' This sample demonstrates a navigator chart controlling another navigator using different functionality modes.

		chart.Size = "600x120"
		chart.TempDirectory = "temp"
		chart.Debug = True
		chart.Palette = New Color() { Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		chart.DefaultSeries.Type = SeriesType.Pyramid
		chart.Navigator.Enabled = True
		chart.ChartArea.Visible = False
		chart.LegendBox.Visible = False

		' The control ID is specified so it can be referenced in javascript.
		chart.Navigator.ControlID = "slPlugin"
		chart.Navigator.NavigationBar.Background.Color = Color.FromArgb(62, 151, 220)

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		chart.SeriesCollection.Add(mySC)


		' --- Setup the second chart
		Chart1.Size = "650x320"
		Chart1.TempDirectory = "temp"
		Chart1.Debug = True
		Chart1.Palette = New Color() { Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart1.DefaultSeries.Type = SeriesType.Pyramid
		Chart1.Navigator.Enabled = True
		Chart1.LegendBox.Visible = False
		Chart1.Navigator.ControlID = "slPlugin2"

		' This disables all interactivity except for mouse tracking on the second chart.
		Chart1.Navigator.EnableXScrollbar = False
		Chart1.Navigator.NavigationBar.Visible = False
		Chart1.Navigator.PreviewAreaNavigationOptons = 0
		Chart1.ChartArea.NavigatorOptions = 0

		Dim mySC2 As SeriesCollection = getRandomData()

		' Add the random data.
		Chart1.SeriesCollection.Add(mySC2)
		 ' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx       

	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2009, 1, 1)
		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 114
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)

		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
	<style type="text/css">
	div,p,td {
		font-family: Arial, Helvetica, sans-serif;
		font-size: x-small;
	}
	.style1 {
		text-align: right;
	}
	</style>

<script type="text/javascript">

	var chartID = "slPlugin";

	// For more information on this code and a client side API reference, see this tutorial: 
	// http://www.dotnetcharting.com/documentation/v6_0/Silverlight_Navigator_API.html

	// This function registers the location update handler when the silverlight chart loads.
	function initEvents() {
		var control = document.getElementById("slPlugin");
		if (control) {
			// When this handler is set, it will automatically call the event to set the initial location.
			control.Content.Chart.SetLocationUpdateHandler("locationUpdated");
			clearInterval(initID);
		}
	}

	// This function handles the update and is called by the chart.
	 function locationUpdated(lowVal, highVal) {
		 document.getElementById('textBox1').value = lowVal;
		 document.getElementById('textBox2').value = highVal;

		 var control = document.getElementById("slPlugin2");
		 control.content.Chart.NavigateToVisibleRange(lowVal, highVal, 300);
	 }

	 // Try to register the location update handler with the chart until it is succeeds.
	 var initID = setInterval("initEvents()", 1000);

	</script>
<body>
	<div align="center">
	This chart is used mainly as an interface so the user can control the time range selection.
		<dnc:Chart ID="chart" runat="server" />
		<hr />
		<br/>
		<table style="width: 600">
			<tr>
				<td colspan="2">This chart does not offer any interactive functionality, it only reacts to the selection of the top chart.</td>
			</tr>
			<tr>
				<td>
			  <input id="textBox1" size="30" /></td>
				<td class="style1">   <input id="textBox2" size="30" /></td>
			</tr>
		</table>
 <dnc:Chart ID="Chart1" runat="server" />
	</div>
</body>
</html>
