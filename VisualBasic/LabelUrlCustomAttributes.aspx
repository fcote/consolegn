<%@ Page Language="vb" Debug="true" Trace="false" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>

<script runat="server">
	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		'This sample demonstrates using custom attributes with labels to create dynamic links.

		'set global properties
		Chart.Title = " Employees' Report"
		Chart.XAxis.Label.Text = "Employee's Name"
		Chart.YAxis.Label.Text = "Annual Salary"
		Chart.YAxis.FormatString = "currency"
		Chart.TempDirectory = "temp"
		Chart.ShadingEffectMode = ShadingEffectMode.Six
		Chart.Size = "900x400"
		Chart.Debug = True
		Chart.ChartArea.Label.Text = "Dynamic links embedded inside annotation labels"

		'Create a series  
		Dim de As DataEngine = New DataEngine()
		de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
		de.SqlStatement = "SELECT * FROM Employees"
        de.Limit = "5"
        de.ShowOther = False
		'Set predefined data fields and some custom fields as attributes for x-Axis. (Employee�s name)
		de.DataFields = "xAxis=name,yAxis=salary,id=EmployeeID,department=Department,email,phone=PhoneNumber"

		Chart.DefaultElement.Annotation = New Annotation("Employee No: %EmployeeID<row>Department: %Department<row>Email: <block url='mailto:%email' fColor='blue' fStyle='underline'>%email<row>Phone: %PhoneNumber<row><block  fColor='blue' fStyle='underline' url='customAttributeTarget.aspx?name=%name&salary=%yvalue&id=%EmployeeID&department=%Department&email=%email&phone=%PhoneNumber'>More Info")

		Chart.SeriesCollection.Add(de.GetSeries())

	End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Custom Attributes</title>
</head>
<body>
	<div style="text-align: center">
		<dotnet:Chart ID="Chart" TitleBox-Position="FullWithLegend" runat="server" />
	</div>
</body>
</html>
