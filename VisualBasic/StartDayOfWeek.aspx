<%@ Page Language="VB" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="dotnetCHARTING" %>

<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   If Not IsPostBack Then
      Dim s As String
      For Each s In  [Enum].GetNames(GetType(DayOfWeek))
         DayOfWeekDropDown.Items.Add(s)
      Next s
   End If 
   'set global properties
   Chart.Title = "sales"
   Chart.ChartArea.XAxis.Label.Text = "Weeks"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Use3D = True
   Chart.DateGrouping = TimeInterval.Weeks
   Chart.XAxis.FormatString = "MM/dd"
   Chart.StartDayOfWeek = CType([Enum].Parse(GetType(DayOfWeek), DayOfWeekDropDown.SelectedItem.Value, True), DayOfWeek)
   Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   Chart.DefaultSeries.Type = SeriesType.AreaLine
   Chart.DefaultSeries.DefaultElement.Transparency = 20
   
   Chart.OverlapFooter = False
   
   
   
   'Add a series
   Chart.Series.Name = "Item Sales "
   Chart.Series.StartDate = New System.DateTime(2002, 1, 8, 0, 0, 0)
   Chart.Series.EndDate = New System.DateTime(2002, 1, 30, 23, 59, 59)
   Chart.Series.SqlStatement = "SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
   Chart.SeriesCollection.Add()
   
   'Add a series
   Chart.Series.Name = "Orders"
   Chart.Series.StartDate = New System.DateTime(2002, 1, 8, 0, 0, 0)
   Chart.Series.EndDate = New System.DateTime(2002, 1, 30, 23, 59, 59)
   Chart.Series.SqlStatement = "SELECT OrderDate,Sum(1) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
   Chart.SeriesCollection.Add()
End Sub 'Page_Load 


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Start Day of Week Options</title>
</head>
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
    <form runat="server">
        <div style="text-align:center">
            <table border="1" cellpadding="3" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" id="AutoNumber1">
                <tbody>
                <tr>
    <td width="1%">
<img border="0" src="../../images/dotnetCharting.gif" width="230" height="94"></td>
    <td width="99%" bgcolor="#BFC0DB">Start Day of Week: 
 <ASP:DropDownList id="DayOfWeekDropDown" runat="server" size="1">
    </ASP:DropDownList>
     <asp:Button id="ButtonSet" runat="server" Text="Set">
    </asp:Button>
 </td>
          </tbody>
            </table>
            <DOTNET:Chart id="Chart" runat="server" Visible="true" />
        </div>
    </form></body>
</html>