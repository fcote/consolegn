<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates the same chart using navigator and without it.
		chart.TempDirectory = "temp"
		chart.XAxis.FormatString = "MMM yyyy"
		chart.Debug = True
		chart.LegendBox.Position = LegendBoxPosition.None
		chart.ShadingEffectMode = ShadingEffectMode.Three
		chart.PaletteName = dotnetCHARTING.Palette.Bright
		chart.Type = ChartType.Combo
		chart.Size = "800x350"
		chart.TempDirectory = "temp"
		chart.Title = "Chart without zooming"
		chart.DefaultSeries.Type = SeriesType.Bar
		chart.XAxis.TimeInterval = TimeInterval.Month

		chart.DefaultSeries.DefaultElement.ToolTip = "%yValue"

		Dim mySC2 As SeriesCollection = getRandomData()
		chart.SeriesCollection.Add(mySC2)

		'This chart is with initial zooming, right click on the chart to access zooming.
		Chart2.Size = "800x350"
		Chart2.TempDirectory = "temp"
		Chart2.Debug = True
		Chart2.LegendBox.Position = LegendBoxPosition.None
		Chart2.ShadingEffectMode = ShadingEffectMode.Three
		Chart2.PaletteName = dotnetCHARTING.Palette.Bright
		Chart2.Type = ChartType.Combo
		Chart2.DefaultSeries.Type = SeriesType.Bar

		Chart2.Title = "A chart with initial zooming. Hover mouse for bar value."

		Chart2.DefaultSeries.DefaultElement.ToolTip = "%yValue"

		Dim mySC3 As SeriesCollection = getRandomData()
		Chart2.SeriesCollection.Add(mySC3)

		'set for initial zooming 
		Chart2.Navigator.Enabled = True

		'configures the viewport to display 1 year
		Chart2.XAxis.Viewport.TimeInterval = New TimeIntervalAdvanced(New TimeSpan(365, 0, 0, 0))

		'displays starting in 2002 (if not set it would display at the first date. This in effect controls the starting scroll position.
	   ' Chart2.XAxis.Viewport.ScaleRange = new ScaleRange(new DateTime(2002, 1, 1), new DateTime(2002, 12, 31));
	End Sub
	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(3)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2000, 1, 1)
		Dim s As Series = New Series()
		For b As Integer = 0 To 95
			Dim e As Element = New Element()
			e.XDateTime = dt
			dt = dt.AddMonths(1)
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		' give each series element its own color
		s.PaletteName = Palette.Two
		SC.Add(s)
		Return SC
	End Function


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="chart" runat="server" />
		<dnc:Chart ID="Chart2" runat="server" />
	</div>
</body>
</html>
