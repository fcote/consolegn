<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates microcharts with tooltips.

		Chart.TempDirectory = "temp"
		Chart.Debug = True

		' Create the annotation.
		Dim an As Annotation = New Annotation(getChartText())
		an.DynamicSize = False
		'an.Label.Font = new Font("Arial", 10);
		an.Background.Color = Color.GreenYellow
		an.Background.ShadingEffectMode = ShadingEffectMode.Background2

		' Use the annotation to create the chart.
		Chart.ObjectChart = an

	End Sub

	Function getChartText() As String
		Dim result As String = "Weeks <block>Hover over the<br> microCharts<hr>"
		result &= "Week 1 <Chart:Column values='5,4,3,5,6,5,7,6,5,7,6,5,7,5,7' Tooltip='Total: 132'><hr>"
		result &= "Week 2 <Chart:Column values='5,4,3,5,6,5,7,3,5,6,3,5,6,5,7' Tooltip='Total: 122'><hr>"
		result &= "Week 3 <Chart:Column values='5,4,3,5,6,5,7,4,3,5,4,3,5,5,7' Tooltip='Total: 92'><hr>"
		result &= "Week 4 <Chart:Column values='5,4,3,5,6,5,7,5,6,5,5,6,5,5,7' Tooltip='Total: 142'>"




		Return result
	End Function
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
