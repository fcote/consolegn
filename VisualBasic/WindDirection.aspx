<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using an image inside a label and rotating it based on element data.

		Chart.Size = "600x350"
		Chart.Title = "Wind Speed & Direction"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.ShadingEffectMode = ShadingEffectMode.Two

		' Style the titlebox.
		Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
		Chart.TitleBox.Padding = 8
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Background2
		Chart.TitleBox.Background.Color = Color.LightSteelBlue

		' Define the element label showing the pointer rotated based on the direction parameter.
		Chart.DefaultElement.SmartLabel.Text = "%YValue mph" & Constants.vbLf & "<Chart:Image src='../../images/pointer.png' rotate='%direction'>"
		Chart.DefaultElement.ShowValue = True

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2009, 1, 1)
		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 7
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			e.YValue = myR.Next(50)
			e.CustomAttributes.Add("direction", myR.Next(180))
			s.Elements.Add(e)
		Next b
		SC.Add(s)
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
