<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using multiple area line series to highlight ranges.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.Type = ChartType.Combo
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.DefaultSeries.Type = SeriesType.Line
		Chart.DefaultSeries.Line.Width = 2
		Chart.LegendBox.Visible = False
		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		Chart.YAxis.Markers.Add(New AxisMarker("", Color.FromArgb(50,Color.Blue), 0,25))

		' An areaLine series is made for elements over 25.
		Dim s2 As Series = New Series()
		s2.Type = SeriesType.AreaLine
		s2.DefaultElement.Marker.Visible = False
		For Each el As Element In mySC(0).Elements
			If el.YValue >= 25 Then
				Dim e1 As Element = New Element()
				e1.YValue = el.YValue
				e1.YValueStart = 25
				e1.XValue = el.XValue
				s2.Elements.Add(e1)
			End If
		Next el

		Chart.SeriesCollection.Add(s2)

		' An areaLine series is made for elements under 0.
		Dim s3 As Series = New Series()
		s3.Type = SeriesType.AreaLine
		s3.DefaultElement.Marker.Visible = False
		For Each el As Element In mySC(0).Elements
			If el.YValue <= 0 Then
				Dim e1 As Element = New Element()
				e1.YValue = el.YValue
				e1.YValueStart = 0
				e1.XValue = el.XValue
				s3.Elements.Add(e1)
			End If
		Next el

		Chart.SeriesCollection.Add(s3)


		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

	End Sub

	Function getRandomData() As SeriesCollection
		Dim cur As Double = 50
		Dim myR As Random = New Random(2)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 19
				Dim e As Element = New Element()
				cur += -25 + myR.Next(50)
				e.YValue = cur
				e.XValue = b
				s.Elements.Add(e)
			Next b
			s(8).YValue = 25
			s(11).YValue = 0
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
