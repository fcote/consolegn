<%@ Page Language="vb" debug="True" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   ' This sample demonstrates the use of Exponential Distribution within StatisticalEngine.
   ' The Distribution Chart
   DistributionChart.Title = " Exponential Distribution"
   DistributionChart.TempDirectory = "temp"
   DistributionChart.Debug = True
   DistributionChart.Size = "600x300"
   DistributionChart.LegendBox.Template = "%icon %name"
   DistributionChart.YAxis.Percent = True
   DistributionChart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   DistributionChart.DefaultSeries.DefaultElement.Marker.Visible = False
   DistributionChart.YAxis.FormatString = "0.000"
   DistributionChart.YAxis.Scale = Scale.Range
   
   'double[] expDistVector = new double[] {0, 1, 2, 3, 4, 5};
   Dim expDistVector() As Double = {0, 1, 2, 3, 4, 10}
   
   
   Dim sampledata As New Series("Sample1")
   Dim i As Integer
   For i = 0 To expDistVector.Length - 1
      Dim el As New Element()
      el.XValue = expDistVector(i)
      sampledata.Elements.Add(el)
   Next i
   
   ' The second parameter of this method is the scale parameter of the exponential probability distribution
   Dim expDistribution As Series = StatisticalEngine.ExponentialDistribution(sampledata, 3.67)
   expDistribution.Type = SeriesType.Spline
   expDistribution.DefaultElement.ShowValue = True
   expDistribution.DefaultElement.SmartLabel.Text = "%YValue%"
   expDistribution.DefaultElement.SmartLabel.Color = Color.Black
   DistributionChart.SeriesCollection.Add(expDistribution)
End Sub 'Page_Load
		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="DistributionChart" runat="server"/>
		</div>
	</body>
</html>
