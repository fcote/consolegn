<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs )

	'set global properties
    Chart.Title="Item sales"
    Chart.ChartArea.XAxis.Label.Text="Months"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.DateGrouping = TimeInterval.Year
 
    Chart.Use3D=true
    Chart.DefaultSeries.DefaultElement.Transparency=20
      


	Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
	Chart.DefaultSeries.StartDate= New System.DateTime(2002,1,1,0,0,0)
    Chart.DefaultSeries.EndDate = New System.DateTime(2002,12,31,23,59,59)

    
    'Add a series
    Chart.Series.Name="Items"
    Chart.Series.Type=SeriesType.Cylinder
    Chart.Series.SqlStatement= "SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
    Chart.SeriesCollection.Add()
    
    'Add Average series
    Chart.Series.Name = "Average"
    Chart.Series.Type = SeriesType.Marker
    Chart.Series.DefaultElement.Marker.Type = ElementMarkerType.Triangle
    Chart.SeriesCollection.Add(Calculation.Average)

    
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Orders Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
