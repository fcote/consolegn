<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates how the navigator can notify and update other parts of the page such as input boxes. It also demonstrates using mixed selection on the preview area.

		Chart.Size = "600x135"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.ChartArea.Visible = False
		Chart.LegendBox.Visible = False
		Chart.DefaultSeries.Type = SeriesType.Line

		Chart.Navigator.Enabled = True

		' The control ID is specified so it can be referenced in javascript.
		Chart.Navigator.ControlID = "slPlugin"

		' Add arbitrary selection to the preview navigation options.
		Chart.Navigator.PreviewAreaNavigationOptons = Chart.Navigator.PreviewAreaNavigationOptons Or PreviewAreaNavigationOptions.ArbitrarySelection

		' Specify a larger height for the preview area.
		Chart.Navigator.PreviewAreaHeight = 55

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx
		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2009, 1, 1)
		Dim s As Series = New Series("Series 1")
		For b As Integer = 1 To 14
			Dim e As Element = New Element()
			dt = dt.AddDays(1)
			e.XDateTime = dt
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<style type="text/css">
	div, p, td
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: x-small;
	}
	.style1
	{
		text-align: right;
	}
</style>
<script type="text/javascript">

	// For more information on this code and a client side API reference, see this tutorial:
	// http://www.dotnetcharting.com/documentation/v6_0/Silverlight_Navigator_API.html

	var chartID = "slPlugin";

	// This function handles the update and is called by the chart.
	function locationUpdated(lowVal, highVal) {
		document.getElementById('textBox1').value = lowVal;
		document.getElementById('textBox2').value = highVal;
	}

	// This function registers the location update handler when the silverlight chart loads.
	function initEvents() {
		var control = document.getElementById("slPlugin");
		if (control) {
			// When this handler is set, it will automatically call the event to set the initial location.
			control.Content.Chart.SetLocationUpdateHandler("locationUpdated");
			clearInterval(initID);
		}
	}

	// Try to register the location update handler with the chart until it is succeeds.
	var initID = setInterval("initEvents()", 1000);

</script>
<body>
	<div align="center">
		<table style="width: 600">
			<tr>
				<td colspan="2">
					The preview area of this chart uses both Cell and Arbitrary selection. When both
					cell and arbitrary selection are enabled, they can be used at the same time. For
					example you can start by selecting a cell and end the selection at an arbitrary
					position and vice versa. Clicking near the top of the preview area starts the selection
					in cell mode while clicking near the bottom starts it at an arbitrary position.
					Moving the mouse pointer up or down allows switching between the modes during selection.
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<dnc:Chart ID="Chart" runat="server" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					These input boxes are updated based on the time selection of the above navigator.
				</td>
			</tr>
			<tr>
				<td>
					<input id="textBox1" size="30" />
				</td>
				<td class="style1">
					<input id="textBox2" size="30" />
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
