<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   'set global properties
   Chart.Title = "Item sales"
   Chart.XAxis.Label.Text = "months"
   Chart.YAxis.FormatString = "currency"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Size = "800X500"
        Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   
   'Make series legend entries invisible and add 3 custom legend entries
   Chart.DefaultSeries.LegendEntry.Visible = False
   Dim singleLegendEntry As New LegendEntry()
   singleLegendEntry.Name = "Sales < $3500"
   singleLegendEntry.Background.Color = Color.Red
        Chart.LegendBox.ExtraEntries.Add(singleLegendEntry)
   singleLegendEntry = New LegendEntry()
   singleLegendEntry.Name = "$3500 < Sales < $5000"
   singleLegendEntry.Background.Color = Color.Yellow
        Chart.LegendBox.ExtraEntries.Add(singleLegendEntry)
   singleLegendEntry = New LegendEntry()
   singleLegendEntry.Name = "$5000 < Sales "
   singleLegendEntry.Background.Color = Color.Green
        Chart.LegendBox.ExtraEntries.Add(singleLegendEntry)
   
   
   
   'Add a series
   Dim de As New DataEngine()
   de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString") 
   de.StartDate = New DateTime(2002, 1, 1, 0, 0, 0)
   de.EndDate = New DateTime(2002, 12, 31, 23, 59, 59)
   de.DateGrouping = TimeInterval.Year
   de.SqlStatement = "SELECT OrderDate,Total, Name FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"   
   Dim sc As SeriesCollection = de.GetSeries()
   ' customize element settings
   'set the color for different customers in each month based on their total sale.
   Dim sr As dotnetCHARTING.Series
   For Each sr In  sc
      Dim el As dotnetCHARTING.Element
      For Each el In  sr.Elements
         If el.YValue < 3500 Then
            el.Color = Color.Red
         Else
            If el.YValue < 5000 Then
               el.Color = Color.Yellow
            Else
               el.Color = Color.Green
            End If
         End If
      Next el
   Next sr
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Invisible Legend Entry</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>