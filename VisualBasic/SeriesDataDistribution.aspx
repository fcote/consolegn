<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(800)
   Chart.Height = Unit.Parse(500)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "DataDistribution"
   Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   Dim mySC As SeriesCollection = getRandomData()
   Chart.SeriesCollection.Add(mySC(0))
   Chart.DefaultSeries.Type = SeriesType.Spline
   
   StatisticalEngine.Options.MatchColors = True
   Dim dataDistribution As Series = StatisticalEngine.DataDistribution(mySC(0))
   Chart.SeriesCollection.Add(dataDistribution)
End Sub 'Page_Load


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim dt As New DateTime(2005, 1, 1)
   Dim a As Integer
   For a = 0 To 1
      Dim s As New Series()
      s.Name = "Downloads in January"
      Dim b As Integer
      For b = 0 To 30
         Dim e As New Element()
         'e.Name = "Element " + b;
         e.YValue = myR.Next(50)
         e.XDateTime = dt.AddDays(b)
         e.Name = e.XDateTime.ToString("d")
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
