<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">




Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(250)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   Chart1.Width = Unit.Parse(600)
   Chart1.Height = Unit.Parse(250)
   Chart1.TempDirectory = "temp"
   Chart1.Debug = True
   
   Chart2.Width = Unit.Parse(600)
   Chart2.Height = Unit.Parse(250)
   Chart2.TempDirectory = "temp"
   Chart2.Debug = True
   
   Chart3.Width = Unit.Parse(600)
   Chart3.Height = Unit.Parse(250)
   Chart3.TempDirectory = "temp"
   Chart3.Debug = True
   
   
   
   ' This sample will demonstrate how to work with tick label separators. 
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
   Dim mySC As SeriesCollection = getRandomData()
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
   
   
   
   ' The first chart doesnt automaticall show the separators so we set the color.
   Chart.Title = "Tick Label Separators on the x axis."
   
   ' The separator will appear because a color is set.
   Chart.XAxis.TickLabelSeparatorLine.Color = Color.Black
   
   
   
   ' The second chart automatically shows separators because the tick labels wrap.
   Chart1.Title = "Automatic tick label separators on the x axis."
   Chart1.SeriesCollection.Add(getRandomData2())
   
   
   ' The third chart automatically shows separators but we turn them off.
   Chart2.Title = "Tick separators turned off."
   ' The separator will not appear because a transparent color is set.
   Chart2.XAxis.TickLabelSeparatorLine.Color = Color.Transparent
   
   Chart2.SeriesCollection.Add(getRandomData2())
   
   
   ' // The fourth chart customize tickLabelSeparator
   Chart3.SeriesCollection.Add(mySC)
   Chart3.XAxis.TickLabelSeparatorLine.Color = Color.Red
   Chart3.XAxis.TickLabelSeparatorLine.DashStyle = DashStyle.Dash
   Chart3.XAxis.TickLabelSeparatorLine.Width = 2
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 0 To 19
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData


Function getRandomData2() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 0 To 4
         Dim e As New Element()
         e.Name = "Long Name, Element " + b.ToString()
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData2

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			
			<dotnet:Chart id="Chart1" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			
			<dotnet:Chart id="Chart3" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>

		</div>
	</body>
</html>
