﻿<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="gn_rapport.aspx.vb" Inherits="gn_rapport" EnableEventValidation="false" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br/>
        <span lang="fr-ca" style="font-family: 'Berlin Sans FB Demi'; font-size: xx-large">Rapport notes évaluations</span>
    <br /><br/>
    <div align="left">
        <asp:DropDownList ID="DropDownList3" runat="server" DataSourceID="SqlDataSource4"
            DataTextField="ECOLE" DataValueField="ECOLE" AutoPostBack="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:CS_GN %>"
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand="">
            <SelectParameters>
                <asp:SessionParameter Name="ID_USER" SessionField="ID_USER" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
      
        <br />
    
    <telerik:radgrid ID="RadGrid1" runat="server" AllowFilteringByColumn="False" AllowSorting="True" DataSourceID="SqlDataSource2" GridLines="None" ShowGroupPanel="True" Skin="Hay">
        <ExportSettings HideStructureColumns="true" ExportOnlyData="True" >
            <Excel Format="ExcelML" />
        </ExportSettings>
        <MasterTableView autogeneratecolumns="true" datasourceid="SqlDataSource2" CommandItemDisplay="Top">
            <RowIndicatorColumn><HeaderStyle Width="20px"></HeaderStyle></RowIndicatorColumn>
            <ExpandCollapseColumn><HeaderStyle Width="20px"></HeaderStyle></ExpandCollapseColumn>
            <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowExportToPdfButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true" />
        </MasterTableView>
        <ClientSettings AllowDragToGroup="True"></ClientSettings>
    </telerik:radgrid>

    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
        ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand="">
    </asp:SqlDataSource>
    
    <br/>
    <br/>
    
    </div>
 </asp:content>
