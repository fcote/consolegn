<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample"
   Chart.DefaultSeries.Type = SeriesType.Line
   
   ' This sample demonstrates how a step line can be created from a normal line.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
   Chart.SeriesCollection.Add(makeDateTimeStepSeries(mySC(0)))
   
   Chart.SeriesCollection(0).Name = "Original Series"
   Chart.SeriesCollection(1).Name = "Stepped Series"
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim dt As New DateTime(2006, 1, 1)
   Dim a As Integer
   For a = 1 To 1
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 9
         Dim e As New Element()
         'e.Name = "Element " + b.ToString();
         dt = dt.AddDays(1)
         e.XDateTime =dt 
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   
   Return SC
End Function 'getRandomData


Function makeDateTimeStepSeries(s As Series) As Series
   ' This method creates a series with an additional element for each original element which creates steps.
   Dim ns As New Series()
   Dim i As Integer
   For i = 0 To s.Elements.Count - 1
      If i > 0 Then
         ns.Elements.Add(New Element(s.Elements((i - 1)).Name, s.Elements((i - 1)).XDateTime.AddSeconds(1), s.Elements(i).YValue))
      End If
      ns.Elements.Add(New Element(s.Elements(i).Name, s.Elements(i).XDateTime, s.Elements(i).YValue))
   Next i
   Return ns
End Function 'makeDateTimeStepSeries

</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
