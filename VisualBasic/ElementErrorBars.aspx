<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Specifying Error Bars"
   Chart.DefaultSeries.Type = SeriesType.Line
   Chart.YAxis.Maximum = 100
   Chart.XAxis.TickLabelAngle = 15
   
   ' This sample will demonstrate setting a common error value of elements on a chart.
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   mySC(0).Elements(0).ErrorHighValue = 80
   mySC(0).Elements(1).ErrorLowValue = 30
   mySC(0).Elements(2).ErrorMinusOffset = 10
   mySC(0).Elements(3).ErrorMinusPercent = 10
   mySC(0).Elements(4).ErrorOffset = 10
   mySC(0).Elements(5).ErrorPercent = 10
   mySC(0).Elements(6).ErrorPlusOffset = 10
   mySC(0).Elements(7).ErrorPlusPercent = 10
   
   
   mySC(0).Elements(0).Name = "ErrorHighValue (80)"
   mySC(0).Elements(1).Name = "ErrorLowValue (30)"
   mySC(0).Elements(2).Name = "ErrorMinusOffset (10)"
   mySC(0).Elements(3).Name = "ErrorMinusPercent (10)"
   mySC(0).Elements(4).Name = "ErrorOffset (10)"
   mySC(0).Elements(5).Name = "ErrorPercent (10)"
   mySC(0).Elements(6).Name = "ErrorPlusOffset (10)"
   mySC(0).Elements(7).Name = "ErrorPlusPercent (10)"
   
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(2)
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      s.Name = "Series " + Convert.ToString(a+1)
      Dim b As Integer
      For b = 0 To 7
         Dim e As New Element()
         e.Name = "Element " + Convert.ToString(b+1)
         e.YValue = 50 '20+ myR.Next(50);
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   'SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
   'SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
   'SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);
   Return SC
End Function 'getRandomData

</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
