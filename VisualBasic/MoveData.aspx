<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using multiple image map techniques on a single chart.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.Type = ChartType.Combo
		Chart.DefaultSeries.Type = SeriesType.Marker
		Chart.DefaultElement.Marker.Size = 25
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"

		Chart.XAxis.Scale = Scale.Normal
		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

		Chart.ChartArea.Label.Text = "Select an element"

		Chart.DefaultElement.URL = "?name=%Name"

		Dim query As String = ""
		If Not Request.QueryString("name") Is Nothing Then
			query = Request.QueryString("name")
			Dim name As String = ""
			Dim SelectedElement As Element = Nothing
			For Each el As Element In mySC(0).Elements
				If el.Name = query Then
					SelectedElement = el
					Exit For
				End If
			Next el
			If Not Request.QueryString("x") Is Nothing Then

				' Move Element
				Dim fileName As String = Chart.FileManager.SaveImage()
				' Get the axis values at xy positions.
				' Get x and y points
				Dim x As Integer = Convert.ToInt32(Page.Request.Params("x"))
				Dim y As Integer = Convert.ToInt32(Page.Request.Params("y"))
				Dim va As Object = Chart.YAxis.GetValueAtY(x & "," & y)
				Dim va2 As Object = Chart.XAxis.GetValueAtX(x & "," & y)
				SelectedElement.YValue = CDbl(va)
				SelectedElement.XValue = CDbl(va2)

				Chart.FileManager.SaveImage()
			Else
				Chart.ChartArea.Label.Text = "Click a new location for " & query
				SelectedElement.Color = Color.Red
				Dim fileName As String = Chart.FileManager.SaveImage()
				Label1.Text = "<form action=""MoveData.aspx"" method=""post"" >"
				Label1.Text &= "<input type=hidden name=name value=" & query & ">"
				Label1.Text &= "<input type=image value=""submit"" border=0 src=""" & fileName & """ ISMAP>"
				Label1.Text &= "</form>"
				Chart.Visible = False
			End If
		End If


	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 4
				Dim e As Element = New Element("Element" & b.ToString())
				e.YValue = myR.Next(50)
				e.XValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
		<form action="MoveData.aspx" method="get" >
		<asp:Label ID="Label1" runat=server />
		</form>
	</div>
</body>
</html>
