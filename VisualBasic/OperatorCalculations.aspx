<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
		<script runat="server">

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      s.Name = "GNP"
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Country " + b.ToString()
         ' Load a very large value.
         e.YValue = myR.Next(50000000) + myR.Next(50000000) + myR.Next(50000000) + myR.Next(50000000)
         e.YValue += myR.Next(50000000) + myR.Next(50000000) + myR.Next(50000000) + myR.Next(50000000)
         e.YValue += myR.Next(500000000) + myR.Next(50000000) + myR.Next(50000000) + myR.Next(500000000)
         e.YValue += myR.Next(500000000) + myR.Next(50000000) + myR.Next(50000000) + myR.Next(500000000)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   Return SC
End Function 'getRandomData

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.TempDirectory = "temp"
   Chart.YAxis.NumberPrecision = 2
   Chart.Debug = True
   
   ' This sample demonstrates how operators can be used on series to manipulate chart data.
   ' Scenario:
   ' We are charting dollar amounts some nation's GNP. These values are very large so we will scale 
   ' the data down and show it in billions.
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx.
   Dim sc As SeriesCollection = getRandomData()
   
   ' We can scale the data by multiplying the series.
   Dim i As Integer
   For i = 0 To sc.Count - 1
      sc(i) = Series.Multiply(sc(i), 0.000000001)
   Next i 
   ' Now we label the axis apropriately.
   Chart.YAxis.Label.Text = "(GNP in Billions)"
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load

</script>
		<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Operator Calculations</title>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px" />
		</div>
	</body>
</html>
