<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates the sizing behavior of annotations with different settings.

		Chart.Size = "900x350"
		Chart.Title = ".netCHARTING Sample - Annotation Sizing Options with and without headers"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Type = ChartType.Organizational
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.DefaultSeries.Line.Width = 3
		Chart.DefaultSeries.Line.Color = Color.Gray
		Chart.ChartArea.Padding = 35
		Chart.TitleBox.Position = TitleBoxPosition.Full

		' Set the default annotation style.
		Chart.DefaultElement.Annotation = New Annotation()
		Chart.DefaultElement.Annotation.Background.Color = Color.DarkSalmon
		Chart.DefaultElement.Annotation.Background.ShadingEffectMode = ShadingEffectMode.Background1
		Chart.DefaultElement.Annotation.Padding = 7
		Chart.DefaultElement.Annotation.DefaultCorner = BoxCorner.Square
		Chart.DefaultElement.Annotation.Label.Font = New Font("Tahoma", 8f)

		' Determine if headers are shown.
		Dim useHeaders As Boolean = False
		If Page.Request.QueryString.Count > 0 Then
			Dim query As String = Page.Request.QueryString("header")
			If Not query Is Nothing AndAlso query = "yes" Then
				useHeaders = True
			End If
		End If



		If useHeaders Then
			Chart.DefaultElement.Annotation.HeaderLabel.Text = "<block fSize='8'>Header"
			Chart.DefaultElement.Annotation.HeaderBackground.Color = Color.DarkOrange
			Chart.DefaultElement.Annotation.HeaderBackground.ShadingEffectMode = ShadingEffectMode.Background2

			Dim an As Annotation = New Annotation("Hide Headers")
			an.Position = New Point(35, 50)
			an.Background.Color = Color.FromArgb(250, 250, 250)
			an.Shadow.Depth = 5
			an.DefaultCorner = BoxCorner.Round
			an.CornerSize = 3
			an.Size = New Size(150, 45)
			an.URL = "?header=no"
			Chart.Annotations.Add(an)

		Else
			' Add button to show headers.
			Dim an As Annotation = New Annotation("Show Headers")
			an.Position = New Point(35, 50)
			an.Background.Color = Color.FromArgb(250, 250, 250)
			an.Shadow.Depth = 5
			an.DefaultCorner = BoxCorner.Round
			an.CornerSize = 3
			an.Size = New Size(150, 45)
			an.URL = "?header=yes"
			Chart.Annotations.Add(an)
		End If

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim eDef As Element = New Element("This text is sized using the default sizing behavior.")
		Dim eDsf As Element = New Element("This behavior is with DynamicSize = false")
		Dim eSs As Element = New Element("size = 100x100")
		Dim eHs As Element = New Element("Annotation Height = 50")
		Dim eWs As Element = New Element("This behavior is with width specified (60x_)")
		eDsf.Parent = eDef
		eSs.Parent = eDef
		eHs.Parent = eDef
		eWs.Parent = eDef

		' Specify sizing options.
		eDsf.Annotation = New Annotation()
		eDsf.Annotation.DynamicSize = False

		eSs.Annotation = New Annotation()
		eSs.Annotation.Size = New Size(100, 100)

		eHs.Annotation = New Annotation()
		eHs.Annotation.Size = New Size(0, 50)

		eWs.Annotation = New Annotation()
		eWs.Annotation.Size = New Size(60, 0)

		Dim s As Series = New Series("",eDsf, eSs, eDef, eHs, eWs)
		Return New SeriesCollection(s)

	End Function



</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
