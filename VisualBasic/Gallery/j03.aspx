<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">
 ' This sample demonstrates how to iterate a layer of shape files.
Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Map
   Chart.Size = "900x550"
   Chart.Title = " USA states with inset Alaska and Hawaii"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.TitleBox.Position = TitleBoxPosition.Full
   Chart.ChartArea.Background = New Background(Color.FromArgb(142, 195, 236), Color.FromArgb(63, 137, 200), 90)
   Dim layer As MapLayer = MapDataEngine.LoadLayer("../../../images/MapFiles/primusa.shp")
   
   Chart.Mapping.DefaultShape.Label.Text = "%State_Abbr"
   Chart.Mapping.DefaultShape.Label.OutlineColor = Color.White
   Chart.Mapping.DefaultShape.Label.Hotspot.ToolTip = "%State_NAME"
   
   Chart.Mapping.MapLayerCollection.Add(layer)
   
   'Set different colors to each state
   Chart.PaletteName = Palette.Three
   Dim i As Integer = 0
   Dim shape As Shape
   For Each shape In  layer.Shapes
      If i >= Chart.Palette.Length Then
         i = 0
      End If
      shape.Background.Color = Chart.Palette(i)
      i += 1
   Next shape
End Sub 'Page_Load

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
