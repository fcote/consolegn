<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
	</head>
<script runat="server">
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates the use of financial indicators MedianPrice and TypicalPrice 
	FinancialChart.Title="Financial Chart without zooming"
	FinancialChart.TempDirectory="temp"
	FinancialChart.Debug=True
	FinancialChart.Size="800X400"
	FinancialChart.YAxis.Label.Text = "Price (USD)"
	FinancialChart.YAxis.FormatString = "currency"
	FinancialChart.YAxis.Scale = Scale.Range
	FinancialChart.TitleBox.Position = TitleBoxPosition.FullWithLegend
	FinancialChart.DefaultSeries.DefaultElement.Marker.Visible = False
	FinancialChart.DefaultSeries.Type = SeriesType.Spline

	' Modify the x axis labels.
	FinancialChart.XAxis.Scale = Scale.Time
	FinancialChart.XAxis.TimeInterval = TimeInterval.Day
	FinancialChart.XAxis.TimeScaleLabels.DayFormatString = "o"
	FinancialChart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month)
	FinancialChart.XAxis.TimeScaleLabels.MonthFormatString = "MMM"


	Dim priceDataEngine As DataEngine = New DataEngine ()
	priceDataEngine.ChartObject = FinancialChart
	priceDataEngine.ChartType = ChartType.Financial
	priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
	priceDataEngine.DateGrouping = TimeInterval.Day
	priceDataEngine.StartDate = New DateTime (2001,12,31,0,0,0)
	priceDataEngine.EndDate = New DateTime (2002,3,31,23,59,59)
	' Here we import data from the FinancialCompany table from within chartsample.mdb
	priceDataEngine.SqlStatement = "SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate "
	priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice"

	Dim stockSC As SeriesCollection = priceDataEngine.GetFinancialSeries ()
	FinancialChart.DefaultSeries.Type = SeriesTypeFinancial.CandleStick
	FinancialChart.DefaultElement.SmartLabel.Text="O:%Open | C:%Close"
	FinancialChart.DefaultElement.ShowValue=True
	FinancialChart.DefaultElement.ToolTip="L:%Low | H:%High"
	FinancialChart.SeriesCollection.Add(stockSC)


	'This chart is with zooming, right click on the chart to access zooming
	'Enable zooming with just one property
	 FinancialChart2.Zoomer.Enabled =True

	FinancialChart2.Title="Financial Chart with zooming, right click on the chart to access zooming"
	FinancialChart2.TempDirectory="temp"
	FinancialChart2.Debug=True
	FinancialChart2.Size="800X400"
	FinancialChart2.YAxis.Label.Text = "Price (USD)"
	FinancialChart2.YAxis.FormatString = "currency"
	FinancialChart2.YAxis.Scale = Scale.Range
	FinancialChart2.TitleBox.Position = TitleBoxPosition.FullWithLegend
	FinancialChart2.DefaultSeries.DefaultElement.Marker.Visible = False
	FinancialChart2.DefaultSeries.Type = SeriesType.Spline

	' Modify the x axis labels.
	FinancialChart2.XAxis.Scale = Scale.Time
	FinancialChart2.XAxis.TimeInterval = TimeInterval.Day
	FinancialChart2.XAxis.TimeScaleLabels.DayFormatString = "o"
	FinancialChart2.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month)
	FinancialChart2.XAxis.TimeScaleLabels.MonthFormatString = "MMM"


	priceDataEngine = New DataEngine ()
	priceDataEngine.ChartObject = FinancialChart2
	priceDataEngine.ChartType = ChartType.Financial
	priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
	priceDataEngine.DateGrouping = TimeInterval.Day
	priceDataEngine.StartDate = New DateTime (2001,12,31,0,0,0)
	priceDataEngine.EndDate = New DateTime (2002,3,31,23,59,59)
	' Here we import data from the FinancialCompany table from within chartsample.mdb
	priceDataEngine.SqlStatement = "SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate "
	priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice"

	stockSC = priceDataEngine.GetFinancialSeries ()
	FinancialChart2.DefaultSeries.Type = SeriesTypeFinancial.CandleStick
	FinancialChart2.DefaultElement.SmartLabel.Text="O:%Open | C:%Close"
	FinancialChart2.DefaultElement.ShowValue=True
	FinancialChart2.DefaultElement.ToolTip="L:%Low | H:%High"
	FinancialChart2.SeriesCollection.Add(stockSC)

End Sub
</script>
	<body>
<center>
	 <dotnet:Chart ID="FinancialChart" runat="server" />
   <br/>
	 <dotnet:Chart ID="FinancialChart2" runat="server" />
   </center>
</body>
</html>

