<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Forecasting Sample</title>
		<script runat="server">



Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of GeneralLinear Forecasting engine in order to
   ' find the function of best fit from three functions spaces. The data used for which
   ' the fuctions are fit is a set of data which represents a FX exchange rate over a given
   ' period of time. We index the period by the number of days after the inital date and the
   ' eights function spaces are the spaces spanned by the following basis elements:
   '	
   ' 1) {(1)}
   ' 2) {(1), (log x)}
   ' 3) {(1), (log x), (log^2 x)}
   ' 4) {(1), (log x), (log^2 x), (log^3 x)}
   ' 5) {(1), (log x), (log^2 x), (log^3 x), (log^4 x)}
   ' 6) {(1), (log x), (log^2 x), (log^3 x), (log^4 x), (log^5 x)}
   ' 7) {(1), (log x), (log^2 x), (log^3 x), (log^4 x), (log^5 x), (log^6 x)}
   ' 8) {(1), (log x), (log^2 x), (log^3 x), (log^4 x), (log^5 x), (log^6 x), (log^7 x)}
   '
   
   ' The Forecast Chart
   ForecastChart.Title = "Exchange"
   ForecastChart.TempDirectory = "temp"
   ForecastChart.Debug = True
   ForecastChart.Size = "1000x800"
   ForecastChart.LegendBox.Template = "%icon %name"
   ForecastChart.XAxis.ScaleRange.ValueLow = 2000
   ForecastChart.YAxis.ScaleRange.ValueLow = 200
   ForecastChart.PaletteName = Palette.Three
   
   
   ' The Forecast data
   Dim de As New DataEngine()
   de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   de.DateGrouping = TimeInterval.Days
   de.StartDate = New DateTime(1980, 3, 10, 0, 0, 0)
   de.EndDate = New DateTime(1982, 12, 10, 23, 59, 59)
   ' Load some data from the Exchange table 
   de.SqlStatement = "SELECT ID, Value AS q FROM Exchange WHERE Data >= #STARTDATE# AND Data <= #ENDDATE# ORDER BY Data "
   
   'Add a series
   Dim scForecast As SeriesCollection = de.GetSeries()
   ForecastChart.SeriesCollection.Add(scForecast)
   
   scForecast(0).Name = "Exchange"
   scForecast(0).Type = SeriesType.Spline
   
   
   ' Takes off the marker off the line and spline series.
   ForecastChart.DefaultSeries.DefaultElement.Marker = New ElementMarker(ElementMarkerType.None)
   ForecastChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   
   ' Generate a series of standard deviation for the given points
   Dim deviation As New Series()
   Dim i As Integer
   For i = 0 To (scForecast(0).Elements.Count) - 1
      Dim el As New Element()
      el.XValue = scForecast(0).Elements(i).XValue
      el.YValue = 1E-10
      deviation.Elements.Add(el)
   Next i
   
   ' Declare a new serie for the ChiSquare elements
   Dim chiSquareSeries As New Series()
   
   ' Note that this line is necessary in order to clear the function basis set by previous
   ' example.
   '
   ForecastEngine.Options.Reset()
   
   ' Set the first model function
   '
   ' The second basis element: (1)
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {0})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel1 As New Series()
   generalLinearModel1 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel1.Name = "0th Degree Logarithm"
   generalLinearModel1.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel1)
   
   ' Set the second model function ; we add log x function to the basis functions
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {1})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel2 As New Series()
   generalLinearModel2 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel2.Name = "1st Degree Logarithm"
   generalLinearModel2.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel2)
   
   ' Set the third model function ; we add log^2 x function to the basis functions
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {2})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel3 As New Series()
   generalLinearModel3 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel3.Name = "2nd Degree Logarithm"
   generalLinearModel3.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel3)
   
   ' We add log^3 x function to the basis functions
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {3})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel4 As New Series()
   generalLinearModel4 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel4.Name = "3rd Degree Logarithm"
   generalLinearModel4.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel4)
   
   ' We add log^4 x function to the basis functions
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {4})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel5 As New Series()
   generalLinearModel5 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel5.Name = "4th Degree Logarithm"
   generalLinearModel5.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel5)
   
   ' We add log^5 x function to the basis functions
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {5})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel6 As New Series()
   generalLinearModel6 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel6.Name = "5th Degree Logarithm"
   generalLinearModel6.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel6)
   
   ' We add log^6 x function to the basis functions
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {6})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel7 As New Series()
   generalLinearModel7 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel7.Name = "6th Degree Logarithm"
   generalLinearModel7.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel7)
   
   ' We add log^7 x function to the basis functions
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {7})
   ' Generate a new series which will draw the best fit line according with the model function which we just set
   Dim generalLinearModel8 As New Series()
   generalLinearModel8 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel8.Name = "7th Degree Logarithm"
   generalLinearModel8.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(generalLinearModel8)
   
   ' Fit the 27th Degree Logarithm
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {8})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {9})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {10})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {11})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {12})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {13})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {14})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {15})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {16})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {17})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {18})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {19})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {20})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {21})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {22})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {23})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {24})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {25})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {26})
   ForecastEngine.Options.AddLogarithmicSum(New Double() {1}, New Double() {1}, New Double() {0}, New Double() {27})
   ' Generate a new series which will draw the function of best from the fnuctoin spacce available.
   Dim generalLinearModel9 As New Series()
   generalLinearModel9 = ForecastEngine.Advanced.GeneralLinear(scForecast(0), deviation)
   generalLinearModel9.Name = "27th Degree Logarithm"
   generalLinearModel9.DefaultElement.Color = Color.FromArgb(0, 0, 0)
   generalLinearModel9.Type = SeriesType.Spline
   
   'The next two lines display on to the chart the value of the ChiSquare
   generalLinearModel9.Elements(1).SmartLabel.Text = "ChiSquared: %ChiSquare"
   generalLinearModel9.Elements(1).ShowValue = True
   
   ForecastChart.SeriesCollection.Add(generalLinearModel9)
End Sub 'Page_Load  
		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="ForecastChart" runat="server"/>
			
			
		</div>
	</body>
</html>
