<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates a vertical linear gauge using shading effect one with gradient axis markers and image axis ticks.
		Chart.Size = "110x220"

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.LegendBox.Visible = False

		Chart.Type = ChartType.Gauges
		Chart.DefaultSeries.GaugeType = GaugeType.Vertical
		Chart.ShadingEffectMode = ShadingEffectMode.One

		Chart.ChartArea.Padding = 0
		Chart.Margin = "-2"
		Chart.ChartArea.ClearColors()
		Chart.DefaultElement.ShowValue = True

		Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round
		Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox
		Chart.YAxis.Orientation = dotnetCHARTING.Orientation.Right

		Chart.YAxis.ExtraTicks.Add(New AxisTick(50, New ElementMarker("../../images/error2.png")))
		Chart.YAxis.ExtraTicks.Add(New AxisTick(30, New ElementMarker("../../images/error.png")))
		Chart.YAxis.Markers.Add(New AxisMarker("", New Background(Color.Orange, Color.Red, -90), 30, 50))
		Chart.YAxis.Markers.Add(New AxisMarker("", New Background(Color.Yellow, Color.Orange, -90), 15, 30))
		Chart.YAxis.Markers.Add(New AxisMarker("", New Background(Color.Green, Color.Yellow, -90), 0, 15))
		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random()
        Dim SC As SeriesCollection = New SeriesCollection()
        Dim a As Integer
        Dim b As Integer
		For a = 1 To 1
			Dim s As Series = New Series("")
			For b = 1 To 1
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = 45
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
