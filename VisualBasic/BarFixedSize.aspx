<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Bar Fixed Size</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   ' This sample will demonstrate how to manipulate the bar size of a chart. This is also useful when drilling
   ' down data to keep the columns looking the size same.
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
   Dim mySC As SeriesCollection = getRandomData()
   
   ' If you would like to keep the columns a specific width for any reason, you can change the StaticColumnWidth 
   ' property of the axis from which it stems.
   Chart.XAxis.StaticColumnWidth = 50
   
   ' For horizontal charts you would use the yAxis.
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim ElementCount As Integer = myR.Next(4, 8)
   
   Dim s As New Series()
   s.Name = "Series 1"
   Dim b As Integer
   For b = 1 To ElementCount - 1
      Dim e As New Element()
      e.Name = "Element " + b.ToString()
      e.YValue = myR.Next(50)
      s.Elements.Add(e)
   Next b
   SC.Add(s)
   
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<font face="Arial" size="2">Refresh this sample to see a different number of elements.
            </font>
		</div>
	</body>
</html>
