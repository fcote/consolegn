<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Size = "700x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Smart palette range coloring."
   Chart.ChartArea.Label.Text = "Automatically specify colors for elements" + ControlChars.Lf + "within a range defined by the SmartColor object."
   Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   
   
   ' This sample demonstrates how to use smart palettes to color elements with values within a specified range.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Create smart palette.
   Dim sp As New SmartPalette()
   
   ' Create smart color with the range and color to use.
   Dim sc As New SmartColor(Color.Purple, New ScaleRange(New DateTime(2006, 1, 4), New DateTime(2006, 1, 8)), ElementValue.YDateTime)
   
   sc.LegendEntry.Visible = False
   
   ' Add the color to the palette, and the palette to the chart.
   sp.Add("Series 1", sc)
   Chart.SmartPalette = sp
   
   
   Chart.YAxis.Markers.Add(New AxisMarker("", Color.FromArgb(100, Color.Purple), New DateTime(2006, 1, 4), New DateTime(2006, 1, 8)))
   Chart.YAxis.Markers(0).LegendEntry.Visible = False
   
   'Chart.LegendBox.ExtraEntries.Add(new LegendEntry("10 < Elements < 30","",Color.Purple));
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   
   Dim a As Integer
   For a = 1 To 1
      Dim dt As New DateTime(2006, 1, 1)
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 14
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         'e.YValue = myR.Next(50);
         e.YDateTime = dt.AddDays(myR.Next(10))
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
