<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates embedding dynamic chart images inside a legendbox.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.Type = ChartType.Combo
		Chart.Size = "680x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.ShadingEffectMode = ShadingEffectMode.Two
		Chart.LegendBox.Visible = False

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		Dim lb As LegendBox = New LegendBox()
		lb.Orientation = dotnetCHARTING.Orientation.Bottom

		' Set the legendbox DataSource.
		lb.DataSource = mySC(0).GetDataSource()

		' Set legend columns and specify the value column image
		lb.Template = "%Value%PercentOfTotal%YValue%Icon%Name"
		lb.DefaultEntry.Value = "<img:EmbeddedLegendChartsStream.aspx?value=%YValue&max=50>"

		' Set legend column alignments
		lb.ColumnAlignments = New StringAlignment(){StringAlignment.Center,StringAlignment.Center,StringAlignment.Center,StringAlignment.Center,StringAlignment.Center}

		'Setup header entry
		lb.HeaderEntry.Visible = True
		lb.HeaderEntry.HeaderMode = LegendEntryHeaderMode.RepeatOnEachColumn
		lb.HeaderEntry.CustomAttributes.Add("YValue", "Value")
		lb.HeaderEntry.CustomAttributes.Add("PercentOfTotal", "POT")
		lb.HeaderEntry.Value = "Bar Indicator"

		Chart.ExtraLegendBoxes.Add(lb)

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 4
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
