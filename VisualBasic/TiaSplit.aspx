<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample"
   Chart.ChartArea.Label.Text = "Splitting a series by a TimeIntervalAdvanced object."
   
   ' This sample demonstrates splitting a single series into multiple series based on a TimeIntervalAdvanced object.
   Chart.DefaultSeries.Type = SeriesType.AreaLine
   Chart.DefaultElement.Marker.Visible = False
   Chart.LegendBox.DefaultEntry.Value = "%Sum"
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC(0).Split(TimeIntervalAdvanced.Month))
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim dt As New DateTime(2006, 1, 1)
   Dim a As Integer
   For a = 1 To 1
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 199
         Dim e As New Element()
         'e.Name = "Element " + b.ToString();
         e.YValue = myR.Next(50)
         dt  = dt.AddDays(1)
         e.XDateTime = dt
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   Return SC
End Function 'getRandomData
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
