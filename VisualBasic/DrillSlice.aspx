<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Drill Slice</title>
		<script runat="server">



Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Use3D = True
   
   ' Set the chart type.
   Chart.Type = ChartType.Donuts
   
   ' Get the static chart data.
   Dim sc As SeriesCollection = getStaticData()
   
   
   ' When a pie slice is clicked we'll pass the series and element name in the query string.
   ' In order to pass info to the next time this page loads we will specify a url to use on the pie slice link.
   ' By setting the default element all elements will inherit this setting.
   Chart.DefaultSeries.DefaultElement.URL = "?eName=%Name&sName=%SeriesName"
   
   
   ' Here we will look for info in the query string that will tell us which slice was clicked.
   ' Get the series and element names from the query string.
   Dim seriesName As String = Request.QueryString("sName")
   Dim elementName As String = Request.QueryString("eName")
   
   ' Use the GetElement method to find our element by passing the series and element name.
   Dim el As Element = sc.GetElement(seriesName, elementName)
   
   ' If we get an element with the specified name, set the ExplodeSlice property to true.
   If Not (el Is Nothing) Then
      el.ExplodeSlice = True
   End If 
   ' Add the static data.
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load
 


Function getStaticData() As SeriesCollection
   ' Create a series collection object.
   Dim SC As New SeriesCollection()
   
   ' Create a series object
   Dim s As New Series("mySeries")
   
   ' Add some elements
   s.Elements.Add(New Element("1", 15))
   s.Elements.Add(New Element("2", 13))
   s.Elements.Add(New Element("3", 17))
   s.Elements.Add(New Element("4", 16))
   
   ' Add the series to the series collection.
   SC.Add(s)
   
   ' Repeat with another series.
   Dim s2 As New Series("mySeries 2")
   s2.Elements.Add(New Element("1", 15))
   s2.Elements.Add(New Element("2", 13))
   s2.Elements.Add(New Element("3", 17))
   s2.Elements.Add(New Element("4", 16))
   
   SC.Add(s2)
   
   Return SC
End Function 'getStaticData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
