<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + CStr(a)
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + CStr(b)
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   
   Return SC
End Function 'getRandomData



Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   ' This sample demonstrates how you can iterate through elements and change their color
   ' based on the data.
   ' We'll set the default type to line so it can be viewed better.
   Chart.DefaultSeries.Type = SeriesType.Line
   
   ' We'll also make the markers bigger so they can be seen better.
   Chart.DefaultSeries.DefaultElement.Marker.Size = 13
   
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx.
   Dim sc As SeriesCollection = getRandomData()
   
   ' The elements values are between 0 and 50 so we'll make any elements outside on 10 to 40 red and within green.
   Dim s As Series
   For Each s In  sc
      Dim el As Element
      For Each el In  s.Elements
         If el.YValue < 10 Or el.YValue > 40 Then
            el.Color = Color.Red
         Else
            el.Color = Color.Green
         End If 
      Next el 
   Next s 
   ' Add the random data.
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load 
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Interate Elements</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
