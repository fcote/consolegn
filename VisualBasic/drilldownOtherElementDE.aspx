<%@ Page Language="vb" Debug="true" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)


	If (Not IsPostBack) Then
		DropDownLimitMode.Items.Add("Top")
		DropDownLimitMode.Items.Add("Bottom")
		DropDownLimitMode.Items.Add("ExcludeTop")
		DropDownLimitMode.Items.Add("ExcludeBottom")
		If Not Request.QueryString("limitMode") Is Nothing Then
			DropDownLimitMode.SelectedIndex = CInt(Fix(System.Enum.Parse(GetType(dotnetCHARTING.LimitMode),Request.QueryString("limitMode"),True)))
		End If
	End If



	'set global properties
	Chart.Title="Sales"
	Chart.ChartArea.YAxis.Label.Text="Sales"
	Chart.ChartArea.YAxis.FormatString="Currency"
	Chart.TempDirectory="temp"
	Chart.Debug=True
	Chart.DefaultSeries.DefaultElement.Transparency=15
	Chart.Use3D=True
	Chart.Size="500x450"
	Chart.TitleBox.Position=TitleBoxPosition.FullWithLegend

	'Add a series
	Chart.Series.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
	Chart.Series.StartDate = New System.DateTime(2002,1,1,0,0,0)
	Chart.Series.EndDate = New System.DateTime(2002,12,31,23,59,59)
	Chart.Series.Name="Sales"
	Chart.Series.SqlStatement= "SELECT Name,Sum(Quantity) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.Name ORDER BY Sum(Quantity) DESC"
	Chart.SeriesCollection.Add()





'This is the same chart with the ShowOther property set
'this will add an element representing the total sales
'for other customers not in the top five.

	'set global properties
	Chart2.Title="Sales"
	Chart2.ChartArea.YAxis.Label.Text="Sales"
	Chart2.ChartArea.YAxis.FormatString="Currency"
	Chart2.TempDirectory="temp"
	Chart2.Debug=True
	Chart2.DefaultSeries.DefaultElement.Transparency=15
	Chart2.Use3D=True
	'"OtherAll", shows all other elements in the second level.
	'"Other" shows the next number of elements has been set in limit propertes.
	Chart2.DrillDownChain="other"
	Chart2.Series.DefaultElement.ToolTip="%yvalue"
	Chart2.Size="500x450"
	Chart2.TitleBox.Position=TitleBoxPosition.FullWithLegend
	Chart2.YAxis.SmartScaleBreak = True





	'Add a series
	Chart2.DefaultSeries.Limit = TextBoxLimit.Text
	Chart2.DefaultSeries.LimitMode = CType(System.Enum.Parse(GetType(dotnetCHARTING.LimitMode), DropDownLimitMode.SelectedItem.Value, True), dotnetCHARTING.LimitMode)

	Dim de As dotnetCHARTING.DataEngine = New dotnetCHARTING.DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
	de.StartDate = New System.DateTime(2002,1,1,0,0,0)
	de.EndDate = New System.DateTime(2002,12,31,23,59,59)

	de.SqlStatement= "SELECT Name,Sum(Quantity) FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.Name  ORDER BY Sum(Quantity) DESC"
	'string seriesLimit = Request.QueryString["seriesLimit"];
	'if (seriesLimit == null)
	'seriesLimit = TextBoxLimit.Text;



	de.Limit= TextBoxLimit.Text 'seriesLimit;
	de.LimitMode = CType(System.Enum.Parse(GetType(dotnetCHARTING.LimitMode), DropDownLimitMode.SelectedItem.Value, True), dotnetCHARTING.LimitMode)
	de.ShowOther=True
	de.OtherElementLabel ="Other"
	Chart2.SeriesCollection.Add(de.GetSeries())


End Sub
Sub ButtonSet_Click(ByVal sender As Object, ByVal e As EventArgs)


End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Drill down on other element</title>
<style type="text/css">
	 body.myBody  
	 {
	 border-top-width:0px;
	  border-left-width:0px;

	 }
	 img.myImage  
	 {
	 border:0px;

	 }
	 td.myTD1  
	 {
	  width:"1%";
		border:1px; 
		margin:0;
	 }
	 td.myTD2  
	 {     
	  width:"99%";
	  background:"#BFC0DB";
	  border-collapse:collapse;


	 }
	 table.myTable
	 {
	  border:1px; 
	  border-style:solid;
	  border-spacing:0;
	  border-color:"#111111";
	  border-collapse:collapse;

	 }

	</style>
</head>
<body class="myBody">
	<form runat="server">
		<div style="text-align:center">
			<table class="myTable" id="AutoNumber1">

				<tr>
	<td class="myTD1">
<img class="myImage" alt="dotnetChartingImage" src="../../images/dotnetCharting.gif" width="230" height="94"/></td>
	<td class="myTD2">Limit:

 <ASP:DropDownList id="DropDownLimitMode" runat="server">
	</ASP:DropDownList>
	<ASP:TextBox id="TextBoxLimit" runat="server"/>
	 <asp:Button id="ButtonSet" OnClick="ButtonSet_Click" runat="server" Text="Set">
	</asp:Button>
 </td>
		  </tr>
			</table>
			</div>
		</form>
 <div style="text-align:center">
 <table>
<tbody>
<tr>
<td>
 <dotnet:Chart id="Chart"  runat="server"/>
 </td><td>
 <dotnet:Chart id="Chart2"  runat="server"/>
</td>
</tr>
</tbody>
</table>
</div>
</body>
</html>
