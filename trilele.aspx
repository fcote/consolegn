<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="trilele.aspx.vb" Inherits="trilele" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:Label ID="Label1" runat="server" Font-Names="Berlin Sans FB Demi" Font-Size="XX-Large"
Text="Trillium: renseignements �l�ves" Width="100%"></asp:Label><br />
<br />

<asp:DropDownList ID="DropDownList1" runat="server"  DataTextField="school_name" DataValueField="school_code"  DataSourceID="SqlDataSource8" AutoPostBack="True" Visible="true">
</asp:DropDownList>

<asp:SqlDataSource ID="SqlDataSource8" runat="server" 
ConnectionString="<%$ ConnectionStrings:CS_Trillium %>" 
ProviderName="<%$ ConnectionStrings:CS_Trillium.ProviderName %>" 
SelectCommand=" 
SELECT DISTINCT school_name, school_code FROM schools ORDER BY school_name ">
</asp:SqlDataSource>

<telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource1" GridLines="None" AllowFilteringByColumn="True" AllowSorting="True" PageSize="15" ShowGroupPanel="True" Skin="Office2007">
<MasterTableView DataSourceID="SqlDataSource1" GroupsDefaultExpanded="False" AutoGenerateColumns="True"></MasterTableView>
<ClientSettings AllowDragToGroup="True"></ClientSettings>
</telerik:RadGrid>
<asp:SqlDataSource ID="SqlDataSource1" runat="server"  
ConnectionString="<%$ ConnectionStrings:CS_Trillium %>"
ProviderName="<%$ ConnectionStrings:CS_Trillium.ProviderName %>"
SelectCommand="
    select 
    MEMBRE_ID    ,
    M_NOM        ,
    M_PRENOM     ,
    NUMERO_ELEVE ,
    ANNEESCOLAIRE,
    NIVEAU       ,
    ECOLE        ,
    CLASSE_CODE  ,
    CODECOURS    ,
    to_char(DATEDEBUT, 'ddmmyyyy')  DATEDEBUT  ,
    to_char(DATEFIN, 'ddmmyyyy')  DATEFIN   ,
    STATUTELEVE  ,
    SEXE         ,
    FLAG_PEI     ,
    TYPE_ECOLE   
    from gn.gn_vw_eleves_ele
    where ECOLE = :school_code
    union 
    select 
    MEMBRE_ID    ,
    M_NOM        ,
    M_PRENOM     ,
    NUMERO_ELEVE ,
    ANNEESCOLAIRE,
    NIVEAU       ,
    ECOLE        ,
    CLASSE_CODE  ,
    CODECOURS    ,
    to_char(DATEDEBUT, 'ddmmyyyy')  DATEDEBUT   ,
    to_char(DATEFIN, 'ddmmyyyy')  DATEFIN   ,
    STATUTELEVE  ,
    SEXE         ,
    FLAG_PEI     ,
    TYPE_ECOLE   
    from gn.gn_vw_eleves_sec
    where ECOLE = :school_code
"
>
<SelectParameters>
<asp:ControlParameter ControlID="DropDownList1" Type="String" Name="school_code"  DefaultValue="206" />
</SelectParameters>
</asp:SqlDataSource>

</asp:Content>
