<%@ Page Language="vb" debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="dotnetCHARTING.Zoomer" %>


<script runat="server">
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	'This sample explores different zoomer properties

	'Enable AJAX zoom / scroll
	Chart.Zoomer.Enabled = True

	'Determines if the chart image is rendered as a single image or in sections.
	'Next property "PreloadedSections" determines the number of sections
	Chart.Zoomer.UseSections = True

	' Determines the number of sections (rows / columns)outside of the viewing portal to render in advance. 
	' This can provide a better experience for the user at the cost of performance and bandwidth.
	Chart.Zoomer.PreloadedSections = 2

	'Determines the visual style when selecting a zoomed region.  Options include box and cross hair
        Chart.Zoomer.SelectionStyle = SelectionStyle.Box

        'Determines the scrolling type with options including drag (click / drag), scroll (using scroll bars / arrows) or both.
        Chart.Zoomer.ScrollType = ScrollType.Both

	'Determines if the help button is enabled.  
	'This button displays in between the scroll bars and pops up a window showing the help text when pressed.
	Chart.Zoomer.HelpEnabled = True
	
	'Controls the position of the ? icon on the chart when there is no scroll bar.
	Chart.Zoomer.HelpButtonPosition = new Point(574,20)

	'Overrides the default help text which displays when hitting the ? icon.
	Chart.Zoomer.HelpText = "Right click on the chart to access zooming and navigation options through the context menu.""Zoom"" enables you to select a new bounding area box to zoom into.  When in zoom mode the cursor will appear as a crosshair and you can click and drag the mouse to create a new zoomed view.  ""Drag"" enables you to click / drag to view more of the chart you would with an online map, you can also use the scroll bars if available.  ""Zoom in"" and ""zoom out"" will zoom in fixed increments on each selection.  To return to the original view, select ""show all."""

	'Determines the amount time in seconds before the tooltip displays (if a tooltip exists for the area hovered over.
	Chart.Zoomer.ToolTipDelay = 1
	Chart.DefaultElement.ToolTip = "(%XValue, %YValue)"
        
        'Set URL for all elements
        Chart.DefaultElement.URL = "http://dotnetcharting.com"
        Chart.DefaultElement.URLTarget = "_blank"
        
	' Controls the zooming direction. Options include XAxis (select a specific x axis
	' region only, y axis is fixed to the maximum extent) , 
	' YAxis (select a specific y axis region only, x axis is fixed to the maximum extent
	' and Both (can select any arbitrary bounding box to zoom to)
	Chart.Zoomer.AxisToZoom = AxisToZoom.YAxis


	'Determines if the default operation after a zoom is reset to drag (enabled by default).
	'If it is set to false, the user can enable it through the context menu.
	Chart.Zoomer.DragAfterZoom = True

	'General chart properties
	Chart.Title = "A chart with zooming, right click on the chart to access zooming"
	Chart.Debug = True
	Chart.Size="600X480"
	Chart.Type = ChartType.Scatter
	Chart.LegendBox.Position = LegendBoxPosition.None
	Chart.DefaultSeries.Line.Width = 5
	Chart.DefaultElement.Marker.Size = 40
	Chart.DefaultElement.Marker.Type = ElementMarkerType.FivePointStar
	Chart.DefaultSeries.DefaultElement.Transparency = 30
	Chart.DefaultSeries.PaletteName = dotnetCHARTING.Palette.Bright
	Chart.XAxis.Interval =2
	' Modify the x axis labels.
	Dim mySC As SeriesCollection = getRandomData()
	' Add the random data.
	Chart.SeriesCollection.Add(mySC)


End Sub
Function getRandomData() As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random()
	For a As Integer = 1 To 1
		Dim s As Series = New Series()
		s.Name = "Series " & a
		For b As Integer = 0 To 44
			Dim e As Element = New Element()
			e.YValue = myR.Next(10,100)
			e.XValue = b
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Image Zoomer Options</title></head>
<body>
<center>
 <dotnet:Chart id="Chart"  runat="server"/>
</center>
</body>
</html>
