﻿<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="gn_eleve.aspx.vb" Inherits="gn_eleve" EnableEventValidation="false" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br/>
        <span lang="fr-ca" style="font-family: 'Berlin Sans FB Demi'; font-size: xx-large">Notes par élève</span>
    <br /><br/>
    <div align="left">
        <asp:DropDownList ID="DropDownList3" runat="server" DataSourceID="SqlDataSource4"
            DataTextField="ECOLE" DataValueField="ECOLE" AutoPostBack="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:CS_GN %>"
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand="">
            <SelectParameters>
                <asp:SessionParameter Name="ID_USER" SessionField="ID_USER" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        <!--SELECT DISTINCT gne2.m_ecole, gne2.nom_ecole
            FROM GN_VW_ENSEIGNANTS GNE2 
            WHERE GNE2.M_MEMBRE_ID = :ID
            !-->
    
        <asp:DropDownList ID="DropDownList1" runat="server" 
            DataSourceID="SqlDataSource1" DataTextField="NOM" 
            DataValueField="MEMBRE_ID" AutoPostBack="True">
        </asp:DropDownList>
        <asp:DropDownList ID="DropDownList4" runat="server">
          <asp:ListItem Value="I">Étape intermédiare</asp:ListItem>
            <asp:ListItem Value="F">Étape finale</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="DropDownList5" runat="server" AutoPostBack="True">
            <asp:ListItem Value="1">Semestre 1</asp:ListItem>
            <asp:ListItem Value="2">Semestre 2</asp:ListItem>
        </asp:DropDownList>
		<asp:DropDownList ID="DropDownList6" runat="server" AutoPostBack="True">
            <asp:ListItem Value="20192020">2019-2020</asp:ListItem>
            <asp:ListItem Value="20182019">2018-2019</asp:ListItem>
			<asp:ListItem Value="20172018">2017-2018</asp:ListItem>
			<asp:ListItem Value="20162017">2016-2017</asp:ListItem>
            <asp:ListItem Value="20152016">2015-2016</asp:ListItem>
            <asp:ListItem Value="20142015">2014-2015</asp:ListItem>
			<asp:ListItem Value="20132014">2013-2014</asp:ListItem>
			<asp:ListItem Value="20122013">2012-2013</asp:ListItem>
			<asp:ListItem Value="20112012">2011-2012</asp:ListItem>
			<asp:ListItem Value="20102011">2010-2011</asp:ListItem>
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" 
            SelectCommand="">
        </asp:SqlDataSource>
<asp:DropDownList ID="DropDownList2" runat="server" 
            DataSourceID="SqlDataSource3" DataTextField="NOM" 
            DataValueField="MEMBRE_ID" AutoPostBack="True">
        </asp:DropDownList>
<br/>
        
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
            ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" 
            SelectCommand="">
        </asp:SqlDataSource>        
        
    
        
        
        
    
        <asp:Button ID="Button1" runat="server" Text="Graphique" />
        
    
        <br />
        <br />

    
    
    <telerik:radgrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True" 
        AllowSorting="True" DataSourceID="SqlDataSource2" GridLines="None" 
        ShowGroupPanel="True" Skin="Hay">
           <ExportSettings HideStructureColumns="true" ExportOnlyData="True" >
            <Excel Format="ExcelML" />
        </ExportSettings>
<MasterTableView autogeneratecolumns="False" datasourceid="SqlDataSource2" CommandItemDisplay="Top">
<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <Columns>
        <telerik:GridBoundColumn DataField="E_CLASSE_CODE" HeaderText="E_CLASSE_CODE" 
            SortExpression="E_CLASSE_CODE" UniqueName="E_CLASSE_CODE">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="E_DESCRIPTION" HeaderText="E_DESCRIPTION" 
            SortExpression="E_DESCRIPTION" UniqueName="E_DESCRIPTION">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="E_DATE" HeaderText="E_DATE" 
            SortExpression="E_DATE" UniqueName="E_DATE">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ENSEIGNANT" HeaderText="ENSEIGNANT" 
            SortExpression="ENSEIGNANT" UniqueName="ENSEIGNANT">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="PRENOM" HeaderText="PRENOM" 
            SortExpression="PRENOM" UniqueName="PRENOM">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NOM" HeaderText="NOM" SortExpression="NOM" 
            UniqueName="NOM">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="M_A_QUITTE" HeaderText="M_A_QUITTE" 
            SortExpression="M_A_QUITTE" UniqueName="M_A_QUITTE">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="CC" HeaderText="CC" UniqueName="CC" AllowFiltering="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="HP" HeaderText="HP" UniqueName="HP" AllowFiltering="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="CO" HeaderText="CO" UniqueName="CO" AllowFiltering="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="MA" HeaderText="MA" UniqueName="MA" AllowFiltering="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NOTE" HeaderText="NOTE" AllowFiltering="False"
            SortExpression="NOTE" UniqueName="NOTE">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NOTE_SUR_100" DataType="System.Decimal" 
            HeaderText="NOTE_SUR_100" SortExpression="NOTE_SUR_100" 
            UniqueName="NOTE_SUR_100">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NOTE_BULLETIN" DataType="System.Decimal" 
            HeaderText="NOTE_BULLETIN" SortExpression="NOTE_BULLETIN" 
            UniqueName="NOTE_BULLETIN">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ABS" HeaderText="ABS" SortExpression="ABS" 
            UniqueName="ABS">
        </telerik:GridBoundColumn>
    </Columns>
    <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true" ShowExportToCsvButton="true" 
            ShowExportToPdfButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true" />
</MasterTableView>
        <ClientSettings AllowDragToGroup="True">
        </ClientSettings>
    </telerik:radgrid>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:CS_GN %>" 
        ProviderName="<%$ ConnectionStrings:CS_GN.ProviderName %>" SelectCommand="">
    </asp:SqlDataSource>
    
    <br/>
    <br/>
    <div align="center"><dotnet:Chart id="chart" runat="server" Visible="false" Background-Color="White" Background-SecondaryColor="White"/></div>
    </div>
 </asp:content>
