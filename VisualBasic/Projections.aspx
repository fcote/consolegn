<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   ' This sample demonstrates different projections available for map rendering.
   Chart.Title = "Americas map with no projection"
   LoadDefaults(Chart)
   
   Chart1.Title = "Americas map with Mercator projection"
   Chart1.Mapping.Projection.Type = ProjectionType.Mercator
	Chart1.Mapping.Projection.Parameters="-60"
   LoadDefaults(Chart1)
   
   Chart2.Title = "Americas map with Lambert Conic projection"
   Chart2.Mapping.Projection.Type = ProjectionType.LambertConic
	Chart2.Mapping.Projection.Parameters="-10,-20,-32,49"
   Chart2.Mapping.ZoomPercentage = 150
   Chart2.Mapping.ZoomCenterPoint = New PointF(47,-120)
   LoadDefaults(Chart2)
End Sub 'Page_Load
 


Sub LoadDefaults(c As Chart)
   c.Type = ChartType.Map
   c.Size = "800x600"
   c.TempDirectory = "temp"
   c.ChartArea.Background = New Background(Color.FromArgb(90, 180, 250), Color.FromArgb(73, 147, 204), 90)
   c.Mapping.MapLayerCollection.Add("../../Images/MapFiles/primusa.shp")
   c.Mapping.MapLayerCollection(0).DefaultShape.Background.Color = Color.Brown
End Sub 'LoadDefaults
</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
			<dotnet:Chart id="Chart1" runat="server" >
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" >
			</dotnet:Chart>

		</div>
	</body>
</html>
