<%@ Page Language="vb" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>

<script runat="server">
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	'Enable zooming with just one property
	Chart.Zoomer.Enabled =True

	'Pre-zoom by setting the viewport to show 1 year for the initial view
	Chart.XAxis.Viewport.TimeInterval = New TimeIntervalAdvanced(New TimeSpan(365, 0, 0, 0))

	'Regular chart settings (not related to zooming or scrolling)
	Chart.Size="640X500"
	Chart.TempDirectory="temp"
	Chart.Debug=False

	Chart.Type=ChartType.Gantt
	Chart.ShadingEffect=True
	Chart.ShadingEffectMode = ShadingEffectMode.Two
	Chart.YAxis.Label.Text="Versions"
	Chart.XAxis.Scale = Scale.Time
	Chart.ChartArea.Label.Text="We continue to innovate and solve" & Constants.vbLf & "real-world client issues. Discover the" & Constants.vbLf & "difference .netCHARTING can make!"
	Chart.XAxis.TimePadding = New TimeSpan(1,0,0,0)
	Chart.XAxis.TimeInterval = TimeInterval.Months
	Chart.LegendBox.Position = LegendBoxPosition.None
	Chart.DefaultElement.ToolTip = "V. %XValue, released %YValue"

	'TitleBox Customization
	Chart.Title=".netCHARTING Version History - Zoom and Scroll"
	Chart.TitleBox.Label.Color = Color.White
	Chart.TitleBox.Label.Shadow.Color = Color.FromArgb(105, 0, 0, 0)
	Chart.TitleBox.Label.Shadow.Depth = 2
	Chart.TitleBox.CornerTopLeft = BoxCorner.Round
	Chart.TitleBox.CornerTopRight = BoxCorner.Round
	Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Two
	Chart.TitleBox.Background.Color = Color.FromArgb(100, 225, 165, 50)


	'Ranged date ticks on X
	Chart.XAxis.Scale = Scale.Time
	Chart.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Dynamic
	Chart.XAxis.TimeInterval = TimeInterval.Month
	Chart.XAxis.TimeScaleLabels.MonthFormatString = "MMM"
	Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Year)

	'Add a series
	Chart.Series.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
	Chart.Series.DataFields="ganttstartdate=startDate,ganttenddate=ReleaseDate,ganttname=Version"
	Chart.Series.DefaultElement.ShowValue=True
	Chart.Series.PaletteName=Palette.Two
	Chart.Series.SqlStatement= "SELECT StartDate,ReleaseDate,Version FROM Versions WHERE Version <> '1.0'"
	Chart.SeriesCollection.Add()
End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Version History</title></head>
<body>
<center>
<dotnet:Chart ID="Chart" runat="server" />
</center>
</body>
</html>
