<%@ Page Language="vb" debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>

<script runat="server">
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	'Enable AJAX zoom / scroll with a single property
	Chart.Zoomer.Enabled = True

	' Add the random data.
	Chart.SeriesCollection.Add(getRandomData(1))
End Sub

Function getRandomData(ByVal seed As Integer) As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random()
	For a As Integer = 1 To 4
		Dim s As Series = New Series()
		s.Name = "Series " & a
		For b As Integer = 1 To 5
			Dim e As Element = New Element()
			e.XValue = 5 + myR.Next(10)
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		s.Type = SeriesType.Line
		SC.Add(s)
	Next a
	Return SC
End Function
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>.netCHARTING Sample</title></head>
<body>
<center>
 <dotnet:Chart id="Chart" Debug="true" Title="Chart with zooming, right click on the chart to access zooming" runat="server"/>
</center>
</body>
</html>
