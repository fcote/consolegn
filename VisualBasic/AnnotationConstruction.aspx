<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dnc" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates a simple and quick way to create multiple annotations with similar styling.

		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		' First create an annotation with parameters used for all others:

		Dim dA As Annotation = New Annotation()

		dA.Background.Color = Color.DarkOrange
		dA.Background.ShadingEffectMode = ShadingEffectMode.Background1
		dA.DefaultCorner = BoxCorner.Square
		dA.Label.Font = New Font("Arial", 10, FontStyle.Bold)
		dA.Label.Color = Color.White
		dA.DynamicSize = False
		dA.Padding = 10
		dA.Size = New Size(150, 25)

		' Now add annotation use the above styling using an annotation constructors that lets you pass a default annotation.

		Dim a1 As Annotation = New Annotation("The first annotation", dA)
		a1.Position = New Point(35, 50)

		Dim a2 As Annotation = New Annotation("The second annotation", dA)
		a2.Position = New Point(35, 80)

		Dim a3 As Annotation = New Annotation("The third annotation", dA)
		a3.Position = New Point(35, 110)

		Dim a4 As Annotation = New Annotation("The fourth annotation", dA)
		a4.Position = New Point(35, 140)

		Chart.Annotations.Add(a1,a2,a3,a4)

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		For a As Integer = 1 To 4
			Dim s As Series = New Series("Series " & a.ToString())
			For b As Integer = 1 To 4
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine(ConfigurationSettings.AppSettings("DNCConnectionString"))
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dnc:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
