<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using embedded font styling with labels.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		'Chart.Palette = new Color[] { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };

		Chart.Type = ChartType.Combo
		Chart.Size = "700x450"
		Chart.Title = ".netCHARTING Embedded Label Font Styling"

		Chart.ShadingEffectMode = ShadingEffectMode.Three

		Chart.ChartArea.Background.ShadingEffectMode = ShadingEffectMode.Five
		Chart.ChartArea.Background.Transparency = 30
		Chart.ChartArea.Shadow.Visible = False
		Chart.LegendBox.Visible = False
		Dim mySC As SeriesCollection = getRandomData()

		mySC(0).PaletteName = Palette.Bright

		Dim a As Annotation = New Annotation()
		mySC(0)(0).Annotation = a
		a.DynamicSize = False
		a.Label.Text = "Font Size 10 | Name: <block fSize='10'>%Name"

		mySC(0)(1).Annotation = New Annotation()
		a = mySC(0)(1).Annotation
		a.DynamicSize = False
		a.Label.Text = "Verdana Font | Name: <block fName='verdana'>%Name"

		mySC(0)(2).Annotation = New Annotation()
		a = mySC(0)(2).Annotation
		a.DynamicSize = False
		a.Label.Text = "Red Font | Name: <block fColor='red'>%Name"

		mySC(0)(3).Annotation = New Annotation()
		a = mySC(0)(3).Annotation
		a.DynamicSize = False
		a.Label.Text = "Digital & Colored | Name: <block fColor='blue' digital='yes'>%Name"

		mySC(0)(4).Annotation = New Annotation()
		a = mySC(0)(4).Annotation
		a.DynamicSize = False
		a.Label.Text = "Glowing | Name: <block glowColor='yellow'>%Name"

		mySC(0)(5).Annotation = New Annotation()
		a = mySC(0)(5).Annotation
		a.DynamicSize = False
		a.Label.Text = "Outlined | Name: <block outlineColor='Green'>%Name"

		mySC(0)(6).Annotation = New Annotation()
		a = mySC(0)(6).Annotation
		a.DynamicSize = False
		a.Label.Text = "Bold | Name: <block fStyle='bold'>%Name"

		mySC(0)(7).Annotation = New Annotation()
		a = mySC(0)(7).Annotation
		a.DynamicSize = False
		a.Label.Text = "Italic | Name: <block fStyle='italic'>%Name"

		mySC(0)(8).Annotation = New Annotation()
		a = mySC(0)(8).Annotation
		a.DynamicSize = False
		a.Label.Text = "Underlined | Name: <block fStyle='underline'>%Name"

		mySC(0)(9).Annotation = New Annotation()
		a = mySC(0)(9).Annotation
		a.DynamicSize = False
		a.Label.Text = "Strikeout | Name: <block fStyle='strikeout'>%Name"

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx
		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(4)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 10
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				e.YValue = b
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
