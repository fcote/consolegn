<%@ Page Language="vb" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<script runat="server">
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates using DateTime values from xml files.
	'set Title 
	Chart.Title="xml with dateTime element"
	Chart.Type = ChartType.Combo
	Chart.Size = "600x480"
	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.LegendBox.Visible = False
	Chart.DefaultSeries.Type = SeriesType.AreaLine

	Chart.XAxis.Scale = Scale.Time
	'Chart.XAxis.TimeInterval = TimeInterval.Day;
	Chart.XAxis.FormatString = "MMM dd"

	' Set the y axis label
	Chart.ChartArea.YAxis.Label.Text="Amount (USD)"

   'Adding series programatically
	   Chart.Series.Name = "Sales"
	   Chart.Series.Data = "../../database/orders3.xml"
	   Chart.Series.DataFields="XDateTime=date,yaxis=total" 'Doesn't show the time scale

	   'Chart.Series.DataFields = "Xvalue=date,yaxis=total";
	Chart.SeriesCollection.Add()


End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>XML Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
