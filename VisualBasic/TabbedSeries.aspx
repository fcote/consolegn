<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates showing series individually by clickable tabs.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.Combo
	Chart.Size = "600x350"
	Chart.MarginTop = 29


	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	Dim mySC As SeriesCollection = getRandomData()


	SetupTabs(Chart,mySC)

	' Determine which series to add.
	Dim seriesIndex As Integer = 0
	If Not Page.Request.Params("seriesIndex") Is Nothing Then
		seriesIndex = Integer.Parse(Page.Request.Params("seriesIndex"))
	End If

	' Color the tab for the series being used.
	Chart.Annotations(seriesIndex).Background.Color = Color.FromArgb(255,255,145)

	 ' Add the specified series.
	Chart.SeriesCollection.Add(mySC(seriesIndex))
End Sub

Sub SetupTabs(ByVal c As Chart, ByVal sc As SeriesCollection)
	Dim i As Integer = 0
	For Each s As Series In sc
		Dim an As Annotation = New Annotation(s.Name)
		an.URL = "?seriesIndex=" & i
		i = i+1
		an.Position = New Point(i*50+32,5)
		an.TextAlignmentVertical = StringAlignment.Center
		c.Annotations.Add(an)
	Next s
End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random(1)
	Dim SC As SeriesCollection = New SeriesCollection()
	For a As Integer = 1 To 4
		Dim s As Series = New Series("Series " & a.ToString())
		For b As Integer = 1 To 4
			Dim e As Element = New Element("Element " & b.ToString())
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
